### BULLRUNNER_V1-GTFSRT-2018-17
#### Metadata
BULLRUNNER_V1-GTFSRT-2018-17.json.zip: 10.10 MB
BULLRUNNER_V1-GTFSRT-2018-17.pb.zip: 7.37 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 19865 | 0 | 0 |
| Sampling | 30.45 | 0.00 | 0.00 |
| 2018-04-23 | 2861 | 0 | 0 |
| 2018-04-24 | 2661 | 0 | 0 |
| 2018-04-25 | 2870 | 0 | 0 |
| 2018-04-26 | 2860 | 0 | 0 |
| 2018-04-27 | 2868 | 0 | 0 |
| 2018-04-28 | 2872 | 0 | 0 |
| 2018-04-29 | 2873 | 0 | 0 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.occupancyStatus``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.trip.routeId``
``entity.vehicle.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/BULLRUNNER_V1-GTFSRT-2018-17.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/BULLRUNNER_V1-GTFSRT-2018-17.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/BULLRUNNER_V1-GTFSRT-2018-17.png)
