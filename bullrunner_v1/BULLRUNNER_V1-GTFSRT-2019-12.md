### BULLRUNNER_V1-GTFSRT-2019-12
#### Metadata
BULLRUNNER_V1-GTFSRT-2019-12.json.zip: 8.97 MB
BULLRUNNER_V1-GTFSRT-2019-12.pb.zip: 6.48 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 18629 | 0 | 0 |
| Sampling | 32.46 | 0.00 | 0.00 |
| 2019-03-18 | 2862 | 0 | 0 |
| 2019-03-19 | 2116 | 0 | 0 |
| 2019-03-20 | 2751 | 0 | 0 |
| 2019-03-21 | 2292 | 0 | 0 |
| 2019-03-22 | 2868 | 0 | 0 |
| 2019-03-23 | 2871 | 0 | 0 |
| 2019-03-24 | 2869 | 0 | 0 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.occupancyStatus``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.trip.routeId``
``entity.vehicle.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/BULLRUNNER_V1-GTFSRT-2019-12.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/BULLRUNNER_V1-GTFSRT-2019-12.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/BULLRUNNER_V1-GTFSRT-2019-12.png)
