### BULLRUNNER_V1-GTFSRT-2019-05
#### Metadata
BULLRUNNER_V1-GTFSRT-2019-05.json.zip: 8.47 MB
BULLRUNNER_V1-GTFSRT-2019-05.pb.zip: 6.11 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 17796 | 0 | 0 |
| Sampling | 33.99 | 0.00 | 0.00 |
| 2019-01-28 | 2727 | 0 | 0 |
| 2019-01-29 | 2134 | 0 | 0 |
| 2019-01-30 | 2203 | 0 | 0 |
| 2019-01-31 | 2384 | 0 | 0 |
| 2019-02-01 | 2644 | 0 | 0 |
| 2019-02-02 | 2868 | 0 | 0 |
| 2019-02-03 | 2836 | 0 | 0 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.occupancyStatus``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.trip.routeId``
``entity.vehicle.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/BULLRUNNER_V1-GTFSRT-2019-05.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/BULLRUNNER_V1-GTFSRT-2019-05.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/BULLRUNNER_V1-GTFSRT-2019-05.png)
