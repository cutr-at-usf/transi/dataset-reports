### BULLRUNNER_V1-GTFSRT-2018-37
#### Metadata
BULLRUNNER_V1-GTFSRT-2018-37.json.zip: 7.74 MB
BULLRUNNER_V1-GTFSRT-2018-37.pb.zip: 5.65 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 16357 | 0 | 0 |
| Sampling | 36.98 | 0.00 | 0.00 |
| 2018-09-10 | 2085 | 0 | 0 |
| 2018-09-11 | 2048 | 0 | 0 |
| 2018-09-12 | 2139 | 0 | 0 |
| 2018-09-13 | 2024 | 0 | 0 |
| 2018-09-14 | 2314 | 0 | 0 |
| 2018-09-15 | 2872 | 0 | 0 |
| 2018-09-16 | 2875 | 0 | 0 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.occupancyStatus``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.trip.routeId``
``entity.vehicle.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/BULLRUNNER_V1-GTFSRT-2018-37.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/BULLRUNNER_V1-GTFSRT-2018-37.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/BULLRUNNER_V1-GTFSRT-2018-37.png)
