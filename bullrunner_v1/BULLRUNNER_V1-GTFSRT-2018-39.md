### BULLRUNNER_V1-GTFSRT-2018-39
#### Metadata
BULLRUNNER_V1-GTFSRT-2018-39.json.zip: 8.04 MB
BULLRUNNER_V1-GTFSRT-2018-39.pb.zip: 5.86 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 16868 | 0 | 0 |
| Sampling | 35.86 | 0.00 | 0.00 |
| 2018-09-24 | 2165 | 0 | 0 |
| 2018-09-25 | 2146 | 0 | 0 |
| 2018-09-26 | 2295 | 0 | 0 |
| 2018-09-27 | 2241 | 0 | 0 |
| 2018-09-28 | 2274 | 0 | 0 |
| 2018-09-29 | 2873 | 0 | 0 |
| 2018-09-30 | 2874 | 0 | 0 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.occupancyStatus``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.trip.routeId``
``entity.vehicle.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/BULLRUNNER_V1-GTFSRT-2018-39.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/BULLRUNNER_V1-GTFSRT-2018-39.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/BULLRUNNER_V1-GTFSRT-2018-39.png)
