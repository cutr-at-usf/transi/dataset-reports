### BULLRUNNER_V1-GTFSRT-2019-25
#### Metadata
BULLRUNNER_V1-GTFSRT-2019-25.json.zip: 9.84 MB
BULLRUNNER_V1-GTFSRT-2019-25.pb.zip: 7.12 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 19833 | 0 | 0 |
| Sampling | 30.50 | 0.00 | 0.00 |
| 2019-06-17 | 2865 | 0 | 0 |
| 2019-06-18 | 2861 | 0 | 0 |
| 2019-06-19 | 2709 | 0 | 0 |
| 2019-06-20 | 2861 | 0 | 0 |
| 2019-06-21 | 2869 | 0 | 0 |
| 2019-06-22 | 2799 | 0 | 0 |
| 2019-06-23 | 2869 | 0 | 0 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.occupancyStatus``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.trip.routeId``
``entity.vehicle.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/BULLRUNNER_V1-GTFSRT-2019-25.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/BULLRUNNER_V1-GTFSRT-2019-25.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/BULLRUNNER_V1-GTFSRT-2019-25.png)
