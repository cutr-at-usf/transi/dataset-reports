### BULLRUNNER_V1-GTFSRT-2019-01
#### Metadata
BULLRUNNER_V1-GTFSRT-2019-01.json.zip: 7.54 MB
BULLRUNNER_V1-GTFSRT-2019-01.pb.zip: 5.52 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 17204 | 0 | 0 |
| Sampling | 30.13 | 0.00 | 0.00 |
| 2018-12-31 | 0 | 0 | 0 |
| 2019-01-01 | 2872 | 0 | 0 |
| 2019-01-02 | 2868 | 0 | 0 |
| 2019-01-03 | 2871 | 0 | 0 |
| 2019-01-04 | 2869 | 0 | 0 |
| 2019-01-05 | 2872 | 0 | 0 |
| 2019-01-06 | 2852 | 0 | 0 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.occupancyStatus``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.trip.routeId``
``entity.vehicle.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/BULLRUNNER_V1-GTFSRT-2019-01.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/BULLRUNNER_V1-GTFSRT-2019-01.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/BULLRUNNER_V1-GTFSRT-2019-01.png)
