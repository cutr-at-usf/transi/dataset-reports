### BULLRUNNER_V1-GTFSRT-2019-27
#### Metadata
BULLRUNNER_V1-GTFSRT-2019-27.json.zip: 9.52 MB
BULLRUNNER_V1-GTFSRT-2019-27.pb.zip: 6.99 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 20040 | 0 | 0 |
| Sampling | 30.18 | 0.00 | 0.00 |
| 2019-07-01 | 2856 | 0 | 0 |
| 2019-07-02 | 2854 | 0 | 0 |
| 2019-07-03 | 2867 | 0 | 0 |
| 2019-07-04 | 2865 | 0 | 0 |
| 2019-07-05 | 2867 | 0 | 0 |
| 2019-07-06 | 2872 | 0 | 0 |
| 2019-07-07 | 2859 | 0 | 0 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.occupancyStatus``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.trip.routeId``
``entity.vehicle.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/BULLRUNNER_V1-GTFSRT-2019-27.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/BULLRUNNER_V1-GTFSRT-2019-27.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/BULLRUNNER_V1-GTFSRT-2019-27.png)
