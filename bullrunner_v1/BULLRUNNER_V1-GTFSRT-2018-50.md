### BULLRUNNER_V1-GTFSRT-2018-50
#### Metadata
BULLRUNNER_V1-GTFSRT-2018-50.json.zip: 9.69 MB
BULLRUNNER_V1-GTFSRT-2018-50.pb.zip: 7.06 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 20068 | 0 | 0 |
| Sampling | 30.14 | 0.00 | 0.00 |
| 2018-12-10 | 2868 | 0 | 0 |
| 2018-12-11 | 2866 | 0 | 0 |
| 2018-12-12 | 2863 | 0 | 0 |
| 2018-12-13 | 2869 | 0 | 0 |
| 2018-12-14 | 2866 | 0 | 0 |
| 2018-12-15 | 2870 | 0 | 0 |
| 2018-12-16 | 2866 | 0 | 0 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.occupancyStatus``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.trip.routeId``
``entity.vehicle.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/BULLRUNNER_V1-GTFSRT-2018-50.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/BULLRUNNER_V1-GTFSRT-2018-50.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/BULLRUNNER_V1-GTFSRT-2018-50.png)
