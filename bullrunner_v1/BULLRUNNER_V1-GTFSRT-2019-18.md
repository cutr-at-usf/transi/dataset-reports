### BULLRUNNER_V1-GTFSRT-2019-18
#### Metadata
BULLRUNNER_V1-GTFSRT-2019-18.json.zip: 6.82 MB
BULLRUNNER_V1-GTFSRT-2019-18.pb.zip: 4.99 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 14869 | 0 | 0 |
| Sampling | 40.68 | 0.00 | 0.00 |
| 2019-04-29 | 2580 | 0 | 0 |
| 2019-04-30 | 2633 | 0 | 0 |
| 2019-05-01 | 2180 | 0 | 0 |
| 2019-05-02 | 1118 | 0 | 0 |
| 2019-05-03 | 1480 | 0 | 0 |
| 2019-05-04 | 2017 | 0 | 0 |
| 2019-05-05 | 2861 | 0 | 0 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.occupancyStatus``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.trip.routeId``
``entity.vehicle.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/BULLRUNNER_V1-GTFSRT-2019-18.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/BULLRUNNER_V1-GTFSRT-2019-18.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/BULLRUNNER_V1-GTFSRT-2019-18.png)
