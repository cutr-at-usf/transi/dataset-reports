### BULLRUNNER_V1-GTFSRT-2018-30
#### Metadata
BULLRUNNER_V1-GTFSRT-2018-30.json.zip: 10.10 MB
BULLRUNNER_V1-GTFSRT-2018-30.pb.zip: 7.35 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 19974 | 0 | 0 |
| Sampling | 30.28 | 0.00 | 0.00 |
| 2018-07-23 | 2867 | 0 | 0 |
| 2018-07-24 | 2867 | 0 | 0 |
| 2018-07-25 | 2766 | 0 | 0 |
| 2018-07-26 | 2862 | 0 | 0 |
| 2018-07-27 | 2868 | 0 | 0 |
| 2018-07-28 | 2871 | 0 | 0 |
| 2018-07-29 | 2873 | 0 | 0 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.occupancyStatus``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.trip.routeId``
``entity.vehicle.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/BULLRUNNER_V1-GTFSRT-2018-30.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/BULLRUNNER_V1-GTFSRT-2018-30.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/BULLRUNNER_V1-GTFSRT-2018-30.png)
