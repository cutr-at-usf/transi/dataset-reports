### BULLRUNNER_V1-GTFSRT-2018-48
#### Metadata
BULLRUNNER_V1-GTFSRT-2018-48.json.zip: 8.85 MB
BULLRUNNER_V1-GTFSRT-2018-48.pb.zip: 6.39 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 18380 | 0 | 0 |
| Sampling | 32.90 | 0.00 | 0.00 |
| 2018-11-26 | 2865 | 0 | 0 |
| 2018-11-27 | 2515 | 0 | 0 |
| 2018-11-28 | 2534 | 0 | 0 |
| 2018-11-29 | 2516 | 0 | 0 |
| 2018-11-30 | 2235 | 0 | 0 |
| 2018-12-01 | 2871 | 0 | 0 |
| 2018-12-02 | 2844 | 0 | 0 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.occupancyStatus``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.trip.routeId``
``entity.vehicle.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/BULLRUNNER_V1-GTFSRT-2018-48.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/BULLRUNNER_V1-GTFSRT-2018-48.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/BULLRUNNER_V1-GTFSRT-2018-48.png)
