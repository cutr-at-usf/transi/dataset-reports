### BULLRUNNER_V1-GTFSRT-2018-44
#### Metadata
BULLRUNNER_V1-GTFSRT-2018-44.json.zip: 8.07 MB
BULLRUNNER_V1-GTFSRT-2018-44.pb.zip: 5.89 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 17023 | 0 | 0 |
| Sampling | 35.74 | 0.00 | 0.00 |
| 2018-10-29 | 2116 | 0 | 0 |
| 2018-10-30 | 2144 | 0 | 0 |
| 2018-10-31 | 2178 | 0 | 0 |
| 2018-11-01 | 2511 | 0 | 0 |
| 2018-11-02 | 2223 | 0 | 0 |
| 2018-11-03 | 2871 | 0 | 0 |
| 2018-11-04 | 2860 | 0 | 0 |
| 2018-11-05 | 120 | 0 | 0 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.occupancyStatus``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.trip.routeId``
``entity.vehicle.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/BULLRUNNER_V1-GTFSRT-2018-44.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/BULLRUNNER_V1-GTFSRT-2018-44.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/BULLRUNNER_V1-GTFSRT-2018-44.png)
