### BULLRUNNER_V1-GTFSRT-2018-51
#### Metadata
BULLRUNNER_V1-GTFSRT-2018-51.json.zip: 7.71 MB
BULLRUNNER_V1-GTFSRT-2018-51.pb.zip: 5.71 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 17682 | 0 | 0 |
| Sampling | 34.21 | 0.00 | 0.00 |
| 2018-12-17 | 1355 | 0 | 0 |
| 2018-12-18 | 2115 | 0 | 0 |
| 2018-12-19 | 2731 | 0 | 0 |
| 2018-12-20 | 2866 | 0 | 0 |
| 2018-12-21 | 2872 | 0 | 0 |
| 2018-12-22 | 2871 | 0 | 0 |
| 2018-12-23 | 2872 | 0 | 0 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.occupancyStatus``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.trip.routeId``
``entity.vehicle.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/BULLRUNNER_V1-GTFSRT-2018-51.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/BULLRUNNER_V1-GTFSRT-2018-51.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/BULLRUNNER_V1-GTFSRT-2018-51.png)
