### BULLRUNNER_V1-GTFSRT-2018-29
#### Metadata
BULLRUNNER_V1-GTFSRT-2018-29.json.zip: 10.18 MB
BULLRUNNER_V1-GTFSRT-2018-29.pb.zip: 7.42 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 20002 | 0 | 0 |
| Sampling | 30.24 | 0.00 | 0.00 |
| 2018-07-16 | 2864 | 0 | 0 |
| 2018-07-17 | 2868 | 0 | 0 |
| 2018-07-18 | 2788 | 0 | 0 |
| 2018-07-19 | 2869 | 0 | 0 |
| 2018-07-20 | 2866 | 0 | 0 |
| 2018-07-21 | 2874 | 0 | 0 |
| 2018-07-22 | 2873 | 0 | 0 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.occupancyStatus``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.trip.routeId``
``entity.vehicle.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/BULLRUNNER_V1-GTFSRT-2018-29.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/BULLRUNNER_V1-GTFSRT-2018-29.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/BULLRUNNER_V1-GTFSRT-2018-29.png)
