### BULLRUNNER_V1-GTFSRT-2018-33
#### Metadata
BULLRUNNER_V1-GTFSRT-2018-33.json.zip: 8.85 MB
BULLRUNNER_V1-GTFSRT-2018-33.pb.zip: 6.47 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 18678 | 0 | 0 |
| Sampling | 32.38 | 0.00 | 0.00 |
| 2018-08-13 | 2869 | 0 | 0 |
| 2018-08-14 | 2866 | 0 | 0 |
| 2018-08-15 | 2733 | 0 | 0 |
| 2018-08-16 | 1602 | 0 | 0 |
| 2018-08-17 | 2867 | 0 | 0 |
| 2018-08-18 | 2872 | 0 | 0 |
| 2018-08-19 | 2869 | 0 | 0 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.occupancyStatus``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.trip.routeId``
``entity.vehicle.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/BULLRUNNER_V1-GTFSRT-2018-33.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/BULLRUNNER_V1-GTFSRT-2018-33.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/BULLRUNNER_V1-GTFSRT-2018-33.png)
