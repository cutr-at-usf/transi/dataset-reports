### BULLRUNNER_V1-GTFSRT-2018-47
#### Metadata
BULLRUNNER_V1-GTFSRT-2018-47.json.zip: 8.33 MB
BULLRUNNER_V1-GTFSRT-2018-47.pb.zip: 6.12 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 19240 | 0 | 0 |
| Sampling | 31.43 | 0.00 | 0.00 |
| 2018-11-19 | 2294 | 0 | 0 |
| 2018-11-20 | 2601 | 0 | 0 |
| 2018-11-21 | 2862 | 0 | 0 |
| 2018-11-22 | 2871 | 0 | 0 |
| 2018-11-23 | 2872 | 0 | 0 |
| 2018-11-24 | 2870 | 0 | 0 |
| 2018-11-25 | 2870 | 0 | 0 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.occupancyStatus``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.trip.routeId``
``entity.vehicle.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/BULLRUNNER_V1-GTFSRT-2018-47.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/BULLRUNNER_V1-GTFSRT-2018-47.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/BULLRUNNER_V1-GTFSRT-2018-47.png)
