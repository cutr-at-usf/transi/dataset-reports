### BULLRUNNER_V1-GTFSRT-2018-20
#### Metadata
BULLRUNNER_V1-GTFSRT-2018-20.json.zip: 10.30 MB
BULLRUNNER_V1-GTFSRT-2018-20.pb.zip: 7.53 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 20081 | 0 | 0 |
| Sampling | 30.12 | 0.00 | 0.00 |
| 2018-05-14 | 2869 | 0 | 0 |
| 2018-05-15 | 2871 | 0 | 0 |
| 2018-05-16 | 2858 | 0 | 0 |
| 2018-05-17 | 2866 | 0 | 0 |
| 2018-05-18 | 2872 | 0 | 0 |
| 2018-05-19 | 2872 | 0 | 0 |
| 2018-05-20 | 2873 | 0 | 0 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.occupancyStatus``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.trip.routeId``
``entity.vehicle.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/BULLRUNNER_V1-GTFSRT-2018-20.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/BULLRUNNER_V1-GTFSRT-2018-20.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/BULLRUNNER_V1-GTFSRT-2018-20.png)
