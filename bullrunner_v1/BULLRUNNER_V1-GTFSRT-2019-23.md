### BULLRUNNER_V1-GTFSRT-2019-23
#### Metadata
BULLRUNNER_V1-GTFSRT-2019-23.json.zip: 7.93 MB
BULLRUNNER_V1-GTFSRT-2019-23.pb.zip: 5.79 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 16799 | 0 | 0 |
| Sampling | 36.00 | 0.00 | 0.00 |
| 2019-06-03 | 2859 | 0 | 0 |
| 2019-06-04 | 2864 | 0 | 0 |
| 2019-06-05 | 1202 | 0 | 0 |
| 2019-06-06 | 1670 | 0 | 0 |
| 2019-06-07 | 2464 | 0 | 0 |
| 2019-06-08 | 2868 | 0 | 0 |
| 2019-06-09 | 2872 | 0 | 0 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.occupancyStatus``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.trip.routeId``
``entity.vehicle.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/BULLRUNNER_V1-GTFSRT-2019-23.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/BULLRUNNER_V1-GTFSRT-2019-23.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/BULLRUNNER_V1-GTFSRT-2019-23.png)
