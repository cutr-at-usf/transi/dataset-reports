### BULLRUNNER_V1-GTFSRT-2019-22
#### Metadata
BULLRUNNER_V1-GTFSRT-2019-22.json.zip: 9.24 MB
BULLRUNNER_V1-GTFSRT-2019-22.pb.zip: 6.75 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 19878 | 0 | 0 |
| Sampling | 30.42 | 0.00 | 0.00 |
| 2019-05-27 | 2870 | 0 | 0 |
| 2019-05-28 | 2865 | 0 | 0 |
| 2019-05-29 | 2689 | 0 | 0 |
| 2019-05-30 | 2867 | 0 | 0 |
| 2019-05-31 | 2862 | 0 | 0 |
| 2019-06-01 | 2867 | 0 | 0 |
| 2019-06-02 | 2858 | 0 | 0 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.occupancyStatus``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.trip.routeId``
``entity.vehicle.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/BULLRUNNER_V1-GTFSRT-2019-22.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/BULLRUNNER_V1-GTFSRT-2019-22.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/BULLRUNNER_V1-GTFSRT-2019-22.png)
