### BULLRUNNER_V1-GTFSRT-2019-19
#### Metadata
BULLRUNNER_V1-GTFSRT-2019-19.json.zip: 9.45 MB
BULLRUNNER_V1-GTFSRT-2019-19.pb.zip: 6.85 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 19948 | 0 | 0 |
| Sampling | 30.32 | 0.00 | 0.00 |
| 2019-05-06 | 2863 | 0 | 0 |
| 2019-05-07 | 2864 | 0 | 0 |
| 2019-05-08 | 2745 | 0 | 0 |
| 2019-05-09 | 2862 | 0 | 0 |
| 2019-05-10 | 2869 | 0 | 0 |
| 2019-05-11 | 2872 | 0 | 0 |
| 2019-05-12 | 2873 | 0 | 0 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.occupancyStatus``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.trip.routeId``
``entity.vehicle.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/BULLRUNNER_V1-GTFSRT-2019-19.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/BULLRUNNER_V1-GTFSRT-2019-19.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/BULLRUNNER_V1-GTFSRT-2019-19.png)
