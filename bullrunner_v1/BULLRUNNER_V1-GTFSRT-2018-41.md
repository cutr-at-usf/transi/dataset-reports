### BULLRUNNER_V1-GTFSRT-2018-41
#### Metadata
BULLRUNNER_V1-GTFSRT-2018-41.json.zip: 7.74 MB
BULLRUNNER_V1-GTFSRT-2018-41.pb.zip: 5.66 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 16391 | 0 | 0 |
| Sampling | 36.90 | 0.00 | 0.00 |
| 2018-10-08 | 2196 | 0 | 0 |
| 2018-10-09 | 2083 | 0 | 0 |
| 2018-10-10 | 2154 | 0 | 0 |
| 2018-10-11 | 2059 | 0 | 0 |
| 2018-10-12 | 2157 | 0 | 0 |
| 2018-10-13 | 2871 | 0 | 0 |
| 2018-10-14 | 2871 | 0 | 0 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.occupancyStatus``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.trip.routeId``
``entity.vehicle.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/BULLRUNNER_V1-GTFSRT-2018-41.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/BULLRUNNER_V1-GTFSRT-2018-41.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/BULLRUNNER_V1-GTFSRT-2018-41.png)
