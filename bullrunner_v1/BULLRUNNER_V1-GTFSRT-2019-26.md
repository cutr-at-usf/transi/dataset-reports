### BULLRUNNER_V1-GTFSRT-2019-26
#### Metadata
BULLRUNNER_V1-GTFSRT-2019-26.json.zip: 10.03 MB
BULLRUNNER_V1-GTFSRT-2019-26.pb.zip: 7.29 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 20039 | 0 | 0 |
| Sampling | 30.18 | 0.00 | 0.00 |
| 2019-06-24 | 2851 | 0 | 0 |
| 2019-06-25 | 2856 | 0 | 0 |
| 2019-06-26 | 2860 | 0 | 0 |
| 2019-06-27 | 2860 | 0 | 0 |
| 2019-06-28 | 2867 | 0 | 0 |
| 2019-06-29 | 2874 | 0 | 0 |
| 2019-06-30 | 2871 | 0 | 0 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.occupancyStatus``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.trip.routeId``
``entity.vehicle.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/BULLRUNNER_V1-GTFSRT-2019-26.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/BULLRUNNER_V1-GTFSRT-2019-26.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/BULLRUNNER_V1-GTFSRT-2019-26.png)
