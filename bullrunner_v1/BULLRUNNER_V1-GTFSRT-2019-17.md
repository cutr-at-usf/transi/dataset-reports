### BULLRUNNER_V1-GTFSRT-2019-17
#### Metadata
BULLRUNNER_V1-GTFSRT-2019-17.json.zip: 8.02 MB
BULLRUNNER_V1-GTFSRT-2019-17.pb.zip: 5.79 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 16751 | 0 | 0 |
| Sampling | 36.11 | 0.00 | 0.00 |
| 2019-04-22 | 2596 | 0 | 0 |
| 2019-04-23 | 1984 | 0 | 0 |
| 2019-04-24 | 2295 | 0 | 0 |
| 2019-04-25 | 2187 | 0 | 0 |
| 2019-04-26 | 2574 | 0 | 0 |
| 2019-04-27 | 2602 | 0 | 0 |
| 2019-04-28 | 2513 | 0 | 0 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.occupancyStatus``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.trip.routeId``
``entity.vehicle.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/BULLRUNNER_V1-GTFSRT-2019-17.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/BULLRUNNER_V1-GTFSRT-2019-17.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/BULLRUNNER_V1-GTFSRT-2019-17.png)
