### BULLRUNNER_V1-GTFSRT-2018-42
#### Metadata
BULLRUNNER_V1-GTFSRT-2018-42.json.zip: 7.81 MB
BULLRUNNER_V1-GTFSRT-2018-42.pb.zip: 5.71 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 16484 | 0 | 0 |
| Sampling | 36.69 | 0.00 | 0.00 |
| 2018-10-15 | 2154 | 0 | 0 |
| 2018-10-16 | 2116 | 0 | 0 |
| 2018-10-17 | 2166 | 0 | 0 |
| 2018-10-18 | 2161 | 0 | 0 |
| 2018-10-19 | 2194 | 0 | 0 |
| 2018-10-20 | 2820 | 0 | 0 |
| 2018-10-21 | 2873 | 0 | 0 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.occupancyStatus``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.trip.routeId``
``entity.vehicle.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/BULLRUNNER_V1-GTFSRT-2018-42.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/BULLRUNNER_V1-GTFSRT-2018-42.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/BULLRUNNER_V1-GTFSRT-2018-42.png)
