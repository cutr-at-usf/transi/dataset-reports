### BULLRUNNER_V1-GTFSRT-2018-23
#### Metadata
BULLRUNNER_V1-GTFSRT-2018-23.json.zip: 9.99 MB
BULLRUNNER_V1-GTFSRT-2018-23.pb.zip: 7.31 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 19635 | 0 | 0 |
| Sampling | 30.80 | 0.00 | 0.00 |
| 2018-06-04 | 2772 | 0 | 0 |
| 2018-06-05 | 2869 | 0 | 0 |
| 2018-06-06 | 2772 | 0 | 0 |
| 2018-06-07 | 2612 | 0 | 0 |
| 2018-06-08 | 2867 | 0 | 0 |
| 2018-06-09 | 2872 | 0 | 0 |
| 2018-06-10 | 2871 | 0 | 0 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.occupancyStatus``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.trip.routeId``
``entity.vehicle.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/BULLRUNNER_V1-GTFSRT-2018-23.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/BULLRUNNER_V1-GTFSRT-2018-23.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/BULLRUNNER_V1-GTFSRT-2018-23.png)
