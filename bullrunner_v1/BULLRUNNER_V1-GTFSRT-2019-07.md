### BULLRUNNER_V1-GTFSRT-2019-07
#### Metadata
BULLRUNNER_V1-GTFSRT-2019-07.json.zip: 9.29 MB
BULLRUNNER_V1-GTFSRT-2019-07.pb.zip: 6.68 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 19167 | 0 | 0 |
| Sampling | 31.56 | 0.00 | 0.00 |
| 2019-02-11 | 2866 | 0 | 0 |
| 2019-02-12 | 2223 | 0 | 0 |
| 2019-02-13 | 2606 | 0 | 0 |
| 2019-02-14 | 2858 | 0 | 0 |
| 2019-02-15 | 2869 | 0 | 0 |
| 2019-02-16 | 2873 | 0 | 0 |
| 2019-02-17 | 2872 | 0 | 0 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.occupancyStatus``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.trip.routeId``
``entity.vehicle.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/BULLRUNNER_V1-GTFSRT-2019-07.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/BULLRUNNER_V1-GTFSRT-2019-07.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/BULLRUNNER_V1-GTFSRT-2019-07.png)
