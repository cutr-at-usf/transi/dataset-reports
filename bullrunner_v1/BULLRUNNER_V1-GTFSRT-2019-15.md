### BULLRUNNER_V1-GTFSRT-2019-15
#### Metadata
BULLRUNNER_V1-GTFSRT-2019-15.json.zip: 7.96 MB
BULLRUNNER_V1-GTFSRT-2019-15.pb.zip: 5.75 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 16840 | 0 | 0 |
| Sampling | 35.92 | 0.00 | 0.00 |
| 2019-04-08 | 2516 | 0 | 0 |
| 2019-04-09 | 2507 | 0 | 0 |
| 2019-04-10 | 2042 | 0 | 0 |
| 2019-04-11 | 1925 | 0 | 0 |
| 2019-04-12 | 2349 | 0 | 0 |
| 2019-04-13 | 2687 | 0 | 0 |
| 2019-04-14 | 2814 | 0 | 0 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.occupancyStatus``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.trip.routeId``
``entity.vehicle.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/BULLRUNNER_V1-GTFSRT-2019-15.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/BULLRUNNER_V1-GTFSRT-2019-15.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/BULLRUNNER_V1-GTFSRT-2019-15.png)
