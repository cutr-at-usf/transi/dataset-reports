### BULLRUNNER_V1-GTFSRT-2018-22
#### Metadata
BULLRUNNER_V1-GTFSRT-2018-22.json.zip: 9.05 MB
BULLRUNNER_V1-GTFSRT-2018-22.pb.zip: 6.62 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 19252 | 0 | 0 |
| Sampling | 31.42 | 0.00 | 0.00 |
| 2018-05-28 | 2610 | 0 | 0 |
| 2018-05-29 | 2733 | 0 | 0 |
| 2018-05-30 | 2778 | 0 | 0 |
| 2018-05-31 | 2545 | 0 | 0 |
| 2018-06-01 | 2867 | 0 | 0 |
| 2018-06-02 | 2867 | 0 | 0 |
| 2018-06-03 | 2852 | 0 | 0 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.occupancyStatus``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.trip.routeId``
``entity.vehicle.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/BULLRUNNER_V1-GTFSRT-2018-22.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/BULLRUNNER_V1-GTFSRT-2018-22.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/BULLRUNNER_V1-GTFSRT-2018-22.png)
