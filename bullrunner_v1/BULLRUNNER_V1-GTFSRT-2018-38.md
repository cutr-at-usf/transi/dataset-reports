### BULLRUNNER_V1-GTFSRT-2018-38
#### Metadata
BULLRUNNER_V1-GTFSRT-2018-38.json.zip: 7.96 MB
BULLRUNNER_V1-GTFSRT-2018-38.pb.zip: 5.82 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 16684 | 0 | 0 |
| Sampling | 36.25 | 0.00 | 0.00 |
| 2018-09-17 | 2032 | 0 | 0 |
| 2018-09-18 | 2235 | 0 | 0 |
| 2018-09-19 | 2244 | 0 | 0 |
| 2018-09-20 | 2167 | 0 | 0 |
| 2018-09-21 | 2267 | 0 | 0 |
| 2018-09-22 | 2868 | 0 | 0 |
| 2018-09-23 | 2871 | 0 | 0 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.occupancyStatus``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.trip.routeId``
``entity.vehicle.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/BULLRUNNER_V1-GTFSRT-2018-38.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/BULLRUNNER_V1-GTFSRT-2018-38.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/BULLRUNNER_V1-GTFSRT-2018-38.png)
