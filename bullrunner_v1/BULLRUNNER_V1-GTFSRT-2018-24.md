### BULLRUNNER_V1-GTFSRT-2018-24
#### Metadata
BULLRUNNER_V1-GTFSRT-2018-24.json.zip: 10.09 MB
BULLRUNNER_V1-GTFSRT-2018-24.pb.zip: 7.38 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 19823 | 0 | 0 |
| Sampling | 30.51 | 0.00 | 0.00 |
| 2018-06-11 | 2872 | 0 | 0 |
| 2018-06-12 | 2870 | 0 | 0 |
| 2018-06-13 | 2696 | 0 | 0 |
| 2018-06-14 | 2779 | 0 | 0 |
| 2018-06-15 | 2865 | 0 | 0 |
| 2018-06-16 | 2871 | 0 | 0 |
| 2018-06-17 | 2870 | 0 | 0 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.occupancyStatus``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.trip.routeId``
``entity.vehicle.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/BULLRUNNER_V1-GTFSRT-2018-24.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/BULLRUNNER_V1-GTFSRT-2018-24.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/BULLRUNNER_V1-GTFSRT-2018-24.png)
