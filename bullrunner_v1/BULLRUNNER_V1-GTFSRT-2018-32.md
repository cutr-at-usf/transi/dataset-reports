### BULLRUNNER_V1-GTFSRT-2018-32
#### Metadata
BULLRUNNER_V1-GTFSRT-2018-32.json.zip: 9.66 MB
BULLRUNNER_V1-GTFSRT-2018-32.pb.zip: 7.06 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 19867 | 0 | 0 |
| Sampling | 30.44 | 0.00 | 0.00 |
| 2018-08-06 | 2867 | 0 | 0 |
| 2018-08-07 | 2787 | 0 | 0 |
| 2018-08-08 | 2756 | 0 | 0 |
| 2018-08-09 | 2848 | 0 | 0 |
| 2018-08-10 | 2865 | 0 | 0 |
| 2018-08-11 | 2871 | 0 | 0 |
| 2018-08-12 | 2873 | 0 | 0 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.occupancyStatus``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.trip.routeId``
``entity.vehicle.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/BULLRUNNER_V1-GTFSRT-2018-32.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/BULLRUNNER_V1-GTFSRT-2018-32.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/BULLRUNNER_V1-GTFSRT-2018-32.png)
