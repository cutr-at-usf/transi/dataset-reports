### BULLRUNNER_V1-GTFSRT-2019-03
#### Metadata
BULLRUNNER_V1-GTFSRT-2019-03.json.zip: 7.79 MB
BULLRUNNER_V1-GTFSRT-2019-03.pb.zip: 5.65 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 16680 | 0 | 0 |
| Sampling | 36.26 | 0.00 | 0.00 |
| 2019-01-14 | 2516 | 0 | 0 |
| 2019-01-15 | 2267 | 0 | 0 |
| 2019-01-16 | 1447 | 0 | 0 |
| 2019-01-17 | 2390 | 0 | 0 |
| 2019-01-18 | 2321 | 0 | 0 |
| 2019-01-19 | 2870 | 0 | 0 |
| 2019-01-20 | 2869 | 0 | 0 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.occupancyStatus``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.trip.routeId``
``entity.vehicle.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/BULLRUNNER_V1-GTFSRT-2019-03.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/BULLRUNNER_V1-GTFSRT-2019-03.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/BULLRUNNER_V1-GTFSRT-2019-03.png)
