### BULLRUNNER_V1-GTFSRT-2018-46
#### Metadata
BULLRUNNER_V1-GTFSRT-2018-46.json.zip: 8.14 MB
BULLRUNNER_V1-GTFSRT-2018-46.pb.zip: 5.94 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 18205 | 0 | 0 |
| Sampling | 33.22 | 0.00 | 0.00 |
| 2018-11-12 | 2869 | 0 | 0 |
| 2018-11-13 | 2278 | 0 | 0 |
| 2018-11-14 | 2526 | 0 | 0 |
| 2018-11-15 | 2526 | 0 | 0 |
| 2018-11-16 | 2261 | 0 | 0 |
| 2018-11-17 | 2871 | 0 | 0 |
| 2018-11-18 | 2874 | 0 | 0 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.occupancyStatus``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.trip.routeId``
``entity.vehicle.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/BULLRUNNER_V1-GTFSRT-2018-46.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/BULLRUNNER_V1-GTFSRT-2018-46.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/BULLRUNNER_V1-GTFSRT-2018-46.png)
