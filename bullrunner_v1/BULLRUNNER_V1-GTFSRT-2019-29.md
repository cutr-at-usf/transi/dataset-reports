### BULLRUNNER_V1-GTFSRT-2019-29
#### Metadata
BULLRUNNER_V1-GTFSRT-2019-29.json.zip: 10.08 MB
BULLRUNNER_V1-GTFSRT-2019-29.pb.zip: 7.32 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 20020 | 0 | 0 |
| Sampling | 30.21 | 0.00 | 0.00 |
| 2019-07-15 | 2845 | 0 | 0 |
| 2019-07-16 | 2860 | 0 | 0 |
| 2019-07-17 | 2861 | 0 | 0 |
| 2019-07-18 | 2857 | 0 | 0 |
| 2019-07-19 | 2867 | 0 | 0 |
| 2019-07-20 | 2867 | 0 | 0 |
| 2019-07-21 | 2863 | 0 | 0 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.occupancyStatus``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.trip.routeId``
``entity.vehicle.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/BULLRUNNER_V1-GTFSRT-2019-29.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/BULLRUNNER_V1-GTFSRT-2019-29.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/BULLRUNNER_V1-GTFSRT-2019-29.png)
