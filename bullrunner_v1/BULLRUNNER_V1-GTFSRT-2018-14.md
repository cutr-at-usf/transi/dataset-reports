### BULLRUNNER_V1-GTFSRT-2018-14
#### Metadata
BULLRUNNER_V1-GTFSRT-2018-14.json.zip: 4.74 MB
BULLRUNNER_V1-GTFSRT-2018-14.pb.zip: 3.48 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 10100 | 0 | 0 |
| Sampling | 30.15 | 0.00 | 0.00 |
| 2018-04-02 | 0 | 0 | 0 |
| 2018-04-03 | 0 | 0 | 0 |
| 2018-04-04 | 0 | 0 | 0 |
| 2018-04-05 | 1497 | 0 | 0 |
| 2018-04-06 | 2861 | 0 | 0 |
| 2018-04-07 | 2875 | 0 | 0 |
| 2018-04-08 | 2867 | 0 | 0 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.occupancyStatus``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.trip.routeId``
``entity.vehicle.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/BULLRUNNER_V1-GTFSRT-2018-14.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/BULLRUNNER_V1-GTFSRT-2018-14.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/BULLRUNNER_V1-GTFSRT-2018-14.png)
