### BULLRUNNER_V1-GTFSRT-2019-04
#### Metadata
BULLRUNNER_V1-GTFSRT-2019-04.json.zip: 7.86 MB
BULLRUNNER_V1-GTFSRT-2019-04.pb.zip: 5.76 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 17746 | 0 | 0 |
| Sampling | 34.08 | 0.00 | 0.00 |
| 2019-01-21 | 2871 | 0 | 0 |
| 2019-01-22 | 2132 | 0 | 0 |
| 2019-01-23 | 2393 | 0 | 0 |
| 2019-01-24 | 2126 | 0 | 0 |
| 2019-01-25 | 2492 | 0 | 0 |
| 2019-01-26 | 2863 | 0 | 0 |
| 2019-01-27 | 2869 | 0 | 0 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.occupancyStatus``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.trip.routeId``
``entity.vehicle.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/BULLRUNNER_V1-GTFSRT-2019-04.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/BULLRUNNER_V1-GTFSRT-2019-04.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/BULLRUNNER_V1-GTFSRT-2019-04.png)
