### BULLRUNNER_V1-GTFSRT-2019-09
#### Metadata
BULLRUNNER_V1-GTFSRT-2019-09.json.zip: 8.38 MB
BULLRUNNER_V1-GTFSRT-2019-09.pb.zip: 6.04 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 17805 | 0 | 0 |
| Sampling | 33.97 | 0.00 | 0.00 |
| 2019-02-25 | 2670 | 0 | 0 |
| 2019-02-26 | 2511 | 0 | 0 |
| 2019-02-27 | 2649 | 0 | 0 |
| 2019-02-28 | 1748 | 0 | 0 |
| 2019-03-01 | 2519 | 0 | 0 |
| 2019-03-02 | 2871 | 0 | 0 |
| 2019-03-03 | 2837 | 0 | 0 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.occupancyStatus``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.trip.routeId``
``entity.vehicle.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/BULLRUNNER_V1-GTFSRT-2019-09.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/BULLRUNNER_V1-GTFSRT-2019-09.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/BULLRUNNER_V1-GTFSRT-2019-09.png)
