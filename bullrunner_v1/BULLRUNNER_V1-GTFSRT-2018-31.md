### BULLRUNNER_V1-GTFSRT-2018-31
#### Metadata
BULLRUNNER_V1-GTFSRT-2018-31.json.zip: 9.97 MB
BULLRUNNER_V1-GTFSRT-2018-31.pb.zip: 7.20 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 19680 | 0 | 0 |
| Sampling | 30.73 | 0.00 | 0.00 |
| 2018-07-30 | 2862 | 0 | 0 |
| 2018-07-31 | 2864 | 0 | 0 |
| 2018-08-01 | 2769 | 0 | 0 |
| 2018-08-02 | 2870 | 0 | 0 |
| 2018-08-03 | 2867 | 0 | 0 |
| 2018-08-04 | 2585 | 0 | 0 |
| 2018-08-05 | 2863 | 0 | 0 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.occupancyStatus``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.trip.routeId``
``entity.vehicle.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/BULLRUNNER_V1-GTFSRT-2018-31.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/BULLRUNNER_V1-GTFSRT-2018-31.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/BULLRUNNER_V1-GTFSRT-2018-31.png)
