### BULLRUNNER_V1-GTFSRT-2019-31
#### Metadata
BULLRUNNER_V1-GTFSRT-2019-31.json.zip: 1.56 MB
BULLRUNNER_V1-GTFSRT-2019-31.pb.zip: 1.13 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 2809 | 0 | 0 |
| Sampling | 30.21 | 0.00 | 0.00 |
| 2019-07-29 | 2809 | 0 | 0 |
| 2019-07-30 | 0 | 0 | 0 |
| 2019-07-31 | 0 | 0 | 0 |
| 2019-08-01 | 0 | 0 | 0 |
| 2019-08-02 | 0 | 0 | 0 |
| 2019-08-03 | 0 | 0 | 0 |
| 2019-08-04 | 0 | 0 | 0 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.occupancyStatus``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.trip.routeId``
``entity.vehicle.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/BULLRUNNER_V1-GTFSRT-2019-31.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/BULLRUNNER_V1-GTFSRT-2019-31.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/BULLRUNNER_V1-GTFSRT-2019-31.png)
