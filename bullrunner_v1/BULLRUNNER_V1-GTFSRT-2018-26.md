### BULLRUNNER_V1-GTFSRT-2018-26
#### Metadata
BULLRUNNER_V1-GTFSRT-2018-26.json.zip: 10.09 MB
BULLRUNNER_V1-GTFSRT-2018-26.pb.zip: 7.37 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 19937 | 0 | 0 |
| Sampling | 30.34 | 0.00 | 0.00 |
| 2018-06-25 | 2859 | 0 | 0 |
| 2018-06-26 | 2871 | 0 | 0 |
| 2018-06-27 | 2757 | 0 | 0 |
| 2018-06-28 | 2868 | 0 | 0 |
| 2018-06-29 | 2866 | 0 | 0 |
| 2018-06-30 | 2872 | 0 | 0 |
| 2018-07-01 | 2844 | 0 | 0 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.occupancyStatus``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.trip.routeId``
``entity.vehicle.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/BULLRUNNER_V1-GTFSRT-2018-26.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/BULLRUNNER_V1-GTFSRT-2018-26.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/BULLRUNNER_V1-GTFSRT-2018-26.png)
