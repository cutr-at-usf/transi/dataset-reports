### BULLRUNNER_V1-GTFSRT-2019-24
#### Metadata
BULLRUNNER_V1-GTFSRT-2019-24.json.zip: 9.82 MB
BULLRUNNER_V1-GTFSRT-2019-24.pb.zip: 7.10 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 19865 | 0 | 0 |
| Sampling | 30.44 | 0.00 | 0.00 |
| 2019-06-10 | 2850 | 0 | 0 |
| 2019-06-11 | 2859 | 0 | 0 |
| 2019-06-12 | 2686 | 0 | 0 |
| 2019-06-13 | 2862 | 0 | 0 |
| 2019-06-14 | 2867 | 0 | 0 |
| 2019-06-15 | 2869 | 0 | 0 |
| 2019-06-16 | 2872 | 0 | 0 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.occupancyStatus``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.trip.routeId``
``entity.vehicle.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/BULLRUNNER_V1-GTFSRT-2019-24.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/BULLRUNNER_V1-GTFSRT-2019-24.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/BULLRUNNER_V1-GTFSRT-2019-24.png)
