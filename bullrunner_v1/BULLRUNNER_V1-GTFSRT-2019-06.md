### BULLRUNNER_V1-GTFSRT-2019-06
#### Metadata
BULLRUNNER_V1-GTFSRT-2019-06.json.zip: 8.34 MB
BULLRUNNER_V1-GTFSRT-2019-06.pb.zip: 6.03 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 17644 | 0 | 0 |
| Sampling | 34.28 | 0.00 | 0.00 |
| 2019-02-04 | 2862 | 0 | 0 |
| 2019-02-05 | 1578 | 0 | 0 |
| 2019-02-06 | 2401 | 0 | 0 |
| 2019-02-07 | 2322 | 0 | 0 |
| 2019-02-08 | 2741 | 0 | 0 |
| 2019-02-09 | 2869 | 0 | 0 |
| 2019-02-10 | 2871 | 0 | 0 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.occupancyStatus``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.trip.routeId``
``entity.vehicle.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/BULLRUNNER_V1-GTFSRT-2019-06.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/BULLRUNNER_V1-GTFSRT-2019-06.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/BULLRUNNER_V1-GTFSRT-2019-06.png)
