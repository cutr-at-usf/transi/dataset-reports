### BULLRUNNER_V1-GTFSRT-2019-13
#### Metadata
BULLRUNNER_V1-GTFSRT-2019-13.json.zip: 8.24 MB
BULLRUNNER_V1-GTFSRT-2019-13.pb.zip: 5.96 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 17496 | 0 | 0 |
| Sampling | 34.57 | 0.00 | 0.00 |
| 2019-03-25 | 2359 | 0 | 0 |
| 2019-03-26 | 2063 | 0 | 0 |
| 2019-03-27 | 2140 | 0 | 0 |
| 2019-03-28 | 2515 | 0 | 0 |
| 2019-03-29 | 2680 | 0 | 0 |
| 2019-03-30 | 2868 | 0 | 0 |
| 2019-03-31 | 2871 | 0 | 0 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.occupancyStatus``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.trip.routeId``
``entity.vehicle.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/BULLRUNNER_V1-GTFSRT-2019-13.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/BULLRUNNER_V1-GTFSRT-2019-13.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/BULLRUNNER_V1-GTFSRT-2019-13.png)
