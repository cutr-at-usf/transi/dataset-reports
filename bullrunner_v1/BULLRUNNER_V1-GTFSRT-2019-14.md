### BULLRUNNER_V1-GTFSRT-2019-14
#### Metadata
BULLRUNNER_V1-GTFSRT-2019-14.json.zip: 7.50 MB
BULLRUNNER_V1-GTFSRT-2019-14.pb.zip: 5.48 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 16031 | 0 | 0 |
| Sampling | 37.73 | 0.00 | 0.00 |
| 2019-04-01 | 2183 | 0 | 0 |
| 2019-04-02 | 2016 | 0 | 0 |
| 2019-04-03 | 2143 | 0 | 0 |
| 2019-04-04 | 1434 | 0 | 0 |
| 2019-04-05 | 2854 | 0 | 0 |
| 2019-04-06 | 2545 | 0 | 0 |
| 2019-04-07 | 2856 | 0 | 0 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.occupancyStatus``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.trip.routeId``
``entity.vehicle.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/BULLRUNNER_V1-GTFSRT-2019-14.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/BULLRUNNER_V1-GTFSRT-2019-14.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/BULLRUNNER_V1-GTFSRT-2019-14.png)
