### BULLRUNNER_V1-GTFSRT-2018-19
#### Metadata
BULLRUNNER_V1-GTFSRT-2018-19.json.zip: 9.80 MB
BULLRUNNER_V1-GTFSRT-2018-19.pb.zip: 7.18 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 19988 | 0 | 0 |
| Sampling | 30.26 | 0.00 | 0.00 |
| 2018-05-07 | 2864 | 0 | 0 |
| 2018-05-08 | 2866 | 0 | 0 |
| 2018-05-09 | 2782 | 0 | 0 |
| 2018-05-10 | 2858 | 0 | 0 |
| 2018-05-11 | 2870 | 0 | 0 |
| 2018-05-12 | 2874 | 0 | 0 |
| 2018-05-13 | 2874 | 0 | 0 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.occupancyStatus``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.trip.routeId``
``entity.vehicle.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/BULLRUNNER_V1-GTFSRT-2018-19.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/BULLRUNNER_V1-GTFSRT-2018-19.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/BULLRUNNER_V1-GTFSRT-2018-19.png)
