### BULLRUNNER_V1-GTFSRT-2019-16
#### Metadata
BULLRUNNER_V1-GTFSRT-2019-16.json.zip: 7.89 MB
BULLRUNNER_V1-GTFSRT-2019-16.pb.zip: 5.70 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 16519 | 0 | 0 |
| Sampling | 36.61 | 0.00 | 0.00 |
| 2019-04-15 | 2608 | 0 | 0 |
| 2019-04-16 | 2296 | 0 | 0 |
| 2019-04-17 | 2432 | 0 | 0 |
| 2019-04-18 | 1794 | 0 | 0 |
| 2019-04-19 | 2274 | 0 | 0 |
| 2019-04-20 | 2515 | 0 | 0 |
| 2019-04-21 | 2600 | 0 | 0 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.occupancyStatus``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.trip.routeId``
``entity.vehicle.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/BULLRUNNER_V1-GTFSRT-2019-16.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/BULLRUNNER_V1-GTFSRT-2019-16.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/BULLRUNNER_V1-GTFSRT-2019-16.png)
