### BULLRUNNER_V1-GTFSRT-2018-16
#### Metadata
BULLRUNNER_V1-GTFSRT-2018-16.json.zip: 10.17 MB
BULLRUNNER_V1-GTFSRT-2018-16.pb.zip: 7.42 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 20064 | 0 | 0 |
| Sampling | 30.14 | 0.00 | 0.00 |
| 2018-04-16 | 2865 | 0 | 0 |
| 2018-04-17 | 2866 | 0 | 0 |
| 2018-04-18 | 2866 | 0 | 0 |
| 2018-04-19 | 2858 | 0 | 0 |
| 2018-04-20 | 2865 | 0 | 0 |
| 2018-04-21 | 2871 | 0 | 0 |
| 2018-04-22 | 2873 | 0 | 0 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.occupancyStatus``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.trip.routeId``
``entity.vehicle.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/BULLRUNNER_V1-GTFSRT-2018-16.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/BULLRUNNER_V1-GTFSRT-2018-16.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/BULLRUNNER_V1-GTFSRT-2018-16.png)
