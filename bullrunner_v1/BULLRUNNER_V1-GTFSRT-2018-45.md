### BULLRUNNER_V1-GTFSRT-2018-45
#### Metadata
BULLRUNNER_V1-GTFSRT-2018-45.json.zip: 8.15 MB
BULLRUNNER_V1-GTFSRT-2018-45.pb.zip: 5.94 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 17120 | 0 | 0 |
| Sampling | 35.33 | 0.00 | 0.00 |
| 2018-11-05 | 2236 | 0 | 0 |
| 2018-11-06 | 2203 | 0 | 0 |
| 2018-11-07 | 2131 | 0 | 0 |
| 2018-11-08 | 2193 | 0 | 0 |
| 2018-11-09 | 2619 | 0 | 0 |
| 2018-11-10 | 2869 | 0 | 0 |
| 2018-11-11 | 2869 | 0 | 0 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.occupancyStatus``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.trip.routeId``
``entity.vehicle.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/BULLRUNNER_V1-GTFSRT-2018-45.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/BULLRUNNER_V1-GTFSRT-2018-45.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/BULLRUNNER_V1-GTFSRT-2018-45.png)
