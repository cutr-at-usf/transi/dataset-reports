### BULLRUNNER_V1-GTFSRT-2019-21
#### Metadata
BULLRUNNER_V1-GTFSRT-2019-21.json.zip: 9.45 MB
BULLRUNNER_V1-GTFSRT-2019-21.pb.zip: 6.85 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 19197 | 0 | 0 |
| Sampling | 31.51 | 0.00 | 0.00 |
| 2019-05-20 | 2866 | 0 | 0 |
| 2019-05-21 | 2863 | 0 | 0 |
| 2019-05-22 | 2170 | 0 | 0 |
| 2019-05-23 | 2770 | 0 | 0 |
| 2019-05-24 | 2855 | 0 | 0 |
| 2019-05-25 | 2802 | 0 | 0 |
| 2019-05-26 | 2871 | 0 | 0 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.occupancyStatus``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.trip.routeId``
``entity.vehicle.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/BULLRUNNER_V1-GTFSRT-2019-21.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/BULLRUNNER_V1-GTFSRT-2019-21.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/BULLRUNNER_V1-GTFSRT-2019-21.png)
