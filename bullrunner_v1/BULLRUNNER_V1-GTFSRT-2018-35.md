### BULLRUNNER_V1-GTFSRT-2018-35
#### Metadata
BULLRUNNER_V1-GTFSRT-2018-35.json.zip: 10.21 MB
BULLRUNNER_V1-GTFSRT-2018-35.pb.zip: 7.46 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 19917 | 0 | 0 |
| Sampling | 30.37 | 0.00 | 0.00 |
| 2018-08-27 | 2862 | 0 | 0 |
| 2018-08-28 | 2854 | 0 | 0 |
| 2018-08-29 | 2752 | 0 | 0 |
| 2018-08-30 | 2860 | 0 | 0 |
| 2018-08-31 | 2861 | 0 | 0 |
| 2018-09-01 | 2871 | 0 | 0 |
| 2018-09-02 | 2857 | 0 | 0 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.occupancyStatus``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.trip.routeId``
``entity.vehicle.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/BULLRUNNER_V1-GTFSRT-2018-35.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/BULLRUNNER_V1-GTFSRT-2018-35.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/BULLRUNNER_V1-GTFSRT-2018-35.png)
