### BULLRUNNER_V1-GTFSRT-2018-28
#### Metadata
BULLRUNNER_V1-GTFSRT-2018-28.json.zip: 10.40 MB
BULLRUNNER_V1-GTFSRT-2018-28.pb.zip: 7.50 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 19995 | 0 | 0 |
| Sampling | 30.25 | 0.00 | 0.00 |
| 2018-07-09 | 2864 | 0 | 0 |
| 2018-07-10 | 2874 | 0 | 0 |
| 2018-07-11 | 2774 | 0 | 0 |
| 2018-07-12 | 2871 | 0 | 0 |
| 2018-07-13 | 2869 | 0 | 0 |
| 2018-07-14 | 2871 | 0 | 0 |
| 2018-07-15 | 2872 | 0 | 0 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.occupancyStatus``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.trip.routeId``
``entity.vehicle.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/BULLRUNNER_V1-GTFSRT-2018-28.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/BULLRUNNER_V1-GTFSRT-2018-28.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/BULLRUNNER_V1-GTFSRT-2018-28.png)
