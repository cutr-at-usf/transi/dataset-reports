### BULLRUNNER_V1-GTFSRT-2019-10
#### Metadata
BULLRUNNER_V1-GTFSRT-2019-10.json.zip: 8.09 MB
BULLRUNNER_V1-GTFSRT-2019-10.pb.zip: 5.88 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 17037 | 0 | 0 |
| Sampling | 35.29 | 0.00 | 0.00 |
| 2019-03-04 | 2509 | 0 | 0 |
| 2019-03-05 | 2038 | 0 | 0 |
| 2019-03-06 | 2503 | 0 | 0 |
| 2019-03-07 | 2107 | 0 | 0 |
| 2019-03-08 | 2261 | 0 | 0 |
| 2019-03-09 | 2869 | 0 | 0 |
| 2019-03-10 | 2750 | 0 | 0 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.occupancyStatus``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.trip.routeId``
``entity.vehicle.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/BULLRUNNER_V1-GTFSRT-2019-10.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/BULLRUNNER_V1-GTFSRT-2019-10.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/BULLRUNNER_V1-GTFSRT-2019-10.png)
