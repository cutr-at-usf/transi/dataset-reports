### BULLRUNNER_V1-GTFSRT-2019-08
#### Metadata
BULLRUNNER_V1-GTFSRT-2019-08.json.zip: 8.75 MB
BULLRUNNER_V1-GTFSRT-2019-08.pb.zip: 6.31 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 18399 | 0 | 0 |
| Sampling | 32.87 | 0.00 | 0.00 |
| 2019-02-18 | 2530 | 0 | 0 |
| 2019-02-19 | 2562 | 0 | 0 |
| 2019-02-20 | 2533 | 0 | 0 |
| 2019-02-21 | 2527 | 0 | 0 |
| 2019-02-22 | 2506 | 0 | 0 |
| 2019-02-23 | 2869 | 0 | 0 |
| 2019-02-24 | 2872 | 0 | 0 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.occupancyStatus``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.trip.routeId``
``entity.vehicle.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/BULLRUNNER_V1-GTFSRT-2019-08.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/BULLRUNNER_V1-GTFSRT-2019-08.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/BULLRUNNER_V1-GTFSRT-2019-08.png)
