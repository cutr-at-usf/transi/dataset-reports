### BULLRUNNER_V1-GTFSRT-2019-11
#### Metadata
BULLRUNNER_V1-GTFSRT-2019-11.json.zip: 9.51 MB
BULLRUNNER_V1-GTFSRT-2019-11.pb.zip: 6.90 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 20076 | 0 | 0 |
| Sampling | 30.13 | 0.00 | 0.00 |
| 2019-03-11 | 2866 | 0 | 0 |
| 2019-03-12 | 2869 | 0 | 0 |
| 2019-03-13 | 2869 | 0 | 0 |
| 2019-03-14 | 2866 | 0 | 0 |
| 2019-03-15 | 2869 | 0 | 0 |
| 2019-03-16 | 2869 | 0 | 0 |
| 2019-03-17 | 2868 | 0 | 0 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.occupancyStatus``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.trip.routeId``
``entity.vehicle.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/BULLRUNNER_V1-GTFSRT-2019-11.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/BULLRUNNER_V1-GTFSRT-2019-11.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/BULLRUNNER_V1-GTFSRT-2019-11.png)
