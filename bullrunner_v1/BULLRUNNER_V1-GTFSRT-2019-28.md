### BULLRUNNER_V1-GTFSRT-2019-28
#### Metadata
BULLRUNNER_V1-GTFSRT-2019-28.json.zip: 10.08 MB
BULLRUNNER_V1-GTFSRT-2019-28.pb.zip: 7.34 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 19975 | 0 | 0 |
| Sampling | 30.28 | 0.00 | 0.00 |
| 2019-07-08 | 2816 | 0 | 0 |
| 2019-07-09 | 2841 | 0 | 0 |
| 2019-07-10 | 2857 | 0 | 0 |
| 2019-07-11 | 2866 | 0 | 0 |
| 2019-07-12 | 2866 | 0 | 0 |
| 2019-07-13 | 2869 | 0 | 0 |
| 2019-07-14 | 2860 | 0 | 0 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.occupancyStatus``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.trip.routeId``
``entity.vehicle.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/BULLRUNNER_V1-GTFSRT-2019-28.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/BULLRUNNER_V1-GTFSRT-2019-28.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/BULLRUNNER_V1-GTFSRT-2019-28.png)
