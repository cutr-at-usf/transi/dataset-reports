### BULLRUNNER_V1-GTFSRT-2018-15
#### Metadata
BULLRUNNER_V1-GTFSRT-2018-15.json.zip: 9.78 MB
BULLRUNNER_V1-GTFSRT-2018-15.pb.zip: 7.13 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 19492 | 0 | 0 |
| Sampling | 31.03 | 0.00 | 0.00 |
| 2018-04-09 | 2865 | 0 | 0 |
| 2018-04-10 | 2850 | 0 | 0 |
| 2018-04-11 | 2862 | 0 | 0 |
| 2018-04-12 | 2356 | 0 | 0 |
| 2018-04-13 | 2831 | 0 | 0 |
| 2018-04-14 | 2873 | 0 | 0 |
| 2018-04-15 | 2855 | 0 | 0 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.occupancyStatus``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.trip.routeId``
``entity.vehicle.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/BULLRUNNER_V1-GTFSRT-2018-15.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/BULLRUNNER_V1-GTFSRT-2018-15.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/BULLRUNNER_V1-GTFSRT-2018-15.png)
