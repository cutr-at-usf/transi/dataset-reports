### BULLRUNNER_V1-GTFSRT-2018-43
#### Metadata
BULLRUNNER_V1-GTFSRT-2018-43.json.zip: 8.06 MB
BULLRUNNER_V1-GTFSRT-2018-43.pb.zip: 5.89 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 16864 | 0 | 0 |
| Sampling | 35.86 | 0.00 | 0.00 |
| 2018-10-22 | 2209 | 0 | 0 |
| 2018-10-23 | 2165 | 0 | 0 |
| 2018-10-24 | 2155 | 0 | 0 |
| 2018-10-25 | 2085 | 0 | 0 |
| 2018-10-26 | 2504 | 0 | 0 |
| 2018-10-27 | 2872 | 0 | 0 |
| 2018-10-28 | 2874 | 0 | 0 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.occupancyStatus``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.trip.routeId``
``entity.vehicle.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/BULLRUNNER_V1-GTFSRT-2018-43.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/BULLRUNNER_V1-GTFSRT-2018-43.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/BULLRUNNER_V1-GTFSRT-2018-43.png)
