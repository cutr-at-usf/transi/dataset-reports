### BULLRUNNER_V1-GTFSRT-2018-36
#### Metadata
BULLRUNNER_V1-GTFSRT-2018-36.json.zip: 8.85 MB
BULLRUNNER_V1-GTFSRT-2018-36.pb.zip: 6.51 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 18858 | 0 | 0 |
| Sampling | 32.07 | 0.00 | 0.00 |
| 2018-09-03 | 2863 | 0 | 0 |
| 2018-09-04 | 2855 | 0 | 0 |
| 2018-09-05 | 2763 | 0 | 0 |
| 2018-09-06 | 2483 | 0 | 0 |
| 2018-09-07 | 2161 | 0 | 0 |
| 2018-09-08 | 2869 | 0 | 0 |
| 2018-09-09 | 2864 | 0 | 0 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.occupancyStatus``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.trip.routeId``
``entity.vehicle.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/BULLRUNNER_V1-GTFSRT-2018-36.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/BULLRUNNER_V1-GTFSRT-2018-36.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/BULLRUNNER_V1-GTFSRT-2018-36.png)
