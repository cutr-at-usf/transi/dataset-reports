### BULLRUNNER_V1-GTFSRT-2018-40
#### Metadata
BULLRUNNER_V1-GTFSRT-2018-40.json.zip: 7.95 MB
BULLRUNNER_V1-GTFSRT-2018-40.pb.zip: 5.82 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 16655 | 0 | 0 |
| Sampling | 36.31 | 0.00 | 0.00 |
| 2018-10-01 | 2201 | 0 | 0 |
| 2018-10-02 | 2229 | 0 | 0 |
| 2018-10-03 | 2194 | 0 | 0 |
| 2018-10-04 | 2043 | 0 | 0 |
| 2018-10-05 | 2285 | 0 | 0 |
| 2018-10-06 | 2863 | 0 | 0 |
| 2018-10-07 | 2840 | 0 | 0 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.occupancyStatus``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.trip.routeId``
``entity.vehicle.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/BULLRUNNER_V1-GTFSRT-2018-40.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/BULLRUNNER_V1-GTFSRT-2018-40.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/BULLRUNNER_V1-GTFSRT-2018-40.png)
