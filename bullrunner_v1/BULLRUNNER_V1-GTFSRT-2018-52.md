### BULLRUNNER_V1-GTFSRT-2018-52
#### Metadata
BULLRUNNER_V1-GTFSRT-2018-52.json.zip: 6.83 MB
BULLRUNNER_V1-GTFSRT-2018-52.pb.zip: 5.27 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 20102 | 0 | 0 |
| Sampling | 30.09 | 0.00 | 0.00 |
| 2018-12-24 | 2873 | 0 | 0 |
| 2018-12-25 | 2871 | 0 | 0 |
| 2018-12-26 | 2872 | 0 | 0 |
| 2018-12-27 | 2871 | 0 | 0 |
| 2018-12-28 | 2871 | 0 | 0 |
| 2018-12-29 | 2874 | 0 | 0 |
| 2018-12-30 | 2870 | 0 | 0 |
#### Structures
##### vehicle_positions
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/BULLRUNNER_V1-GTFSRT-2018-52.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/BULLRUNNER_V1-GTFSRT-2018-52.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/BULLRUNNER_V1-GTFSRT-2018-52.png)
