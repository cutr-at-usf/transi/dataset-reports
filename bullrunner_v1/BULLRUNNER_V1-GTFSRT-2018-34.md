### BULLRUNNER_V1-GTFSRT-2018-34
#### Metadata
BULLRUNNER_V1-GTFSRT-2018-34.json.zip: 10.31 MB
BULLRUNNER_V1-GTFSRT-2018-34.pb.zip: 7.54 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 19910 | 0 | 0 |
| Sampling | 30.38 | 0.00 | 0.00 |
| 2018-08-20 | 2862 | 0 | 0 |
| 2018-08-21 | 2862 | 0 | 0 |
| 2018-08-22 | 2729 | 0 | 0 |
| 2018-08-23 | 2862 | 0 | 0 |
| 2018-08-24 | 2862 | 0 | 0 |
| 2018-08-25 | 2865 | 0 | 0 |
| 2018-08-26 | 2868 | 0 | 0 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.occupancyStatus``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.trip.routeId``
``entity.vehicle.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/BULLRUNNER_V1-GTFSRT-2018-34.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/BULLRUNNER_V1-GTFSRT-2018-34.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/BULLRUNNER_V1-GTFSRT-2018-34.png)
