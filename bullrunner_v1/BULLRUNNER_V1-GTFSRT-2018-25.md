### BULLRUNNER_V1-GTFSRT-2018-25
#### Metadata
BULLRUNNER_V1-GTFSRT-2018-25.json.zip: 10.12 MB
BULLRUNNER_V1-GTFSRT-2018-25.pb.zip: 7.39 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 19871 | 0 | 0 |
| Sampling | 30.44 | 0.00 | 0.00 |
| 2018-06-18 | 2870 | 0 | 0 |
| 2018-06-19 | 2865 | 0 | 0 |
| 2018-06-20 | 2749 | 0 | 0 |
| 2018-06-21 | 2867 | 0 | 0 |
| 2018-06-22 | 2868 | 0 | 0 |
| 2018-06-23 | 2872 | 0 | 0 |
| 2018-06-24 | 2780 | 0 | 0 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.occupancyStatus``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.trip.routeId``
``entity.vehicle.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/BULLRUNNER_V1-GTFSRT-2018-25.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/BULLRUNNER_V1-GTFSRT-2018-25.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/BULLRUNNER_V1-GTFSRT-2018-25.png)
