### BULLRUNNER_V1-GTFSRT-2018-18
#### Metadata
BULLRUNNER_V1-GTFSRT-2018-18.json.zip: 8.40 MB
BULLRUNNER_V1-GTFSRT-2018-18.pb.zip: 6.15 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 17244 | 0 | 0 |
| Sampling | 35.07 | 0.00 | 0.00 |
| 2018-04-30 | 2868 | 0 | 0 |
| 2018-05-01 | 2859 | 0 | 0 |
| 2018-05-02 | 2862 | 0 | 0 |
| 2018-05-03 | 2326 | 0 | 0 |
| 2018-05-04 | 1532 | 0 | 0 |
| 2018-05-05 | 1946 | 0 | 0 |
| 2018-05-06 | 2851 | 0 | 0 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.occupancyStatus``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.trip.routeId``
``entity.vehicle.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/BULLRUNNER_V1-GTFSRT-2018-18.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/BULLRUNNER_V1-GTFSRT-2018-18.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/BULLRUNNER_V1-GTFSRT-2018-18.png)
