### BULLRUNNER_V1-GTFSRT-2018-49
#### Metadata
BULLRUNNER_V1-GTFSRT-2018-49.json.zip: 9.39 MB
BULLRUNNER_V1-GTFSRT-2018-49.pb.zip: 6.83 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 18965 | 0 | 0 |
| Sampling | 31.89 | 0.00 | 0.00 |
| 2018-12-03 | 2865 | 0 | 0 |
| 2018-12-04 | 2867 | 0 | 0 |
| 2018-12-05 | 2866 | 0 | 0 |
| 2018-12-06 | 2864 | 0 | 0 |
| 2018-12-07 | 2651 | 0 | 0 |
| 2018-12-08 | 1998 | 0 | 0 |
| 2018-12-09 | 2854 | 0 | 0 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.occupancyStatus``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.trip.routeId``
``entity.vehicle.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/BULLRUNNER_V1-GTFSRT-2018-49.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/BULLRUNNER_V1-GTFSRT-2018-49.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/BULLRUNNER_V1-GTFSRT-2018-49.png)
