### BULLRUNNER_V1-GTFSRT-2019-20
#### Metadata
BULLRUNNER_V1-GTFSRT-2019-20.json.zip: 9.83 MB
BULLRUNNER_V1-GTFSRT-2019-20.pb.zip: 7.13 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 19868 | 0 | 0 |
| Sampling | 30.44 | 0.00 | 0.00 |
| 2019-05-13 | 2862 | 0 | 0 |
| 2019-05-14 | 2861 | 0 | 0 |
| 2019-05-15 | 2667 | 0 | 0 |
| 2019-05-16 | 2870 | 0 | 0 |
| 2019-05-17 | 2868 | 0 | 0 |
| 2019-05-18 | 2868 | 0 | 0 |
| 2019-05-19 | 2872 | 0 | 0 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.occupancyStatus``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.trip.routeId``
``entity.vehicle.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/BULLRUNNER_V1-GTFSRT-2019-20.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/BULLRUNNER_V1-GTFSRT-2019-20.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/BULLRUNNER_V1-GTFSRT-2019-20.png)
