### BULLRUNNER_V1-GTFSRT-2018-21
#### Metadata
BULLRUNNER_V1-GTFSRT-2018-21.json.zip: 9.99 MB
BULLRUNNER_V1-GTFSRT-2018-21.pb.zip: 7.29 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 19575 | 0 | 0 |
| Sampling | 30.90 | 0.00 | 0.00 |
| 2018-05-21 | 2850 | 0 | 0 |
| 2018-05-22 | 2869 | 0 | 0 |
| 2018-05-23 | 2858 | 0 | 0 |
| 2018-05-24 | 2799 | 0 | 0 |
| 2018-05-25 | 2761 | 0 | 0 |
| 2018-05-26 | 2758 | 0 | 0 |
| 2018-05-27 | 2680 | 0 | 0 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.occupancyStatus``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.trip.routeId``
``entity.vehicle.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/BULLRUNNER_V1-GTFSRT-2018-21.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/BULLRUNNER_V1-GTFSRT-2018-21.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/BULLRUNNER_V1-GTFSRT-2018-21.png)
