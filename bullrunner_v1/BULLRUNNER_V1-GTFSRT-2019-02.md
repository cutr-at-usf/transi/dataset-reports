### BULLRUNNER_V1-GTFSRT-2019-02
#### Metadata
BULLRUNNER_V1-GTFSRT-2019-02.json.zip: 8.17 MB
BULLRUNNER_V1-GTFSRT-2019-02.pb.zip: 5.94 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 17190 | 0 | 0 |
| Sampling | 35.18 | 0.00 | 0.00 |
| 2019-01-07 | 2509 | 0 | 0 |
| 2019-01-08 | 2357 | 0 | 0 |
| 2019-01-09 | 2277 | 0 | 0 |
| 2019-01-10 | 2153 | 0 | 0 |
| 2019-01-11 | 2159 | 0 | 0 |
| 2019-01-12 | 2866 | 0 | 0 |
| 2019-01-13 | 2869 | 0 | 0 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.occupancyStatus``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.trip.routeId``
``entity.vehicle.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/BULLRUNNER_V1-GTFSRT-2019-02.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/BULLRUNNER_V1-GTFSRT-2019-02.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/BULLRUNNER_V1-GTFSRT-2019-02.png)
