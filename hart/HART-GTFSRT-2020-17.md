### HART-GTFSRT-2020-17
#### Metadata
HART-GTFSRT-2020-17.json.zip: 48.54 MB
HART-GTFSRT-2020-17.pb.zip: 41.85 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 19838 | 19831 | 0 |
| Sampling | 30.49 | 30.50 | 0.00 |
| 2020-04-20 | 2843 | 2843 | 0 |
| 2020-04-21 | 2842 | 2842 | 0 |
| 2020-04-22 | 2771 | 2769 | 0 |
| 2020-04-23 | 2843 | 2839 | 0 |
| 2020-04-24 | 2847 | 2847 | 0 |
| 2020-04-25 | 2846 | 2846 | 0 |
| 2020-04-26 | 2846 | 2845 | 0 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.position.speed``
``entity.vehicle.timestamp``
``entity.vehicle.trip.tripId``
``entity.vehicle.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
``entity.id``
``entity.tripUpdate.stopTimeUpdate.arrival.delay``
``entity.tripUpdate.stopTimeUpdate.arrival.uncertainty``
``entity.tripUpdate.stopTimeUpdate.departure.delay``
``entity.tripUpdate.stopTimeUpdate.departure.uncertainty``
``entity.tripUpdate.stopTimeUpdate.stopId``
``entity.tripUpdate.stopTimeUpdate.stopSequence``
``entity.tripUpdate.timestamp``
``entity.tripUpdate.trip.tripId``
``entity.tripUpdate.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/HART-GTFSRT-2020-17.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/HART-GTFSRT-2020-17.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/HART-GTFSRT-2020-17.png)
