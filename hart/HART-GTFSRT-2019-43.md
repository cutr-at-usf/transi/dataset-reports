### HART-GTFSRT-2019-43
#### Metadata
HART-GTFSRT-2019-43.json.zip: 76.19 MB
HART-GTFSRT-2019-43.pb.zip: 68.41 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 19878 | 19878 | 0 |
| Sampling | 30.43 | 30.43 | 0.00 |
| 2019-10-21 | 2840 | 2840 | 0 |
| 2019-10-22 | 2838 | 2838 | 0 |
| 2019-10-23 | 2836 | 2836 | 0 |
| 2019-10-24 | 2838 | 2837 | 0 |
| 2019-10-25 | 2840 | 2841 | 0 |
| 2019-10-26 | 2842 | 2842 | 0 |
| 2019-10-27 | 2844 | 2844 | 0 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.position.speed``
``entity.vehicle.timestamp``
``entity.vehicle.trip.tripId``
``entity.vehicle.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
``entity.id``
``entity.tripUpdate.stopTimeUpdate.arrival.delay``
``entity.tripUpdate.stopTimeUpdate.arrival.uncertainty``
``entity.tripUpdate.stopTimeUpdate.departure.delay``
``entity.tripUpdate.stopTimeUpdate.departure.uncertainty``
``entity.tripUpdate.stopTimeUpdate.stopId``
``entity.tripUpdate.stopTimeUpdate.stopSequence``
``entity.tripUpdate.timestamp``
``entity.tripUpdate.trip.tripId``
``entity.tripUpdate.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/HART-GTFSRT-2019-43.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/HART-GTFSRT-2019-43.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/HART-GTFSRT-2019-43.png)
