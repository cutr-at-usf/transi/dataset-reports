### HART-GTFSRT-2019-33
#### Metadata
HART-GTFSRT-2019-33.json.zip: 64.36 MB
HART-GTFSRT-2019-33.pb.zip: 56.94 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 18524 | 18524 | 0 |
| Sampling | 32.65 | 32.65 | 0.00 |
| 2019-08-12 | 2629 | 2629 | 0 |
| 2019-08-13 | 2616 | 2616 | 0 |
| 2019-08-14 | 2534 | 2535 | 0 |
| 2019-08-15 | 2440 | 2439 | 0 |
| 2019-08-16 | 2635 | 2635 | 0 |
| 2019-08-17 | 2833 | 2833 | 0 |
| 2019-08-18 | 2837 | 2837 | 0 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.position.speed``
``entity.vehicle.trip.tripId``
``entity.vehicle.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
``entity.id``
``entity.tripUpdate.stopTimeUpdate.arrival.delay``
``entity.tripUpdate.stopTimeUpdate.arrival.uncertainty``
``entity.tripUpdate.stopTimeUpdate.departure.delay``
``entity.tripUpdate.stopTimeUpdate.departure.uncertainty``
``entity.tripUpdate.stopTimeUpdate.stopId``
``entity.tripUpdate.stopTimeUpdate.stopSequence``
``entity.tripUpdate.trip.tripId``
``entity.tripUpdate.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/HART-GTFSRT-2019-33.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/HART-GTFSRT-2019-33.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/HART-GTFSRT-2019-33.png)
