### HART-GTFSRT-2018-40
#### Metadata
HART-GTFSRT-2018-40.json.zip: 68.82 MB
HART-GTFSRT-2018-40.pb.zip: 61.14 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 19788 | 19788 | 0 |
| Sampling | 30.56 | 30.56 | 0.00 |
| 2018-10-01 | 2831 | 2831 | 0 |
| 2018-10-02 | 2832 | 2832 | 0 |
| 2018-10-03 | 2832 | 2832 | 0 |
| 2018-10-04 | 2834 | 2834 | 0 |
| 2018-10-05 | 2835 | 2835 | 0 |
| 2018-10-06 | 2829 | 2829 | 0 |
| 2018-10-07 | 2795 | 2795 | 0 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.position.speed``
``entity.vehicle.trip.tripId``
``entity.vehicle.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
``entity.id``
``entity.tripUpdate.stopTimeUpdate.arrival.delay``
``entity.tripUpdate.stopTimeUpdate.arrival.uncertainty``
``entity.tripUpdate.stopTimeUpdate.departure.delay``
``entity.tripUpdate.stopTimeUpdate.departure.uncertainty``
``entity.tripUpdate.stopTimeUpdate.stopId``
``entity.tripUpdate.stopTimeUpdate.stopSequence``
``entity.tripUpdate.trip.tripId``
``entity.tripUpdate.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/HART-GTFSRT-2018-40.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/HART-GTFSRT-2018-40.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/HART-GTFSRT-2018-40.png)
