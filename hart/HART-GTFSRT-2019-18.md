### HART-GTFSRT-2019-18
#### Metadata
HART-GTFSRT-2019-18.json.zip: 63.44 MB
HART-GTFSRT-2019-18.pb.zip: 55.95 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 18499 | 18499 | 0 |
| Sampling | 32.69 | 32.69 | 0.00 |
| 2019-04-29 | 2555 | 2555 | 0 |
| 2019-04-30 | 2596 | 2596 | 0 |
| 2019-05-01 | 2457 | 2457 | 0 |
| 2019-05-02 | 2612 | 2612 | 0 |
| 2019-05-03 | 2648 | 2648 | 0 |
| 2019-05-04 | 2818 | 2818 | 0 |
| 2019-05-05 | 2813 | 2813 | 0 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.position.speed``
``entity.vehicle.trip.tripId``
``entity.vehicle.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
``entity.id``
``entity.tripUpdate.stopTimeUpdate.arrival.delay``
``entity.tripUpdate.stopTimeUpdate.arrival.uncertainty``
``entity.tripUpdate.stopTimeUpdate.departure.delay``
``entity.tripUpdate.stopTimeUpdate.departure.uncertainty``
``entity.tripUpdate.stopTimeUpdate.stopId``
``entity.tripUpdate.stopTimeUpdate.stopSequence``
``entity.tripUpdate.trip.tripId``
``entity.tripUpdate.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/HART-GTFSRT-2019-18.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/HART-GTFSRT-2019-18.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/HART-GTFSRT-2019-18.png)
