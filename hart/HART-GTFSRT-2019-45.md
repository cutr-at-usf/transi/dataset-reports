### HART-GTFSRT-2019-45
#### Metadata
HART-GTFSRT-2019-45.json.zip: 75.05 MB
HART-GTFSRT-2019-45.pb.zip: 67.37 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 19625 | 19624 | 0 |
| Sampling | 30.82 | 30.82 | 0.00 |
| 2019-11-04 | 2825 | 2824 | 0 |
| 2019-11-05 | 2835 | 2835 | 0 |
| 2019-11-06 | 2699 | 2699 | 0 |
| 2019-11-07 | 2743 | 2743 | 0 |
| 2019-11-08 | 2839 | 2839 | 0 |
| 2019-11-09 | 2842 | 2842 | 0 |
| 2019-11-10 | 2842 | 2842 | 0 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.position.speed``
``entity.vehicle.timestamp``
``entity.vehicle.trip.tripId``
``entity.vehicle.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
``entity.id``
``entity.tripUpdate.stopTimeUpdate.arrival.delay``
``entity.tripUpdate.stopTimeUpdate.arrival.uncertainty``
``entity.tripUpdate.stopTimeUpdate.departure.delay``
``entity.tripUpdate.stopTimeUpdate.departure.uncertainty``
``entity.tripUpdate.stopTimeUpdate.stopId``
``entity.tripUpdate.stopTimeUpdate.stopSequence``
``entity.tripUpdate.timestamp``
``entity.tripUpdate.trip.tripId``
``entity.tripUpdate.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/HART-GTFSRT-2019-45.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/HART-GTFSRT-2019-45.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/HART-GTFSRT-2019-45.png)
