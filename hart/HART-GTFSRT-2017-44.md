### HART-GTFSRT-2017-44
#### Metadata
HART-GTFSRT-2017-44.json.zip: 33.77 MB
HART-GTFSRT-2017-44.pb.zip: 29.76 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 10970 | 10971 | 0 |
| Sampling | 55.46 | 55.46 | 0.00 |
| 2017-10-30 | 1758 | 1759 | 0 |
| 2017-10-31 | 1889 | 1889 | 0 |
| 2017-11-01 | 1903 | 1903 | 0 |
| 2017-11-02 | 353 | 353 | 0 |
| 2017-11-03 | 1902 | 1902 | 0 |
| 2017-11-04 | 1903 | 1903 | 0 |
| 2017-11-05 | 1144 | 1144 | 0 |
| 2017-11-06 | 118 | 118 | 0 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.position.speed``
``entity.vehicle.trip.tripId``
``entity.vehicle.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
``entity.id``
``entity.tripUpdate.stopTimeUpdate.arrival.delay``
``entity.tripUpdate.stopTimeUpdate.arrival.uncertainty``
``entity.tripUpdate.stopTimeUpdate.departure.delay``
``entity.tripUpdate.stopTimeUpdate.departure.uncertainty``
``entity.tripUpdate.stopTimeUpdate.stopId``
``entity.tripUpdate.stopTimeUpdate.stopSequence``
``entity.tripUpdate.trip.tripId``
``entity.tripUpdate.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/HART-GTFSRT-2017-44.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/HART-GTFSRT-2017-44.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/HART-GTFSRT-2017-44.png)
