### HART-GTFSRT-2019-29
#### Metadata
HART-GTFSRT-2019-29.json.zip: 68.60 MB
HART-GTFSRT-2019-29.pb.zip: 60.62 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 19852 | 19852 | 0 |
| Sampling | 30.46 | 30.46 | 0.00 |
| 2019-07-15 | 2828 | 2828 | 0 |
| 2019-07-16 | 2841 | 2841 | 0 |
| 2019-07-17 | 2836 | 2836 | 0 |
| 2019-07-18 | 2838 | 2838 | 0 |
| 2019-07-19 | 2837 | 2837 | 0 |
| 2019-07-20 | 2833 | 2833 | 0 |
| 2019-07-21 | 2839 | 2839 | 0 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.position.speed``
``entity.vehicle.trip.tripId``
``entity.vehicle.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
``entity.id``
``entity.tripUpdate.stopTimeUpdate.arrival.delay``
``entity.tripUpdate.stopTimeUpdate.arrival.uncertainty``
``entity.tripUpdate.stopTimeUpdate.departure.delay``
``entity.tripUpdate.stopTimeUpdate.departure.uncertainty``
``entity.tripUpdate.stopTimeUpdate.stopId``
``entity.tripUpdate.stopTimeUpdate.stopSequence``
``entity.tripUpdate.trip.tripId``
``entity.tripUpdate.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/HART-GTFSRT-2019-29.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/HART-GTFSRT-2019-29.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/HART-GTFSRT-2019-29.png)
