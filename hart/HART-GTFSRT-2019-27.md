### HART-GTFSRT-2019-27
#### Metadata
HART-GTFSRT-2019-27.json.zip: 65.11 MB
HART-GTFSRT-2019-27.pb.zip: 57.26 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 19856 | 19856 | 0 |
| Sampling | 30.46 | 30.46 | 0.00 |
| 2019-07-01 | 2836 | 2836 | 0 |
| 2019-07-02 | 2832 | 2832 | 0 |
| 2019-07-03 | 2836 | 2836 | 0 |
| 2019-07-04 | 2843 | 2843 | 0 |
| 2019-07-05 | 2840 | 2840 | 0 |
| 2019-07-06 | 2841 | 2841 | 0 |
| 2019-07-07 | 2828 | 2828 | 0 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.position.speed``
``entity.vehicle.trip.tripId``
``entity.vehicle.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
``entity.id``
``entity.tripUpdate.stopTimeUpdate.arrival.delay``
``entity.tripUpdate.stopTimeUpdate.arrival.uncertainty``
``entity.tripUpdate.stopTimeUpdate.departure.delay``
``entity.tripUpdate.stopTimeUpdate.departure.uncertainty``
``entity.tripUpdate.stopTimeUpdate.stopId``
``entity.tripUpdate.stopTimeUpdate.stopSequence``
``entity.tripUpdate.trip.tripId``
``entity.tripUpdate.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/HART-GTFSRT-2019-27.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/HART-GTFSRT-2019-27.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/HART-GTFSRT-2019-27.png)
