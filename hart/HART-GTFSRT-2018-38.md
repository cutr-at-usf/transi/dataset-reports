### HART-GTFSRT-2018-38
#### Metadata
HART-GTFSRT-2018-38.json.zip: 78.93 MB
HART-GTFSRT-2018-38.pb.zip: 70.69 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 19857 | 19857 | 0 |
| Sampling | 30.46 | 30.46 | 0.00 |
| 2018-09-17 | 2837 | 2837 | 0 |
| 2018-09-18 | 2838 | 2838 | 0 |
| 2018-09-19 | 2836 | 2836 | 0 |
| 2018-09-20 | 2834 | 2834 | 0 |
| 2018-09-21 | 2836 | 2836 | 0 |
| 2018-09-22 | 2838 | 2838 | 0 |
| 2018-09-23 | 2838 | 2838 | 0 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.position.speed``
``entity.vehicle.trip.tripId``
``entity.vehicle.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
``entity.id``
``entity.tripUpdate.stopTimeUpdate.arrival.delay``
``entity.tripUpdate.stopTimeUpdate.arrival.uncertainty``
``entity.tripUpdate.stopTimeUpdate.departure.delay``
``entity.tripUpdate.stopTimeUpdate.departure.uncertainty``
``entity.tripUpdate.stopTimeUpdate.stopId``
``entity.tripUpdate.stopTimeUpdate.stopSequence``
``entity.tripUpdate.trip.tripId``
``entity.tripUpdate.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/HART-GTFSRT-2018-38.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/HART-GTFSRT-2018-38.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/HART-GTFSRT-2018-38.png)
