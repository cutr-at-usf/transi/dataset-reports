### HART-GTFSRT-2018-41
#### Metadata
HART-GTFSRT-2018-41.json.zip: 66.63 MB
HART-GTFSRT-2018-41.pb.zip: 59.02 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 19858 | 19858 | 0 |
| Sampling | 30.46 | 30.46 | 0.00 |
| 2018-10-08 | 2833 | 2833 | 0 |
| 2018-10-09 | 2835 | 2835 | 0 |
| 2018-10-10 | 2836 | 2836 | 0 |
| 2018-10-11 | 2838 | 2838 | 0 |
| 2018-10-12 | 2836 | 2836 | 0 |
| 2018-10-13 | 2840 | 2840 | 0 |
| 2018-10-14 | 2840 | 2840 | 0 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.position.speed``
``entity.vehicle.trip.tripId``
``entity.vehicle.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
``entity.id``
``entity.tripUpdate.stopTimeUpdate.arrival.delay``
``entity.tripUpdate.stopTimeUpdate.arrival.uncertainty``
``entity.tripUpdate.stopTimeUpdate.departure.delay``
``entity.tripUpdate.stopTimeUpdate.departure.uncertainty``
``entity.tripUpdate.stopTimeUpdate.stopId``
``entity.tripUpdate.stopTimeUpdate.stopSequence``
``entity.tripUpdate.trip.tripId``
``entity.tripUpdate.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/HART-GTFSRT-2018-41.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/HART-GTFSRT-2018-41.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/HART-GTFSRT-2018-41.png)
