### HART-GTFSRT-2018-15
#### Metadata
HART-GTFSRT-2018-15.json.zip: 64.01 MB
HART-GTFSRT-2018-15.pb.zip: 56.64 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 19351 | 19351 | 0 |
| Sampling | 31.25 | 31.25 | 0.00 |
| 2018-04-09 | 2840 | 2840 | 0 |
| 2018-04-10 | 2823 | 2823 | 0 |
| 2018-04-11 | 2833 | 2833 | 0 |
| 2018-04-12 | 2340 | 2340 | 0 |
| 2018-04-13 | 2830 | 2830 | 0 |
| 2018-04-14 | 2840 | 2840 | 0 |
| 2018-04-15 | 2845 | 2845 | 0 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.position.speed``
``entity.vehicle.trip.tripId``
``entity.vehicle.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
``entity.id``
``entity.tripUpdate.stopTimeUpdate.arrival.delay``
``entity.tripUpdate.stopTimeUpdate.arrival.uncertainty``
``entity.tripUpdate.stopTimeUpdate.departure.delay``
``entity.tripUpdate.stopTimeUpdate.departure.uncertainty``
``entity.tripUpdate.stopTimeUpdate.stopId``
``entity.tripUpdate.stopTimeUpdate.stopSequence``
``entity.tripUpdate.trip.tripId``
``entity.tripUpdate.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/HART-GTFSRT-2018-15.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/HART-GTFSRT-2018-15.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/HART-GTFSRT-2018-15.png)
