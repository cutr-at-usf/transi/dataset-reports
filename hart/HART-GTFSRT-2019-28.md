### HART-GTFSRT-2019-28
#### Metadata
HART-GTFSRT-2019-28.json.zip: 68.75 MB
HART-GTFSRT-2019-28.pb.zip: 60.73 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 19835 | 19833 | 0 |
| Sampling | 30.49 | 30.49 | 0.00 |
| 2019-07-08 | 2832 | 2832 | 0 |
| 2019-07-09 | 2823 | 2823 | 0 |
| 2019-07-10 | 2835 | 2835 | 0 |
| 2019-07-11 | 2836 | 2835 | 0 |
| 2019-07-12 | 2840 | 2840 | 0 |
| 2019-07-13 | 2837 | 2837 | 0 |
| 2019-07-14 | 2832 | 2831 | 0 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.position.speed``
``entity.vehicle.trip.tripId``
``entity.vehicle.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
``entity.id``
``entity.tripUpdate.stopTimeUpdate.arrival.delay``
``entity.tripUpdate.stopTimeUpdate.arrival.uncertainty``
``entity.tripUpdate.stopTimeUpdate.departure.delay``
``entity.tripUpdate.stopTimeUpdate.departure.uncertainty``
``entity.tripUpdate.stopTimeUpdate.stopId``
``entity.tripUpdate.stopTimeUpdate.stopSequence``
``entity.tripUpdate.trip.tripId``
``entity.tripUpdate.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/HART-GTFSRT-2019-28.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/HART-GTFSRT-2019-28.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/HART-GTFSRT-2019-28.png)
