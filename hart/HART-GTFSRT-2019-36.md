### HART-GTFSRT-2019-36
#### Metadata
HART-GTFSRT-2019-36.json.zip: 62.26 MB
HART-GTFSRT-2019-36.pb.zip: 54.82 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 18948 | 18944 | 0 |
| Sampling | 30.81 | 30.82 | 0.00 |
| 2019-09-02 | 2840 | 2840 | 0 |
| 2019-09-03 | 2767 | 2767 | 0 |
| 2019-09-04 | 2699 | 2695 | 0 |
| 2019-09-05 | 2836 | 2836 | 0 |
| 2019-09-06 | 2835 | 2835 | 0 |
| 2019-09-07 | 2830 | 2830 | 0 |
| 2019-09-08 | 2141 | 2141 | 0 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.position.speed``
``entity.vehicle.trip.tripId``
``entity.vehicle.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
``entity.id``
``entity.tripUpdate.stopTimeUpdate.arrival.delay``
``entity.tripUpdate.stopTimeUpdate.arrival.uncertainty``
``entity.tripUpdate.stopTimeUpdate.departure.delay``
``entity.tripUpdate.stopTimeUpdate.departure.uncertainty``
``entity.tripUpdate.stopTimeUpdate.stopId``
``entity.tripUpdate.stopTimeUpdate.stopSequence``
``entity.tripUpdate.trip.tripId``
``entity.tripUpdate.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/HART-GTFSRT-2019-36.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/HART-GTFSRT-2019-36.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/HART-GTFSRT-2019-36.png)
