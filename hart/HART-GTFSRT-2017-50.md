### HART-GTFSRT-2017-50
#### Metadata
HART-GTFSRT-2017-50.json.zip: 63.73 MB
HART-GTFSRT-2017-50.pb.zip: 56.34 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 19895 | 19895 | 0 |
| Sampling | 30.40 | 30.40 | 0.00 |
| 2017-12-11 | 2843 | 2842 | 0 |
| 2017-12-12 | 2841 | 2843 | 0 |
| 2017-12-13 | 2837 | 2837 | 0 |
| 2017-12-14 | 2841 | 2841 | 0 |
| 2017-12-15 | 2843 | 2842 | 0 |
| 2017-12-16 | 2845 | 2845 | 0 |
| 2017-12-17 | 2845 | 2845 | 0 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.position.speed``
``entity.vehicle.trip.tripId``
``entity.vehicle.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
``entity.id``
``entity.tripUpdate.stopTimeUpdate.arrival.delay``
``entity.tripUpdate.stopTimeUpdate.arrival.uncertainty``
``entity.tripUpdate.stopTimeUpdate.departure.delay``
``entity.tripUpdate.stopTimeUpdate.departure.uncertainty``
``entity.tripUpdate.stopTimeUpdate.stopId``
``entity.tripUpdate.stopTimeUpdate.stopSequence``
``entity.tripUpdate.trip.tripId``
``entity.tripUpdate.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/HART-GTFSRT-2017-50.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/HART-GTFSRT-2017-50.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/HART-GTFSRT-2017-50.png)
