### HART-GTFSRT-2018-08
#### Metadata
HART-GTFSRT-2018-08.json.zip: 60.87 MB
HART-GTFSRT-2018-08.pb.zip: 53.78 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 18928 | 18926 | 0 |
| Sampling | 31.95 | 31.96 | 0.00 |
| 2018-02-19 | 2801 | 2801 | 0 |
| 2018-02-20 | 2670 | 2670 | 0 |
| 2018-02-21 | 2754 | 2753 | 0 |
| 2018-02-22 | 2658 | 2658 | 0 |
| 2018-02-23 | 2586 | 2586 | 0 |
| 2018-02-24 | 2613 | 2613 | 0 |
| 2018-02-25 | 2846 | 2845 | 0 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.position.speed``
``entity.vehicle.trip.tripId``
``entity.vehicle.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
``entity.id``
``entity.tripUpdate.stopTimeUpdate.arrival.delay``
``entity.tripUpdate.stopTimeUpdate.arrival.uncertainty``
``entity.tripUpdate.stopTimeUpdate.departure.delay``
``entity.tripUpdate.stopTimeUpdate.departure.uncertainty``
``entity.tripUpdate.stopTimeUpdate.stopId``
``entity.tripUpdate.stopTimeUpdate.stopSequence``
``entity.tripUpdate.trip.tripId``
``entity.tripUpdate.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/HART-GTFSRT-2018-08.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/HART-GTFSRT-2018-08.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/HART-GTFSRT-2018-08.png)
