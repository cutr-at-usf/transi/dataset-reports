### HART-GTFSRT-2019-30
#### Metadata
HART-GTFSRT-2019-30.json.zip: 68.38 MB
HART-GTFSRT-2019-30.pb.zip: 60.44 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 19700 | 19698 | 0 |
| Sampling | 30.70 | 30.70 | 0.00 |
| 2019-07-22 | 2837 | 2837 | 0 |
| 2019-07-23 | 2836 | 2835 | 0 |
| 2019-07-24 | 2708 | 2708 | 0 |
| 2019-07-25 | 2804 | 2804 | 0 |
| 2019-07-26 | 2836 | 2836 | 0 |
| 2019-07-27 | 2840 | 2840 | 0 |
| 2019-07-28 | 2839 | 2838 | 0 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.position.speed``
``entity.vehicle.trip.tripId``
``entity.vehicle.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
``entity.id``
``entity.tripUpdate.stopTimeUpdate.arrival.delay``
``entity.tripUpdate.stopTimeUpdate.arrival.uncertainty``
``entity.tripUpdate.stopTimeUpdate.departure.delay``
``entity.tripUpdate.stopTimeUpdate.departure.uncertainty``
``entity.tripUpdate.stopTimeUpdate.stopId``
``entity.tripUpdate.stopTimeUpdate.stopSequence``
``entity.tripUpdate.trip.tripId``
``entity.tripUpdate.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/HART-GTFSRT-2019-30.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/HART-GTFSRT-2019-30.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/HART-GTFSRT-2019-30.png)
