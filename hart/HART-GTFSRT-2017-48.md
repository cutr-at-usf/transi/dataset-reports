### HART-GTFSRT-2017-48
#### Metadata
HART-GTFSRT-2017-48.json.zip: 63.61 MB
HART-GTFSRT-2017-48.pb.zip: 56.22 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 19816 | 19816 | 0 |
| Sampling | 30.52 | 30.52 | 0.00 |
| 2017-11-27 | 2839 | 2840 | 0 |
| 2017-11-28 | 2839 | 2839 | 0 |
| 2017-11-29 | 2841 | 2841 | 0 |
| 2017-11-30 | 2836 | 2836 | 0 |
| 2017-12-01 | 2826 | 2825 | 0 |
| 2017-12-02 | 2830 | 2830 | 0 |
| 2017-12-03 | 2805 | 2805 | 0 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.position.speed``
``entity.vehicle.trip.tripId``
``entity.vehicle.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
``entity.id``
``entity.tripUpdate.stopTimeUpdate.arrival.delay``
``entity.tripUpdate.stopTimeUpdate.arrival.uncertainty``
``entity.tripUpdate.stopTimeUpdate.departure.delay``
``entity.tripUpdate.stopTimeUpdate.departure.uncertainty``
``entity.tripUpdate.stopTimeUpdate.stopId``
``entity.tripUpdate.stopTimeUpdate.stopSequence``
``entity.tripUpdate.trip.tripId``
``entity.tripUpdate.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/HART-GTFSRT-2017-48.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/HART-GTFSRT-2017-48.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/HART-GTFSRT-2017-48.png)
