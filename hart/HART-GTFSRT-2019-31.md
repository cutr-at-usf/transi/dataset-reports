### HART-GTFSRT-2019-31
#### Metadata
HART-GTFSRT-2019-31.json.zip: 68.73 MB
HART-GTFSRT-2019-31.pb.zip: 60.78 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 19823 | 19824 | 0 |
| Sampling | 30.51 | 30.51 | 0.00 |
| 2019-07-29 | 2837 | 2837 | 0 |
| 2019-07-30 | 2835 | 2835 | 0 |
| 2019-07-31 | 2836 | 2836 | 0 |
| 2019-08-01 | 2835 | 2836 | 0 |
| 2019-08-02 | 2835 | 2835 | 0 |
| 2019-08-03 | 2839 | 2839 | 0 |
| 2019-08-04 | 2806 | 2806 | 0 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.position.speed``
``entity.vehicle.trip.tripId``
``entity.vehicle.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
``entity.id``
``entity.tripUpdate.stopTimeUpdate.arrival.delay``
``entity.tripUpdate.stopTimeUpdate.arrival.uncertainty``
``entity.tripUpdate.stopTimeUpdate.departure.delay``
``entity.tripUpdate.stopTimeUpdate.departure.uncertainty``
``entity.tripUpdate.stopTimeUpdate.stopId``
``entity.tripUpdate.stopTimeUpdate.stopSequence``
``entity.tripUpdate.trip.tripId``
``entity.tripUpdate.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/HART-GTFSRT-2019-31.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/HART-GTFSRT-2019-31.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/HART-GTFSRT-2019-31.png)
