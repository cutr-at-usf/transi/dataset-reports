### HART-GTFSRT-2017-45
#### Metadata
HART-GTFSRT-2017-45.json.zip: 62.82 MB
HART-GTFSRT-2017-45.pb.zip: 55.48 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 19741 | 19742 | 0 |
| Sampling | 30.64 | 30.64 | 0.00 |
| 2017-11-06 | 2832 | 2832 | 0 |
| 2017-11-07 | 2825 | 2825 | 0 |
| 2017-11-08 | 2750 | 2750 | 0 |
| 2017-11-09 | 2834 | 2834 | 0 |
| 2017-11-10 | 2833 | 2834 | 0 |
| 2017-11-11 | 2830 | 2830 | 0 |
| 2017-11-12 | 2837 | 2837 | 0 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.position.speed``
``entity.vehicle.trip.tripId``
``entity.vehicle.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
``entity.id``
``entity.tripUpdate.stopTimeUpdate.arrival.delay``
``entity.tripUpdate.stopTimeUpdate.arrival.uncertainty``
``entity.tripUpdate.stopTimeUpdate.departure.delay``
``entity.tripUpdate.stopTimeUpdate.departure.uncertainty``
``entity.tripUpdate.stopTimeUpdate.stopId``
``entity.tripUpdate.stopTimeUpdate.stopSequence``
``entity.tripUpdate.trip.tripId``
``entity.tripUpdate.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/HART-GTFSRT-2017-45.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/HART-GTFSRT-2017-45.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/HART-GTFSRT-2017-45.png)
