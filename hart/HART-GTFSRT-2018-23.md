### HART-GTFSRT-2018-23
#### Metadata
HART-GTFSRT-2018-23.json.zip: 65.31 MB
HART-GTFSRT-2018-23.pb.zip: 57.76 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 19729 | 19729 | 0 |
| Sampling | 30.66 | 30.66 | 0.00 |
| 2018-06-04 | 2777 | 2777 | 0 |
| 2018-06-05 | 2839 | 2839 | 0 |
| 2018-06-06 | 2740 | 2740 | 0 |
| 2018-06-07 | 2842 | 2842 | 0 |
| 2018-06-08 | 2843 | 2843 | 0 |
| 2018-06-09 | 2844 | 2844 | 0 |
| 2018-06-10 | 2844 | 2844 | 0 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.position.speed``
``entity.vehicle.trip.tripId``
``entity.vehicle.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
``entity.id``
``entity.tripUpdate.stopTimeUpdate.arrival.delay``
``entity.tripUpdate.stopTimeUpdate.arrival.uncertainty``
``entity.tripUpdate.stopTimeUpdate.departure.delay``
``entity.tripUpdate.stopTimeUpdate.departure.uncertainty``
``entity.tripUpdate.stopTimeUpdate.stopId``
``entity.tripUpdate.stopTimeUpdate.stopSequence``
``entity.tripUpdate.trip.tripId``
``entity.tripUpdate.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/HART-GTFSRT-2018-23.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/HART-GTFSRT-2018-23.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/HART-GTFSRT-2018-23.png)
