### HART-GTFSRT-2018-42
#### Metadata
HART-GTFSRT-2018-42.json.zip: 68.15 MB
HART-GTFSRT-2018-42.pb.zip: 60.45 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 19839 | 19840 | 0 |
| Sampling | 30.49 | 30.48 | 0.00 |
| 2018-10-15 | 2831 | 2832 | 0 |
| 2018-10-16 | 2831 | 2831 | 0 |
| 2018-10-17 | 2833 | 2833 | 0 |
| 2018-10-18 | 2835 | 2835 | 0 |
| 2018-10-19 | 2836 | 2836 | 0 |
| 2018-10-20 | 2835 | 2835 | 0 |
| 2018-10-21 | 2838 | 2838 | 0 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.position.speed``
``entity.vehicle.trip.tripId``
``entity.vehicle.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
``entity.id``
``entity.tripUpdate.stopTimeUpdate.arrival.delay``
``entity.tripUpdate.stopTimeUpdate.arrival.uncertainty``
``entity.tripUpdate.stopTimeUpdate.departure.delay``
``entity.tripUpdate.stopTimeUpdate.departure.uncertainty``
``entity.tripUpdate.stopTimeUpdate.stopId``
``entity.tripUpdate.stopTimeUpdate.stopSequence``
``entity.tripUpdate.trip.tripId``
``entity.tripUpdate.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/HART-GTFSRT-2018-42.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/HART-GTFSRT-2018-42.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/HART-GTFSRT-2018-42.png)
