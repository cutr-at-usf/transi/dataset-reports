### HART-GTFSRT-2017-49
#### Metadata
HART-GTFSRT-2017-49.json.zip: 63.59 MB
HART-GTFSRT-2017-49.pb.zip: 56.24 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 19817 | 19817 | 0 |
| Sampling | 30.52 | 30.52 | 0.00 |
| 2017-12-04 | 2783 | 2783 | 0 |
| 2017-12-05 | 2831 | 2831 | 0 |
| 2017-12-06 | 2838 | 2838 | 0 |
| 2017-12-07 | 2838 | 2839 | 0 |
| 2017-12-08 | 2842 | 2842 | 0 |
| 2017-12-09 | 2841 | 2840 | 0 |
| 2017-12-10 | 2844 | 2844 | 0 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.position.speed``
``entity.vehicle.trip.tripId``
``entity.vehicle.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
``entity.id``
``entity.tripUpdate.stopTimeUpdate.arrival.delay``
``entity.tripUpdate.stopTimeUpdate.arrival.uncertainty``
``entity.tripUpdate.stopTimeUpdate.departure.delay``
``entity.tripUpdate.stopTimeUpdate.departure.uncertainty``
``entity.tripUpdate.stopTimeUpdate.stopId``
``entity.tripUpdate.stopTimeUpdate.stopSequence``
``entity.tripUpdate.trip.tripId``
``entity.tripUpdate.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/HART-GTFSRT-2017-49.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/HART-GTFSRT-2017-49.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/HART-GTFSRT-2017-49.png)
