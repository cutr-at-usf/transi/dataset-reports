### HART-GTFSRT-2018-50
#### Metadata
HART-GTFSRT-2018-50.json.zip: 69.01 MB
HART-GTFSRT-2018-50.pb.zip: 60.90 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 19869 | 19869 | 0 |
| Sampling | 30.44 | 30.44 | 0.00 |
| 2018-12-10 | 2839 | 2839 | 0 |
| 2018-12-11 | 2836 | 2836 | 0 |
| 2018-12-12 | 2840 | 2840 | 0 |
| 2018-12-13 | 2840 | 2840 | 0 |
| 2018-12-14 | 2838 | 2838 | 0 |
| 2018-12-15 | 2838 | 2838 | 0 |
| 2018-12-16 | 2838 | 2838 | 0 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.position.speed``
``entity.vehicle.trip.tripId``
``entity.vehicle.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
``entity.id``
``entity.tripUpdate.stopTimeUpdate.arrival.delay``
``entity.tripUpdate.stopTimeUpdate.arrival.uncertainty``
``entity.tripUpdate.stopTimeUpdate.departure.delay``
``entity.tripUpdate.stopTimeUpdate.departure.uncertainty``
``entity.tripUpdate.stopTimeUpdate.stopId``
``entity.tripUpdate.stopTimeUpdate.stopSequence``
``entity.tripUpdate.trip.tripId``
``entity.tripUpdate.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/HART-GTFSRT-2018-50.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/HART-GTFSRT-2018-50.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/HART-GTFSRT-2018-50.png)
