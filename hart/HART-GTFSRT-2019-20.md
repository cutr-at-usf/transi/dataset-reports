### HART-GTFSRT-2019-20
#### Metadata
HART-GTFSRT-2019-20.json.zip: 69.06 MB
HART-GTFSRT-2019-20.pb.zip: 60.97 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 19864 | 19864 | 0 |
| Sampling | 30.45 | 30.45 | 0.00 |
| 2019-05-13 | 2840 | 2840 | 0 |
| 2019-05-14 | 2837 | 2837 | 0 |
| 2019-05-15 | 2837 | 2837 | 0 |
| 2019-05-16 | 2837 | 2837 | 0 |
| 2019-05-17 | 2837 | 2837 | 0 |
| 2019-05-18 | 2838 | 2838 | 0 |
| 2019-05-19 | 2838 | 2838 | 0 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.position.speed``
``entity.vehicle.trip.tripId``
``entity.vehicle.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
``entity.id``
``entity.tripUpdate.stopTimeUpdate.arrival.delay``
``entity.tripUpdate.stopTimeUpdate.arrival.uncertainty``
``entity.tripUpdate.stopTimeUpdate.departure.delay``
``entity.tripUpdate.stopTimeUpdate.departure.uncertainty``
``entity.tripUpdate.stopTimeUpdate.stopId``
``entity.tripUpdate.stopTimeUpdate.stopSequence``
``entity.tripUpdate.trip.tripId``
``entity.tripUpdate.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/HART-GTFSRT-2019-20.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/HART-GTFSRT-2019-20.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/HART-GTFSRT-2019-20.png)
