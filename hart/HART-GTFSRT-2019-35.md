### HART-GTFSRT-2019-35
#### Metadata
HART-GTFSRT-2019-35.json.zip: 66.92 MB
HART-GTFSRT-2019-35.pb.zip: 59.20 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 19279 | 19279 | 0 |
| Sampling | 31.37 | 31.37 | 0.00 |
| 2019-08-26 | 2834 | 2834 | 0 |
| 2019-08-27 | 2829 | 2829 | 0 |
| 2019-08-28 | 2728 | 2728 | 0 |
| 2019-08-29 | 2836 | 2836 | 0 |
| 2019-08-30 | 2835 | 2835 | 0 |
| 2019-08-31 | 2458 | 2458 | 0 |
| 2019-09-01 | 2759 | 2759 | 0 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.position.speed``
``entity.vehicle.trip.tripId``
``entity.vehicle.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
``entity.id``
``entity.tripUpdate.stopTimeUpdate.arrival.delay``
``entity.tripUpdate.stopTimeUpdate.arrival.uncertainty``
``entity.tripUpdate.stopTimeUpdate.departure.delay``
``entity.tripUpdate.stopTimeUpdate.departure.uncertainty``
``entity.tripUpdate.stopTimeUpdate.stopId``
``entity.tripUpdate.stopTimeUpdate.stopSequence``
``entity.tripUpdate.trip.tripId``
``entity.tripUpdate.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/HART-GTFSRT-2019-35.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/HART-GTFSRT-2019-35.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/HART-GTFSRT-2019-35.png)
