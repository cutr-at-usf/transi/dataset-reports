### HART-GTFSRT-2018-14
#### Metadata
HART-GTFSRT-2018-14.json.zip: 41.56 MB
HART-GTFSRT-2018-14.pb.zip: 36.66 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 12886 | 12887 | 0 |
| Sampling | 30.46 | 30.46 | 0.00 |
| 2018-04-02 | 0 | 0 | 0 |
| 2018-04-03 | 0 | 0 | 0 |
| 2018-04-04 | 1539 | 1539 | 0 |
| 2018-04-05 | 2821 | 2821 | 0 |
| 2018-04-06 | 2841 | 2841 | 0 |
| 2018-04-07 | 2844 | 2844 | 0 |
| 2018-04-08 | 2841 | 2842 | 0 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.position.speed``
``entity.vehicle.trip.tripId``
``entity.vehicle.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
``entity.id``
``entity.tripUpdate.stopTimeUpdate.arrival.delay``
``entity.tripUpdate.stopTimeUpdate.arrival.uncertainty``
``entity.tripUpdate.stopTimeUpdate.departure.delay``
``entity.tripUpdate.stopTimeUpdate.departure.uncertainty``
``entity.tripUpdate.stopTimeUpdate.stopId``
``entity.tripUpdate.stopTimeUpdate.stopSequence``
``entity.tripUpdate.trip.tripId``
``entity.tripUpdate.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/HART-GTFSRT-2018-14.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/HART-GTFSRT-2018-14.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/HART-GTFSRT-2018-14.png)
