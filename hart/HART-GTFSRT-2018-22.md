### HART-GTFSRT-2018-22
#### Metadata
HART-GTFSRT-2018-22.json.zip: 58.95 MB
HART-GTFSRT-2018-22.pb.zip: 51.86 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 19040 | 19040 | 0 |
| Sampling | 31.76 | 31.76 | 0.00 |
| 2018-05-28 | 2581 | 2581 | 0 |
| 2018-05-29 | 2712 | 2711 | 0 |
| 2018-05-30 | 2740 | 2740 | 0 |
| 2018-05-31 | 2517 | 2517 | 0 |
| 2018-06-01 | 2833 | 2834 | 0 |
| 2018-06-02 | 2838 | 2838 | 0 |
| 2018-06-03 | 2819 | 2819 | 0 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.position.speed``
``entity.vehicle.trip.tripId``
``entity.vehicle.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
``entity.id``
``entity.tripUpdate.stopTimeUpdate.arrival.delay``
``entity.tripUpdate.stopTimeUpdate.arrival.uncertainty``
``entity.tripUpdate.stopTimeUpdate.departure.delay``
``entity.tripUpdate.stopTimeUpdate.departure.uncertainty``
``entity.tripUpdate.stopTimeUpdate.stopId``
``entity.tripUpdate.stopTimeUpdate.stopSequence``
``entity.tripUpdate.trip.tripId``
``entity.tripUpdate.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/HART-GTFSRT-2018-22.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/HART-GTFSRT-2018-22.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/HART-GTFSRT-2018-22.png)
