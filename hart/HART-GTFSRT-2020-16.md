### HART-GTFSRT-2020-16
#### Metadata
HART-GTFSRT-2020-16.json.zip: 49.93 MB
HART-GTFSRT-2020-16.pb.zip: 43.11 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 19814 | 19809 | 0 |
| Sampling | 30.52 | 30.53 | 0.00 |
| 2020-04-13 | 2840 | 2840 | 0 |
| 2020-04-14 | 2843 | 2842 | 0 |
| 2020-04-15 | 2771 | 2768 | 0 |
| 2020-04-16 | 2835 | 2835 | 0 |
| 2020-04-17 | 2841 | 2841 | 0 |
| 2020-04-18 | 2841 | 2841 | 0 |
| 2020-04-19 | 2843 | 2842 | 0 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.position.speed``
``entity.vehicle.timestamp``
``entity.vehicle.trip.tripId``
``entity.vehicle.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
``entity.id``
``entity.tripUpdate.stopTimeUpdate.arrival.delay``
``entity.tripUpdate.stopTimeUpdate.arrival.uncertainty``
``entity.tripUpdate.stopTimeUpdate.departure.delay``
``entity.tripUpdate.stopTimeUpdate.departure.uncertainty``
``entity.tripUpdate.stopTimeUpdate.stopId``
``entity.tripUpdate.stopTimeUpdate.stopSequence``
``entity.tripUpdate.timestamp``
``entity.tripUpdate.trip.tripId``
``entity.tripUpdate.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/HART-GTFSRT-2020-16.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/HART-GTFSRT-2020-16.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/HART-GTFSRT-2020-16.png)
