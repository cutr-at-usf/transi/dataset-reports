### HART-GTFSRT-2018-46
#### Metadata
HART-GTFSRT-2018-46.json.zip: 67.55 MB
HART-GTFSRT-2018-46.pb.zip: 59.84 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 19839 | 19838 | 0 |
| Sampling | 30.49 | 30.49 | 0.00 |
| 2018-11-12 | 2837 | 2837 | 0 |
| 2018-11-13 | 2834 | 2833 | 0 |
| 2018-11-14 | 2828 | 2828 | 0 |
| 2018-11-15 | 2830 | 2830 | 0 |
| 2018-11-16 | 2834 | 2834 | 0 |
| 2018-11-17 | 2837 | 2837 | 0 |
| 2018-11-18 | 2839 | 2839 | 0 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.position.speed``
``entity.vehicle.trip.tripId``
``entity.vehicle.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
``entity.id``
``entity.tripUpdate.stopTimeUpdate.arrival.delay``
``entity.tripUpdate.stopTimeUpdate.arrival.uncertainty``
``entity.tripUpdate.stopTimeUpdate.departure.delay``
``entity.tripUpdate.stopTimeUpdate.departure.uncertainty``
``entity.tripUpdate.stopTimeUpdate.stopId``
``entity.tripUpdate.stopTimeUpdate.stopSequence``
``entity.tripUpdate.trip.tripId``
``entity.tripUpdate.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/HART-GTFSRT-2018-46.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/HART-GTFSRT-2018-46.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/HART-GTFSRT-2018-46.png)
