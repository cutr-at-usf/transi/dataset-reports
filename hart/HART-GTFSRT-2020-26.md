### HART-GTFSRT-2020-26
#### Metadata
HART-GTFSRT-2020-26.json.zip: 50.44 MB
HART-GTFSRT-2020-26.pb.zip: 43.57 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 19922 | 19921 | 0 |
| Sampling | 30.36 | 30.36 | 0.00 |
| 2020-06-22 | 2846 | 2846 | 0 |
| 2020-06-23 | 2846 | 2846 | 0 |
| 2020-06-24 | 2846 | 2845 | 0 |
| 2020-06-25 | 2845 | 2845 | 0 |
| 2020-06-26 | 2848 | 2848 | 0 |
| 2020-06-27 | 2847 | 2847 | 0 |
| 2020-06-28 | 2844 | 2844 | 0 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.position.speed``
``entity.vehicle.timestamp``
``entity.vehicle.trip.tripId``
``entity.vehicle.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
``entity.id``
``entity.tripUpdate.stopTimeUpdate.arrival.delay``
``entity.tripUpdate.stopTimeUpdate.arrival.uncertainty``
``entity.tripUpdate.stopTimeUpdate.departure.delay``
``entity.tripUpdate.stopTimeUpdate.departure.uncertainty``
``entity.tripUpdate.stopTimeUpdate.stopId``
``entity.tripUpdate.stopTimeUpdate.stopSequence``
``entity.tripUpdate.timestamp``
``entity.tripUpdate.trip.tripId``
``entity.tripUpdate.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/HART-GTFSRT-2020-26.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/HART-GTFSRT-2020-26.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/HART-GTFSRT-2020-26.png)
