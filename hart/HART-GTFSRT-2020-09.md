### HART-GTFSRT-2020-09
#### Metadata
HART-GTFSRT-2020-09.json.zip: 67.58 MB
HART-GTFSRT-2020-09.pb.zip: 60.61 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 17608 | 17608 | 0 |
| Sampling | 34.35 | 34.35 | 0.00 |
| 2020-02-24 | 2838 | 2838 | 0 |
| 2020-02-25 | 2840 | 2841 | 0 |
| 2020-02-26 | 2084 | 2083 | 0 |
| 2020-02-27 | 1439 | 1439 | 0 |
| 2020-02-28 | 2732 | 2732 | 0 |
| 2020-02-29 | 2843 | 2843 | 0 |
| 2020-03-01 | 2832 | 2832 | 0 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.position.speed``
``entity.vehicle.timestamp``
``entity.vehicle.trip.tripId``
``entity.vehicle.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
``entity.id``
``entity.tripUpdate.stopTimeUpdate.arrival.delay``
``entity.tripUpdate.stopTimeUpdate.arrival.uncertainty``
``entity.tripUpdate.stopTimeUpdate.departure.delay``
``entity.tripUpdate.stopTimeUpdate.departure.uncertainty``
``entity.tripUpdate.stopTimeUpdate.stopId``
``entity.tripUpdate.stopTimeUpdate.stopSequence``
``entity.tripUpdate.timestamp``
``entity.tripUpdate.trip.tripId``
``entity.tripUpdate.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/HART-GTFSRT-2020-09.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/HART-GTFSRT-2020-09.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/HART-GTFSRT-2020-09.png)
