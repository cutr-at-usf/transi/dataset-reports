### HART-GTFSRT-2018-03
#### Metadata
HART-GTFSRT-2018-03.json.zip: 51.09 MB
HART-GTFSRT-2018-03.pb.zip: 44.95 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 16758 | 16756 | 0 |
| Sampling | 36.09 | 36.09 | 0.00 |
| 2018-01-15 | 1945 | 1944 | 0 |
| 2018-01-16 | 997 | 996 | 0 |
| 2018-01-17 | 2832 | 2832 | 0 |
| 2018-01-18 | 2837 | 2837 | 0 |
| 2018-01-19 | 2759 | 2759 | 0 |
| 2018-01-20 | 2645 | 2645 | 0 |
| 2018-01-21 | 2743 | 2743 | 0 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.position.speed``
``entity.vehicle.trip.tripId``
``entity.vehicle.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
``entity.id``
``entity.tripUpdate.stopTimeUpdate.arrival.delay``
``entity.tripUpdate.stopTimeUpdate.arrival.uncertainty``
``entity.tripUpdate.stopTimeUpdate.departure.delay``
``entity.tripUpdate.stopTimeUpdate.departure.uncertainty``
``entity.tripUpdate.stopTimeUpdate.stopId``
``entity.tripUpdate.stopTimeUpdate.stopSequence``
``entity.tripUpdate.trip.tripId``
``entity.tripUpdate.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/HART-GTFSRT-2018-03.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/HART-GTFSRT-2018-03.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/HART-GTFSRT-2018-03.png)
