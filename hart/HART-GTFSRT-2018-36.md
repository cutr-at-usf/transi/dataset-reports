### HART-GTFSRT-2018-36
#### Metadata
HART-GTFSRT-2018-36.json.zip: 64.04 MB
HART-GTFSRT-2018-36.pb.zip: 56.58 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 19626 | 19566 | 0 |
| Sampling | 30.82 | 30.91 | 0.00 |
| 2018-09-03 | 2836 | 2836 | 0 |
| 2018-09-04 | 2837 | 2836 | 0 |
| 2018-09-05 | 2733 | 2733 | 0 |
| 2018-09-06 | 2826 | 2803 | 0 |
| 2018-09-07 | 2723 | 2722 | 0 |
| 2018-09-08 | 2830 | 2795 | 0 |
| 2018-09-09 | 2841 | 2841 | 0 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.position.speed``
``entity.vehicle.trip.tripId``
``entity.vehicle.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
``entity.id``
``entity.tripUpdate.stopTimeUpdate.arrival.delay``
``entity.tripUpdate.stopTimeUpdate.arrival.uncertainty``
``entity.tripUpdate.stopTimeUpdate.departure.delay``
``entity.tripUpdate.stopTimeUpdate.departure.uncertainty``
``entity.tripUpdate.stopTimeUpdate.stopId``
``entity.tripUpdate.stopTimeUpdate.stopSequence``
``entity.tripUpdate.trip.tripId``
``entity.tripUpdate.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/HART-GTFSRT-2018-36.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/HART-GTFSRT-2018-36.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/HART-GTFSRT-2018-36.png)
