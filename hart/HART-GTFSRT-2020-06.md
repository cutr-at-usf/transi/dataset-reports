### HART-GTFSRT-2020-06
#### Metadata
HART-GTFSRT-2020-06.json.zip: 75.85 MB
HART-GTFSRT-2020-06.pb.zip: 68.00 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 19869 | 19868 | 0 |
| Sampling | 30.44 | 30.44 | 0.00 |
| 2020-02-03 | 2833 | 2832 | 0 |
| 2020-02-04 | 2840 | 2840 | 0 |
| 2020-02-05 | 2839 | 2839 | 0 |
| 2020-02-06 | 2838 | 2838 | 0 |
| 2020-02-07 | 2838 | 2838 | 0 |
| 2020-02-08 | 2841 | 2841 | 0 |
| 2020-02-09 | 2840 | 2840 | 0 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.position.speed``
``entity.vehicle.timestamp``
``entity.vehicle.trip.tripId``
``entity.vehicle.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
``entity.id``
``entity.tripUpdate.stopTimeUpdate.arrival.delay``
``entity.tripUpdate.stopTimeUpdate.arrival.uncertainty``
``entity.tripUpdate.stopTimeUpdate.departure.delay``
``entity.tripUpdate.stopTimeUpdate.departure.uncertainty``
``entity.tripUpdate.stopTimeUpdate.stopId``
``entity.tripUpdate.stopTimeUpdate.stopSequence``
``entity.tripUpdate.timestamp``
``entity.tripUpdate.trip.tripId``
``entity.tripUpdate.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/HART-GTFSRT-2020-06.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/HART-GTFSRT-2020-06.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/HART-GTFSRT-2020-06.png)
