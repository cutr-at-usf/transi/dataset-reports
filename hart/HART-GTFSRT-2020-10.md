### HART-GTFSRT-2020-10
#### Metadata
HART-GTFSRT-2020-10.json.zip: 71.80 MB
HART-GTFSRT-2020-10.pb.zip: 64.41 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 18817 | 18816 | 0 |
| Sampling | 31.95 | 31.95 | 0.00 |
| 2020-03-02 | 2834 | 2835 | 0 |
| 2020-03-03 | 2831 | 2831 | 0 |
| 2020-03-04 | 2779 | 2779 | 0 |
| 2020-03-05 | 2839 | 2837 | 0 |
| 2020-03-06 | 2838 | 2838 | 0 |
| 2020-03-07 | 2840 | 2840 | 0 |
| 2020-03-08 | 1856 | 1856 | 0 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.position.speed``
``entity.vehicle.timestamp``
``entity.vehicle.trip.tripId``
``entity.vehicle.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
``entity.id``
``entity.tripUpdate.stopTimeUpdate.arrival.delay``
``entity.tripUpdate.stopTimeUpdate.arrival.uncertainty``
``entity.tripUpdate.stopTimeUpdate.departure.delay``
``entity.tripUpdate.stopTimeUpdate.departure.uncertainty``
``entity.tripUpdate.stopTimeUpdate.stopId``
``entity.tripUpdate.stopTimeUpdate.stopSequence``
``entity.tripUpdate.timestamp``
``entity.tripUpdate.trip.tripId``
``entity.tripUpdate.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/HART-GTFSRT-2020-10.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/HART-GTFSRT-2020-10.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/HART-GTFSRT-2020-10.png)
