### HART-GTFSRT-2018-32
#### Metadata
HART-GTFSRT-2018-32.json.zip: 68.43 MB
HART-GTFSRT-2018-32.pb.zip: 60.72 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 19757 | 19754 | 0 |
| Sampling | 30.61 | 30.62 | 0.00 |
| 2018-08-06 | 2838 | 2838 | 0 |
| 2018-08-07 | 2832 | 2829 | 0 |
| 2018-08-08 | 2735 | 2735 | 0 |
| 2018-08-09 | 2834 | 2834 | 0 |
| 2018-08-10 | 2834 | 2834 | 0 |
| 2018-08-11 | 2842 | 2842 | 0 |
| 2018-08-12 | 2842 | 2842 | 0 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.position.speed``
``entity.vehicle.trip.tripId``
``entity.vehicle.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
``entity.id``
``entity.tripUpdate.stopTimeUpdate.arrival.delay``
``entity.tripUpdate.stopTimeUpdate.arrival.uncertainty``
``entity.tripUpdate.stopTimeUpdate.departure.delay``
``entity.tripUpdate.stopTimeUpdate.departure.uncertainty``
``entity.tripUpdate.stopTimeUpdate.stopId``
``entity.tripUpdate.stopTimeUpdate.stopSequence``
``entity.tripUpdate.trip.tripId``
``entity.tripUpdate.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/HART-GTFSRT-2018-32.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/HART-GTFSRT-2018-32.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/HART-GTFSRT-2018-32.png)
