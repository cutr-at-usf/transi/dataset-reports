### HART-GTFSRT-2019-14
#### Metadata
HART-GTFSRT-2019-14.json.zip: 68.67 MB
HART-GTFSRT-2019-14.pb.zip: 60.68 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 19702 | 19657 | 0 |
| Sampling | 30.70 | 30.77 | 0.00 |
| 2019-04-01 | 2835 | 2835 | 0 |
| 2019-04-02 | 2839 | 2839 | 0 |
| 2019-04-03 | 2733 | 2730 | 0 |
| 2019-04-04 | 2828 | 2813 | 0 |
| 2019-04-05 | 2829 | 2804 | 0 |
| 2019-04-06 | 2832 | 2830 | 0 |
| 2019-04-07 | 2806 | 2806 | 0 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.position.speed``
``entity.vehicle.trip.tripId``
``entity.vehicle.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
``entity.id``
``entity.tripUpdate.stopTimeUpdate.arrival.delay``
``entity.tripUpdate.stopTimeUpdate.arrival.uncertainty``
``entity.tripUpdate.stopTimeUpdate.departure.delay``
``entity.tripUpdate.stopTimeUpdate.departure.uncertainty``
``entity.tripUpdate.stopTimeUpdate.stopId``
``entity.tripUpdate.stopTimeUpdate.stopSequence``
``entity.tripUpdate.trip.tripId``
``entity.tripUpdate.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/HART-GTFSRT-2019-14.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/HART-GTFSRT-2019-14.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/HART-GTFSRT-2019-14.png)
