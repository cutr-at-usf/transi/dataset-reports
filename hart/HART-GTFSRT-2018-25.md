### HART-GTFSRT-2018-25
#### Metadata
HART-GTFSRT-2018-25.json.zip: 65.62 MB
HART-GTFSRT-2018-25.pb.zip: 58.07 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 19762 | 19762 | 0 |
| Sampling | 30.60 | 30.60 | 0.00 |
| 2018-06-18 | 2838 | 2838 | 0 |
| 2018-06-19 | 2840 | 2840 | 0 |
| 2018-06-20 | 2722 | 2723 | 0 |
| 2018-06-21 | 2839 | 2838 | 0 |
| 2018-06-22 | 2840 | 2840 | 0 |
| 2018-06-23 | 2841 | 2841 | 0 |
| 2018-06-24 | 2842 | 2842 | 0 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.position.speed``
``entity.vehicle.trip.tripId``
``entity.vehicle.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
``entity.id``
``entity.tripUpdate.stopTimeUpdate.arrival.delay``
``entity.tripUpdate.stopTimeUpdate.arrival.uncertainty``
``entity.tripUpdate.stopTimeUpdate.departure.delay``
``entity.tripUpdate.stopTimeUpdate.departure.uncertainty``
``entity.tripUpdate.stopTimeUpdate.stopId``
``entity.tripUpdate.stopTimeUpdate.stopSequence``
``entity.tripUpdate.trip.tripId``
``entity.tripUpdate.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/HART-GTFSRT-2018-25.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/HART-GTFSRT-2018-25.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/HART-GTFSRT-2018-25.png)
