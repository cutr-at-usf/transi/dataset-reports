### HART-GTFSRT-2020-04
#### Metadata
HART-GTFSRT-2020-04.json.zip: 75.37 MB
HART-GTFSRT-2020-04.pb.zip: 67.58 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 19817 | 19816 | 0 |
| Sampling | 30.52 | 30.52 | 0.00 |
| 2020-01-20 | 2838 | 2838 | 0 |
| 2020-01-21 | 2835 | 2835 | 0 |
| 2020-01-22 | 2791 | 2791 | 0 |
| 2020-01-23 | 2832 | 2833 | 0 |
| 2020-01-24 | 2838 | 2837 | 0 |
| 2020-01-25 | 2840 | 2840 | 0 |
| 2020-01-26 | 2843 | 2842 | 0 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.position.speed``
``entity.vehicle.timestamp``
``entity.vehicle.trip.tripId``
``entity.vehicle.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
``entity.id``
``entity.tripUpdate.stopTimeUpdate.arrival.delay``
``entity.tripUpdate.stopTimeUpdate.arrival.uncertainty``
``entity.tripUpdate.stopTimeUpdate.departure.delay``
``entity.tripUpdate.stopTimeUpdate.departure.uncertainty``
``entity.tripUpdate.stopTimeUpdate.stopId``
``entity.tripUpdate.stopTimeUpdate.stopSequence``
``entity.tripUpdate.timestamp``
``entity.tripUpdate.trip.tripId``
``entity.tripUpdate.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/HART-GTFSRT-2020-04.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/HART-GTFSRT-2020-04.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/HART-GTFSRT-2020-04.png)
