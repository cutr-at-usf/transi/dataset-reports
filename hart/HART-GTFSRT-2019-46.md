### HART-GTFSRT-2019-46
#### Metadata
HART-GTFSRT-2019-46.json.zip: 75.23 MB
HART-GTFSRT-2019-46.pb.zip: 67.49 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 19802 | 19793 | 0 |
| Sampling | 30.54 | 30.56 | 0.00 |
| 2019-11-11 | 2839 | 2839 | 0 |
| 2019-11-12 | 2836 | 2836 | 0 |
| 2019-11-13 | 2833 | 2833 | 0 |
| 2019-11-14 | 2836 | 2835 | 0 |
| 2019-11-15 | 2785 | 2778 | 0 |
| 2019-11-16 | 2840 | 2841 | 0 |
| 2019-11-17 | 2833 | 2831 | 0 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.position.speed``
``entity.vehicle.timestamp``
``entity.vehicle.trip.tripId``
``entity.vehicle.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
``entity.id``
``entity.tripUpdate.stopTimeUpdate.arrival.delay``
``entity.tripUpdate.stopTimeUpdate.arrival.uncertainty``
``entity.tripUpdate.stopTimeUpdate.departure.delay``
``entity.tripUpdate.stopTimeUpdate.departure.uncertainty``
``entity.tripUpdate.stopTimeUpdate.stopId``
``entity.tripUpdate.stopTimeUpdate.stopSequence``
``entity.tripUpdate.timestamp``
``entity.tripUpdate.trip.tripId``
``entity.tripUpdate.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/HART-GTFSRT-2019-46.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/HART-GTFSRT-2019-46.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/HART-GTFSRT-2019-46.png)
