### HART-GTFSRT-2019-22
#### Metadata
HART-GTFSRT-2019-22.json.zip: 63.03 MB
HART-GTFSRT-2019-22.pb.zip: 55.40 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 19667 | 19667 | 0 |
| Sampling | 30.75 | 30.75 | 0.00 |
| 2019-05-27 | 2840 | 2840 | 0 |
| 2019-05-28 | 2837 | 2837 | 0 |
| 2019-05-29 | 2657 | 2657 | 0 |
| 2019-05-30 | 2837 | 2837 | 0 |
| 2019-05-31 | 2836 | 2836 | 0 |
| 2019-06-01 | 2837 | 2837 | 0 |
| 2019-06-02 | 2823 | 2823 | 0 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.position.speed``
``entity.vehicle.trip.tripId``
``entity.vehicle.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
``entity.id``
``entity.tripUpdate.stopTimeUpdate.arrival.delay``
``entity.tripUpdate.stopTimeUpdate.arrival.uncertainty``
``entity.tripUpdate.stopTimeUpdate.departure.delay``
``entity.tripUpdate.stopTimeUpdate.departure.uncertainty``
``entity.tripUpdate.stopTimeUpdate.stopId``
``entity.tripUpdate.stopTimeUpdate.stopSequence``
``entity.tripUpdate.trip.tripId``
``entity.tripUpdate.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/HART-GTFSRT-2019-22.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/HART-GTFSRT-2019-22.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/HART-GTFSRT-2019-22.png)
