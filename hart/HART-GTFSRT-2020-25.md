### HART-GTFSRT-2020-25
#### Metadata
HART-GTFSRT-2020-25.json.zip: 50.15 MB
HART-GTFSRT-2020-25.pb.zip: 43.31 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 19869 | 19851 | 0 |
| Sampling | 30.44 | 30.47 | 0.00 |
| 2020-06-15 | 2847 | 2847 | 0 |
| 2020-06-16 | 2848 | 2848 | 0 |
| 2020-06-17 | 2847 | 2847 | 0 |
| 2020-06-18 | 2834 | 2828 | 0 |
| 2020-06-19 | 2811 | 2798 | 0 |
| 2020-06-20 | 2837 | 2837 | 0 |
| 2020-06-21 | 2845 | 2846 | 0 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.position.speed``
``entity.vehicle.timestamp``
``entity.vehicle.trip.tripId``
``entity.vehicle.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
``entity.id``
``entity.tripUpdate.stopTimeUpdate.arrival.delay``
``entity.tripUpdate.stopTimeUpdate.arrival.uncertainty``
``entity.tripUpdate.stopTimeUpdate.departure.delay``
``entity.tripUpdate.stopTimeUpdate.departure.uncertainty``
``entity.tripUpdate.stopTimeUpdate.stopId``
``entity.tripUpdate.stopTimeUpdate.stopSequence``
``entity.tripUpdate.timestamp``
``entity.tripUpdate.trip.tripId``
``entity.tripUpdate.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/HART-GTFSRT-2020-25.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/HART-GTFSRT-2020-25.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/HART-GTFSRT-2020-25.png)
