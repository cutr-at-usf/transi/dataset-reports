### HART-GTFSRT-2019-08
#### Metadata
HART-GTFSRT-2019-08.json.zip: 69.11 MB
HART-GTFSRT-2019-08.pb.zip: 61.07 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 19842 | 19813 | 0 |
| Sampling | 30.48 | 30.53 | 0.00 |
| 2019-02-18 | 2822 | 2821 | 0 |
| 2019-02-19 | 2832 | 2831 | 0 |
| 2019-02-20 | 2837 | 2837 | 0 |
| 2019-02-21 | 2838 | 2838 | 0 |
| 2019-02-22 | 2836 | 2836 | 0 |
| 2019-02-23 | 2838 | 2834 | 0 |
| 2019-02-24 | 2839 | 2816 | 0 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.position.speed``
``entity.vehicle.trip.tripId``
``entity.vehicle.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
``entity.id``
``entity.tripUpdate.stopTimeUpdate.arrival.delay``
``entity.tripUpdate.stopTimeUpdate.arrival.uncertainty``
``entity.tripUpdate.stopTimeUpdate.departure.delay``
``entity.tripUpdate.stopTimeUpdate.departure.uncertainty``
``entity.tripUpdate.stopTimeUpdate.stopId``
``entity.tripUpdate.stopTimeUpdate.stopSequence``
``entity.tripUpdate.trip.tripId``
``entity.tripUpdate.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/HART-GTFSRT-2019-08.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/HART-GTFSRT-2019-08.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/HART-GTFSRT-2019-08.png)
