### HART-GTFSRT-2017-46
#### Metadata
HART-GTFSRT-2017-46.json.zip: 63.23 MB
HART-GTFSRT-2017-46.pb.zip: 55.82 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 19843 | 19839 | 0 |
| Sampling | 30.48 | 30.49 | 0.00 |
| 2017-11-13 | 2835 | 2830 | 0 |
| 2017-11-14 | 2822 | 2822 | 0 |
| 2017-11-15 | 2837 | 2838 | 0 |
| 2017-11-16 | 2838 | 2838 | 0 |
| 2017-11-17 | 2839 | 2839 | 0 |
| 2017-11-18 | 2840 | 2840 | 0 |
| 2017-11-19 | 2832 | 2832 | 0 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.position.speed``
``entity.vehicle.trip.tripId``
``entity.vehicle.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
``entity.id``
``entity.tripUpdate.stopTimeUpdate.arrival.delay``
``entity.tripUpdate.stopTimeUpdate.arrival.uncertainty``
``entity.tripUpdate.stopTimeUpdate.departure.delay``
``entity.tripUpdate.stopTimeUpdate.departure.uncertainty``
``entity.tripUpdate.stopTimeUpdate.stopId``
``entity.tripUpdate.stopTimeUpdate.stopSequence``
``entity.tripUpdate.trip.tripId``
``entity.tripUpdate.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/HART-GTFSRT-2017-46.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/HART-GTFSRT-2017-46.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/HART-GTFSRT-2017-46.png)
