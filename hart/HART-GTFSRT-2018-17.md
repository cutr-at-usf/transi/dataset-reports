### HART-GTFSRT-2018-17
#### Metadata
HART-GTFSRT-2018-17.json.zip: 65.82 MB
HART-GTFSRT-2018-17.pb.zip: 58.35 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 19510 | 19510 | 0 |
| Sampling | 31.00 | 31.00 | 0.00 |
| 2018-04-23 | 2842 | 2842 | 0 |
| 2018-04-24 | 2833 | 2832 | 0 |
| 2018-04-25 | 2845 | 2845 | 0 |
| 2018-04-26 | 2459 | 2460 | 0 |
| 2018-04-27 | 2841 | 2842 | 0 |
| 2018-04-28 | 2845 | 2844 | 0 |
| 2018-04-29 | 2845 | 2845 | 0 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.position.speed``
``entity.vehicle.trip.tripId``
``entity.vehicle.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
``entity.id``
``entity.tripUpdate.stopTimeUpdate.arrival.delay``
``entity.tripUpdate.stopTimeUpdate.arrival.uncertainty``
``entity.tripUpdate.stopTimeUpdate.departure.delay``
``entity.tripUpdate.stopTimeUpdate.departure.uncertainty``
``entity.tripUpdate.stopTimeUpdate.stopId``
``entity.tripUpdate.stopTimeUpdate.stopSequence``
``entity.tripUpdate.trip.tripId``
``entity.tripUpdate.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/HART-GTFSRT-2018-17.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/HART-GTFSRT-2018-17.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/HART-GTFSRT-2018-17.png)
