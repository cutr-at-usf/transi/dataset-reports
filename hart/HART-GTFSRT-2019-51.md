### HART-GTFSRT-2019-51
#### Metadata
HART-GTFSRT-2019-51.json.zip: 75.89 MB
HART-GTFSRT-2019-51.pb.zip: 68.09 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 19810 | 19808 | 0 |
| Sampling | 30.53 | 30.53 | 0.00 |
| 2019-12-16 | 2840 | 2840 | 0 |
| 2019-12-17 | 2836 | 2835 | 0 |
| 2019-12-18 | 2784 | 2783 | 0 |
| 2019-12-19 | 2838 | 2838 | 0 |
| 2019-12-20 | 2836 | 2837 | 0 |
| 2019-12-21 | 2837 | 2836 | 0 |
| 2019-12-22 | 2839 | 2839 | 0 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.position.speed``
``entity.vehicle.timestamp``
``entity.vehicle.trip.tripId``
``entity.vehicle.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
``entity.id``
``entity.tripUpdate.stopTimeUpdate.arrival.delay``
``entity.tripUpdate.stopTimeUpdate.arrival.uncertainty``
``entity.tripUpdate.stopTimeUpdate.departure.delay``
``entity.tripUpdate.stopTimeUpdate.departure.uncertainty``
``entity.tripUpdate.stopTimeUpdate.stopId``
``entity.tripUpdate.stopTimeUpdate.stopSequence``
``entity.tripUpdate.timestamp``
``entity.tripUpdate.trip.tripId``
``entity.tripUpdate.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/HART-GTFSRT-2019-51.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/HART-GTFSRT-2019-51.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/HART-GTFSRT-2019-51.png)
