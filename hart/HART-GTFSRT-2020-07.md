### HART-GTFSRT-2020-07
#### Metadata
HART-GTFSRT-2020-07.json.zip: 75.41 MB
HART-GTFSRT-2020-07.pb.zip: 67.63 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 19766 | 19764 | 0 |
| Sampling | 30.60 | 30.60 | 0.00 |
| 2020-02-10 | 2838 | 2838 | 0 |
| 2020-02-11 | 2833 | 2833 | 0 |
| 2020-02-12 | 2747 | 2747 | 0 |
| 2020-02-13 | 2839 | 2839 | 0 |
| 2020-02-14 | 2837 | 2837 | 0 |
| 2020-02-15 | 2836 | 2835 | 0 |
| 2020-02-16 | 2836 | 2835 | 0 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.position.speed``
``entity.vehicle.timestamp``
``entity.vehicle.trip.tripId``
``entity.vehicle.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
``entity.id``
``entity.tripUpdate.stopTimeUpdate.arrival.delay``
``entity.tripUpdate.stopTimeUpdate.arrival.uncertainty``
``entity.tripUpdate.stopTimeUpdate.departure.delay``
``entity.tripUpdate.stopTimeUpdate.departure.uncertainty``
``entity.tripUpdate.stopTimeUpdate.stopId``
``entity.tripUpdate.stopTimeUpdate.stopSequence``
``entity.tripUpdate.timestamp``
``entity.tripUpdate.trip.tripId``
``entity.tripUpdate.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/HART-GTFSRT-2020-07.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/HART-GTFSRT-2020-07.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/HART-GTFSRT-2020-07.png)
