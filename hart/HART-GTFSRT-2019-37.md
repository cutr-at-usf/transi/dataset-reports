### HART-GTFSRT-2019-37
#### Metadata
HART-GTFSRT-2019-37.json.zip: 62.13 MB
HART-GTFSRT-2019-37.pb.zip: 55.06 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 17734 | 17733 | 0 |
| Sampling | 30.50 | 30.50 | 0.00 |
| 2019-09-09 | 729 | 729 | 0 |
| 2019-09-10 | 2829 | 2828 | 0 |
| 2019-09-11 | 2822 | 2822 | 0 |
| 2019-09-12 | 2839 | 2839 | 0 |
| 2019-09-13 | 2838 | 2838 | 0 |
| 2019-09-14 | 2839 | 2839 | 0 |
| 2019-09-15 | 2838 | 2838 | 0 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.position.speed``
``entity.vehicle.timestamp``
``entity.vehicle.trip.tripId``
``entity.vehicle.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
``entity.id``
``entity.tripUpdate.stopTimeUpdate.arrival.delay``
``entity.tripUpdate.stopTimeUpdate.arrival.uncertainty``
``entity.tripUpdate.stopTimeUpdate.departure.delay``
``entity.tripUpdate.stopTimeUpdate.departure.uncertainty``
``entity.tripUpdate.stopTimeUpdate.stopId``
``entity.tripUpdate.stopTimeUpdate.stopSequence``
``entity.tripUpdate.timestamp``
``entity.tripUpdate.trip.tripId``
``entity.tripUpdate.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/HART-GTFSRT-2019-37.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/HART-GTFSRT-2019-37.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/HART-GTFSRT-2019-37.png)
