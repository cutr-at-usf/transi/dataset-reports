### HART-GTFSRT-2018-24
#### Metadata
HART-GTFSRT-2018-24.json.zip: 65.46 MB
HART-GTFSRT-2018-24.pb.zip: 57.93 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 19764 | 19763 | 0 |
| Sampling | 30.60 | 30.60 | 0.00 |
| 2018-06-11 | 2842 | 2842 | 0 |
| 2018-06-12 | 2842 | 2842 | 0 |
| 2018-06-13 | 2727 | 2728 | 0 |
| 2018-06-14 | 2837 | 2836 | 0 |
| 2018-06-15 | 2842 | 2842 | 0 |
| 2018-06-16 | 2840 | 2840 | 0 |
| 2018-06-17 | 2834 | 2833 | 0 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.position.speed``
``entity.vehicle.trip.tripId``
``entity.vehicle.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
``entity.id``
``entity.tripUpdate.stopTimeUpdate.arrival.delay``
``entity.tripUpdate.stopTimeUpdate.arrival.uncertainty``
``entity.tripUpdate.stopTimeUpdate.departure.delay``
``entity.tripUpdate.stopTimeUpdate.departure.uncertainty``
``entity.tripUpdate.stopTimeUpdate.stopId``
``entity.tripUpdate.stopTimeUpdate.stopSequence``
``entity.tripUpdate.trip.tripId``
``entity.tripUpdate.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/HART-GTFSRT-2018-24.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/HART-GTFSRT-2018-24.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/HART-GTFSRT-2018-24.png)
