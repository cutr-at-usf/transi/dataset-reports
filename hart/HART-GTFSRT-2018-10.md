### HART-GTFSRT-2018-10
#### Metadata
HART-GTFSRT-2018-10.json.zip: 65.98 MB
HART-GTFSRT-2018-10.pb.zip: 58.40 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 19742 | 19742 | 0 |
| Sampling | 30.45 | 30.45 | 0.00 |
| 2018-03-05 | 2839 | 2839 | 0 |
| 2018-03-06 | 2840 | 2840 | 0 |
| 2018-03-07 | 2840 | 2840 | 0 |
| 2018-03-08 | 2819 | 2819 | 0 |
| 2018-03-09 | 2839 | 2839 | 0 |
| 2018-03-10 | 2841 | 2841 | 0 |
| 2018-03-11 | 2724 | 2724 | 0 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.position.speed``
``entity.vehicle.trip.tripId``
``entity.vehicle.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
``entity.id``
``entity.tripUpdate.stopTimeUpdate.arrival.delay``
``entity.tripUpdate.stopTimeUpdate.arrival.uncertainty``
``entity.tripUpdate.stopTimeUpdate.departure.delay``
``entity.tripUpdate.stopTimeUpdate.departure.uncertainty``
``entity.tripUpdate.stopTimeUpdate.stopId``
``entity.tripUpdate.stopTimeUpdate.stopSequence``
``entity.tripUpdate.trip.tripId``
``entity.tripUpdate.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/HART-GTFSRT-2018-10.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/HART-GTFSRT-2018-10.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/HART-GTFSRT-2018-10.png)
