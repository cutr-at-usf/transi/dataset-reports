### HART-GTFSRT-2017-43
#### Metadata
HART-GTFSRT-2017-43.json.zip: 0.26 MB
HART-GTFSRT-2017-43.pb.zip: 0.21 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 148 | 148 | 0 |
| Sampling | 45.31 | 45.31 | 0.00 |
| 2017-10-23 | 0 | 0 | 0 |
| 2017-10-24 | 0 | 0 | 0 |
| 2017-10-25 | 0 | 0 | 0 |
| 2017-10-26 | 0 | 0 | 0 |
| 2017-10-27 | 0 | 0 | 0 |
| 2017-10-28 | 0 | 0 | 0 |
| 2017-10-29 | 148 | 148 | 0 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.position.speed``
``entity.vehicle.trip.tripId``
``entity.vehicle.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
``entity.id``
``entity.tripUpdate.stopTimeUpdate.arrival.delay``
``entity.tripUpdate.stopTimeUpdate.arrival.uncertainty``
``entity.tripUpdate.stopTimeUpdate.departure.delay``
``entity.tripUpdate.stopTimeUpdate.departure.uncertainty``
``entity.tripUpdate.stopTimeUpdate.stopId``
``entity.tripUpdate.stopTimeUpdate.stopSequence``
``entity.tripUpdate.trip.tripId``
``entity.tripUpdate.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/HART-GTFSRT-2017-43.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/HART-GTFSRT-2017-43.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/HART-GTFSRT-2017-43.png)
