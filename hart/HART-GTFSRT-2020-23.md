### HART-GTFSRT-2020-23
#### Metadata
HART-GTFSRT-2020-23.json.zip: 48.99 MB
HART-GTFSRT-2020-23.pb.zip: 42.31 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 19865 | 19866 | 0 |
| Sampling | 30.45 | 30.44 | 0.00 |
| 2020-06-01 | 2846 | 2846 | 0 |
| 2020-06-02 | 2845 | 2845 | 0 |
| 2020-06-03 | 2805 | 2805 | 0 |
| 2020-06-04 | 2830 | 2831 | 0 |
| 2020-06-05 | 2848 | 2848 | 0 |
| 2020-06-06 | 2845 | 2845 | 0 |
| 2020-06-07 | 2846 | 2846 | 0 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.position.speed``
``entity.vehicle.timestamp``
``entity.vehicle.trip.tripId``
``entity.vehicle.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
``entity.id``
``entity.tripUpdate.stopTimeUpdate.arrival.delay``
``entity.tripUpdate.stopTimeUpdate.arrival.uncertainty``
``entity.tripUpdate.stopTimeUpdate.departure.delay``
``entity.tripUpdate.stopTimeUpdate.departure.uncertainty``
``entity.tripUpdate.stopTimeUpdate.stopId``
``entity.tripUpdate.stopTimeUpdate.stopSequence``
``entity.tripUpdate.timestamp``
``entity.tripUpdate.trip.tripId``
``entity.tripUpdate.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/HART-GTFSRT-2020-23.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/HART-GTFSRT-2020-23.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/HART-GTFSRT-2020-23.png)
