### HART-GTFSRT-2018-01
#### Metadata
HART-GTFSRT-2018-01.json.zip: 58.92 MB
HART-GTFSRT-2018-01.pb.zip: 51.73 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 19828 | 19817 | 0 |
| Sampling | 30.50 | 30.52 | 0.00 |
| 2018-01-01 | 2847 | 2847 | 0 |
| 2018-01-02 | 2845 | 2845 | 0 |
| 2018-01-03 | 2823 | 2821 | 0 |
| 2018-01-04 | 2806 | 2798 | 0 |
| 2018-01-05 | 2844 | 2843 | 0 |
| 2018-01-06 | 2846 | 2846 | 0 |
| 2018-01-07 | 2817 | 2817 | 0 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.position.speed``
``entity.vehicle.trip.tripId``
``entity.vehicle.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
``entity.id``
``entity.tripUpdate.stopTimeUpdate.arrival.delay``
``entity.tripUpdate.stopTimeUpdate.arrival.uncertainty``
``entity.tripUpdate.stopTimeUpdate.departure.delay``
``entity.tripUpdate.stopTimeUpdate.departure.uncertainty``
``entity.tripUpdate.stopTimeUpdate.stopId``
``entity.tripUpdate.stopTimeUpdate.stopSequence``
``entity.tripUpdate.trip.tripId``
``entity.tripUpdate.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/HART-GTFSRT-2018-01.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/HART-GTFSRT-2018-01.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/HART-GTFSRT-2018-01.png)
