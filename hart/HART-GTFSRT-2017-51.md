### HART-GTFSRT-2017-51
#### Metadata
HART-GTFSRT-2017-51.json.zip: 63.38 MB
HART-GTFSRT-2017-51.pb.zip: 56.01 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 19904 | 19902 | 0 |
| Sampling | 30.39 | 30.39 | 0.00 |
| 2017-12-18 | 2843 | 2843 | 0 |
| 2017-12-19 | 2843 | 2842 | 0 |
| 2017-12-20 | 2842 | 2842 | 0 |
| 2017-12-21 | 2843 | 2843 | 0 |
| 2017-12-22 | 2843 | 2843 | 0 |
| 2017-12-23 | 2846 | 2845 | 0 |
| 2017-12-24 | 2844 | 2844 | 0 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.position.speed``
``entity.vehicle.trip.tripId``
``entity.vehicle.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
``entity.id``
``entity.tripUpdate.stopTimeUpdate.arrival.delay``
``entity.tripUpdate.stopTimeUpdate.arrival.uncertainty``
``entity.tripUpdate.stopTimeUpdate.departure.delay``
``entity.tripUpdate.stopTimeUpdate.departure.uncertainty``
``entity.tripUpdate.stopTimeUpdate.stopId``
``entity.tripUpdate.stopTimeUpdate.stopSequence``
``entity.tripUpdate.trip.tripId``
``entity.tripUpdate.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/HART-GTFSRT-2017-51.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/HART-GTFSRT-2017-51.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/HART-GTFSRT-2017-51.png)
