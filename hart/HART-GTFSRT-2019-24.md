### HART-GTFSRT-2019-24
#### Metadata
HART-GTFSRT-2019-24.json.zip: 68.27 MB
HART-GTFSRT-2019-24.pb.zip: 60.27 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 19676 | 19676 | 0 |
| Sampling | 30.74 | 30.74 | 0.00 |
| 2019-06-10 | 2835 | 2835 | 0 |
| 2019-06-11 | 2838 | 2838 | 0 |
| 2019-06-12 | 2654 | 2654 | 0 |
| 2019-06-13 | 2835 | 2835 | 0 |
| 2019-06-14 | 2836 | 2836 | 0 |
| 2019-06-15 | 2839 | 2839 | 0 |
| 2019-06-16 | 2839 | 2839 | 0 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.position.speed``
``entity.vehicle.trip.tripId``
``entity.vehicle.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
``entity.id``
``entity.tripUpdate.stopTimeUpdate.arrival.delay``
``entity.tripUpdate.stopTimeUpdate.arrival.uncertainty``
``entity.tripUpdate.stopTimeUpdate.departure.delay``
``entity.tripUpdate.stopTimeUpdate.departure.uncertainty``
``entity.tripUpdate.stopTimeUpdate.stopId``
``entity.tripUpdate.stopTimeUpdate.stopSequence``
``entity.tripUpdate.trip.tripId``
``entity.tripUpdate.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/HART-GTFSRT-2019-24.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/HART-GTFSRT-2019-24.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/HART-GTFSRT-2019-24.png)
