### HART-GTFSRT-2019-06
#### Metadata
HART-GTFSRT-2019-06.json.zip: 68.74 MB
HART-GTFSRT-2019-06.pb.zip: 60.69 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 19857 | 19852 | 0 |
| Sampling | 30.46 | 30.47 | 0.00 |
| 2019-02-04 | 2838 | 2838 | 0 |
| 2019-02-05 | 2836 | 2835 | 0 |
| 2019-02-06 | 2838 | 2838 | 0 |
| 2019-02-07 | 2831 | 2830 | 0 |
| 2019-02-08 | 2837 | 2834 | 0 |
| 2019-02-09 | 2838 | 2837 | 0 |
| 2019-02-10 | 2839 | 2840 | 0 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.position.speed``
``entity.vehicle.trip.tripId``
``entity.vehicle.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
``entity.id``
``entity.tripUpdate.stopTimeUpdate.arrival.delay``
``entity.tripUpdate.stopTimeUpdate.arrival.uncertainty``
``entity.tripUpdate.stopTimeUpdate.departure.delay``
``entity.tripUpdate.stopTimeUpdate.departure.uncertainty``
``entity.tripUpdate.stopTimeUpdate.stopId``
``entity.tripUpdate.stopTimeUpdate.stopSequence``
``entity.tripUpdate.trip.tripId``
``entity.tripUpdate.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/HART-GTFSRT-2019-06.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/HART-GTFSRT-2019-06.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/HART-GTFSRT-2019-06.png)
