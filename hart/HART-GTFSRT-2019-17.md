### HART-GTFSRT-2019-17
#### Metadata
HART-GTFSRT-2019-17.json.zip: 61.33 MB
HART-GTFSRT-2019-17.pb.zip: 54.17 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 17638 | 17638 | 0 |
| Sampling | 34.29 | 34.29 | 0.00 |
| 2019-04-22 | 2552 | 2552 | 0 |
| 2019-04-23 | 2530 | 2530 | 0 |
| 2019-04-24 | 2541 | 2541 | 0 |
| 2019-04-25 | 2486 | 2486 | 0 |
| 2019-04-26 | 2520 | 2520 | 0 |
| 2019-04-27 | 2550 | 2550 | 0 |
| 2019-04-28 | 2459 | 2459 | 0 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.position.speed``
``entity.vehicle.trip.tripId``
``entity.vehicle.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
``entity.id``
``entity.tripUpdate.stopTimeUpdate.arrival.delay``
``entity.tripUpdate.stopTimeUpdate.arrival.uncertainty``
``entity.tripUpdate.stopTimeUpdate.departure.delay``
``entity.tripUpdate.stopTimeUpdate.departure.uncertainty``
``entity.tripUpdate.stopTimeUpdate.stopId``
``entity.tripUpdate.stopTimeUpdate.stopSequence``
``entity.tripUpdate.trip.tripId``
``entity.tripUpdate.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/HART-GTFSRT-2019-17.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/HART-GTFSRT-2019-17.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/HART-GTFSRT-2019-17.png)
