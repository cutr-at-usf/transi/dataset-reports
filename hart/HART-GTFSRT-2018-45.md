### HART-GTFSRT-2018-45
#### Metadata
HART-GTFSRT-2018-45.json.zip: 76.05 MB
HART-GTFSRT-2018-45.pb.zip: 67.96 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 19835 | 19835 | 0 |
| Sampling | 30.49 | 30.49 | 0.00 |
| 2018-11-05 | 2834 | 2834 | 0 |
| 2018-11-06 | 2832 | 2833 | 0 |
| 2018-11-07 | 2829 | 2829 | 0 |
| 2018-11-08 | 2829 | 2829 | 0 |
| 2018-11-09 | 2837 | 2837 | 0 |
| 2018-11-10 | 2840 | 2840 | 0 |
| 2018-11-11 | 2834 | 2833 | 0 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.position.speed``
``entity.vehicle.trip.tripId``
``entity.vehicle.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
``entity.id``
``entity.tripUpdate.stopTimeUpdate.arrival.delay``
``entity.tripUpdate.stopTimeUpdate.arrival.uncertainty``
``entity.tripUpdate.stopTimeUpdate.departure.delay``
``entity.tripUpdate.stopTimeUpdate.departure.uncertainty``
``entity.tripUpdate.stopTimeUpdate.stopId``
``entity.tripUpdate.stopTimeUpdate.stopSequence``
``entity.tripUpdate.trip.tripId``
``entity.tripUpdate.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/HART-GTFSRT-2018-45.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/HART-GTFSRT-2018-45.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/HART-GTFSRT-2018-45.png)
