### HART-GTFSRT-2018-37
#### Metadata
HART-GTFSRT-2018-37.json.zip: 69.23 MB
HART-GTFSRT-2018-37.pb.zip: 61.49 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 19863 | 19861 | 0 |
| Sampling | 30.45 | 30.45 | 0.00 |
| 2018-09-10 | 2834 | 2834 | 0 |
| 2018-09-11 | 2838 | 2838 | 0 |
| 2018-09-12 | 2833 | 2833 | 0 |
| 2018-09-13 | 2838 | 2838 | 0 |
| 2018-09-14 | 2835 | 2833 | 0 |
| 2018-09-15 | 2843 | 2843 | 0 |
| 2018-09-16 | 2842 | 2842 | 0 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.position.speed``
``entity.vehicle.trip.tripId``
``entity.vehicle.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
``entity.id``
``entity.tripUpdate.stopTimeUpdate.arrival.delay``
``entity.tripUpdate.stopTimeUpdate.arrival.uncertainty``
``entity.tripUpdate.stopTimeUpdate.departure.delay``
``entity.tripUpdate.stopTimeUpdate.departure.uncertainty``
``entity.tripUpdate.stopTimeUpdate.stopId``
``entity.tripUpdate.stopTimeUpdate.stopSequence``
``entity.tripUpdate.trip.tripId``
``entity.tripUpdate.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/HART-GTFSRT-2018-37.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/HART-GTFSRT-2018-37.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/HART-GTFSRT-2018-37.png)
