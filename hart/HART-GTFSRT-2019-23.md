### HART-GTFSRT-2019-23
#### Metadata
HART-GTFSRT-2019-23.json.zip: 68.03 MB
HART-GTFSRT-2019-23.pb.zip: 60.06 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 19676 | 19677 | 0 |
| Sampling | 30.74 | 30.74 | 0.00 |
| 2019-06-03 | 2838 | 2838 | 0 |
| 2019-06-04 | 2836 | 2837 | 0 |
| 2019-06-05 | 2653 | 2652 | 0 |
| 2019-06-06 | 2836 | 2836 | 0 |
| 2019-06-07 | 2837 | 2837 | 0 |
| 2019-06-08 | 2837 | 2837 | 0 |
| 2019-06-09 | 2839 | 2840 | 0 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.position.speed``
``entity.vehicle.trip.tripId``
``entity.vehicle.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
``entity.id``
``entity.tripUpdate.stopTimeUpdate.arrival.delay``
``entity.tripUpdate.stopTimeUpdate.arrival.uncertainty``
``entity.tripUpdate.stopTimeUpdate.departure.delay``
``entity.tripUpdate.stopTimeUpdate.departure.uncertainty``
``entity.tripUpdate.stopTimeUpdate.stopId``
``entity.tripUpdate.stopTimeUpdate.stopSequence``
``entity.tripUpdate.trip.tripId``
``entity.tripUpdate.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/HART-GTFSRT-2019-23.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/HART-GTFSRT-2019-23.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/HART-GTFSRT-2019-23.png)
