### HART-GTFSRT-2018-09
#### Metadata
HART-GTFSRT-2018-09.json.zip: 65.91 MB
HART-GTFSRT-2018-09.pb.zip: 58.32 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 19856 | 19856 | 0 |
| Sampling | 30.46 | 30.46 | 0.00 |
| 2018-02-26 | 2836 | 2836 | 0 |
| 2018-02-27 | 2836 | 2836 | 0 |
| 2018-02-28 | 2841 | 2841 | 0 |
| 2018-03-01 | 2843 | 2843 | 0 |
| 2018-03-02 | 2830 | 2829 | 0 |
| 2018-03-03 | 2840 | 2840 | 0 |
| 2018-03-04 | 2830 | 2831 | 0 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.position.speed``
``entity.vehicle.trip.tripId``
``entity.vehicle.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
``entity.id``
``entity.tripUpdate.stopTimeUpdate.arrival.delay``
``entity.tripUpdate.stopTimeUpdate.arrival.uncertainty``
``entity.tripUpdate.stopTimeUpdate.departure.delay``
``entity.tripUpdate.stopTimeUpdate.departure.uncertainty``
``entity.tripUpdate.stopTimeUpdate.stopId``
``entity.tripUpdate.stopTimeUpdate.stopSequence``
``entity.tripUpdate.trip.tripId``
``entity.tripUpdate.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/HART-GTFSRT-2018-09.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/HART-GTFSRT-2018-09.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/HART-GTFSRT-2018-09.png)
