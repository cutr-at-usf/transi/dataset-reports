### HART-GTFSRT-2019-19
#### Metadata
HART-GTFSRT-2019-19.json.zip: 68.34 MB
HART-GTFSRT-2019-19.pb.zip: 60.33 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 19751 | 19751 | 0 |
| Sampling | 30.62 | 30.62 | 0.00 |
| 2019-05-06 | 2837 | 2837 | 0 |
| 2019-05-07 | 2840 | 2840 | 0 |
| 2019-05-08 | 2711 | 2711 | 0 |
| 2019-05-09 | 2840 | 2840 | 0 |
| 2019-05-10 | 2838 | 2838 | 0 |
| 2019-05-11 | 2843 | 2843 | 0 |
| 2019-05-12 | 2842 | 2842 | 0 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.position.speed``
``entity.vehicle.trip.tripId``
``entity.vehicle.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
``entity.id``
``entity.tripUpdate.stopTimeUpdate.arrival.delay``
``entity.tripUpdate.stopTimeUpdate.arrival.uncertainty``
``entity.tripUpdate.stopTimeUpdate.departure.delay``
``entity.tripUpdate.stopTimeUpdate.departure.uncertainty``
``entity.tripUpdate.stopTimeUpdate.stopId``
``entity.tripUpdate.stopTimeUpdate.stopSequence``
``entity.tripUpdate.trip.tripId``
``entity.tripUpdate.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/HART-GTFSRT-2019-19.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/HART-GTFSRT-2019-19.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/HART-GTFSRT-2019-19.png)
