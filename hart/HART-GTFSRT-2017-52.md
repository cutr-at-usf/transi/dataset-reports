### HART-GTFSRT-2017-52
#### Metadata
HART-GTFSRT-2017-52.json.zip: 59.85 MB
HART-GTFSRT-2017-52.pb.zip: 52.59 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 19911 | 19911 | 0 |
| Sampling | 30.38 | 30.38 | 0.00 |
| 2017-12-25 | 2845 | 2845 | 0 |
| 2017-12-26 | 2844 | 2844 | 0 |
| 2017-12-27 | 2844 | 2844 | 0 |
| 2017-12-28 | 2845 | 2845 | 0 |
| 2017-12-29 | 2844 | 2844 | 0 |
| 2017-12-30 | 2844 | 2844 | 0 |
| 2017-12-31 | 2845 | 2845 | 0 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.position.speed``
``entity.vehicle.trip.tripId``
``entity.vehicle.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
``entity.id``
``entity.tripUpdate.stopTimeUpdate.arrival.delay``
``entity.tripUpdate.stopTimeUpdate.arrival.uncertainty``
``entity.tripUpdate.stopTimeUpdate.departure.delay``
``entity.tripUpdate.stopTimeUpdate.departure.uncertainty``
``entity.tripUpdate.stopTimeUpdate.stopId``
``entity.tripUpdate.stopTimeUpdate.stopSequence``
``entity.tripUpdate.trip.tripId``
``entity.tripUpdate.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/HART-GTFSRT-2017-52.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/HART-GTFSRT-2017-52.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/HART-GTFSRT-2017-52.png)
