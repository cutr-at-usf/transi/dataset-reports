### HART-GTFSRT-2020-01
#### Metadata
HART-GTFSRT-2020-01.json.zip: 71.27 MB
HART-GTFSRT-2020-01.pb.zip: 63.64 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 19759 | 19756 | 0 |
| Sampling | 30.61 | 30.61 | 0.00 |
| 2019-12-30 | 2835 | 2835 | 0 |
| 2019-12-31 | 2837 | 2837 | 0 |
| 2020-01-01 | 2739 | 2738 | 0 |
| 2020-01-02 | 2839 | 2839 | 0 |
| 2020-01-03 | 2838 | 2838 | 0 |
| 2020-01-04 | 2841 | 2841 | 0 |
| 2020-01-05 | 2830 | 2828 | 0 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.position.speed``
``entity.vehicle.timestamp``
``entity.vehicle.trip.tripId``
``entity.vehicle.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
``entity.id``
``entity.tripUpdate.stopTimeUpdate.arrival.delay``
``entity.tripUpdate.stopTimeUpdate.arrival.uncertainty``
``entity.tripUpdate.stopTimeUpdate.departure.delay``
``entity.tripUpdate.stopTimeUpdate.departure.uncertainty``
``entity.tripUpdate.stopTimeUpdate.stopId``
``entity.tripUpdate.stopTimeUpdate.stopSequence``
``entity.tripUpdate.timestamp``
``entity.tripUpdate.trip.tripId``
``entity.tripUpdate.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/HART-GTFSRT-2020-01.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/HART-GTFSRT-2020-01.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/HART-GTFSRT-2020-01.png)
