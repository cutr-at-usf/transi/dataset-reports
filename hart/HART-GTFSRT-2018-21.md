### HART-GTFSRT-2018-21
#### Metadata
HART-GTFSRT-2018-21.json.zip: 64.49 MB
HART-GTFSRT-2018-21.pb.zip: 57.12 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 19382 | 19382 | 0 |
| Sampling | 31.20 | 31.20 | 0.00 |
| 2018-05-21 | 2839 | 2839 | 0 |
| 2018-05-22 | 2832 | 2832 | 0 |
| 2018-05-23 | 2842 | 2842 | 0 |
| 2018-05-24 | 2768 | 2768 | 0 |
| 2018-05-25 | 2751 | 2751 | 0 |
| 2018-05-26 | 2714 | 2714 | 0 |
| 2018-05-27 | 2636 | 2636 | 0 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.position.speed``
``entity.vehicle.trip.tripId``
``entity.vehicle.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
``entity.id``
``entity.tripUpdate.stopTimeUpdate.arrival.delay``
``entity.tripUpdate.stopTimeUpdate.arrival.uncertainty``
``entity.tripUpdate.stopTimeUpdate.departure.delay``
``entity.tripUpdate.stopTimeUpdate.departure.uncertainty``
``entity.tripUpdate.stopTimeUpdate.stopId``
``entity.tripUpdate.stopTimeUpdate.stopSequence``
``entity.tripUpdate.trip.tripId``
``entity.tripUpdate.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/HART-GTFSRT-2018-21.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/HART-GTFSRT-2018-21.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/HART-GTFSRT-2018-21.png)
