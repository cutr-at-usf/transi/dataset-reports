### HART-GTFSRT-2019-40
#### Metadata
HART-GTFSRT-2019-40.json.zip: 75.95 MB
HART-GTFSRT-2019-40.pb.zip: 68.18 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 19875 | 19875 | 0 |
| Sampling | 30.43 | 30.43 | 0.00 |
| 2019-09-30 | 2838 | 2838 | 0 |
| 2019-10-01 | 2841 | 2841 | 0 |
| 2019-10-02 | 2841 | 2841 | 0 |
| 2019-10-03 | 2841 | 2841 | 0 |
| 2019-10-04 | 2837 | 2837 | 0 |
| 2019-10-05 | 2841 | 2841 | 0 |
| 2019-10-06 | 2836 | 2836 | 0 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.position.speed``
``entity.vehicle.timestamp``
``entity.vehicle.trip.tripId``
``entity.vehicle.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
``entity.id``
``entity.tripUpdate.stopTimeUpdate.arrival.delay``
``entity.tripUpdate.stopTimeUpdate.arrival.uncertainty``
``entity.tripUpdate.stopTimeUpdate.departure.delay``
``entity.tripUpdate.stopTimeUpdate.departure.uncertainty``
``entity.tripUpdate.stopTimeUpdate.stopId``
``entity.tripUpdate.stopTimeUpdate.stopSequence``
``entity.tripUpdate.timestamp``
``entity.tripUpdate.trip.tripId``
``entity.tripUpdate.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/HART-GTFSRT-2019-40.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/HART-GTFSRT-2019-40.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/HART-GTFSRT-2019-40.png)
