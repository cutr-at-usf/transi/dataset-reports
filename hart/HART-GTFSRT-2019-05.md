### HART-GTFSRT-2019-05
#### Metadata
HART-GTFSRT-2019-05.json.zip: 68.59 MB
HART-GTFSRT-2019-05.pb.zip: 60.55 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 19756 | 19747 | 0 |
| Sampling | 30.61 | 30.63 | 0.00 |
| 2019-01-28 | 2795 | 2787 | 0 |
| 2019-01-29 | 2825 | 2824 | 0 |
| 2019-01-30 | 2825 | 2825 | 0 |
| 2019-01-31 | 2839 | 2839 | 0 |
| 2019-02-01 | 2840 | 2840 | 0 |
| 2019-02-02 | 2835 | 2835 | 0 |
| 2019-02-03 | 2797 | 2797 | 0 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.position.speed``
``entity.vehicle.trip.tripId``
``entity.vehicle.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
``entity.id``
``entity.tripUpdate.stopTimeUpdate.arrival.delay``
``entity.tripUpdate.stopTimeUpdate.arrival.uncertainty``
``entity.tripUpdate.stopTimeUpdate.departure.delay``
``entity.tripUpdate.stopTimeUpdate.departure.uncertainty``
``entity.tripUpdate.stopTimeUpdate.stopId``
``entity.tripUpdate.stopTimeUpdate.stopSequence``
``entity.tripUpdate.trip.tripId``
``entity.tripUpdate.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/HART-GTFSRT-2019-05.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/HART-GTFSRT-2019-05.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/HART-GTFSRT-2019-05.png)
