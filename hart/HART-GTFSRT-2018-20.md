### HART-GTFSRT-2018-20
#### Metadata
HART-GTFSRT-2018-20.json.zip: 65.73 MB
HART-GTFSRT-2018-20.pb.zip: 58.17 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 19900 | 19900 | 0 |
| Sampling | 30.39 | 30.39 | 0.00 |
| 2018-05-14 | 2841 | 2842 | 0 |
| 2018-05-15 | 2844 | 2844 | 0 |
| 2018-05-16 | 2844 | 2844 | 0 |
| 2018-05-17 | 2844 | 2844 | 0 |
| 2018-05-18 | 2842 | 2841 | 0 |
| 2018-05-19 | 2842 | 2842 | 0 |
| 2018-05-20 | 2843 | 2843 | 0 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.position.speed``
``entity.vehicle.trip.tripId``
``entity.vehicle.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
``entity.id``
``entity.tripUpdate.stopTimeUpdate.arrival.delay``
``entity.tripUpdate.stopTimeUpdate.arrival.uncertainty``
``entity.tripUpdate.stopTimeUpdate.departure.delay``
``entity.tripUpdate.stopTimeUpdate.departure.uncertainty``
``entity.tripUpdate.stopTimeUpdate.stopId``
``entity.tripUpdate.stopTimeUpdate.stopSequence``
``entity.tripUpdate.trip.tripId``
``entity.tripUpdate.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/HART-GTFSRT-2018-20.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/HART-GTFSRT-2018-20.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/HART-GTFSRT-2018-20.png)
