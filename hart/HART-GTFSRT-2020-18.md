### HART-GTFSRT-2020-18
#### Metadata
HART-GTFSRT-2020-18.json.zip: 47.81 MB
HART-GTFSRT-2020-18.pb.zip: 41.14 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 19906 | 19905 | 0 |
| Sampling | 30.38 | 30.38 | 0.00 |
| 2020-04-27 | 2846 | 2846 | 0 |
| 2020-04-28 | 2845 | 2844 | 0 |
| 2020-04-29 | 2844 | 2844 | 0 |
| 2020-04-30 | 2845 | 2845 | 0 |
| 2020-05-01 | 2845 | 2845 | 0 |
| 2020-05-02 | 2846 | 2846 | 0 |
| 2020-05-03 | 2835 | 2835 | 0 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.position.speed``
``entity.vehicle.timestamp``
``entity.vehicle.trip.tripId``
``entity.vehicle.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
``entity.id``
``entity.tripUpdate.stopTimeUpdate.arrival.delay``
``entity.tripUpdate.stopTimeUpdate.arrival.uncertainty``
``entity.tripUpdate.stopTimeUpdate.departure.delay``
``entity.tripUpdate.stopTimeUpdate.departure.uncertainty``
``entity.tripUpdate.stopTimeUpdate.stopId``
``entity.tripUpdate.stopTimeUpdate.stopSequence``
``entity.tripUpdate.timestamp``
``entity.tripUpdate.trip.tripId``
``entity.tripUpdate.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/HART-GTFSRT-2020-18.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/HART-GTFSRT-2020-18.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/HART-GTFSRT-2020-18.png)
