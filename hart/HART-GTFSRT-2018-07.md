### HART-GTFSRT-2018-07
#### Metadata
HART-GTFSRT-2018-07.json.zip: 63.65 MB
HART-GTFSRT-2018-07.pb.zip: 56.20 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 19860 | 19859 | 0 |
| Sampling | 30.45 | 30.45 | 0.00 |
| 2018-02-12 | 2839 | 2839 | 0 |
| 2018-02-13 | 2839 | 2839 | 0 |
| 2018-02-14 | 2840 | 2839 | 0 |
| 2018-02-15 | 2823 | 2823 | 0 |
| 2018-02-16 | 2835 | 2835 | 0 |
| 2018-02-17 | 2844 | 2844 | 0 |
| 2018-02-18 | 2840 | 2840 | 0 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.position.speed``
``entity.vehicle.trip.tripId``
``entity.vehicle.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
``entity.id``
``entity.tripUpdate.stopTimeUpdate.arrival.delay``
``entity.tripUpdate.stopTimeUpdate.arrival.uncertainty``
``entity.tripUpdate.stopTimeUpdate.departure.delay``
``entity.tripUpdate.stopTimeUpdate.departure.uncertainty``
``entity.tripUpdate.stopTimeUpdate.stopId``
``entity.tripUpdate.stopTimeUpdate.stopSequence``
``entity.tripUpdate.trip.tripId``
``entity.tripUpdate.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/HART-GTFSRT-2018-07.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/HART-GTFSRT-2018-07.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/HART-GTFSRT-2018-07.png)
