### HART-GTFSRT-2018-27
#### Metadata
HART-GTFSRT-2018-27.json.zip: 64.19 MB
HART-GTFSRT-2018-27.pb.zip: 56.60 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 19799 | 19799 | 0 |
| Sampling | 30.55 | 30.55 | 0.00 |
| 2018-07-02 | 2842 | 2842 | 0 |
| 2018-07-03 | 2843 | 2843 | 0 |
| 2018-07-04 | 2752 | 2752 | 0 |
| 2018-07-05 | 2842 | 2842 | 0 |
| 2018-07-06 | 2837 | 2837 | 0 |
| 2018-07-07 | 2841 | 2841 | 0 |
| 2018-07-08 | 2842 | 2842 | 0 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.position.speed``
``entity.vehicle.trip.tripId``
``entity.vehicle.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
``entity.id``
``entity.tripUpdate.stopTimeUpdate.arrival.delay``
``entity.tripUpdate.stopTimeUpdate.arrival.uncertainty``
``entity.tripUpdate.stopTimeUpdate.departure.delay``
``entity.tripUpdate.stopTimeUpdate.departure.uncertainty``
``entity.tripUpdate.stopTimeUpdate.stopId``
``entity.tripUpdate.stopTimeUpdate.stopSequence``
``entity.tripUpdate.trip.tripId``
``entity.tripUpdate.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/HART-GTFSRT-2018-27.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/HART-GTFSRT-2018-27.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/HART-GTFSRT-2018-27.png)
