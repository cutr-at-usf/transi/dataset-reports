### HART-GTFSRT-2020-19
#### Metadata
HART-GTFSRT-2020-19.json.zip: 48.13 MB
HART-GTFSRT-2020-19.pb.zip: 41.47 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 19711 | 19709 | 0 |
| Sampling | 30.68 | 30.69 | 0.00 |
| 2020-05-04 | 2845 | 2845 | 0 |
| 2020-05-05 | 2736 | 2735 | 0 |
| 2020-05-06 | 2770 | 2769 | 0 |
| 2020-05-07 | 2847 | 2847 | 0 |
| 2020-05-08 | 2847 | 2847 | 0 |
| 2020-05-09 | 2819 | 2820 | 0 |
| 2020-05-10 | 2847 | 2846 | 0 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.position.speed``
``entity.vehicle.timestamp``
``entity.vehicle.trip.tripId``
``entity.vehicle.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
``entity.id``
``entity.tripUpdate.stopTimeUpdate.arrival.delay``
``entity.tripUpdate.stopTimeUpdate.arrival.uncertainty``
``entity.tripUpdate.stopTimeUpdate.departure.delay``
``entity.tripUpdate.stopTimeUpdate.departure.uncertainty``
``entity.tripUpdate.stopTimeUpdate.stopId``
``entity.tripUpdate.stopTimeUpdate.stopSequence``
``entity.tripUpdate.timestamp``
``entity.tripUpdate.trip.tripId``
``entity.tripUpdate.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/HART-GTFSRT-2020-19.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/HART-GTFSRT-2020-19.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/HART-GTFSRT-2020-19.png)
