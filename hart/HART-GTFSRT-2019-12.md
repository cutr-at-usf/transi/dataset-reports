### HART-GTFSRT-2019-12
#### Metadata
HART-GTFSRT-2019-12.json.zip: 68.38 MB
HART-GTFSRT-2019-12.pb.zip: 60.42 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 19720 | 19718 | 0 |
| Sampling | 30.67 | 30.67 | 0.00 |
| 2019-03-18 | 2837 | 2837 | 0 |
| 2019-03-19 | 2841 | 2841 | 0 |
| 2019-03-20 | 2831 | 2830 | 0 |
| 2019-03-21 | 2726 | 2727 | 0 |
| 2019-03-22 | 2797 | 2795 | 0 |
| 2019-03-23 | 2845 | 2845 | 0 |
| 2019-03-24 | 2843 | 2843 | 0 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.position.speed``
``entity.vehicle.trip.tripId``
``entity.vehicle.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
``entity.id``
``entity.tripUpdate.stopTimeUpdate.arrival.delay``
``entity.tripUpdate.stopTimeUpdate.arrival.uncertainty``
``entity.tripUpdate.stopTimeUpdate.departure.delay``
``entity.tripUpdate.stopTimeUpdate.departure.uncertainty``
``entity.tripUpdate.stopTimeUpdate.stopId``
``entity.tripUpdate.stopTimeUpdate.stopSequence``
``entity.tripUpdate.trip.tripId``
``entity.tripUpdate.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/HART-GTFSRT-2019-12.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/HART-GTFSRT-2019-12.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/HART-GTFSRT-2019-12.png)
