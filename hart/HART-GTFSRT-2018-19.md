### HART-GTFSRT-2018-19
#### Metadata
HART-GTFSRT-2018-19.json.zip: 66.06 MB
HART-GTFSRT-2018-19.pb.zip: 58.47 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 19878 | 19879 | 0 |
| Sampling | 30.43 | 30.42 | 0.00 |
| 2018-05-07 | 2833 | 2834 | 0 |
| 2018-05-08 | 2838 | 2838 | 0 |
| 2018-05-09 | 2841 | 2841 | 0 |
| 2018-05-10 | 2840 | 2840 | 0 |
| 2018-05-11 | 2838 | 2839 | 0 |
| 2018-05-12 | 2844 | 2843 | 0 |
| 2018-05-13 | 2844 | 2844 | 0 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.position.speed``
``entity.vehicle.trip.tripId``
``entity.vehicle.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
``entity.id``
``entity.tripUpdate.stopTimeUpdate.arrival.delay``
``entity.tripUpdate.stopTimeUpdate.arrival.uncertainty``
``entity.tripUpdate.stopTimeUpdate.departure.delay``
``entity.tripUpdate.stopTimeUpdate.departure.uncertainty``
``entity.tripUpdate.stopTimeUpdate.stopId``
``entity.tripUpdate.stopTimeUpdate.stopSequence``
``entity.tripUpdate.trip.tripId``
``entity.tripUpdate.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/HART-GTFSRT-2018-19.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/HART-GTFSRT-2018-19.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/HART-GTFSRT-2018-19.png)
