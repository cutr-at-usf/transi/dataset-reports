### HART-GTFSRT-2019-44
#### Metadata
HART-GTFSRT-2019-44.json.zip: 71.14 MB
HART-GTFSRT-2019-44.pb.zip: 63.93 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 18243 | 18242 | 0 |
| Sampling | 33.35 | 33.35 | 0.00 |
| 2019-10-28 | 2242 | 2241 | 0 |
| 2019-10-29 | 1791 | 1791 | 0 |
| 2019-10-30 | 2839 | 2839 | 0 |
| 2019-10-31 | 2839 | 2839 | 0 |
| 2019-11-01 | 2836 | 2836 | 0 |
| 2019-11-02 | 2837 | 2837 | 0 |
| 2019-11-03 | 2742 | 2742 | 0 |
| 2019-11-04 | 117 | 117 | 0 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.position.speed``
``entity.vehicle.timestamp``
``entity.vehicle.trip.tripId``
``entity.vehicle.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
``entity.id``
``entity.tripUpdate.stopTimeUpdate.arrival.delay``
``entity.tripUpdate.stopTimeUpdate.arrival.uncertainty``
``entity.tripUpdate.stopTimeUpdate.departure.delay``
``entity.tripUpdate.stopTimeUpdate.departure.uncertainty``
``entity.tripUpdate.stopTimeUpdate.stopId``
``entity.tripUpdate.stopTimeUpdate.stopSequence``
``entity.tripUpdate.timestamp``
``entity.tripUpdate.trip.tripId``
``entity.tripUpdate.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/HART-GTFSRT-2019-44.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/HART-GTFSRT-2019-44.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/HART-GTFSRT-2019-44.png)
