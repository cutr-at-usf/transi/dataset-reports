### HART-GTFSRT-2019-10
#### Metadata
HART-GTFSRT-2019-10.json.zip: 72.46 MB
HART-GTFSRT-2019-10.pb.zip: 64.19 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 19695 | 19690 | 0 |
| Sampling | 30.53 | 30.53 | 0.00 |
| 2019-03-04 | 2839 | 2838 | 0 |
| 2019-03-05 | 2838 | 2838 | 0 |
| 2019-03-06 | 2835 | 2834 | 0 |
| 2019-03-07 | 2778 | 2775 | 0 |
| 2019-03-08 | 2838 | 2839 | 0 |
| 2019-03-09 | 2843 | 2842 | 0 |
| 2019-03-10 | 2724 | 2724 | 0 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.position.speed``
``entity.vehicle.trip.tripId``
``entity.vehicle.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
``entity.id``
``entity.tripUpdate.stopTimeUpdate.arrival.delay``
``entity.tripUpdate.stopTimeUpdate.arrival.uncertainty``
``entity.tripUpdate.stopTimeUpdate.departure.delay``
``entity.tripUpdate.stopTimeUpdate.departure.uncertainty``
``entity.tripUpdate.stopTimeUpdate.stopId``
``entity.tripUpdate.stopTimeUpdate.stopSequence``
``entity.tripUpdate.trip.tripId``
``entity.tripUpdate.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/HART-GTFSRT-2019-10.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/HART-GTFSRT-2019-10.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/HART-GTFSRT-2019-10.png)
