### HART-GTFSRT-2018-35
#### Metadata
HART-GTFSRT-2018-35.json.zip: 68.57 MB
HART-GTFSRT-2018-35.pb.zip: 60.86 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 19733 | 19730 | 0 |
| Sampling | 30.65 | 30.65 | 0.00 |
| 2018-08-27 | 2832 | 2832 | 0 |
| 2018-08-28 | 2833 | 2831 | 0 |
| 2018-08-29 | 2726 | 2725 | 0 |
| 2018-08-30 | 2833 | 2833 | 0 |
| 2018-08-31 | 2835 | 2835 | 0 |
| 2018-09-01 | 2843 | 2843 | 0 |
| 2018-09-02 | 2831 | 2831 | 0 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.position.speed``
``entity.vehicle.trip.tripId``
``entity.vehicle.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
``entity.id``
``entity.tripUpdate.stopTimeUpdate.arrival.delay``
``entity.tripUpdate.stopTimeUpdate.arrival.uncertainty``
``entity.tripUpdate.stopTimeUpdate.departure.delay``
``entity.tripUpdate.stopTimeUpdate.departure.uncertainty``
``entity.tripUpdate.stopTimeUpdate.stopId``
``entity.tripUpdate.stopTimeUpdate.stopSequence``
``entity.tripUpdate.trip.tripId``
``entity.tripUpdate.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/HART-GTFSRT-2018-35.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/HART-GTFSRT-2018-35.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/HART-GTFSRT-2018-35.png)
