### HART-GTFSRT-2018-29
#### Metadata
HART-GTFSRT-2018-29.json.zip: 68.50 MB
HART-GTFSRT-2018-29.pb.zip: 60.75 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 19821 | 19820 | 0 |
| Sampling | 30.51 | 30.51 | 0.00 |
| 2018-07-16 | 2844 | 2844 | 0 |
| 2018-07-17 | 2843 | 2842 | 0 |
| 2018-07-18 | 2762 | 2762 | 0 |
| 2018-07-19 | 2842 | 2842 | 0 |
| 2018-07-20 | 2841 | 2841 | 0 |
| 2018-07-21 | 2844 | 2844 | 0 |
| 2018-07-22 | 2845 | 2845 | 0 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.position.speed``
``entity.vehicle.trip.tripId``
``entity.vehicle.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
``entity.id``
``entity.tripUpdate.stopTimeUpdate.arrival.delay``
``entity.tripUpdate.stopTimeUpdate.arrival.uncertainty``
``entity.tripUpdate.stopTimeUpdate.departure.delay``
``entity.tripUpdate.stopTimeUpdate.departure.uncertainty``
``entity.tripUpdate.stopTimeUpdate.stopId``
``entity.tripUpdate.stopTimeUpdate.stopSequence``
``entity.tripUpdate.trip.tripId``
``entity.tripUpdate.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/HART-GTFSRT-2018-29.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/HART-GTFSRT-2018-29.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/HART-GTFSRT-2018-29.png)
