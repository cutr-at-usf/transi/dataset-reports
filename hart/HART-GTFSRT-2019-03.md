### HART-GTFSRT-2019-03
#### Metadata
HART-GTFSRT-2019-03.json.zip: 65.86 MB
HART-GTFSRT-2019-03.pb.zip: 58.11 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 19312 | 19247 | 0 |
| Sampling | 31.32 | 31.42 | 0.00 |
| 2019-01-14 | 2831 | 2814 | 0 |
| 2019-01-15 | 2818 | 2767 | 0 |
| 2019-01-16 | 2371 | 2372 | 0 |
| 2019-01-17 | 2819 | 2819 | 0 |
| 2019-01-18 | 2824 | 2824 | 0 |
| 2019-01-19 | 2825 | 2825 | 0 |
| 2019-01-20 | 2824 | 2826 | 0 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.position.speed``
``entity.vehicle.trip.tripId``
``entity.vehicle.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
``entity.id``
``entity.tripUpdate.stopTimeUpdate.arrival.delay``
``entity.tripUpdate.stopTimeUpdate.arrival.uncertainty``
``entity.tripUpdate.stopTimeUpdate.departure.delay``
``entity.tripUpdate.stopTimeUpdate.departure.uncertainty``
``entity.tripUpdate.stopTimeUpdate.stopId``
``entity.tripUpdate.stopTimeUpdate.stopSequence``
``entity.tripUpdate.trip.tripId``
``entity.tripUpdate.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/HART-GTFSRT-2019-03.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/HART-GTFSRT-2019-03.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/HART-GTFSRT-2019-03.png)
