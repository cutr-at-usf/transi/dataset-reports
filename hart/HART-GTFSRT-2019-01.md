### HART-GTFSRT-2019-01
#### Metadata
HART-GTFSRT-2019-01.json.zip: 64.86 MB
HART-GTFSRT-2019-01.pb.zip: 56.97 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 19854 | 19854 | 0 |
| Sampling | 30.46 | 30.46 | 0.00 |
| 2018-12-31 | 2837 | 2837 | 0 |
| 2019-01-01 | 2842 | 2842 | 0 |
| 2019-01-02 | 2841 | 2841 | 0 |
| 2019-01-03 | 2838 | 2838 | 0 |
| 2019-01-04 | 2840 | 2840 | 0 |
| 2019-01-05 | 2841 | 2841 | 0 |
| 2019-01-06 | 2815 | 2815 | 0 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.position.speed``
``entity.vehicle.trip.tripId``
``entity.vehicle.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
``entity.id``
``entity.tripUpdate.stopTimeUpdate.arrival.delay``
``entity.tripUpdate.stopTimeUpdate.arrival.uncertainty``
``entity.tripUpdate.stopTimeUpdate.departure.delay``
``entity.tripUpdate.stopTimeUpdate.departure.uncertainty``
``entity.tripUpdate.stopTimeUpdate.stopId``
``entity.tripUpdate.stopTimeUpdate.stopSequence``
``entity.tripUpdate.trip.tripId``
``entity.tripUpdate.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/HART-GTFSRT-2019-01.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/HART-GTFSRT-2019-01.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/HART-GTFSRT-2019-01.png)
