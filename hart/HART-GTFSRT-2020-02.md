### HART-GTFSRT-2020-02
#### Metadata
HART-GTFSRT-2020-02.json.zip: 75.85 MB
HART-GTFSRT-2020-02.pb.zip: 68.03 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 19799 | 19798 | 0 |
| Sampling | 30.55 | 30.55 | 0.00 |
| 2020-01-06 | 2835 | 2835 | 0 |
| 2020-01-07 | 2832 | 2831 | 0 |
| 2020-01-08 | 2780 | 2781 | 0 |
| 2020-01-09 | 2838 | 2838 | 0 |
| 2020-01-10 | 2835 | 2835 | 0 |
| 2020-01-11 | 2838 | 2838 | 0 |
| 2020-01-12 | 2841 | 2840 | 0 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.position.speed``
``entity.vehicle.timestamp``
``entity.vehicle.trip.tripId``
``entity.vehicle.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
``entity.id``
``entity.tripUpdate.stopTimeUpdate.arrival.delay``
``entity.tripUpdate.stopTimeUpdate.arrival.uncertainty``
``entity.tripUpdate.stopTimeUpdate.departure.delay``
``entity.tripUpdate.stopTimeUpdate.departure.uncertainty``
``entity.tripUpdate.stopTimeUpdate.stopId``
``entity.tripUpdate.stopTimeUpdate.stopSequence``
``entity.tripUpdate.timestamp``
``entity.tripUpdate.trip.tripId``
``entity.tripUpdate.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/HART-GTFSRT-2020-02.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/HART-GTFSRT-2020-02.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/HART-GTFSRT-2020-02.png)
