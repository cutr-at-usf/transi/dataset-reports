### HART-GTFSRT-2018-34
#### Metadata
HART-GTFSRT-2018-34.json.zip: 68.38 MB
HART-GTFSRT-2018-34.pb.zip: 60.69 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 19725 | 19725 | 0 |
| Sampling | 30.66 | 30.66 | 0.00 |
| 2018-08-20 | 2835 | 2835 | 0 |
| 2018-08-21 | 2836 | 2836 | 0 |
| 2018-08-22 | 2693 | 2693 | 0 |
| 2018-08-23 | 2837 | 2837 | 0 |
| 2018-08-24 | 2838 | 2838 | 0 |
| 2018-08-25 | 2843 | 2843 | 0 |
| 2018-08-26 | 2843 | 2843 | 0 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.position.speed``
``entity.vehicle.trip.tripId``
``entity.vehicle.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
``entity.id``
``entity.tripUpdate.stopTimeUpdate.arrival.delay``
``entity.tripUpdate.stopTimeUpdate.arrival.uncertainty``
``entity.tripUpdate.stopTimeUpdate.departure.delay``
``entity.tripUpdate.stopTimeUpdate.departure.uncertainty``
``entity.tripUpdate.stopTimeUpdate.stopId``
``entity.tripUpdate.stopTimeUpdate.stopSequence``
``entity.tripUpdate.trip.tripId``
``entity.tripUpdate.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/HART-GTFSRT-2018-34.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/HART-GTFSRT-2018-34.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/HART-GTFSRT-2018-34.png)
