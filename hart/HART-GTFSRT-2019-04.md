### HART-GTFSRT-2019-04
#### Metadata
HART-GTFSRT-2019-04.json.zip: 68.24 MB
HART-GTFSRT-2019-04.pb.zip: 60.20 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 19759 | 19758 | 0 |
| Sampling | 30.61 | 30.61 | 0.00 |
| 2019-01-21 | 2825 | 2825 | 0 |
| 2019-01-22 | 2820 | 2820 | 0 |
| 2019-01-23 | 2818 | 2818 | 0 |
| 2019-01-24 | 2824 | 2824 | 0 |
| 2019-01-25 | 2824 | 2823 | 0 |
| 2019-01-26 | 2823 | 2823 | 0 |
| 2019-01-27 | 2825 | 2825 | 0 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.position.speed``
``entity.vehicle.trip.tripId``
``entity.vehicle.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
``entity.id``
``entity.tripUpdate.stopTimeUpdate.arrival.delay``
``entity.tripUpdate.stopTimeUpdate.arrival.uncertainty``
``entity.tripUpdate.stopTimeUpdate.departure.delay``
``entity.tripUpdate.stopTimeUpdate.departure.uncertainty``
``entity.tripUpdate.stopTimeUpdate.stopId``
``entity.tripUpdate.stopTimeUpdate.stopSequence``
``entity.tripUpdate.trip.tripId``
``entity.tripUpdate.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/HART-GTFSRT-2019-04.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/HART-GTFSRT-2019-04.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/HART-GTFSRT-2019-04.png)
