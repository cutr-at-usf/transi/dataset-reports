### HART-GTFSRT-2019-41
#### Metadata
HART-GTFSRT-2019-41.json.zip: 75.21 MB
HART-GTFSRT-2019-41.pb.zip: 67.51 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 19705 | 19703 | 0 |
| Sampling | 30.69 | 30.70 | 0.00 |
| 2019-10-07 | 2839 | 2839 | 0 |
| 2019-10-08 | 2841 | 2841 | 0 |
| 2019-10-09 | 2696 | 2694 | 0 |
| 2019-10-10 | 2811 | 2811 | 0 |
| 2019-10-11 | 2837 | 2837 | 0 |
| 2019-10-12 | 2840 | 2840 | 0 |
| 2019-10-13 | 2841 | 2841 | 0 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.position.speed``
``entity.vehicle.timestamp``
``entity.vehicle.trip.tripId``
``entity.vehicle.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
``entity.id``
``entity.tripUpdate.stopTimeUpdate.arrival.delay``
``entity.tripUpdate.stopTimeUpdate.arrival.uncertainty``
``entity.tripUpdate.stopTimeUpdate.departure.delay``
``entity.tripUpdate.stopTimeUpdate.departure.uncertainty``
``entity.tripUpdate.stopTimeUpdate.stopId``
``entity.tripUpdate.stopTimeUpdate.stopSequence``
``entity.tripUpdate.timestamp``
``entity.tripUpdate.trip.tripId``
``entity.tripUpdate.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/HART-GTFSRT-2019-41.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/HART-GTFSRT-2019-41.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/HART-GTFSRT-2019-41.png)
