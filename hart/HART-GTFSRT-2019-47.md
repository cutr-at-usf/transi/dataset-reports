### HART-GTFSRT-2019-47
#### Metadata
HART-GTFSRT-2019-47.json.zip: 75.79 MB
HART-GTFSRT-2019-47.pb.zip: 68.03 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 19789 | 19789 | 0 |
| Sampling | 30.56 | 30.56 | 0.00 |
| 2019-11-18 | 2823 | 2822 | 0 |
| 2019-11-19 | 2838 | 2839 | 0 |
| 2019-11-20 | 2777 | 2777 | 0 |
| 2019-11-21 | 2839 | 2839 | 0 |
| 2019-11-22 | 2836 | 2835 | 0 |
| 2019-11-23 | 2834 | 2834 | 0 |
| 2019-11-24 | 2842 | 2843 | 0 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.position.speed``
``entity.vehicle.timestamp``
``entity.vehicle.trip.tripId``
``entity.vehicle.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
``entity.id``
``entity.tripUpdate.stopTimeUpdate.arrival.delay``
``entity.tripUpdate.stopTimeUpdate.arrival.uncertainty``
``entity.tripUpdate.stopTimeUpdate.departure.delay``
``entity.tripUpdate.stopTimeUpdate.departure.uncertainty``
``entity.tripUpdate.stopTimeUpdate.stopId``
``entity.tripUpdate.stopTimeUpdate.stopSequence``
``entity.tripUpdate.timestamp``
``entity.tripUpdate.trip.tripId``
``entity.tripUpdate.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/HART-GTFSRT-2019-47.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/HART-GTFSRT-2019-47.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/HART-GTFSRT-2019-47.png)
