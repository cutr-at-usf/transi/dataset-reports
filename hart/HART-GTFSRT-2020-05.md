### HART-GTFSRT-2020-05
#### Metadata
HART-GTFSRT-2020-05.json.zip: 75.63 MB
HART-GTFSRT-2020-05.pb.zip: 67.78 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 19800 | 19802 | 0 |
| Sampling | 30.55 | 30.54 | 0.00 |
| 2020-01-27 | 2837 | 2839 | 0 |
| 2020-01-28 | 2835 | 2835 | 0 |
| 2020-01-29 | 2786 | 2786 | 0 |
| 2020-01-30 | 2842 | 2842 | 0 |
| 2020-01-31 | 2840 | 2840 | 0 |
| 2020-02-01 | 2831 | 2831 | 0 |
| 2020-02-02 | 2829 | 2829 | 0 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.position.speed``
``entity.vehicle.timestamp``
``entity.vehicle.trip.tripId``
``entity.vehicle.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
``entity.id``
``entity.tripUpdate.stopTimeUpdate.arrival.delay``
``entity.tripUpdate.stopTimeUpdate.arrival.uncertainty``
``entity.tripUpdate.stopTimeUpdate.departure.delay``
``entity.tripUpdate.stopTimeUpdate.departure.uncertainty``
``entity.tripUpdate.stopTimeUpdate.stopId``
``entity.tripUpdate.stopTimeUpdate.stopSequence``
``entity.tripUpdate.timestamp``
``entity.tripUpdate.trip.tripId``
``entity.tripUpdate.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/HART-GTFSRT-2020-05.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/HART-GTFSRT-2020-05.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/HART-GTFSRT-2020-05.png)
