### HART-GTFSRT-2019-07
#### Metadata
HART-GTFSRT-2019-07.json.zip: 68.94 MB
HART-GTFSRT-2019-07.pb.zip: 60.91 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 19847 | 19842 | 0 |
| Sampling | 30.47 | 30.48 | 0.00 |
| 2019-02-11 | 2838 | 2837 | 0 |
| 2019-02-12 | 2818 | 2814 | 0 |
| 2019-02-13 | 2836 | 2837 | 0 |
| 2019-02-14 | 2836 | 2836 | 0 |
| 2019-02-15 | 2836 | 2836 | 0 |
| 2019-02-16 | 2841 | 2841 | 0 |
| 2019-02-17 | 2842 | 2841 | 0 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.position.speed``
``entity.vehicle.trip.tripId``
``entity.vehicle.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
``entity.id``
``entity.tripUpdate.stopTimeUpdate.arrival.delay``
``entity.tripUpdate.stopTimeUpdate.arrival.uncertainty``
``entity.tripUpdate.stopTimeUpdate.departure.delay``
``entity.tripUpdate.stopTimeUpdate.departure.uncertainty``
``entity.tripUpdate.stopTimeUpdate.stopId``
``entity.tripUpdate.stopTimeUpdate.stopSequence``
``entity.tripUpdate.trip.tripId``
``entity.tripUpdate.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/HART-GTFSRT-2019-07.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/HART-GTFSRT-2019-07.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/HART-GTFSRT-2019-07.png)
