### HART-GTFSRT-2018-47
#### Metadata
HART-GTFSRT-2018-47.json.zip: 64.41 MB
HART-GTFSRT-2018-47.pb.zip: 56.85 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 19848 | 19849 | 0 |
| Sampling | 30.47 | 30.47 | 0.00 |
| 2018-11-19 | 2833 | 2833 | 0 |
| 2018-11-20 | 2829 | 2830 | 0 |
| 2018-11-21 | 2837 | 2837 | 0 |
| 2018-11-22 | 2837 | 2837 | 0 |
| 2018-11-23 | 2837 | 2837 | 0 |
| 2018-11-24 | 2838 | 2838 | 0 |
| 2018-11-25 | 2837 | 2837 | 0 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.position.speed``
``entity.vehicle.trip.tripId``
``entity.vehicle.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
``entity.id``
``entity.tripUpdate.stopTimeUpdate.arrival.delay``
``entity.tripUpdate.stopTimeUpdate.arrival.uncertainty``
``entity.tripUpdate.stopTimeUpdate.departure.delay``
``entity.tripUpdate.stopTimeUpdate.departure.uncertainty``
``entity.tripUpdate.stopTimeUpdate.stopId``
``entity.tripUpdate.stopTimeUpdate.stopSequence``
``entity.tripUpdate.trip.tripId``
``entity.tripUpdate.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/HART-GTFSRT-2018-47.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/HART-GTFSRT-2018-47.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/HART-GTFSRT-2018-47.png)
