### HART-GTFSRT-2017-47
#### Metadata
HART-GTFSRT-2017-47.json.zip: 56.90 MB
HART-GTFSRT-2017-47.pb.zip: 49.74 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 19895 | 19895 | 0 |
| Sampling | 30.40 | 30.40 | 0.00 |
| 2017-11-20 | 2843 | 2843 | 0 |
| 2017-11-21 | 2842 | 2842 | 0 |
| 2017-11-22 | 2839 | 2839 | 0 |
| 2017-11-23 | 2840 | 2840 | 0 |
| 2017-11-24 | 2837 | 2837 | 0 |
| 2017-11-25 | 2846 | 2846 | 0 |
| 2017-11-26 | 2848 | 2848 | 0 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.position.speed``
``entity.vehicle.trip.tripId``
``entity.vehicle.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
``entity.id``
``entity.tripUpdate.stopTimeUpdate.arrival.delay``
``entity.tripUpdate.stopTimeUpdate.arrival.uncertainty``
``entity.tripUpdate.stopTimeUpdate.departure.delay``
``entity.tripUpdate.stopTimeUpdate.departure.uncertainty``
``entity.tripUpdate.stopTimeUpdate.stopId``
``entity.tripUpdate.stopTimeUpdate.stopSequence``
``entity.tripUpdate.trip.tripId``
``entity.tripUpdate.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/HART-GTFSRT-2017-47.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/HART-GTFSRT-2017-47.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/HART-GTFSRT-2017-47.png)
