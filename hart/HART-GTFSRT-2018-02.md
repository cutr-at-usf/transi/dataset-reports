### HART-GTFSRT-2018-02
#### Metadata
HART-GTFSRT-2018-02.json.zip: 61.43 MB
HART-GTFSRT-2018-02.pb.zip: 54.30 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 19149 | 19149 | 0 |
| Sampling | 31.58 | 31.58 | 0.00 |
| 2018-01-08 | 2844 | 2844 | 0 |
| 2018-01-09 | 2840 | 2840 | 0 |
| 2018-01-10 | 2842 | 2841 | 0 |
| 2018-01-11 | 2843 | 2843 | 0 |
| 2018-01-12 | 2834 | 2835 | 0 |
| 2018-01-13 | 2117 | 2117 | 0 |
| 2018-01-14 | 2829 | 2829 | 0 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.position.speed``
``entity.vehicle.trip.tripId``
``entity.vehicle.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
``entity.id``
``entity.tripUpdate.stopTimeUpdate.arrival.delay``
``entity.tripUpdate.stopTimeUpdate.arrival.uncertainty``
``entity.tripUpdate.stopTimeUpdate.departure.delay``
``entity.tripUpdate.stopTimeUpdate.departure.uncertainty``
``entity.tripUpdate.stopTimeUpdate.stopId``
``entity.tripUpdate.stopTimeUpdate.stopSequence``
``entity.tripUpdate.trip.tripId``
``entity.tripUpdate.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/HART-GTFSRT-2018-02.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/HART-GTFSRT-2018-02.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/HART-GTFSRT-2018-02.png)
