### HART-GTFSRT-2019-38
#### Metadata
HART-GTFSRT-2019-38.json.zip: 75.60 MB
HART-GTFSRT-2019-38.pb.zip: 67.83 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 19814 | 19809 | 0 |
| Sampling | 30.52 | 30.53 | 0.00 |
| 2019-09-16 | 2834 | 2833 | 0 |
| 2019-09-17 | 2837 | 2837 | 0 |
| 2019-09-18 | 2788 | 2786 | 0 |
| 2019-09-19 | 2837 | 2837 | 0 |
| 2019-09-20 | 2836 | 2834 | 0 |
| 2019-09-21 | 2842 | 2841 | 0 |
| 2019-09-22 | 2840 | 2841 | 0 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.position.speed``
``entity.vehicle.timestamp``
``entity.vehicle.trip.tripId``
``entity.vehicle.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
``entity.id``
``entity.tripUpdate.stopTimeUpdate.arrival.delay``
``entity.tripUpdate.stopTimeUpdate.arrival.uncertainty``
``entity.tripUpdate.stopTimeUpdate.departure.delay``
``entity.tripUpdate.stopTimeUpdate.departure.uncertainty``
``entity.tripUpdate.stopTimeUpdate.stopId``
``entity.tripUpdate.stopTimeUpdate.stopSequence``
``entity.tripUpdate.timestamp``
``entity.tripUpdate.trip.tripId``
``entity.tripUpdate.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/HART-GTFSRT-2019-38.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/HART-GTFSRT-2019-38.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/HART-GTFSRT-2019-38.png)
