### HART-GTFSRT-2018-12
#### Metadata
HART-GTFSRT-2018-12.json.zip: 63.77 MB
HART-GTFSRT-2018-12.pb.zip: 56.40 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 19420 | 19397 | 0 |
| Sampling | 31.14 | 31.18 | 0.00 |
| 2018-03-19 | 2826 | 2816 | 0 |
| 2018-03-20 | 2818 | 2802 | 0 |
| 2018-03-21 | 2774 | 2774 | 0 |
| 2018-03-22 | 2739 | 2741 | 0 |
| 2018-03-23 | 2632 | 2633 | 0 |
| 2018-03-24 | 2790 | 2790 | 0 |
| 2018-03-25 | 2841 | 2841 | 0 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.position.speed``
``entity.vehicle.trip.tripId``
``entity.vehicle.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
``entity.id``
``entity.tripUpdate.stopTimeUpdate.arrival.delay``
``entity.tripUpdate.stopTimeUpdate.arrival.uncertainty``
``entity.tripUpdate.stopTimeUpdate.departure.delay``
``entity.tripUpdate.stopTimeUpdate.departure.uncertainty``
``entity.tripUpdate.stopTimeUpdate.stopId``
``entity.tripUpdate.stopTimeUpdate.stopSequence``
``entity.tripUpdate.trip.tripId``
``entity.tripUpdate.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/HART-GTFSRT-2018-12.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/HART-GTFSRT-2018-12.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/HART-GTFSRT-2018-12.png)
