### HART-GTFSRT-2018-05
#### Metadata
HART-GTFSRT-2018-05.json.zip: 63.73 MB
HART-GTFSRT-2018-05.pb.zip: 56.29 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 19845 | 19844 | 0 |
| Sampling | 30.48 | 30.48 | 0.00 |
| 2018-01-29 | 2838 | 2838 | 0 |
| 2018-01-30 | 2838 | 2838 | 0 |
| 2018-01-31 | 2825 | 2825 | 0 |
| 2018-02-01 | 2838 | 2838 | 0 |
| 2018-02-02 | 2840 | 2839 | 0 |
| 2018-02-03 | 2844 | 2844 | 0 |
| 2018-02-04 | 2822 | 2822 | 0 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.position.speed``
``entity.vehicle.trip.tripId``
``entity.vehicle.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
``entity.id``
``entity.tripUpdate.stopTimeUpdate.arrival.delay``
``entity.tripUpdate.stopTimeUpdate.arrival.uncertainty``
``entity.tripUpdate.stopTimeUpdate.departure.delay``
``entity.tripUpdate.stopTimeUpdate.departure.uncertainty``
``entity.tripUpdate.stopTimeUpdate.stopId``
``entity.tripUpdate.stopTimeUpdate.stopSequence``
``entity.tripUpdate.trip.tripId``
``entity.tripUpdate.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/HART-GTFSRT-2018-05.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/HART-GTFSRT-2018-05.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/HART-GTFSRT-2018-05.png)
