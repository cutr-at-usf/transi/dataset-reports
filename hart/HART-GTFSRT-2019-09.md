### HART-GTFSRT-2019-09
#### Metadata
HART-GTFSRT-2019-09.json.zip: 66.61 MB
HART-GTFSRT-2019-09.pb.zip: 58.73 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 19822 | 19811 | 0 |
| Sampling | 30.51 | 30.53 | 0.00 |
| 2019-02-25 | 2839 | 2839 | 0 |
| 2019-02-26 | 2838 | 2838 | 0 |
| 2019-02-27 | 2836 | 2836 | 0 |
| 2019-02-28 | 2835 | 2835 | 0 |
| 2019-03-01 | 2835 | 2835 | 0 |
| 2019-03-02 | 2840 | 2840 | 0 |
| 2019-03-03 | 2799 | 2788 | 0 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.position.speed``
``entity.vehicle.trip.tripId``
``entity.vehicle.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
``entity.id``
``entity.tripUpdate.stopTimeUpdate.arrival.delay``
``entity.tripUpdate.stopTimeUpdate.arrival.uncertainty``
``entity.tripUpdate.stopTimeUpdate.departure.delay``
``entity.tripUpdate.stopTimeUpdate.departure.uncertainty``
``entity.tripUpdate.stopTimeUpdate.stopId``
``entity.tripUpdate.stopTimeUpdate.stopSequence``
``entity.tripUpdate.trip.tripId``
``entity.tripUpdate.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/HART-GTFSRT-2019-09.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/HART-GTFSRT-2019-09.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/HART-GTFSRT-2019-09.png)
