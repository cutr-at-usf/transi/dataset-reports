### HART-GTFSRT-2019-32
#### Metadata
HART-GTFSRT-2019-32.json.zip: 72.23 MB
HART-GTFSRT-2019-32.pb.zip: 64.11 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 19806 | 19806 | 0 |
| Sampling | 30.54 | 30.54 | 0.00 |
| 2019-08-05 | 2837 | 2837 | 0 |
| 2019-08-06 | 2836 | 2836 | 0 |
| 2019-08-07 | 2838 | 2838 | 0 |
| 2019-08-08 | 2821 | 2821 | 0 |
| 2019-08-09 | 2837 | 2837 | 0 |
| 2019-08-10 | 2832 | 2832 | 0 |
| 2019-08-11 | 2805 | 2805 | 0 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.position.speed``
``entity.vehicle.trip.tripId``
``entity.vehicle.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
``entity.id``
``entity.tripUpdate.stopTimeUpdate.arrival.delay``
``entity.tripUpdate.stopTimeUpdate.arrival.uncertainty``
``entity.tripUpdate.stopTimeUpdate.departure.delay``
``entity.tripUpdate.stopTimeUpdate.departure.uncertainty``
``entity.tripUpdate.stopTimeUpdate.stopId``
``entity.tripUpdate.stopTimeUpdate.stopSequence``
``entity.tripUpdate.trip.tripId``
``entity.tripUpdate.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/HART-GTFSRT-2019-32.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/HART-GTFSRT-2019-32.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/HART-GTFSRT-2019-32.png)
