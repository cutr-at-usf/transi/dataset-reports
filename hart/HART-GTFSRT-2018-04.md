### HART-GTFSRT-2018-04
#### Metadata
HART-GTFSRT-2018-04.json.zip: 57.87 MB
HART-GTFSRT-2018-04.pb.zip: 51.06 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 18550 | 18550 | 0 |
| Sampling | 32.60 | 32.60 | 0.00 |
| 2018-01-22 | 2798 | 2798 | 0 |
| 2018-01-23 | 1934 | 1934 | 0 |
| 2018-01-24 | 2696 | 2697 | 0 |
| 2018-01-25 | 2841 | 2840 | 0 |
| 2018-01-26 | 2845 | 2845 | 0 |
| 2018-01-27 | 2692 | 2692 | 0 |
| 2018-01-28 | 2744 | 2744 | 0 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.position.speed``
``entity.vehicle.trip.tripId``
``entity.vehicle.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
``entity.id``
``entity.tripUpdate.stopTimeUpdate.arrival.delay``
``entity.tripUpdate.stopTimeUpdate.arrival.uncertainty``
``entity.tripUpdate.stopTimeUpdate.departure.delay``
``entity.tripUpdate.stopTimeUpdate.departure.uncertainty``
``entity.tripUpdate.stopTimeUpdate.stopId``
``entity.tripUpdate.stopTimeUpdate.stopSequence``
``entity.tripUpdate.trip.tripId``
``entity.tripUpdate.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/HART-GTFSRT-2018-04.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/HART-GTFSRT-2018-04.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/HART-GTFSRT-2018-04.png)
