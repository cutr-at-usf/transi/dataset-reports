### HART-GTFSRT-2018-31
#### Metadata
HART-GTFSRT-2018-31.json.zip: 68.25 MB
HART-GTFSRT-2018-31.pb.zip: 60.50 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 19772 | 19769 | 0 |
| Sampling | 30.59 | 30.59 | 0.00 |
| 2018-07-30 | 2842 | 2842 | 0 |
| 2018-07-31 | 2843 | 2843 | 0 |
| 2018-08-01 | 2748 | 2748 | 0 |
| 2018-08-02 | 2840 | 2839 | 0 |
| 2018-08-03 | 2831 | 2830 | 0 |
| 2018-08-04 | 2840 | 2841 | 0 |
| 2018-08-05 | 2828 | 2826 | 0 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.position.speed``
``entity.vehicle.trip.tripId``
``entity.vehicle.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
``entity.id``
``entity.tripUpdate.stopTimeUpdate.arrival.delay``
``entity.tripUpdate.stopTimeUpdate.arrival.uncertainty``
``entity.tripUpdate.stopTimeUpdate.departure.delay``
``entity.tripUpdate.stopTimeUpdate.departure.uncertainty``
``entity.tripUpdate.stopTimeUpdate.stopId``
``entity.tripUpdate.stopTimeUpdate.stopSequence``
``entity.tripUpdate.trip.tripId``
``entity.tripUpdate.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/HART-GTFSRT-2018-31.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/HART-GTFSRT-2018-31.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/HART-GTFSRT-2018-31.png)
