### HART-GTFSRT-2020-15
#### Metadata
HART-GTFSRT-2020-15.json.zip: 51.39 MB
HART-GTFSRT-2020-15.pb.zip: 44.54 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 19755 | 19753 | 0 |
| Sampling | 30.62 | 30.62 | 0.00 |
| 2020-04-06 | 2841 | 2841 | 0 |
| 2020-04-07 | 2842 | 2840 | 0 |
| 2020-04-08 | 2712 | 2712 | 0 |
| 2020-04-09 | 2843 | 2843 | 0 |
| 2020-04-10 | 2833 | 2833 | 0 |
| 2020-04-11 | 2842 | 2842 | 0 |
| 2020-04-12 | 2842 | 2842 | 0 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.position.speed``
``entity.vehicle.timestamp``
``entity.vehicle.trip.tripId``
``entity.vehicle.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
``entity.id``
``entity.tripUpdate.stopTimeUpdate.arrival.delay``
``entity.tripUpdate.stopTimeUpdate.arrival.uncertainty``
``entity.tripUpdate.stopTimeUpdate.departure.delay``
``entity.tripUpdate.stopTimeUpdate.departure.uncertainty``
``entity.tripUpdate.stopTimeUpdate.stopId``
``entity.tripUpdate.stopTimeUpdate.stopSequence``
``entity.tripUpdate.timestamp``
``entity.tripUpdate.trip.tripId``
``entity.tripUpdate.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/HART-GTFSRT-2020-15.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/HART-GTFSRT-2020-15.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/HART-GTFSRT-2020-15.png)
