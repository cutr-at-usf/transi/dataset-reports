### HART-GTFSRT-2019-26
#### Metadata
HART-GTFSRT-2019-26.json.zip: 68.94 MB
HART-GTFSRT-2019-26.pb.zip: 60.89 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 19826 | 19825 | 0 |
| Sampling | 30.51 | 30.51 | 0.00 |
| 2019-06-24 | 2830 | 2829 | 0 |
| 2019-06-25 | 2820 | 2820 | 0 |
| 2019-06-26 | 2828 | 2828 | 0 |
| 2019-06-27 | 2835 | 2835 | 0 |
| 2019-06-28 | 2834 | 2834 | 0 |
| 2019-06-29 | 2839 | 2839 | 0 |
| 2019-06-30 | 2840 | 2840 | 0 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.position.speed``
``entity.vehicle.trip.tripId``
``entity.vehicle.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
``entity.id``
``entity.tripUpdate.stopTimeUpdate.arrival.delay``
``entity.tripUpdate.stopTimeUpdate.arrival.uncertainty``
``entity.tripUpdate.stopTimeUpdate.departure.delay``
``entity.tripUpdate.stopTimeUpdate.departure.uncertainty``
``entity.tripUpdate.stopTimeUpdate.stopId``
``entity.tripUpdate.stopTimeUpdate.stopSequence``
``entity.tripUpdate.trip.tripId``
``entity.tripUpdate.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/HART-GTFSRT-2019-26.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/HART-GTFSRT-2019-26.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/HART-GTFSRT-2019-26.png)
