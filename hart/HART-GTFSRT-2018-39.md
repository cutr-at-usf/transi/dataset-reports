### HART-GTFSRT-2018-39
#### Metadata
HART-GTFSRT-2018-39.json.zip: 68.50 MB
HART-GTFSRT-2018-39.pb.zip: 60.82 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 19824 | 19825 | 0 |
| Sampling | 30.51 | 30.51 | 0.00 |
| 2018-09-24 | 2836 | 2836 | 0 |
| 2018-09-25 | 2835 | 2835 | 0 |
| 2018-09-26 | 2820 | 2821 | 0 |
| 2018-09-27 | 2829 | 2829 | 0 |
| 2018-09-28 | 2831 | 2831 | 0 |
| 2018-09-29 | 2834 | 2834 | 0 |
| 2018-09-30 | 2839 | 2839 | 0 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.position.speed``
``entity.vehicle.trip.tripId``
``entity.vehicle.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
``entity.id``
``entity.tripUpdate.stopTimeUpdate.arrival.delay``
``entity.tripUpdate.stopTimeUpdate.arrival.uncertainty``
``entity.tripUpdate.stopTimeUpdate.departure.delay``
``entity.tripUpdate.stopTimeUpdate.departure.uncertainty``
``entity.tripUpdate.stopTimeUpdate.stopId``
``entity.tripUpdate.stopTimeUpdate.stopSequence``
``entity.tripUpdate.trip.tripId``
``entity.tripUpdate.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/HART-GTFSRT-2018-39.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/HART-GTFSRT-2018-39.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/HART-GTFSRT-2018-39.png)
