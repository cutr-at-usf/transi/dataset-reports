### HART-GTFSRT-2018-06
#### Metadata
HART-GTFSRT-2018-06.json.zip: 62.78 MB
HART-GTFSRT-2018-06.pb.zip: 55.52 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 19480 | 19479 | 0 |
| Sampling | 31.05 | 31.05 | 0.00 |
| 2018-02-05 | 2841 | 2841 | 0 |
| 2018-02-06 | 2793 | 2793 | 0 |
| 2018-02-07 | 2756 | 2756 | 0 |
| 2018-02-08 | 2754 | 2753 | 0 |
| 2018-02-09 | 2813 | 2813 | 0 |
| 2018-02-10 | 2782 | 2782 | 0 |
| 2018-02-11 | 2741 | 2741 | 0 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.position.speed``
``entity.vehicle.trip.tripId``
``entity.vehicle.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
``entity.id``
``entity.tripUpdate.stopTimeUpdate.arrival.delay``
``entity.tripUpdate.stopTimeUpdate.arrival.uncertainty``
``entity.tripUpdate.stopTimeUpdate.departure.delay``
``entity.tripUpdate.stopTimeUpdate.departure.uncertainty``
``entity.tripUpdate.stopTimeUpdate.stopId``
``entity.tripUpdate.stopTimeUpdate.stopSequence``
``entity.tripUpdate.trip.tripId``
``entity.tripUpdate.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/HART-GTFSRT-2018-06.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/HART-GTFSRT-2018-06.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/HART-GTFSRT-2018-06.png)
