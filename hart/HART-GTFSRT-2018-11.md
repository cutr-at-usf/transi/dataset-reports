### HART-GTFSRT-2018-11
#### Metadata
HART-GTFSRT-2018-11.json.zip: 65.58 MB
HART-GTFSRT-2018-11.pb.zip: 58.01 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 19855 | 19842 | 0 |
| Sampling | 30.46 | 30.48 | 0.00 |
| 2018-03-12 | 2839 | 2839 | 0 |
| 2018-03-13 | 2840 | 2840 | 0 |
| 2018-03-14 | 2838 | 2838 | 0 |
| 2018-03-15 | 2817 | 2804 | 0 |
| 2018-03-16 | 2838 | 2838 | 0 |
| 2018-03-17 | 2841 | 2841 | 0 |
| 2018-03-18 | 2842 | 2842 | 0 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.position.speed``
``entity.vehicle.trip.tripId``
``entity.vehicle.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
``entity.id``
``entity.tripUpdate.stopTimeUpdate.arrival.delay``
``entity.tripUpdate.stopTimeUpdate.arrival.uncertainty``
``entity.tripUpdate.stopTimeUpdate.departure.delay``
``entity.tripUpdate.stopTimeUpdate.departure.uncertainty``
``entity.tripUpdate.stopTimeUpdate.stopId``
``entity.tripUpdate.stopTimeUpdate.stopSequence``
``entity.tripUpdate.trip.tripId``
``entity.tripUpdate.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/HART-GTFSRT-2018-11.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/HART-GTFSRT-2018-11.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/HART-GTFSRT-2018-11.png)
