### HART-GTFSRT-2018-30
#### Metadata
HART-GTFSRT-2018-30.json.zip: 68.45 MB
HART-GTFSRT-2018-30.pb.zip: 60.69 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 19778 | 19777 | 0 |
| Sampling | 30.58 | 30.58 | 0.00 |
| 2018-07-23 | 2841 | 2841 | 0 |
| 2018-07-24 | 2842 | 2842 | 0 |
| 2018-07-25 | 2728 | 2727 | 0 |
| 2018-07-26 | 2844 | 2844 | 0 |
| 2018-07-27 | 2839 | 2839 | 0 |
| 2018-07-28 | 2841 | 2841 | 0 |
| 2018-07-29 | 2843 | 2843 | 0 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.position.speed``
``entity.vehicle.trip.tripId``
``entity.vehicle.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
``entity.id``
``entity.tripUpdate.stopTimeUpdate.arrival.delay``
``entity.tripUpdate.stopTimeUpdate.arrival.uncertainty``
``entity.tripUpdate.stopTimeUpdate.departure.delay``
``entity.tripUpdate.stopTimeUpdate.departure.uncertainty``
``entity.tripUpdate.stopTimeUpdate.stopId``
``entity.tripUpdate.stopTimeUpdate.stopSequence``
``entity.tripUpdate.trip.tripId``
``entity.tripUpdate.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/HART-GTFSRT-2018-30.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/HART-GTFSRT-2018-30.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/HART-GTFSRT-2018-30.png)
