### HART-GTFSRT-2018-16
#### Metadata
HART-GTFSRT-2018-16.json.zip: 64.46 MB
HART-GTFSRT-2018-16.pb.zip: 57.01 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 19616 | 19614 | 0 |
| Sampling | 30.83 | 30.84 | 0.00 |
| 2018-04-16 | 2839 | 2839 | 0 |
| 2018-04-17 | 2840 | 2840 | 0 |
| 2018-04-18 | 2590 | 2589 | 0 |
| 2018-04-19 | 2815 | 2815 | 0 |
| 2018-04-20 | 2843 | 2843 | 0 |
| 2018-04-21 | 2844 | 2844 | 0 |
| 2018-04-22 | 2845 | 2844 | 0 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.position.speed``
``entity.vehicle.trip.tripId``
``entity.vehicle.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
``entity.id``
``entity.tripUpdate.stopTimeUpdate.arrival.delay``
``entity.tripUpdate.stopTimeUpdate.arrival.uncertainty``
``entity.tripUpdate.stopTimeUpdate.departure.delay``
``entity.tripUpdate.stopTimeUpdate.departure.uncertainty``
``entity.tripUpdate.stopTimeUpdate.stopId``
``entity.tripUpdate.stopTimeUpdate.stopSequence``
``entity.tripUpdate.trip.tripId``
``entity.tripUpdate.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/HART-GTFSRT-2018-16.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/HART-GTFSRT-2018-16.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/HART-GTFSRT-2018-16.png)
