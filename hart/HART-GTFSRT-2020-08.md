### HART-GTFSRT-2020-08
#### Metadata
HART-GTFSRT-2020-08.json.zip: 75.73 MB
HART-GTFSRT-2020-08.pb.zip: 67.93 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 19849 | 19848 | 0 |
| Sampling | 30.47 | 30.47 | 0.00 |
| 2020-02-17 | 2835 | 2835 | 0 |
| 2020-02-18 | 2835 | 2835 | 0 |
| 2020-02-19 | 2831 | 2831 | 0 |
| 2020-02-20 | 2838 | 2837 | 0 |
| 2020-02-21 | 2837 | 2837 | 0 |
| 2020-02-22 | 2840 | 2840 | 0 |
| 2020-02-23 | 2833 | 2833 | 0 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.position.speed``
``entity.vehicle.timestamp``
``entity.vehicle.trip.tripId``
``entity.vehicle.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
``entity.id``
``entity.tripUpdate.stopTimeUpdate.arrival.delay``
``entity.tripUpdate.stopTimeUpdate.arrival.uncertainty``
``entity.tripUpdate.stopTimeUpdate.departure.delay``
``entity.tripUpdate.stopTimeUpdate.departure.uncertainty``
``entity.tripUpdate.stopTimeUpdate.stopId``
``entity.tripUpdate.stopTimeUpdate.stopSequence``
``entity.tripUpdate.timestamp``
``entity.tripUpdate.trip.tripId``
``entity.tripUpdate.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/HART-GTFSRT-2020-08.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/HART-GTFSRT-2020-08.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/HART-GTFSRT-2020-08.png)
