### HART-GTFSRT-2018-26
#### Metadata
HART-GTFSRT-2018-26.json.zip: 65.87 MB
HART-GTFSRT-2018-26.pb.zip: 58.34 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 19726 | 19724 | 0 |
| Sampling | 30.66 | 30.66 | 0.00 |
| 2018-06-25 | 2839 | 2839 | 0 |
| 2018-06-26 | 2838 | 2838 | 0 |
| 2018-06-27 | 2724 | 2723 | 0 |
| 2018-06-28 | 2837 | 2837 | 0 |
| 2018-06-29 | 2839 | 2839 | 0 |
| 2018-06-30 | 2840 | 2839 | 0 |
| 2018-07-01 | 2809 | 2809 | 0 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.position.speed``
``entity.vehicle.trip.tripId``
``entity.vehicle.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
``entity.id``
``entity.tripUpdate.stopTimeUpdate.arrival.delay``
``entity.tripUpdate.stopTimeUpdate.arrival.uncertainty``
``entity.tripUpdate.stopTimeUpdate.departure.delay``
``entity.tripUpdate.stopTimeUpdate.departure.uncertainty``
``entity.tripUpdate.stopTimeUpdate.stopId``
``entity.tripUpdate.stopTimeUpdate.stopSequence``
``entity.tripUpdate.trip.tripId``
``entity.tripUpdate.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/HART-GTFSRT-2018-26.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/HART-GTFSRT-2018-26.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/HART-GTFSRT-2018-26.png)
