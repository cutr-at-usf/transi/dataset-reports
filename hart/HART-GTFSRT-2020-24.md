### HART-GTFSRT-2020-24
#### Metadata
HART-GTFSRT-2020-24.json.zip: 50.55 MB
HART-GTFSRT-2020-24.pb.zip: 43.66 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 19864 | 19865 | 0 |
| Sampling | 30.45 | 30.45 | 0.00 |
| 2020-06-08 | 2843 | 2843 | 0 |
| 2020-06-09 | 2845 | 2845 | 0 |
| 2020-06-10 | 2805 | 2806 | 0 |
| 2020-06-11 | 2828 | 2828 | 0 |
| 2020-06-12 | 2847 | 2847 | 0 |
| 2020-06-13 | 2848 | 2848 | 0 |
| 2020-06-14 | 2848 | 2848 | 0 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.position.speed``
``entity.vehicle.timestamp``
``entity.vehicle.trip.tripId``
``entity.vehicle.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
``entity.id``
``entity.tripUpdate.stopTimeUpdate.arrival.delay``
``entity.tripUpdate.stopTimeUpdate.arrival.uncertainty``
``entity.tripUpdate.stopTimeUpdate.departure.delay``
``entity.tripUpdate.stopTimeUpdate.departure.uncertainty``
``entity.tripUpdate.stopTimeUpdate.stopId``
``entity.tripUpdate.stopTimeUpdate.stopSequence``
``entity.tripUpdate.timestamp``
``entity.tripUpdate.trip.tripId``
``entity.tripUpdate.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/HART-GTFSRT-2020-24.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/HART-GTFSRT-2020-24.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/HART-GTFSRT-2020-24.png)
