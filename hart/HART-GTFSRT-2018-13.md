### HART-GTFSRT-2018-13
#### Metadata
HART-GTFSRT-2018-13.json.zip: 64.47 MB
HART-GTFSRT-2018-13.pb.zip: 57.13 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 19195 | 19194 | 0 |
| Sampling | 30.45 | 30.45 | 0.00 |
| 2018-03-26 | 2841 | 2841 | 0 |
| 2018-03-27 | 2838 | 2838 | 0 |
| 2018-03-28 | 2840 | 2839 | 0 |
| 2018-03-29 | 2836 | 2836 | 0 |
| 2018-03-30 | 2841 | 2841 | 0 |
| 2018-03-31 | 2843 | 2843 | 0 |
| 2018-04-01 | 2156 | 2156 | 0 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.position.speed``
``entity.vehicle.trip.tripId``
``entity.vehicle.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
``entity.id``
``entity.tripUpdate.stopTimeUpdate.arrival.delay``
``entity.tripUpdate.stopTimeUpdate.arrival.uncertainty``
``entity.tripUpdate.stopTimeUpdate.departure.delay``
``entity.tripUpdate.stopTimeUpdate.departure.uncertainty``
``entity.tripUpdate.stopTimeUpdate.stopId``
``entity.tripUpdate.stopTimeUpdate.stopSequence``
``entity.tripUpdate.trip.tripId``
``entity.tripUpdate.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/HART-GTFSRT-2018-13.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/HART-GTFSRT-2018-13.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/HART-GTFSRT-2018-13.png)
