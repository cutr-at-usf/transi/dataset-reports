### HART-GTFSRT-2018-18
#### Metadata
HART-GTFSRT-2018-18.json.zip: 66.10 MB
HART-GTFSRT-2018-18.pb.zip: 58.53 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 19787 | 19787 | 0 |
| Sampling | 30.57 | 30.57 | 0.00 |
| 2018-04-30 | 2842 | 2842 | 0 |
| 2018-05-01 | 2843 | 2843 | 0 |
| 2018-05-02 | 2839 | 2839 | 0 |
| 2018-05-03 | 2842 | 2842 | 0 |
| 2018-05-04 | 2838 | 2838 | 0 |
| 2018-05-05 | 2766 | 2766 | 0 |
| 2018-05-06 | 2817 | 2817 | 0 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.position.speed``
``entity.vehicle.trip.tripId``
``entity.vehicle.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
``entity.id``
``entity.tripUpdate.stopTimeUpdate.arrival.delay``
``entity.tripUpdate.stopTimeUpdate.arrival.uncertainty``
``entity.tripUpdate.stopTimeUpdate.departure.delay``
``entity.tripUpdate.stopTimeUpdate.departure.uncertainty``
``entity.tripUpdate.stopTimeUpdate.stopId``
``entity.tripUpdate.stopTimeUpdate.stopSequence``
``entity.tripUpdate.trip.tripId``
``entity.tripUpdate.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/HART-GTFSRT-2018-18.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/HART-GTFSRT-2018-18.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/HART-GTFSRT-2018-18.png)
