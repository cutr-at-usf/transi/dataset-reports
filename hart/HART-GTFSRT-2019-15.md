### HART-GTFSRT-2019-15
#### Metadata
HART-GTFSRT-2019-15.json.zip: 64.84 MB
HART-GTFSRT-2019-15.pb.zip: 57.27 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 18695 | 18689 | 0 |
| Sampling | 32.35 | 32.36 | 0.00 |
| 2019-04-08 | 2835 | 2835 | 0 |
| 2019-04-09 | 2831 | 2831 | 0 |
| 2019-04-10 | 2656 | 2650 | 0 |
| 2019-04-11 | 2445 | 2446 | 0 |
| 2019-04-12 | 2521 | 2521 | 0 |
| 2019-04-13 | 2632 | 2631 | 0 |
| 2019-04-14 | 2775 | 2775 | 0 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.position.speed``
``entity.vehicle.trip.tripId``
``entity.vehicle.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
``entity.id``
``entity.tripUpdate.stopTimeUpdate.arrival.delay``
``entity.tripUpdate.stopTimeUpdate.arrival.uncertainty``
``entity.tripUpdate.stopTimeUpdate.departure.delay``
``entity.tripUpdate.stopTimeUpdate.departure.uncertainty``
``entity.tripUpdate.stopTimeUpdate.stopId``
``entity.tripUpdate.stopTimeUpdate.stopSequence``
``entity.tripUpdate.trip.tripId``
``entity.tripUpdate.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/HART-GTFSRT-2019-15.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/HART-GTFSRT-2019-15.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/HART-GTFSRT-2019-15.png)
