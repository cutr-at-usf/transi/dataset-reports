### HART-GTFSRT-2019-21
#### Metadata
HART-GTFSRT-2019-21.json.zip: 67.39 MB
HART-GTFSRT-2019-21.pb.zip: 59.50 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 19507 | 19506 | 0 |
| Sampling | 31.00 | 31.01 | 0.00 |
| 2019-05-20 | 2834 | 2834 | 0 |
| 2019-05-21 | 2837 | 2837 | 0 |
| 2019-05-22 | 2580 | 2580 | 0 |
| 2019-05-23 | 2738 | 2737 | 0 |
| 2019-05-24 | 2837 | 2837 | 0 |
| 2019-05-25 | 2840 | 2840 | 0 |
| 2019-05-26 | 2841 | 2841 | 0 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.position.speed``
``entity.vehicle.trip.tripId``
``entity.vehicle.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
``entity.id``
``entity.tripUpdate.stopTimeUpdate.arrival.delay``
``entity.tripUpdate.stopTimeUpdate.arrival.uncertainty``
``entity.tripUpdate.stopTimeUpdate.departure.delay``
``entity.tripUpdate.stopTimeUpdate.departure.uncertainty``
``entity.tripUpdate.stopTimeUpdate.stopId``
``entity.tripUpdate.stopTimeUpdate.stopSequence``
``entity.tripUpdate.trip.tripId``
``entity.tripUpdate.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/HART-GTFSRT-2019-21.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/HART-GTFSRT-2019-21.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/HART-GTFSRT-2019-21.png)
