### HART-GTFSRT-2020-03
#### Metadata
HART-GTFSRT-2020-03.json.zip: 75.87 MB
HART-GTFSRT-2020-03.pb.zip: 68.05 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 19802 | 19800 | 0 |
| Sampling | 30.54 | 30.55 | 0.00 |
| 2020-01-13 | 2834 | 2834 | 0 |
| 2020-01-14 | 2835 | 2834 | 0 |
| 2020-01-15 | 2782 | 2782 | 0 |
| 2020-01-16 | 2837 | 2837 | 0 |
| 2020-01-17 | 2834 | 2833 | 0 |
| 2020-01-18 | 2837 | 2838 | 0 |
| 2020-01-19 | 2843 | 2842 | 0 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.position.speed``
``entity.vehicle.timestamp``
``entity.vehicle.trip.tripId``
``entity.vehicle.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
``entity.id``
``entity.tripUpdate.stopTimeUpdate.arrival.delay``
``entity.tripUpdate.stopTimeUpdate.arrival.uncertainty``
``entity.tripUpdate.stopTimeUpdate.departure.delay``
``entity.tripUpdate.stopTimeUpdate.departure.uncertainty``
``entity.tripUpdate.stopTimeUpdate.stopId``
``entity.tripUpdate.stopTimeUpdate.stopSequence``
``entity.tripUpdate.timestamp``
``entity.tripUpdate.trip.tripId``
``entity.tripUpdate.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/HART-GTFSRT-2020-03.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/HART-GTFSRT-2020-03.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/HART-GTFSRT-2020-03.png)
