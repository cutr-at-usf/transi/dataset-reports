### HART-GTFSRT-2018-28
#### Metadata
HART-GTFSRT-2018-28.json.zip: 68.27 MB
HART-GTFSRT-2018-28.pb.zip: 60.52 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 19779 | 19777 | 0 |
| Sampling | 30.58 | 30.58 | 0.00 |
| 2018-07-09 | 2840 | 2840 | 0 |
| 2018-07-10 | 2837 | 2835 | 0 |
| 2018-07-11 | 2731 | 2731 | 0 |
| 2018-07-12 | 2839 | 2839 | 0 |
| 2018-07-13 | 2843 | 2843 | 0 |
| 2018-07-14 | 2845 | 2845 | 0 |
| 2018-07-15 | 2844 | 2844 | 0 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.position.speed``
``entity.vehicle.trip.tripId``
``entity.vehicle.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
``entity.id``
``entity.tripUpdate.stopTimeUpdate.arrival.delay``
``entity.tripUpdate.stopTimeUpdate.arrival.uncertainty``
``entity.tripUpdate.stopTimeUpdate.departure.delay``
``entity.tripUpdate.stopTimeUpdate.departure.uncertainty``
``entity.tripUpdate.stopTimeUpdate.stopId``
``entity.tripUpdate.stopTimeUpdate.stopSequence``
``entity.tripUpdate.trip.tripId``
``entity.tripUpdate.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/HART-GTFSRT-2018-28.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/HART-GTFSRT-2018-28.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/HART-GTFSRT-2018-28.png)
