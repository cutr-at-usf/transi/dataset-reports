### HART-GTFSRT-2019-39
#### Metadata
HART-GTFSRT-2019-39.json.zip: 76.01 MB
HART-GTFSRT-2019-39.pb.zip: 68.20 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 19881 | 19881 | 0 |
| Sampling | 30.42 | 30.42 | 0.00 |
| 2019-09-23 | 2838 | 2838 | 0 |
| 2019-09-24 | 2840 | 2840 | 0 |
| 2019-09-25 | 2840 | 2840 | 0 |
| 2019-09-26 | 2840 | 2840 | 0 |
| 2019-09-27 | 2841 | 2841 | 0 |
| 2019-09-28 | 2840 | 2840 | 0 |
| 2019-09-29 | 2842 | 2842 | 0 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.position.speed``
``entity.vehicle.timestamp``
``entity.vehicle.trip.tripId``
``entity.vehicle.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
``entity.id``
``entity.tripUpdate.stopTimeUpdate.arrival.delay``
``entity.tripUpdate.stopTimeUpdate.arrival.uncertainty``
``entity.tripUpdate.stopTimeUpdate.departure.delay``
``entity.tripUpdate.stopTimeUpdate.departure.uncertainty``
``entity.tripUpdate.stopTimeUpdate.stopId``
``entity.tripUpdate.stopTimeUpdate.stopSequence``
``entity.tripUpdate.timestamp``
``entity.tripUpdate.trip.tripId``
``entity.tripUpdate.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/HART-GTFSRT-2019-39.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/HART-GTFSRT-2019-39.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/HART-GTFSRT-2019-39.png)
