### HART-GTFSRT-2018-52
#### Metadata
HART-GTFSRT-2018-52.json.zip: 65.05 MB
HART-GTFSRT-2018-52.pb.zip: 57.12 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 19874 | 19874 | 0 |
| Sampling | 30.43 | 30.43 | 0.00 |
| 2018-12-24 | 2839 | 2839 | 0 |
| 2018-12-25 | 2839 | 2840 | 0 |
| 2018-12-26 | 2839 | 2839 | 0 |
| 2018-12-27 | 2840 | 2840 | 0 |
| 2018-12-28 | 2837 | 2836 | 0 |
| 2018-12-29 | 2840 | 2840 | 0 |
| 2018-12-30 | 2840 | 2840 | 0 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.position.speed``
``entity.vehicle.trip.tripId``
``entity.vehicle.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
``entity.id``
``entity.tripUpdate.stopTimeUpdate.arrival.delay``
``entity.tripUpdate.stopTimeUpdate.arrival.uncertainty``
``entity.tripUpdate.stopTimeUpdate.departure.delay``
``entity.tripUpdate.stopTimeUpdate.departure.uncertainty``
``entity.tripUpdate.stopTimeUpdate.stopId``
``entity.tripUpdate.stopTimeUpdate.stopSequence``
``entity.tripUpdate.trip.tripId``
``entity.tripUpdate.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/HART-GTFSRT-2018-52.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/HART-GTFSRT-2018-52.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/HART-GTFSRT-2018-52.png)
