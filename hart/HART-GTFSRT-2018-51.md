### HART-GTFSRT-2018-51
#### Metadata
HART-GTFSRT-2018-51.json.zip: 68.93 MB
HART-GTFSRT-2018-51.pb.zip: 60.86 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 19879 | 19878 | 0 |
| Sampling | 30.42 | 30.43 | 0.00 |
| 2018-12-17 | 2840 | 2840 | 0 |
| 2018-12-18 | 2839 | 2839 | 0 |
| 2018-12-19 | 2839 | 2838 | 0 |
| 2018-12-20 | 2838 | 2838 | 0 |
| 2018-12-21 | 2839 | 2839 | 0 |
| 2018-12-22 | 2842 | 2842 | 0 |
| 2018-12-23 | 2842 | 2842 | 0 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.position.speed``
``entity.vehicle.trip.tripId``
``entity.vehicle.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
``entity.id``
``entity.tripUpdate.stopTimeUpdate.arrival.delay``
``entity.tripUpdate.stopTimeUpdate.arrival.uncertainty``
``entity.tripUpdate.stopTimeUpdate.departure.delay``
``entity.tripUpdate.stopTimeUpdate.departure.uncertainty``
``entity.tripUpdate.stopTimeUpdate.stopId``
``entity.tripUpdate.stopTimeUpdate.stopSequence``
``entity.tripUpdate.trip.tripId``
``entity.tripUpdate.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/HART-GTFSRT-2018-51.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/HART-GTFSRT-2018-51.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/HART-GTFSRT-2018-51.png)
