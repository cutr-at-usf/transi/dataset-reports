### HART-GTFSRT-2019-49
#### Metadata
HART-GTFSRT-2019-49.json.zip: 75.18 MB
HART-GTFSRT-2019-49.pb.zip: 67.41 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 19764 | 19762 | 0 |
| Sampling | 30.60 | 30.60 | 0.00 |
| 2019-12-02 | 2838 | 2838 | 0 |
| 2019-12-03 | 2836 | 2837 | 0 |
| 2019-12-04 | 2725 | 2724 | 0 |
| 2019-12-05 | 2841 | 2841 | 0 |
| 2019-12-06 | 2841 | 2841 | 0 |
| 2019-12-07 | 2841 | 2840 | 0 |
| 2019-12-08 | 2842 | 2841 | 0 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.position.speed``
``entity.vehicle.timestamp``
``entity.vehicle.trip.tripId``
``entity.vehicle.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
``entity.id``
``entity.tripUpdate.stopTimeUpdate.arrival.delay``
``entity.tripUpdate.stopTimeUpdate.arrival.uncertainty``
``entity.tripUpdate.stopTimeUpdate.departure.delay``
``entity.tripUpdate.stopTimeUpdate.departure.uncertainty``
``entity.tripUpdate.stopTimeUpdate.stopId``
``entity.tripUpdate.stopTimeUpdate.stopSequence``
``entity.tripUpdate.timestamp``
``entity.tripUpdate.trip.tripId``
``entity.tripUpdate.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/HART-GTFSRT-2019-49.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/HART-GTFSRT-2019-49.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/HART-GTFSRT-2019-49.png)
