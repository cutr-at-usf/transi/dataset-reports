### HART-GTFSRT-2019-16
#### Metadata
HART-GTFSRT-2019-16.json.zip: 60.90 MB
HART-GTFSRT-2019-16.pb.zip: 53.78 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 17590 | 17591 | 0 |
| Sampling | 34.38 | 34.38 | 0.00 |
| 2019-04-15 | 2698 | 2698 | 0 |
| 2019-04-16 | 2528 | 2529 | 0 |
| 2019-04-17 | 2524 | 2524 | 0 |
| 2019-04-18 | 2464 | 2465 | 0 |
| 2019-04-19 | 2393 | 2392 | 0 |
| 2019-04-20 | 2445 | 2445 | 0 |
| 2019-04-21 | 2538 | 2538 | 0 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.position.speed``
``entity.vehicle.trip.tripId``
``entity.vehicle.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
``entity.id``
``entity.tripUpdate.stopTimeUpdate.arrival.delay``
``entity.tripUpdate.stopTimeUpdate.arrival.uncertainty``
``entity.tripUpdate.stopTimeUpdate.departure.delay``
``entity.tripUpdate.stopTimeUpdate.departure.uncertainty``
``entity.tripUpdate.stopTimeUpdate.stopId``
``entity.tripUpdate.stopTimeUpdate.stopSequence``
``entity.tripUpdate.trip.tripId``
``entity.tripUpdate.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/HART-GTFSRT-2019-16.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/HART-GTFSRT-2019-16.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/HART-GTFSRT-2019-16.png)
