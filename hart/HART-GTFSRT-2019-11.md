### HART-GTFSRT-2019-11
#### Metadata
HART-GTFSRT-2019-11.json.zip: 68.52 MB
HART-GTFSRT-2019-11.pb.zip: 60.53 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 19718 | 19716 | 0 |
| Sampling | 30.67 | 30.68 | 0.00 |
| 2019-03-11 | 2840 | 2840 | 0 |
| 2019-03-12 | 2840 | 2839 | 0 |
| 2019-03-13 | 2678 | 2678 | 0 |
| 2019-03-14 | 2839 | 2839 | 0 |
| 2019-03-15 | 2840 | 2839 | 0 |
| 2019-03-16 | 2842 | 2842 | 0 |
| 2019-03-17 | 2839 | 2839 | 0 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.position.speed``
``entity.vehicle.trip.tripId``
``entity.vehicle.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
``entity.id``
``entity.tripUpdate.stopTimeUpdate.arrival.delay``
``entity.tripUpdate.stopTimeUpdate.arrival.uncertainty``
``entity.tripUpdate.stopTimeUpdate.departure.delay``
``entity.tripUpdate.stopTimeUpdate.departure.uncertainty``
``entity.tripUpdate.stopTimeUpdate.stopId``
``entity.tripUpdate.stopTimeUpdate.stopSequence``
``entity.tripUpdate.trip.tripId``
``entity.tripUpdate.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/HART-GTFSRT-2019-11.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/HART-GTFSRT-2019-11.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/HART-GTFSRT-2019-11.png)
