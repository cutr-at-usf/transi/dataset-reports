### HART-GTFSRT-2019-25
#### Metadata
HART-GTFSRT-2019-25.json.zip: 68.18 MB
HART-GTFSRT-2019-25.pb.zip: 60.19 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 19634 | 19634 | 0 |
| Sampling | 30.80 | 30.80 | 0.00 |
| 2019-06-17 | 2838 | 2838 | 0 |
| 2019-06-18 | 2839 | 2839 | 0 |
| 2019-06-19 | 2682 | 2682 | 0 |
| 2019-06-20 | 2835 | 2835 | 0 |
| 2019-06-21 | 2839 | 2839 | 0 |
| 2019-06-22 | 2766 | 2766 | 0 |
| 2019-06-23 | 2835 | 2835 | 0 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.position.speed``
``entity.vehicle.trip.tripId``
``entity.vehicle.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
``entity.id``
``entity.tripUpdate.stopTimeUpdate.arrival.delay``
``entity.tripUpdate.stopTimeUpdate.arrival.uncertainty``
``entity.tripUpdate.stopTimeUpdate.departure.delay``
``entity.tripUpdate.stopTimeUpdate.departure.uncertainty``
``entity.tripUpdate.stopTimeUpdate.stopId``
``entity.tripUpdate.stopTimeUpdate.stopSequence``
``entity.tripUpdate.trip.tripId``
``entity.tripUpdate.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/HART-GTFSRT-2019-25.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/HART-GTFSRT-2019-25.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/HART-GTFSRT-2019-25.png)
