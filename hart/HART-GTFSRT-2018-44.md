### HART-GTFSRT-2018-44
#### Metadata
HART-GTFSRT-2018-44.json.zip: 67.93 MB
HART-GTFSRT-2018-44.pb.zip: 60.21 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 19941 | 19941 | 0 |
| Sampling | 30.51 | 30.51 | 0.00 |
| 2018-10-29 | 2834 | 2834 | 0 |
| 2018-10-30 | 2835 | 2835 | 0 |
| 2018-10-31 | 2831 | 2831 | 0 |
| 2018-11-01 | 2832 | 2832 | 0 |
| 2018-11-02 | 2835 | 2835 | 0 |
| 2018-11-03 | 2841 | 2841 | 0 |
| 2018-11-04 | 2814 | 2814 | 0 |
| 2018-11-05 | 119 | 119 | 0 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.position.speed``
``entity.vehicle.trip.tripId``
``entity.vehicle.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
``entity.id``
``entity.tripUpdate.stopTimeUpdate.arrival.delay``
``entity.tripUpdate.stopTimeUpdate.arrival.uncertainty``
``entity.tripUpdate.stopTimeUpdate.departure.delay``
``entity.tripUpdate.stopTimeUpdate.departure.uncertainty``
``entity.tripUpdate.stopTimeUpdate.stopId``
``entity.tripUpdate.stopTimeUpdate.stopSequence``
``entity.tripUpdate.trip.tripId``
``entity.tripUpdate.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/HART-GTFSRT-2018-44.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/HART-GTFSRT-2018-44.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/HART-GTFSRT-2018-44.png)
