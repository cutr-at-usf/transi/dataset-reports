### HART-GTFSRT-2020-11
#### Metadata
HART-GTFSRT-2020-11.json.zip: 75.29 MB
HART-GTFSRT-2020-11.pb.zip: 67.50 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 19797 | 19801 | 0 |
| Sampling | 30.55 | 30.54 | 0.00 |
| 2020-03-09 | 2835 | 2834 | 0 |
| 2020-03-10 | 2837 | 2836 | 0 |
| 2020-03-11 | 2770 | 2771 | 0 |
| 2020-03-12 | 2842 | 2842 | 0 |
| 2020-03-13 | 2841 | 2841 | 0 |
| 2020-03-14 | 2843 | 2843 | 0 |
| 2020-03-15 | 2829 | 2834 | 0 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.position.speed``
``entity.vehicle.timestamp``
``entity.vehicle.trip.tripId``
``entity.vehicle.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
``entity.id``
``entity.tripUpdate.stopTimeUpdate.arrival.delay``
``entity.tripUpdate.stopTimeUpdate.arrival.uncertainty``
``entity.tripUpdate.stopTimeUpdate.departure.delay``
``entity.tripUpdate.stopTimeUpdate.departure.uncertainty``
``entity.tripUpdate.stopTimeUpdate.stopId``
``entity.tripUpdate.stopTimeUpdate.stopSequence``
``entity.tripUpdate.timestamp``
``entity.tripUpdate.trip.tripId``
``entity.tripUpdate.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/HART-GTFSRT-2020-11.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/HART-GTFSRT-2020-11.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/HART-GTFSRT-2020-11.png)
