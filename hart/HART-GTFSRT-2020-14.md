### HART-GTFSRT-2020-14
#### Metadata
HART-GTFSRT-2020-14.json.zip: 60.99 MB
HART-GTFSRT-2020-14.pb.zip: 53.73 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 19866 | 19864 | 0 |
| Sampling | 30.44 | 30.45 | 0.00 |
| 2020-03-30 | 2841 | 2841 | 0 |
| 2020-03-31 | 2840 | 2839 | 0 |
| 2020-04-01 | 2838 | 2838 | 0 |
| 2020-04-02 | 2836 | 2836 | 0 |
| 2020-04-03 | 2839 | 2838 | 0 |
| 2020-04-04 | 2841 | 2841 | 0 |
| 2020-04-05 | 2831 | 2831 | 0 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.position.speed``
``entity.vehicle.timestamp``
``entity.vehicle.trip.tripId``
``entity.vehicle.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
``entity.id``
``entity.tripUpdate.stopTimeUpdate.arrival.delay``
``entity.tripUpdate.stopTimeUpdate.arrival.uncertainty``
``entity.tripUpdate.stopTimeUpdate.departure.delay``
``entity.tripUpdate.stopTimeUpdate.departure.uncertainty``
``entity.tripUpdate.stopTimeUpdate.stopId``
``entity.tripUpdate.stopTimeUpdate.stopSequence``
``entity.tripUpdate.timestamp``
``entity.tripUpdate.trip.tripId``
``entity.tripUpdate.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/HART-GTFSRT-2020-14.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/HART-GTFSRT-2020-14.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/HART-GTFSRT-2020-14.png)
