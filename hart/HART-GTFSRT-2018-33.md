### HART-GTFSRT-2018-33
#### Metadata
HART-GTFSRT-2018-33.json.zip: 68.43 MB
HART-GTFSRT-2018-33.pb.zip: 60.74 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 19729 | 19729 | 0 |
| Sampling | 30.66 | 30.66 | 0.00 |
| 2018-08-13 | 2840 | 2840 | 0 |
| 2018-08-14 | 2835 | 2835 | 0 |
| 2018-08-15 | 2698 | 2698 | 0 |
| 2018-08-16 | 2837 | 2837 | 0 |
| 2018-08-17 | 2836 | 2836 | 0 |
| 2018-08-18 | 2841 | 2841 | 0 |
| 2018-08-19 | 2842 | 2842 | 0 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.position.speed``
``entity.vehicle.trip.tripId``
``entity.vehicle.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
``entity.id``
``entity.tripUpdate.stopTimeUpdate.arrival.delay``
``entity.tripUpdate.stopTimeUpdate.arrival.uncertainty``
``entity.tripUpdate.stopTimeUpdate.departure.delay``
``entity.tripUpdate.stopTimeUpdate.departure.uncertainty``
``entity.tripUpdate.stopTimeUpdate.stopId``
``entity.tripUpdate.stopTimeUpdate.stopSequence``
``entity.tripUpdate.trip.tripId``
``entity.tripUpdate.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/HART-GTFSRT-2018-33.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/HART-GTFSRT-2018-33.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/HART-GTFSRT-2018-33.png)
