### HART-GTFSRT-2020-13
#### Metadata
HART-GTFSRT-2020-13.json.zip: 74.63 MB
HART-GTFSRT-2020-13.pb.zip: 66.85 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 19756 | 19755 | 0 |
| Sampling | 30.61 | 30.61 | 0.00 |
| 2020-03-23 | 2841 | 2840 | 0 |
| 2020-03-24 | 2838 | 2838 | 0 |
| 2020-03-25 | 2715 | 2714 | 0 |
| 2020-03-26 | 2840 | 2841 | 0 |
| 2020-03-27 | 2842 | 2842 | 0 |
| 2020-03-28 | 2839 | 2839 | 0 |
| 2020-03-29 | 2841 | 2841 | 0 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.position.speed``
``entity.vehicle.timestamp``
``entity.vehicle.trip.tripId``
``entity.vehicle.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
``entity.id``
``entity.tripUpdate.stopTimeUpdate.arrival.delay``
``entity.tripUpdate.stopTimeUpdate.arrival.uncertainty``
``entity.tripUpdate.stopTimeUpdate.departure.delay``
``entity.tripUpdate.stopTimeUpdate.departure.uncertainty``
``entity.tripUpdate.stopTimeUpdate.stopId``
``entity.tripUpdate.stopTimeUpdate.stopSequence``
``entity.tripUpdate.timestamp``
``entity.tripUpdate.trip.tripId``
``entity.tripUpdate.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/HART-GTFSRT-2020-13.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/HART-GTFSRT-2020-13.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/HART-GTFSRT-2020-13.png)
