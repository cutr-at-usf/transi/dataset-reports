### HART-GTFSRT-2019-50
#### Metadata
HART-GTFSRT-2019-50.json.zip: 75.88 MB
HART-GTFSRT-2019-50.pb.zip: 68.04 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 19828 | 19825 | 0 |
| Sampling | 30.50 | 30.51 | 0.00 |
| 2019-12-09 | 2839 | 2839 | 0 |
| 2019-12-10 | 2837 | 2834 | 0 |
| 2019-12-11 | 2805 | 2805 | 0 |
| 2019-12-12 | 2830 | 2830 | 0 |
| 2019-12-13 | 2837 | 2837 | 0 |
| 2019-12-14 | 2841 | 2841 | 0 |
| 2019-12-15 | 2839 | 2839 | 0 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.position.speed``
``entity.vehicle.timestamp``
``entity.vehicle.trip.tripId``
``entity.vehicle.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
``entity.id``
``entity.tripUpdate.stopTimeUpdate.arrival.delay``
``entity.tripUpdate.stopTimeUpdate.arrival.uncertainty``
``entity.tripUpdate.stopTimeUpdate.departure.delay``
``entity.tripUpdate.stopTimeUpdate.departure.uncertainty``
``entity.tripUpdate.stopTimeUpdate.stopId``
``entity.tripUpdate.stopTimeUpdate.stopSequence``
``entity.tripUpdate.timestamp``
``entity.tripUpdate.trip.tripId``
``entity.tripUpdate.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/HART-GTFSRT-2019-50.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/HART-GTFSRT-2019-50.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/HART-GTFSRT-2019-50.png)
