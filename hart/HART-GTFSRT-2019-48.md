### HART-GTFSRT-2019-48
#### Metadata
HART-GTFSRT-2019-48.json.zip: 68.21 MB
HART-GTFSRT-2019-48.pb.zip: 60.75 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 19870 | 19868 | 0 |
| Sampling | 30.44 | 30.44 | 0.00 |
| 2019-11-25 | 2840 | 2840 | 0 |
| 2019-11-26 | 2838 | 2838 | 0 |
| 2019-11-27 | 2842 | 2841 | 0 |
| 2019-11-28 | 2841 | 2840 | 0 |
| 2019-11-29 | 2839 | 2839 | 0 |
| 2019-11-30 | 2840 | 2840 | 0 |
| 2019-12-01 | 2830 | 2830 | 0 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.position.speed``
``entity.vehicle.timestamp``
``entity.vehicle.trip.tripId``
``entity.vehicle.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
``entity.id``
``entity.tripUpdate.stopTimeUpdate.arrival.delay``
``entity.tripUpdate.stopTimeUpdate.arrival.uncertainty``
``entity.tripUpdate.stopTimeUpdate.departure.delay``
``entity.tripUpdate.stopTimeUpdate.departure.uncertainty``
``entity.tripUpdate.stopTimeUpdate.stopId``
``entity.tripUpdate.stopTimeUpdate.stopSequence``
``entity.tripUpdate.timestamp``
``entity.tripUpdate.trip.tripId``
``entity.tripUpdate.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/HART-GTFSRT-2019-48.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/HART-GTFSRT-2019-48.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/HART-GTFSRT-2019-48.png)
