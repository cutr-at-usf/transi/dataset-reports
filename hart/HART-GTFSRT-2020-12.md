### HART-GTFSRT-2020-12
#### Metadata
HART-GTFSRT-2020-12.json.zip: 75.18 MB
HART-GTFSRT-2020-12.pb.zip: 67.36 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 19882 | 19881 | 0 |
| Sampling | 30.42 | 30.42 | 0.00 |
| 2020-03-16 | 2841 | 2841 | 0 |
| 2020-03-17 | 2840 | 2840 | 0 |
| 2020-03-18 | 2836 | 2837 | 0 |
| 2020-03-19 | 2841 | 2840 | 0 |
| 2020-03-20 | 2842 | 2841 | 0 |
| 2020-03-21 | 2840 | 2840 | 0 |
| 2020-03-22 | 2842 | 2842 | 0 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.position.speed``
``entity.vehicle.timestamp``
``entity.vehicle.trip.tripId``
``entity.vehicle.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
``entity.id``
``entity.tripUpdate.stopTimeUpdate.arrival.delay``
``entity.tripUpdate.stopTimeUpdate.arrival.uncertainty``
``entity.tripUpdate.stopTimeUpdate.departure.delay``
``entity.tripUpdate.stopTimeUpdate.departure.uncertainty``
``entity.tripUpdate.stopTimeUpdate.stopId``
``entity.tripUpdate.stopTimeUpdate.stopSequence``
``entity.tripUpdate.timestamp``
``entity.tripUpdate.trip.tripId``
``entity.tripUpdate.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/HART-GTFSRT-2020-12.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/HART-GTFSRT-2020-12.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/HART-GTFSRT-2020-12.png)
