### HART-GTFSRT-2018-48
#### Metadata
HART-GTFSRT-2018-48.json.zip: 67.53 MB
HART-GTFSRT-2018-48.pb.zip: 59.80 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 19818 | 19819 | 0 |
| Sampling | 30.52 | 30.52 | 0.00 |
| 2018-11-26 | 2834 | 2834 | 0 |
| 2018-11-27 | 2832 | 2833 | 0 |
| 2018-11-28 | 2837 | 2837 | 0 |
| 2018-11-29 | 2830 | 2830 | 0 |
| 2018-11-30 | 2837 | 2837 | 0 |
| 2018-12-01 | 2839 | 2839 | 0 |
| 2018-12-02 | 2809 | 2809 | 0 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.position.speed``
``entity.vehicle.trip.tripId``
``entity.vehicle.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
``entity.id``
``entity.tripUpdate.stopTimeUpdate.arrival.delay``
``entity.tripUpdate.stopTimeUpdate.arrival.uncertainty``
``entity.tripUpdate.stopTimeUpdate.departure.delay``
``entity.tripUpdate.stopTimeUpdate.departure.uncertainty``
``entity.tripUpdate.stopTimeUpdate.stopId``
``entity.tripUpdate.stopTimeUpdate.stopSequence``
``entity.tripUpdate.trip.tripId``
``entity.tripUpdate.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/HART-GTFSRT-2018-48.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/HART-GTFSRT-2018-48.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/HART-GTFSRT-2018-48.png)
