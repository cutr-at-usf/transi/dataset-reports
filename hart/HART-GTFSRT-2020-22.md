### HART-GTFSRT-2020-22
#### Metadata
HART-GTFSRT-2020-22.json.zip: 48.48 MB
HART-GTFSRT-2020-22.pb.zip: 41.77 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 19782 | 19781 | 0 |
| Sampling | 30.57 | 30.57 | 0.00 |
| 2020-05-25 | 2846 | 2846 | 0 |
| 2020-05-26 | 2846 | 2846 | 0 |
| 2020-05-27 | 2702 | 2702 | 0 |
| 2020-05-28 | 2846 | 2845 | 0 |
| 2020-05-29 | 2847 | 2848 | 0 |
| 2020-05-30 | 2848 | 2847 | 0 |
| 2020-05-31 | 2847 | 2847 | 0 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.position.speed``
``entity.vehicle.timestamp``
``entity.vehicle.trip.tripId``
``entity.vehicle.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
``entity.id``
``entity.tripUpdate.stopTimeUpdate.arrival.delay``
``entity.tripUpdate.stopTimeUpdate.arrival.uncertainty``
``entity.tripUpdate.stopTimeUpdate.departure.delay``
``entity.tripUpdate.stopTimeUpdate.departure.uncertainty``
``entity.tripUpdate.stopTimeUpdate.stopId``
``entity.tripUpdate.stopTimeUpdate.stopSequence``
``entity.tripUpdate.timestamp``
``entity.tripUpdate.trip.tripId``
``entity.tripUpdate.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/HART-GTFSRT-2020-22.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/HART-GTFSRT-2020-22.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/HART-GTFSRT-2020-22.png)
