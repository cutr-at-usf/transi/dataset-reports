### HART-GTFSRT-2019-02
#### Metadata
HART-GTFSRT-2019-02.json.zip: 69.00 MB
HART-GTFSRT-2019-02.pb.zip: 60.94 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 19852 | 19849 | 0 |
| Sampling | 30.47 | 30.47 | 0.00 |
| 2019-01-07 | 2836 | 2835 | 0 |
| 2019-01-08 | 2836 | 2836 | 0 |
| 2019-01-09 | 2835 | 2835 | 0 |
| 2019-01-10 | 2835 | 2835 | 0 |
| 2019-01-11 | 2833 | 2833 | 0 |
| 2019-01-12 | 2835 | 2834 | 0 |
| 2019-01-13 | 2842 | 2841 | 0 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.position.speed``
``entity.vehicle.trip.tripId``
``entity.vehicle.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
``entity.id``
``entity.tripUpdate.stopTimeUpdate.arrival.delay``
``entity.tripUpdate.stopTimeUpdate.arrival.uncertainty``
``entity.tripUpdate.stopTimeUpdate.departure.delay``
``entity.tripUpdate.stopTimeUpdate.departure.uncertainty``
``entity.tripUpdate.stopTimeUpdate.stopId``
``entity.tripUpdate.stopTimeUpdate.stopSequence``
``entity.tripUpdate.trip.tripId``
``entity.tripUpdate.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/HART-GTFSRT-2019-02.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/HART-GTFSRT-2019-02.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/HART-GTFSRT-2019-02.png)
