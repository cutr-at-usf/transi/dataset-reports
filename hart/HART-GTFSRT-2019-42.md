### HART-GTFSRT-2019-42
#### Metadata
HART-GTFSRT-2019-42.json.zip: 75.85 MB
HART-GTFSRT-2019-42.pb.zip: 68.11 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 19567 | 19566 | 0 |
| Sampling | 30.91 | 30.91 | 0.00 |
| 2019-10-14 | 2838 | 2838 | 0 |
| 2019-10-15 | 2834 | 2834 | 0 |
| 2019-10-16 | 2837 | 2837 | 0 |
| 2019-10-17 | 2838 | 2838 | 0 |
| 2019-10-18 | 2840 | 2840 | 0 |
| 2019-10-19 | 2752 | 2751 | 0 |
| 2019-10-20 | 2628 | 2628 | 0 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.position.speed``
``entity.vehicle.timestamp``
``entity.vehicle.trip.tripId``
``entity.vehicle.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
``entity.id``
``entity.tripUpdate.stopTimeUpdate.arrival.delay``
``entity.tripUpdate.stopTimeUpdate.arrival.uncertainty``
``entity.tripUpdate.stopTimeUpdate.departure.delay``
``entity.tripUpdate.stopTimeUpdate.departure.uncertainty``
``entity.tripUpdate.stopTimeUpdate.stopId``
``entity.tripUpdate.stopTimeUpdate.stopSequence``
``entity.tripUpdate.timestamp``
``entity.tripUpdate.trip.tripId``
``entity.tripUpdate.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/HART-GTFSRT-2019-42.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/HART-GTFSRT-2019-42.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/HART-GTFSRT-2019-42.png)
