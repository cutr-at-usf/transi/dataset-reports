### HART-GTFSRT-2020-27
#### Metadata
HART-GTFSRT-2020-27.json.zip: 49.65 MB
HART-GTFSRT-2020-27.pb.zip: 42.83 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 19932 | 19932 | 0 |
| Sampling | 30.34 | 30.34 | 0.00 |
| 2020-06-29 | 2845 | 2845 | 0 |
| 2020-06-30 | 2848 | 2848 | 0 |
| 2020-07-01 | 2847 | 2847 | 0 |
| 2020-07-02 | 2849 | 2849 | 0 |
| 2020-07-03 | 2848 | 2848 | 0 |
| 2020-07-04 | 2847 | 2847 | 0 |
| 2020-07-05 | 2848 | 2848 | 0 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.position.speed``
``entity.vehicle.timestamp``
``entity.vehicle.trip.tripId``
``entity.vehicle.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
``entity.id``
``entity.tripUpdate.stopTimeUpdate.arrival.delay``
``entity.tripUpdate.stopTimeUpdate.arrival.uncertainty``
``entity.tripUpdate.stopTimeUpdate.departure.delay``
``entity.tripUpdate.stopTimeUpdate.departure.uncertainty``
``entity.tripUpdate.stopTimeUpdate.stopId``
``entity.tripUpdate.stopTimeUpdate.stopSequence``
``entity.tripUpdate.timestamp``
``entity.tripUpdate.trip.tripId``
``entity.tripUpdate.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/HART-GTFSRT-2020-27.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/HART-GTFSRT-2020-27.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/HART-GTFSRT-2020-27.png)
