### HART-GTFSRT-2019-34
#### Metadata
HART-GTFSRT-2019-34.json.zip: 68.34 MB
HART-GTFSRT-2019-34.pb.zip: 60.44 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 19634 | 19636 | 0 |
| Sampling | 30.80 | 30.80 | 0.00 |
| 2019-08-19 | 2825 | 2825 | 0 |
| 2019-08-20 | 2834 | 2835 | 0 |
| 2019-08-21 | 2722 | 2723 | 0 |
| 2019-08-22 | 2820 | 2820 | 0 |
| 2019-08-23 | 2781 | 2781 | 0 |
| 2019-08-24 | 2818 | 2818 | 0 |
| 2019-08-25 | 2834 | 2834 | 0 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.position.speed``
``entity.vehicle.trip.tripId``
``entity.vehicle.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
``entity.id``
``entity.tripUpdate.stopTimeUpdate.arrival.delay``
``entity.tripUpdate.stopTimeUpdate.arrival.uncertainty``
``entity.tripUpdate.stopTimeUpdate.departure.delay``
``entity.tripUpdate.stopTimeUpdate.departure.uncertainty``
``entity.tripUpdate.stopTimeUpdate.stopId``
``entity.tripUpdate.stopTimeUpdate.stopSequence``
``entity.tripUpdate.trip.tripId``
``entity.tripUpdate.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/HART-GTFSRT-2019-34.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/HART-GTFSRT-2019-34.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/HART-GTFSRT-2019-34.png)
