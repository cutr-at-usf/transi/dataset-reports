# MBTA-GTFSRT-2020-01
## Metadata
### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 9584 | 9584 | 9550 |
| 2019-12-30 | 1365 | 1365 | 1363 |
| 2019-12-31 | 1364 | 1364 | 1346 |
| 2020-01-01 | 1361 | 1361 | 1358 |
| 2020-01-02 | 1361 | 1361 | 1361 |
| 2020-01-03 | 1368 | 1368 | 1364 |
| 2020-01-04 | 1378 | 1378 | 1374 |
| 2020-01-05 | 1387 | 1387 | 1384 |
## Structures
### vehicle_positions
``entity.id``
``entity.vehicle.currentStatus``
``entity.vehicle.currentStopSequence``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.position.speed``
``entity.vehicle.stopId``
``entity.vehicle.timestamp``
``entity.vehicle.trip.directionId``
``entity.vehicle.trip.routeId``
``entity.vehicle.trip.scheduleRelationship``
``entity.vehicle.trip.startDate``
``entity.vehicle.trip.tripId``
``entity.vehicle.vehicle.id``
``entity.vehicle.vehicle.label``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
### trip_updates
``entity.id``
``entity.tripUpdate.stopTimeUpdate.arrival.time``
``entity.tripUpdate.stopTimeUpdate.arrival.uncertainty``
``entity.tripUpdate.stopTimeUpdate.departure.time``
``entity.tripUpdate.stopTimeUpdate.departure.uncertainty``
``entity.tripUpdate.stopTimeUpdate.scheduleRelationship``
``entity.tripUpdate.stopTimeUpdate.stopId``
``entity.tripUpdate.stopTimeUpdate.stopSequence``
``entity.tripUpdate.trip.directionId``
``entity.tripUpdate.trip.routeId``
``entity.tripUpdate.trip.scheduleRelationship``
``entity.tripUpdate.trip.startDate``
``entity.tripUpdate.trip.tripId``
``entity.tripUpdate.vehicle.id``
``entity.tripUpdate.vehicle.label``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
### alerts
``entity.alert.activePeriod.end``
``entity.alert.activePeriod.start``
``entity.alert.cause``
``entity.alert.descriptionText.translation.language``
``entity.alert.descriptionText.translation.text``
``entity.alert.effect``
``entity.alert.headerText.translation.language``
``entity.alert.headerText.translation.text``
``entity.alert.informedEntity.agencyId``
``entity.alert.informedEntity.routeId``
``entity.alert.informedEntity.routeType``
``entity.alert.informedEntity.stopId``
``entity.alert.informedEntity.trip.tripId``
``entity.id``
``header.gtfsRealtimeVersion``
``header.timestamp``
## Graphics
### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/MBTA-GTFSRT-2020-01.png)
### Trip Entities
![trip entity timeline](img-timeline-trip/MBTA-GTFSRT-2020-01.png)
### Alert Entities
![alert entity timeline](img-timeline-alert/MBTA-GTFSRT-2020-01.png)
