# MBTA-GTFSRT-2019-03
## Metadata
### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 9402 | 9387 | 9395 |
| 2019-01-14 | 1368 | 1361 | 1365 |
| 2019-01-15 | 1364 | 1355 | 1368 |
| 2019-01-16 | 1151 | 1151 | 1144 |
| 2019-01-17 | 1368 | 1369 | 1368 |
| 2019-01-18 | 1367 | 1367 | 1368 |
| 2019-01-19 | 1387 | 1387 | 1386 |
| 2019-01-20 | 1397 | 1397 | 1396 |
## Structures
### vehicle_positions
``entity.id``
``entity.vehicle.currentStatus``
``entity.vehicle.currentStopSequence``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.position.speed``
``entity.vehicle.stopId``
``entity.vehicle.timestamp``
``entity.vehicle.trip.directionId``
``entity.vehicle.trip.routeId``
``entity.vehicle.trip.scheduleRelationship``
``entity.vehicle.trip.startDate``
``entity.vehicle.trip.tripId``
``entity.vehicle.vehicle.id``
``entity.vehicle.vehicle.label``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
### trip_updates
``entity.id``
``entity.tripUpdate.stopTimeUpdate.arrival.time``
``entity.tripUpdate.stopTimeUpdate.arrival.uncertainty``
``entity.tripUpdate.stopTimeUpdate.departure.time``
``entity.tripUpdate.stopTimeUpdate.departure.uncertainty``
``entity.tripUpdate.stopTimeUpdate.scheduleRelationship``
``entity.tripUpdate.stopTimeUpdate.stopId``
``entity.tripUpdate.stopTimeUpdate.stopSequence``
``entity.tripUpdate.trip.directionId``
``entity.tripUpdate.trip.routeId``
``entity.tripUpdate.trip.scheduleRelationship``
``entity.tripUpdate.trip.startDate``
``entity.tripUpdate.trip.tripId``
``entity.tripUpdate.vehicle.id``
``entity.tripUpdate.vehicle.label``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
### alerts
``entity.alert.activePeriod.end``
``entity.alert.activePeriod.start``
``entity.alert.cause``
``entity.alert.descriptionText.translation.language``
``entity.alert.descriptionText.translation.text``
``entity.alert.effect``
``entity.alert.headerText.translation.language``
``entity.alert.headerText.translation.text``
``entity.alert.informedEntity.agencyId``
``entity.alert.informedEntity.routeId``
``entity.alert.informedEntity.routeType``
``entity.alert.informedEntity.stopId``
``entity.alert.informedEntity.trip.tripId``
``entity.id``
``header.gtfsRealtimeVersion``
``header.timestamp``
## Graphics
### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/MBTA-GTFSRT-2019-03.png)
### Trip Entities
![trip entity timeline](img-timeline-trip/MBTA-GTFSRT-2019-03.png)
### Alert Entities
![alert entity timeline](img-timeline-alert/MBTA-GTFSRT-2019-03.png)
