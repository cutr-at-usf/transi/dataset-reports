# MBTA-GTFSRT-2018-06
## Metadata
### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 9288 | 8809 | 9283 |
| 2018-02-05 | 1349 | 1271 | 1349 |
| 2018-02-06 | 1314 | 1249 | 1313 |
| 2018-02-07 | 1311 | 1235 | 1309 |
| 2018-02-08 | 1312 | 1243 | 1311 |
| 2018-02-09 | 1331 | 1268 | 1331 |
| 2018-02-10 | 1334 | 1254 | 1334 |
| 2018-02-11 | 1337 | 1289 | 1336 |
## Structures
### vehicle_positions
``entity.id``
``entity.vehicle.currentStatus``
``entity.vehicle.currentStopSequence``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.position.speed``
``entity.vehicle.stopId``
``entity.vehicle.timestamp``
``entity.vehicle.trip.directionId``
``entity.vehicle.trip.routeId``
``entity.vehicle.trip.scheduleRelationship``
``entity.vehicle.trip.startDate``
``entity.vehicle.trip.tripId``
``entity.vehicle.vehicle.id``
``entity.vehicle.vehicle.label``
``header.gtfsRealtimeVersion``
``header.timestamp``
### trip_updates
``entity.id``
``entity.tripUpdate.stopTimeUpdate.arrival.time``
``entity.tripUpdate.stopTimeUpdate.departure.time``
``entity.tripUpdate.stopTimeUpdate.scheduleRelationship``
``entity.tripUpdate.stopTimeUpdate.stopId``
``entity.tripUpdate.stopTimeUpdate.stopSequence``
``entity.tripUpdate.timestamp``
``entity.tripUpdate.trip.directionId``
``entity.tripUpdate.trip.routeId``
``entity.tripUpdate.trip.scheduleRelationship``
``entity.tripUpdate.trip.startDate``
``entity.tripUpdate.trip.tripId``
``entity.tripUpdate.vehicle.id``
``entity.tripUpdate.vehicle.label``
``header.gtfsRealtimeVersion``
``header.timestamp``
### alerts
``entity.alert.activePeriod.end``
``entity.alert.activePeriod.start``
``entity.alert.cause``
``entity.alert.descriptionText.translation.language``
``entity.alert.descriptionText.translation.text``
``entity.alert.effect``
``entity.alert.headerText.translation.language``
``entity.alert.headerText.translation.text``
``entity.alert.informedEntity.agencyId``
``entity.alert.informedEntity.routeId``
``entity.alert.informedEntity.routeType``
``entity.alert.informedEntity.stopId``
``entity.alert.informedEntity.trip.tripId``
``entity.id``
``header.gtfsRealtimeVersion``
``header.timestamp``
## Graphics
### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/MBTA-GTFSRT-2018-06.png)
### Trip Entities
![trip entity timeline](img-timeline-trip/MBTA-GTFSRT-2018-06.png)
### Alert Entities
![alert entity timeline](img-timeline-alert/MBTA-GTFSRT-2018-06.png)
