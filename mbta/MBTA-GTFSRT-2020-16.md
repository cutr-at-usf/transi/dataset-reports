# MBTA-GTFSRT-2020-16
## Metadata
### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 9607 | 9606 | 9596 |
| 2020-04-13 | 1380 | 1380 | 1381 |
| 2020-04-14 | 1382 | 1381 | 1378 |
| 2020-04-15 | 1362 | 1362 | 1359 |
| 2020-04-16 | 1368 | 1368 | 1367 |
| 2020-04-17 | 1376 | 1376 | 1374 |
| 2020-04-18 | 1353 | 1353 | 1351 |
| 2020-04-19 | 1386 | 1386 | 1386 |
## Structures
### vehicle_positions
``entity.id``
``entity.vehicle.currentStatus``
``entity.vehicle.currentStopSequence``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.position.speed``
``entity.vehicle.stopId``
``entity.vehicle.timestamp``
``entity.vehicle.trip.directionId``
``entity.vehicle.trip.routeId``
``entity.vehicle.trip.scheduleRelationship``
``entity.vehicle.trip.startDate``
``entity.vehicle.trip.startTime``
``entity.vehicle.trip.tripId``
``entity.vehicle.vehicle.id``
``entity.vehicle.vehicle.label``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
### trip_updates
``entity.id``
``entity.tripUpdate.stopTimeUpdate.arrival.time``
``entity.tripUpdate.stopTimeUpdate.arrival.uncertainty``
``entity.tripUpdate.stopTimeUpdate.departure.time``
``entity.tripUpdate.stopTimeUpdate.departure.uncertainty``
``entity.tripUpdate.stopTimeUpdate.scheduleRelationship``
``entity.tripUpdate.stopTimeUpdate.stopId``
``entity.tripUpdate.stopTimeUpdate.stopSequence``
``entity.tripUpdate.timestamp``
``entity.tripUpdate.trip.directionId``
``entity.tripUpdate.trip.routeId``
``entity.tripUpdate.trip.scheduleRelationship``
``entity.tripUpdate.trip.startDate``
``entity.tripUpdate.trip.startTime``
``entity.tripUpdate.trip.tripId``
``entity.tripUpdate.vehicle.id``
``entity.tripUpdate.vehicle.label``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
### alerts
``entity.alert.activePeriod.end``
``entity.alert.activePeriod.start``
``entity.alert.cause``
``entity.alert.descriptionText.translation.language``
``entity.alert.descriptionText.translation.text``
``entity.alert.effect``
``entity.alert.headerText.translation.language``
``entity.alert.headerText.translation.text``
``entity.alert.informedEntity.agencyId``
``entity.alert.informedEntity.routeId``
``entity.alert.informedEntity.routeType``
``entity.alert.informedEntity.stopId``
``entity.alert.informedEntity.trip.tripId``
``entity.alert.url.translation.language``
``entity.alert.url.translation.text``
``entity.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
## Graphics
### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/MBTA-GTFSRT-2020-16.png)
### Trip Entities
![trip entity timeline](img-timeline-trip/MBTA-GTFSRT-2020-16.png)
### Alert Entities
![alert entity timeline](img-timeline-alert/MBTA-GTFSRT-2020-16.png)
