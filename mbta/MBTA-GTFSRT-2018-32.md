# MBTA-GTFSRT-2018-32
## Metadata
### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 9624 | 9607 | 9623 |
| 2018-08-06 | 1374 | 1374 | 1374 |
| 2018-08-07 | 1369 | 1351 | 1366 |
| 2018-08-08 | 1338 | 1337 | 1338 |
| 2018-08-09 | 1377 | 1377 | 1377 |
| 2018-08-10 | 1371 | 1378 | 1372 |
| 2018-08-11 | 1392 | 1394 | 1392 |
| 2018-08-12 | 1403 | 1396 | 1404 |
## Structures
### vehicle_positions
``entity.alert.activePeriod.end``
``entity.alert.activePeriod.start``
``entity.alert.cause``
``entity.alert.descriptionText.translation.language``
``entity.alert.descriptionText.translation.text``
``entity.alert.effect``
``entity.alert.headerText.translation.language``
``entity.alert.headerText.translation.text``
``entity.alert.informedEntity.agencyId``
``entity.alert.informedEntity.routeId``
``entity.alert.informedEntity.routeType``
``entity.alert.informedEntity.stopId``
``entity.alert.informedEntity.trip.tripId``
``entity.id``
``header.gtfsRealtimeVersion``
``header.timestamp``
### trip_updates
``entity.id``
``entity.tripUpdate.stopTimeUpdate.arrival.time``
``entity.tripUpdate.stopTimeUpdate.departure.time``
``entity.tripUpdate.stopTimeUpdate.scheduleRelationship``
``entity.tripUpdate.stopTimeUpdate.stopId``
``entity.tripUpdate.stopTimeUpdate.stopSequence``
``entity.tripUpdate.trip.directionId``
``entity.tripUpdate.trip.routeId``
``entity.tripUpdate.trip.scheduleRelationship``
``entity.tripUpdate.trip.startDate``
``entity.tripUpdate.trip.tripId``
``entity.tripUpdate.vehicle.id``
``entity.tripUpdate.vehicle.label``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
### alerts
``entity.alert.activePeriod.end``
``entity.alert.activePeriod.start``
``entity.alert.cause``
``entity.alert.descriptionText.translation.language``
``entity.alert.descriptionText.translation.text``
``entity.alert.effect``
``entity.alert.headerText.translation.language``
``entity.alert.headerText.translation.text``
``entity.alert.informedEntity.agencyId``
``entity.alert.informedEntity.routeId``
``entity.alert.informedEntity.routeType``
``entity.alert.informedEntity.stopId``
``entity.alert.informedEntity.trip.tripId``
``entity.id``
``header.gtfsRealtimeVersion``
``header.timestamp``
## Graphics
### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/MBTA-GTFSRT-2018-32.png)
### Trip Entities
![trip entity timeline](img-timeline-trip/MBTA-GTFSRT-2018-32.png)
### Alert Entities
![alert entity timeline](img-timeline-alert/MBTA-GTFSRT-2018-32.png)
