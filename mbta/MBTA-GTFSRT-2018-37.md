# MBTA-GTFSRT-2018-37
## Metadata
### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 9612 | 9631 | 9647 |
| 2018-09-10 | 1368 | 1377 | 1374 |
| 2018-09-11 | 1354 | 1369 | 1370 |
| 2018-09-12 | 1369 | 1372 | 1370 |
| 2018-09-13 | 1376 | 1371 | 1369 |
| 2018-09-14 | 1364 | 1361 | 1371 |
| 2018-09-15 | 1387 | 1383 | 1392 |
| 2018-09-16 | 1394 | 1398 | 1401 |
## Structures
### vehicle_positions
``entity.id``
``entity.vehicle.currentStatus``
``entity.vehicle.currentStopSequence``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.position.speed``
``entity.vehicle.stopId``
``entity.vehicle.timestamp``
``entity.vehicle.trip.directionId``
``entity.vehicle.trip.routeId``
``entity.vehicle.trip.scheduleRelationship``
``entity.vehicle.trip.startDate``
``entity.vehicle.trip.tripId``
``entity.vehicle.vehicle.id``
``entity.vehicle.vehicle.label``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
### trip_updates
``entity.id``
``entity.tripUpdate.stopTimeUpdate.arrival.time``
``entity.tripUpdate.stopTimeUpdate.departure.time``
``entity.tripUpdate.stopTimeUpdate.scheduleRelationship``
``entity.tripUpdate.stopTimeUpdate.stopId``
``entity.tripUpdate.stopTimeUpdate.stopSequence``
``entity.tripUpdate.trip.directionId``
``entity.tripUpdate.trip.routeId``
``entity.tripUpdate.trip.scheduleRelationship``
``entity.tripUpdate.trip.startDate``
``entity.tripUpdate.trip.tripId``
``entity.tripUpdate.vehicle.id``
``entity.tripUpdate.vehicle.label``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
### alerts
``entity.alert.activePeriod.end``
``entity.alert.activePeriod.start``
``entity.alert.cause``
``entity.alert.descriptionText.translation.language``
``entity.alert.descriptionText.translation.text``
``entity.alert.effect``
``entity.alert.headerText.translation.language``
``entity.alert.headerText.translation.text``
``entity.alert.informedEntity.agencyId``
``entity.alert.informedEntity.routeId``
``entity.alert.informedEntity.routeType``
``entity.alert.informedEntity.stopId``
``entity.alert.informedEntity.trip.tripId``
``entity.id``
``header.gtfsRealtimeVersion``
``header.timestamp``
## Graphics
### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/MBTA-GTFSRT-2018-37.png)
### Trip Entities
![trip entity timeline](img-timeline-trip/MBTA-GTFSRT-2018-37.png)
### Alert Entities
![alert entity timeline](img-timeline-alert/MBTA-GTFSRT-2018-37.png)
