### PSTA-GTFSRT-2020-05
#### Metadata
PSTA-GTFSRT-2020-05.json.zip: 357.60 MB
PSTA-GTFSRT-2020-05.pb.zip: 310.62 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 16954 | 16955 | 16955 |
| Sampling | 35.67 | 35.67 | 35.67 |
| 2020-01-27 | 2429 | 2429 | 2430 |
| 2020-01-28 | 2424 | 2425 | 2424 |
| 2020-01-29 | 2389 | 2389 | 2389 |
| 2020-01-30 | 2425 | 2425 | 2425 |
| 2020-01-31 | 2429 | 2429 | 2429 |
| 2020-02-01 | 2426 | 2426 | 2426 |
| 2020-02-02 | 2432 | 2432 | 2432 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.position.speed``
``entity.vehicle.timestamp``
``entity.vehicle.trip.routeId``
``entity.vehicle.trip.scheduleRelationship``
``entity.vehicle.trip.tripId``
``entity.vehicle.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
``entity.id``
``entity.tripUpdate.stopTimeUpdate.arrival.time``
``entity.tripUpdate.stopTimeUpdate.departure.time``
``entity.tripUpdate.stopTimeUpdate.scheduleRelationship``
``entity.tripUpdate.stopTimeUpdate.stopId``
``entity.tripUpdate.stopTimeUpdate.stopSequence``
``entity.tripUpdate.timestamp``
``entity.tripUpdate.trip.routeId``
``entity.tripUpdate.trip.scheduleRelationship``
``entity.tripUpdate.trip.tripId``
``entity.tripUpdate.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### alerts
``entity.alert.cause``
``entity.alert.descriptionText.translation.language``
``entity.alert.descriptionText.translation.text``
``entity.alert.effect``
``entity.alert.headerText.translation.language``
``entity.alert.headerText.translation.text``
``entity.alert.informedEntity.agencyId``
``entity.alert.informedEntity.stopId``
``entity.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/PSTA-GTFSRT-2020-05.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/PSTA-GTFSRT-2020-05.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/PSTA-GTFSRT-2020-05.png)
