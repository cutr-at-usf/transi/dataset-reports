### PSTA-GTFSRT-2017-46
#### Metadata
PSTA-GTFSRT-2017-46.json.zip: 333.36 MB
PSTA-GTFSRT-2017-46.pb.zip: 297.10 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 17032 | 17032 | 17033 |
| Sampling | 35.51 | 35.51 | 35.51 |
| 2017-11-13 | 2433 | 2433 | 2433 |
| 2017-11-14 | 2420 | 2420 | 2420 |
| 2017-11-15 | 2433 | 2434 | 2434 |
| 2017-11-16 | 2435 | 2435 | 2435 |
| 2017-11-17 | 2434 | 2434 | 2434 |
| 2017-11-18 | 2439 | 2438 | 2439 |
| 2017-11-19 | 2438 | 2438 | 2438 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.position.speed``
``entity.vehicle.timestamp``
``entity.vehicle.trip.routeId``
``entity.vehicle.trip.tripId``
``entity.vehicle.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
``entity.id``
``entity.tripUpdate.stopTimeUpdate.arrival.time``
``entity.tripUpdate.stopTimeUpdate.departure.time``
``entity.tripUpdate.stopTimeUpdate.stopId``
``entity.tripUpdate.stopTimeUpdate.stopSequence``
``entity.tripUpdate.timestamp``
``entity.tripUpdate.trip.routeId``
``entity.tripUpdate.trip.scheduleRelationship``
``entity.tripUpdate.trip.tripId``
``entity.tripUpdate.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### alerts
``entity.alert.descriptionText.translation.language``
``entity.alert.descriptionText.translation.text``
``entity.alert.headerText.translation.language``
``entity.alert.headerText.translation.text``
``entity.alert.informedEntity.agencyId``
``entity.alert.informedEntity.stopId``
``entity.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/PSTA-GTFSRT-2017-46.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/PSTA-GTFSRT-2017-46.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/PSTA-GTFSRT-2017-46.png)
