### PSTA-GTFSRT-2018-30
#### Metadata
PSTA-GTFSRT-2018-30.json.zip: 331.77 MB
PSTA-GTFSRT-2018-30.pb.zip: 296.21 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 16968 | 16968 | 16968 |
| Sampling | 35.64 | 35.64 | 35.64 |
| 2018-07-23 | 2439 | 2439 | 2439 |
| 2018-07-24 | 2436 | 2436 | 2436 |
| 2018-07-25 | 2335 | 2335 | 2335 |
| 2018-07-26 | 2436 | 2436 | 2436 |
| 2018-07-27 | 2436 | 2436 | 2436 |
| 2018-07-28 | 2439 | 2439 | 2439 |
| 2018-07-29 | 2447 | 2447 | 2447 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.position.speed``
``entity.vehicle.timestamp``
``entity.vehicle.trip.routeId``
``entity.vehicle.trip.tripId``
``entity.vehicle.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
``entity.id``
``entity.tripUpdate.stopTimeUpdate.arrival.time``
``entity.tripUpdate.stopTimeUpdate.departure.time``
``entity.tripUpdate.stopTimeUpdate.stopId``
``entity.tripUpdate.stopTimeUpdate.stopSequence``
``entity.tripUpdate.timestamp``
``entity.tripUpdate.trip.routeId``
``entity.tripUpdate.trip.scheduleRelationship``
``entity.tripUpdate.trip.tripId``
``entity.tripUpdate.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### alerts
``entity.alert.descriptionText.translation.language``
``entity.alert.descriptionText.translation.text``
``entity.alert.headerText.translation.language``
``entity.alert.headerText.translation.text``
``entity.alert.informedEntity.agencyId``
``entity.alert.informedEntity.stopId``
``entity.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/PSTA-GTFSRT-2018-30.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/PSTA-GTFSRT-2018-30.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/PSTA-GTFSRT-2018-30.png)
