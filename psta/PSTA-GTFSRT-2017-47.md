### PSTA-GTFSRT-2017-47
#### Metadata
PSTA-GTFSRT-2017-47.json.zip: 293.74 MB
PSTA-GTFSRT-2017-47.pb.zip: 262.09 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 17064 | 17062 | 17064 |
| Sampling | 35.44 | 35.45 | 35.44 |
| 2017-11-20 | 2434 | 2433 | 2434 |
| 2017-11-21 | 2435 | 2434 | 2435 |
| 2017-11-22 | 2436 | 2436 | 2436 |
| 2017-11-23 | 2442 | 2442 | 2442 |
| 2017-11-24 | 2433 | 2433 | 2433 |
| 2017-11-25 | 2439 | 2439 | 2439 |
| 2017-11-26 | 2445 | 2445 | 2445 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.position.speed``
``entity.vehicle.timestamp``
``entity.vehicle.trip.routeId``
``entity.vehicle.trip.tripId``
``entity.vehicle.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
``entity.id``
``entity.tripUpdate.stopTimeUpdate.arrival.time``
``entity.tripUpdate.stopTimeUpdate.departure.time``
``entity.tripUpdate.stopTimeUpdate.stopId``
``entity.tripUpdate.stopTimeUpdate.stopSequence``
``entity.tripUpdate.timestamp``
``entity.tripUpdate.trip.routeId``
``entity.tripUpdate.trip.scheduleRelationship``
``entity.tripUpdate.trip.tripId``
``entity.tripUpdate.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### alerts
``entity.alert.descriptionText.translation.language``
``entity.alert.descriptionText.translation.text``
``entity.alert.headerText.translation.language``
``entity.alert.headerText.translation.text``
``entity.alert.informedEntity.agencyId``
``entity.alert.informedEntity.stopId``
``entity.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/PSTA-GTFSRT-2017-47.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/PSTA-GTFSRT-2017-47.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/PSTA-GTFSRT-2017-47.png)
