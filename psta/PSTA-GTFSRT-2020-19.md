### PSTA-GTFSRT-2020-19
#### Metadata
PSTA-GTFSRT-2020-19.json.zip: 255.90 MB
PSTA-GTFSRT-2020-19.pb.zip: 222.48 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 16869 | 16869 | 16869 |
| Sampling | 35.85 | 35.85 | 35.85 |
| 2020-05-04 | 2440 | 2440 | 2440 |
| 2020-05-05 | 2350 | 2350 | 2350 |
| 2020-05-06 | 2377 | 2377 | 2377 |
| 2020-05-07 | 2441 | 2441 | 2441 |
| 2020-05-08 | 2393 | 2393 | 2393 |
| 2020-05-09 | 2421 | 2421 | 2421 |
| 2020-05-10 | 2447 | 2447 | 2447 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.position.speed``
``entity.vehicle.timestamp``
``entity.vehicle.trip.routeId``
``entity.vehicle.trip.scheduleRelationship``
``entity.vehicle.trip.tripId``
``entity.vehicle.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
``entity.id``
``entity.tripUpdate.stopTimeUpdate.arrival.time``
``entity.tripUpdate.stopTimeUpdate.departure.time``
``entity.tripUpdate.stopTimeUpdate.scheduleRelationship``
``entity.tripUpdate.stopTimeUpdate.stopId``
``entity.tripUpdate.stopTimeUpdate.stopSequence``
``entity.tripUpdate.timestamp``
``entity.tripUpdate.trip.routeId``
``entity.tripUpdate.trip.scheduleRelationship``
``entity.tripUpdate.trip.tripId``
``entity.tripUpdate.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### alerts
``entity.alert.cause``
``entity.alert.descriptionText.translation.language``
``entity.alert.descriptionText.translation.text``
``entity.alert.effect``
``entity.alert.headerText.translation.language``
``entity.alert.headerText.translation.text``
``entity.alert.informedEntity.agencyId``
``entity.alert.informedEntity.stopId``
``entity.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/PSTA-GTFSRT-2020-19.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/PSTA-GTFSRT-2020-19.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/PSTA-GTFSRT-2020-19.png)
