### PSTA-GTFSRT-2018-36
#### Metadata
PSTA-GTFSRT-2018-36.json.zip: 309.75 MB
PSTA-GTFSRT-2018-36.pb.zip: 276.14 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 16861 | 16860 | 16861 |
| Sampling | 35.87 | 35.87 | 35.87 |
| 2018-09-03 | 2438 | 2438 | 2438 |
| 2018-09-04 | 2431 | 2431 | 2431 |
| 2018-09-05 | 2346 | 2345 | 2346 |
| 2018-09-06 | 2434 | 2434 | 2434 |
| 2018-09-07 | 2335 | 2335 | 2335 |
| 2018-09-08 | 2439 | 2439 | 2439 |
| 2018-09-09 | 2438 | 2438 | 2438 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.position.speed``
``entity.vehicle.timestamp``
``entity.vehicle.trip.routeId``
``entity.vehicle.trip.tripId``
``entity.vehicle.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
``entity.id``
``entity.tripUpdate.stopTimeUpdate.arrival.time``
``entity.tripUpdate.stopTimeUpdate.departure.time``
``entity.tripUpdate.stopTimeUpdate.stopId``
``entity.tripUpdate.stopTimeUpdate.stopSequence``
``entity.tripUpdate.timestamp``
``entity.tripUpdate.trip.routeId``
``entity.tripUpdate.trip.scheduleRelationship``
``entity.tripUpdate.trip.tripId``
``entity.tripUpdate.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### alerts
``entity.alert.descriptionText.translation.language``
``entity.alert.descriptionText.translation.text``
``entity.alert.headerText.translation.language``
``entity.alert.headerText.translation.text``
``entity.alert.informedEntity.agencyId``
``entity.alert.informedEntity.stopId``
``entity.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/PSTA-GTFSRT-2018-36.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/PSTA-GTFSRT-2018-36.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/PSTA-GTFSRT-2018-36.png)
