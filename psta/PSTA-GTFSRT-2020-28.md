### PSTA-GTFSRT-2020-28
#### Metadata
PSTA-GTFSRT-2020-28.json.zip: 286.02 MB
PSTA-GTFSRT-2020-28.pb.zip: 248.51 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 16929 | 16928 | 16929 |
| Sampling | 35.73 | 35.73 | 35.73 |
| 2020-07-06 | 2440 | 2439 | 2440 |
| 2020-07-07 | 2440 | 2440 | 2440 |
| 2020-07-08 | 2301 | 2301 | 2301 |
| 2020-07-09 | 2412 | 2412 | 2412 |
| 2020-07-10 | 2443 | 2443 | 2443 |
| 2020-07-11 | 2445 | 2445 | 2445 |
| 2020-07-12 | 2448 | 2448 | 2448 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.position.speed``
``entity.vehicle.timestamp``
``entity.vehicle.trip.routeId``
``entity.vehicle.trip.scheduleRelationship``
``entity.vehicle.trip.tripId``
``entity.vehicle.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
``entity.id``
``entity.tripUpdate.stopTimeUpdate.arrival.time``
``entity.tripUpdate.stopTimeUpdate.departure.time``
``entity.tripUpdate.stopTimeUpdate.scheduleRelationship``
``entity.tripUpdate.stopTimeUpdate.stopId``
``entity.tripUpdate.stopTimeUpdate.stopSequence``
``entity.tripUpdate.timestamp``
``entity.tripUpdate.trip.routeId``
``entity.tripUpdate.trip.scheduleRelationship``
``entity.tripUpdate.trip.tripId``
``entity.tripUpdate.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### alerts
``entity.alert.cause``
``entity.alert.descriptionText.translation.language``
``entity.alert.descriptionText.translation.text``
``entity.alert.effect``
``entity.alert.headerText.translation.language``
``entity.alert.headerText.translation.text``
``entity.alert.informedEntity.agencyId``
``entity.alert.informedEntity.stopId``
``entity.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/PSTA-GTFSRT-2020-28.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/PSTA-GTFSRT-2020-28.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/PSTA-GTFSRT-2020-28.png)
