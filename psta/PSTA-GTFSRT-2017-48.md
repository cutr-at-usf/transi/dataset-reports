### PSTA-GTFSRT-2017-48
#### Metadata
PSTA-GTFSRT-2017-48.json.zip: 335.14 MB
PSTA-GTFSRT-2017-48.pb.zip: 298.55 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 16997 | 16995 | 16997 |
| Sampling | 35.58 | 35.59 | 35.58 |
| 2017-11-27 | 2434 | 2434 | 2434 |
| 2017-11-28 | 2433 | 2433 | 2433 |
| 2017-11-29 | 2434 | 2434 | 2434 |
| 2017-11-30 | 2433 | 2433 | 2433 |
| 2017-12-01 | 2421 | 2420 | 2421 |
| 2017-12-02 | 2427 | 2426 | 2427 |
| 2017-12-03 | 2415 | 2415 | 2415 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.position.speed``
``entity.vehicle.timestamp``
``entity.vehicle.trip.routeId``
``entity.vehicle.trip.tripId``
``entity.vehicle.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
``entity.id``
``entity.tripUpdate.stopTimeUpdate.arrival.time``
``entity.tripUpdate.stopTimeUpdate.departure.time``
``entity.tripUpdate.stopTimeUpdate.stopId``
``entity.tripUpdate.stopTimeUpdate.stopSequence``
``entity.tripUpdate.timestamp``
``entity.tripUpdate.trip.routeId``
``entity.tripUpdate.trip.scheduleRelationship``
``entity.tripUpdate.trip.tripId``
``entity.tripUpdate.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### alerts
``entity.alert.descriptionText.translation.language``
``entity.alert.descriptionText.translation.text``
``entity.alert.headerText.translation.language``
``entity.alert.headerText.translation.text``
``entity.alert.informedEntity.agencyId``
``entity.alert.informedEntity.stopId``
``entity.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/PSTA-GTFSRT-2017-48.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/PSTA-GTFSRT-2017-48.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/PSTA-GTFSRT-2017-48.png)
