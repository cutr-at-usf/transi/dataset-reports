### PSTA-GTFSRT-2019-49
#### Metadata
PSTA-GTFSRT-2019-49.json.zip: 353.30 MB
PSTA-GTFSRT-2019-49.pb.zip: 307.08 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 16939 | 16939 | 16940 |
| Sampling | 35.70 | 35.70 | 35.70 |
| 2019-12-02 | 2428 | 2428 | 2429 |
| 2019-12-03 | 2429 | 2429 | 2429 |
| 2019-12-04 | 2347 | 2347 | 2347 |
| 2019-12-05 | 2428 | 2428 | 2428 |
| 2019-12-06 | 2430 | 2430 | 2430 |
| 2019-12-07 | 2435 | 2435 | 2435 |
| 2019-12-08 | 2442 | 2442 | 2442 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.position.speed``
``entity.vehicle.timestamp``
``entity.vehicle.trip.routeId``
``entity.vehicle.trip.scheduleRelationship``
``entity.vehicle.trip.tripId``
``entity.vehicle.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
``entity.id``
``entity.tripUpdate.stopTimeUpdate.arrival.time``
``entity.tripUpdate.stopTimeUpdate.departure.time``
``entity.tripUpdate.stopTimeUpdate.scheduleRelationship``
``entity.tripUpdate.stopTimeUpdate.stopId``
``entity.tripUpdate.stopTimeUpdate.stopSequence``
``entity.tripUpdate.timestamp``
``entity.tripUpdate.trip.routeId``
``entity.tripUpdate.trip.scheduleRelationship``
``entity.tripUpdate.trip.tripId``
``entity.tripUpdate.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### alerts
``entity.alert.cause``
``entity.alert.descriptionText.translation.language``
``entity.alert.descriptionText.translation.text``
``entity.alert.effect``
``entity.alert.headerText.translation.language``
``entity.alert.headerText.translation.text``
``entity.alert.informedEntity.agencyId``
``entity.alert.informedEntity.stopId``
``entity.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/PSTA-GTFSRT-2019-49.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/PSTA-GTFSRT-2019-49.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/PSTA-GTFSRT-2019-49.png)
