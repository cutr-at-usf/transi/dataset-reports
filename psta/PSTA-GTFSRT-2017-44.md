### PSTA-GTFSRT-2017-44
#### Metadata
PSTA-GTFSRT-2017-44.json.zip: 10.31 MB
PSTA-GTFSRT-2017-44.pb.zip: 9.18 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 905 | 905 | 905 |
| Sampling | 35.83 | 35.83 | 35.83 |
| 2017-10-30 | 0 | 0 | 0 |
| 2017-10-31 | 0 | 0 | 0 |
| 2017-11-01 | 0 | 0 | 0 |
| 2017-11-02 | 0 | 0 | 0 |
| 2017-11-03 | 0 | 0 | 0 |
| 2017-11-04 | 0 | 0 | 0 |
| 2017-11-05 | 804 | 804 | 804 |
| 2017-11-06 | 101 | 101 | 101 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.position.speed``
``entity.vehicle.timestamp``
``entity.vehicle.trip.routeId``
``entity.vehicle.trip.tripId``
``entity.vehicle.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
``entity.id``
``entity.tripUpdate.stopTimeUpdate.arrival.time``
``entity.tripUpdate.stopTimeUpdate.departure.time``
``entity.tripUpdate.stopTimeUpdate.stopId``
``entity.tripUpdate.stopTimeUpdate.stopSequence``
``entity.tripUpdate.timestamp``
``entity.tripUpdate.trip.routeId``
``entity.tripUpdate.trip.tripId``
``entity.tripUpdate.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### alerts
``entity.alert.descriptionText.translation.language``
``entity.alert.descriptionText.translation.text``
``entity.alert.headerText.translation.language``
``entity.alert.headerText.translation.text``
``entity.alert.informedEntity.agencyId``
``entity.alert.informedEntity.stopId``
``entity.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/PSTA-GTFSRT-2017-44.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/PSTA-GTFSRT-2017-44.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/PSTA-GTFSRT-2017-44.png)
