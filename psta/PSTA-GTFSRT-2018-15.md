### PSTA-GTFSRT-2018-15
#### Metadata
PSTA-GTFSRT-2018-15.json.zip: 324.52 MB
PSTA-GTFSRT-2018-15.pb.zip: 289.10 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 16434 | 16433 | 16434 |
| Sampling | 36.80 | 36.80 | 36.80 |
| 2018-04-09 | 2431 | 2431 | 2431 |
| 2018-04-10 | 2257 | 2257 | 2257 |
| 2018-04-11 | 2429 | 2429 | 2429 |
| 2018-04-12 | 2004 | 2003 | 2004 |
| 2018-04-13 | 2428 | 2428 | 2428 |
| 2018-04-14 | 2439 | 2439 | 2439 |
| 2018-04-15 | 2446 | 2446 | 2446 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.position.speed``
``entity.vehicle.timestamp``
``entity.vehicle.trip.routeId``
``entity.vehicle.trip.tripId``
``entity.vehicle.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
``entity.id``
``entity.tripUpdate.stopTimeUpdate.arrival.time``
``entity.tripUpdate.stopTimeUpdate.departure.time``
``entity.tripUpdate.stopTimeUpdate.stopId``
``entity.tripUpdate.stopTimeUpdate.stopSequence``
``entity.tripUpdate.timestamp``
``entity.tripUpdate.trip.routeId``
``entity.tripUpdate.trip.scheduleRelationship``
``entity.tripUpdate.trip.tripId``
``entity.tripUpdate.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### alerts
``entity.alert.descriptionText.translation.language``
``entity.alert.descriptionText.translation.text``
``entity.alert.headerText.translation.language``
``entity.alert.headerText.translation.text``
``entity.alert.informedEntity.agencyId``
``entity.alert.informedEntity.stopId``
``entity.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/PSTA-GTFSRT-2018-15.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/PSTA-GTFSRT-2018-15.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/PSTA-GTFSRT-2018-15.png)
