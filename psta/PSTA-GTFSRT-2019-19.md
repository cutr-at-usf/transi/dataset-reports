### PSTA-GTFSRT-2019-19
#### Metadata
PSTA-GTFSRT-2019-19.json.zip: 365.71 MB
PSTA-GTFSRT-2019-19.pb.zip: 316.41 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 16902 | 16902 | 16902 |
| Sampling | 35.78 | 35.78 | 35.78 |
| 2019-05-06 | 2426 | 2426 | 2426 |
| 2019-05-07 | 2430 | 2430 | 2430 |
| 2019-05-08 | 2314 | 2314 | 2314 |
| 2019-05-09 | 2429 | 2429 | 2429 |
| 2019-05-10 | 2427 | 2427 | 2427 |
| 2019-05-11 | 2435 | 2435 | 2435 |
| 2019-05-12 | 2441 | 2441 | 2441 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.position.speed``
``entity.vehicle.timestamp``
``entity.vehicle.trip.routeId``
``entity.vehicle.trip.scheduleRelationship``
``entity.vehicle.trip.tripId``
``entity.vehicle.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
``entity.id``
``entity.tripUpdate.stopTimeUpdate.arrival.time``
``entity.tripUpdate.stopTimeUpdate.departure.time``
``entity.tripUpdate.stopTimeUpdate.scheduleRelationship``
``entity.tripUpdate.stopTimeUpdate.stopId``
``entity.tripUpdate.stopTimeUpdate.stopSequence``
``entity.tripUpdate.timestamp``
``entity.tripUpdate.trip.routeId``
``entity.tripUpdate.trip.scheduleRelationship``
``entity.tripUpdate.trip.tripId``
``entity.tripUpdate.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### alerts
``entity.alert.cause``
``entity.alert.descriptionText.translation.language``
``entity.alert.descriptionText.translation.text``
``entity.alert.effect``
``entity.alert.headerText.translation.language``
``entity.alert.headerText.translation.text``
``entity.alert.informedEntity.agencyId``
``entity.alert.informedEntity.stopId``
``entity.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/PSTA-GTFSRT-2019-19.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/PSTA-GTFSRT-2019-19.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/PSTA-GTFSRT-2019-19.png)
