### PSTA-GTFSRT-2020-27
#### Metadata
PSTA-GTFSRT-2020-27.json.zip: 269.81 MB
PSTA-GTFSRT-2020-27.pb.zip: 234.28 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 17091 | 17090 | 17092 |
| Sampling | 35.39 | 35.39 | 35.38 |
| 2020-06-29 | 2437 | 2437 | 2437 |
| 2020-06-30 | 2440 | 2440 | 2440 |
| 2020-07-01 | 2436 | 2435 | 2437 |
| 2020-07-02 | 2440 | 2440 | 2440 |
| 2020-07-03 | 2439 | 2439 | 2439 |
| 2020-07-04 | 2452 | 2452 | 2452 |
| 2020-07-05 | 2447 | 2447 | 2447 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.position.speed``
``entity.vehicle.timestamp``
``entity.vehicle.trip.routeId``
``entity.vehicle.trip.scheduleRelationship``
``entity.vehicle.trip.tripId``
``entity.vehicle.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
``entity.id``
``entity.tripUpdate.stopTimeUpdate.arrival.time``
``entity.tripUpdate.stopTimeUpdate.departure.time``
``entity.tripUpdate.stopTimeUpdate.scheduleRelationship``
``entity.tripUpdate.stopTimeUpdate.stopId``
``entity.tripUpdate.stopTimeUpdate.stopSequence``
``entity.tripUpdate.timestamp``
``entity.tripUpdate.trip.routeId``
``entity.tripUpdate.trip.scheduleRelationship``
``entity.tripUpdate.trip.tripId``
``entity.tripUpdate.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### alerts
``entity.alert.cause``
``entity.alert.descriptionText.translation.language``
``entity.alert.descriptionText.translation.text``
``entity.alert.effect``
``entity.alert.headerText.translation.language``
``entity.alert.headerText.translation.text``
``entity.alert.informedEntity.agencyId``
``entity.alert.informedEntity.stopId``
``entity.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/PSTA-GTFSRT-2020-27.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/PSTA-GTFSRT-2020-27.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/PSTA-GTFSRT-2020-27.png)
