### PSTA-GTFSRT-2018-40
#### Metadata
PSTA-GTFSRT-2018-40.json.zip: 343.38 MB
PSTA-GTFSRT-2018-40.pb.zip: 306.18 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 16973 | 16972 | 16973 |
| Sampling | 35.63 | 35.63 | 35.63 |
| 2018-10-01 | 2429 | 2429 | 2429 |
| 2018-10-02 | 2430 | 2430 | 2430 |
| 2018-10-03 | 2425 | 2425 | 2425 |
| 2018-10-04 | 2429 | 2429 | 2429 |
| 2018-10-05 | 2430 | 2430 | 2430 |
| 2018-10-06 | 2427 | 2426 | 2427 |
| 2018-10-07 | 2403 | 2403 | 2403 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.position.speed``
``entity.vehicle.timestamp``
``entity.vehicle.trip.routeId``
``entity.vehicle.trip.tripId``
``entity.vehicle.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
``entity.id``
``entity.tripUpdate.stopTimeUpdate.arrival.time``
``entity.tripUpdate.stopTimeUpdate.departure.time``
``entity.tripUpdate.stopTimeUpdate.stopId``
``entity.tripUpdate.stopTimeUpdate.stopSequence``
``entity.tripUpdate.timestamp``
``entity.tripUpdate.trip.routeId``
``entity.tripUpdate.trip.scheduleRelationship``
``entity.tripUpdate.trip.tripId``
``entity.tripUpdate.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### alerts
``entity.alert.descriptionText.translation.language``
``entity.alert.descriptionText.translation.text``
``entity.alert.headerText.translation.language``
``entity.alert.headerText.translation.text``
``entity.alert.informedEntity.agencyId``
``entity.alert.informedEntity.stopId``
``entity.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/PSTA-GTFSRT-2018-40.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/PSTA-GTFSRT-2018-40.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/PSTA-GTFSRT-2018-40.png)
