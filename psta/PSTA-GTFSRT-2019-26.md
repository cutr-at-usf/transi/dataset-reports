### PSTA-GTFSRT-2019-26
#### Metadata
PSTA-GTFSRT-2019-26.json.zip: 355.37 MB
PSTA-GTFSRT-2019-26.pb.zip: 308.61 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 16980 | 16980 | 16980 |
| Sampling | 35.62 | 35.62 | 35.62 |
| 2019-06-24 | 2424 | 2424 | 2424 |
| 2019-06-25 | 2408 | 2408 | 2408 |
| 2019-06-26 | 2421 | 2421 | 2421 |
| 2019-06-27 | 2425 | 2425 | 2425 |
| 2019-06-28 | 2428 | 2428 | 2428 |
| 2019-06-29 | 2434 | 2434 | 2434 |
| 2019-06-30 | 2440 | 2440 | 2440 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.position.speed``
``entity.vehicle.timestamp``
``entity.vehicle.trip.routeId``
``entity.vehicle.trip.scheduleRelationship``
``entity.vehicle.trip.tripId``
``entity.vehicle.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
``entity.id``
``entity.tripUpdate.stopTimeUpdate.arrival.time``
``entity.tripUpdate.stopTimeUpdate.departure.time``
``entity.tripUpdate.stopTimeUpdate.scheduleRelationship``
``entity.tripUpdate.stopTimeUpdate.stopId``
``entity.tripUpdate.stopTimeUpdate.stopSequence``
``entity.tripUpdate.timestamp``
``entity.tripUpdate.trip.routeId``
``entity.tripUpdate.trip.scheduleRelationship``
``entity.tripUpdate.trip.tripId``
``entity.tripUpdate.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### alerts
``entity.alert.cause``
``entity.alert.descriptionText.translation.language``
``entity.alert.descriptionText.translation.text``
``entity.alert.effect``
``entity.alert.headerText.translation.language``
``entity.alert.headerText.translation.text``
``entity.alert.informedEntity.agencyId``
``entity.alert.informedEntity.stopId``
``entity.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/PSTA-GTFSRT-2019-26.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/PSTA-GTFSRT-2019-26.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/PSTA-GTFSRT-2019-26.png)
