### PSTA-GTFSRT-2019-36
#### Metadata
PSTA-GTFSRT-2019-36.json.zip: 258.54 MB
PSTA-GTFSRT-2019-36.pb.zip: 224.00 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 16262 | 16253 | 16263 |
| Sampling | 35.90 | 35.92 | 35.90 |
| 2019-09-02 | 2439 | 2439 | 2439 |
| 2019-09-03 | 2367 | 2366 | 2367 |
| 2019-09-04 | 2342 | 2341 | 2342 |
| 2019-09-05 | 2430 | 2425 | 2430 |
| 2019-09-06 | 2431 | 2429 | 2432 |
| 2019-09-07 | 2428 | 2428 | 2428 |
| 2019-09-08 | 1825 | 1825 | 1825 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.position.speed``
``entity.vehicle.timestamp``
``entity.vehicle.trip.routeId``
``entity.vehicle.trip.scheduleRelationship``
``entity.vehicle.trip.tripId``
``entity.vehicle.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
``entity.id``
``entity.tripUpdate.stopTimeUpdate.arrival.time``
``entity.tripUpdate.stopTimeUpdate.departure.time``
``entity.tripUpdate.stopTimeUpdate.scheduleRelationship``
``entity.tripUpdate.stopTimeUpdate.stopId``
``entity.tripUpdate.stopTimeUpdate.stopSequence``
``entity.tripUpdate.timestamp``
``entity.tripUpdate.trip.routeId``
``entity.tripUpdate.trip.scheduleRelationship``
``entity.tripUpdate.trip.tripId``
``entity.tripUpdate.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### alerts
``entity.alert.cause``
``entity.alert.descriptionText.translation.language``
``entity.alert.descriptionText.translation.text``
``entity.alert.effect``
``entity.alert.headerText.translation.language``
``entity.alert.headerText.translation.text``
``entity.alert.informedEntity.agencyId``
``entity.alert.informedEntity.stopId``
``entity.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/PSTA-GTFSRT-2019-36.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/PSTA-GTFSRT-2019-36.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/PSTA-GTFSRT-2019-36.png)
