### PSTA-GTFSRT-2017-50
#### Metadata
PSTA-GTFSRT-2017-50.json.zip: 325.59 MB
PSTA-GTFSRT-2017-50.pb.zip: 289.83 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 16952 | 16952 | 16953 |
| Sampling | 35.68 | 35.68 | 35.68 |
| 2017-12-11 | 2436 | 2436 | 2436 |
| 2017-12-12 | 2435 | 2435 | 2434 |
| 2017-12-13 | 2433 | 2433 | 2434 |
| 2017-12-14 | 2333 | 2333 | 2333 |
| 2017-12-15 | 2431 | 2431 | 2432 |
| 2017-12-16 | 2439 | 2439 | 2439 |
| 2017-12-17 | 2445 | 2445 | 2445 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.position.speed``
``entity.vehicle.timestamp``
``entity.vehicle.trip.routeId``
``entity.vehicle.trip.tripId``
``entity.vehicle.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
``entity.id``
``entity.tripUpdate.stopTimeUpdate.arrival.time``
``entity.tripUpdate.stopTimeUpdate.departure.time``
``entity.tripUpdate.stopTimeUpdate.stopId``
``entity.tripUpdate.stopTimeUpdate.stopSequence``
``entity.tripUpdate.timestamp``
``entity.tripUpdate.trip.routeId``
``entity.tripUpdate.trip.scheduleRelationship``
``entity.tripUpdate.trip.tripId``
``entity.tripUpdate.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### alerts
``entity.alert.descriptionText.translation.language``
``entity.alert.descriptionText.translation.text``
``entity.alert.headerText.translation.language``
``entity.alert.headerText.translation.text``
``entity.alert.informedEntity.agencyId``
``entity.alert.informedEntity.stopId``
``entity.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/PSTA-GTFSRT-2017-50.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/PSTA-GTFSRT-2017-50.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/PSTA-GTFSRT-2017-50.png)
