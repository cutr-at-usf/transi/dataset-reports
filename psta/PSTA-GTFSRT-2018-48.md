### PSTA-GTFSRT-2018-48
#### Metadata
PSTA-GTFSRT-2018-48.json.zip: 331.92 MB
PSTA-GTFSRT-2018-48.pb.zip: 296.78 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 16881 | 16872 | 16886 |
| Sampling | 35.83 | 35.85 | 35.82 |
| 2018-11-26 | 2425 | 2425 | 2426 |
| 2018-11-27 | 2429 | 2429 | 2429 |
| 2018-11-28 | 2431 | 2431 | 2431 |
| 2018-11-29 | 2428 | 2426 | 2428 |
| 2018-11-30 | 2315 | 2310 | 2319 |
| 2018-12-01 | 2430 | 2429 | 2430 |
| 2018-12-02 | 2423 | 2422 | 2423 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.position.speed``
``entity.vehicle.timestamp``
``entity.vehicle.trip.routeId``
``entity.vehicle.trip.tripId``
``entity.vehicle.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
``entity.id``
``entity.tripUpdate.stopTimeUpdate.arrival.time``
``entity.tripUpdate.stopTimeUpdate.departure.time``
``entity.tripUpdate.stopTimeUpdate.stopId``
``entity.tripUpdate.stopTimeUpdate.stopSequence``
``entity.tripUpdate.timestamp``
``entity.tripUpdate.trip.routeId``
``entity.tripUpdate.trip.scheduleRelationship``
``entity.tripUpdate.trip.tripId``
``entity.tripUpdate.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### alerts
``entity.alert.descriptionText.translation.language``
``entity.alert.descriptionText.translation.text``
``entity.alert.headerText.translation.language``
``entity.alert.headerText.translation.text``
``entity.alert.informedEntity.agencyId``
``entity.alert.informedEntity.stopId``
``entity.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/PSTA-GTFSRT-2018-48.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/PSTA-GTFSRT-2018-48.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/PSTA-GTFSRT-2018-48.png)
