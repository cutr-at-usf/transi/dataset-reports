### PSTA-GTFSRT-2020-03
#### Metadata
PSTA-GTFSRT-2020-03.json.zip: 359.79 MB
PSTA-GTFSRT-2020-03.pb.zip: 312.38 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 16929 | 16927 | 16933 |
| Sampling | 35.72 | 35.73 | 35.72 |
| 2020-01-13 | 2426 | 2426 | 2426 |
| 2020-01-14 | 2425 | 2424 | 2425 |
| 2020-01-15 | 2384 | 2384 | 2384 |
| 2020-01-16 | 2403 | 2403 | 2405 |
| 2020-01-17 | 2423 | 2423 | 2424 |
| 2020-01-18 | 2435 | 2435 | 2435 |
| 2020-01-19 | 2433 | 2432 | 2434 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.position.speed``
``entity.vehicle.timestamp``
``entity.vehicle.trip.routeId``
``entity.vehicle.trip.scheduleRelationship``
``entity.vehicle.trip.tripId``
``entity.vehicle.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
``entity.id``
``entity.tripUpdate.stopTimeUpdate.arrival.time``
``entity.tripUpdate.stopTimeUpdate.departure.time``
``entity.tripUpdate.stopTimeUpdate.scheduleRelationship``
``entity.tripUpdate.stopTimeUpdate.stopId``
``entity.tripUpdate.stopTimeUpdate.stopSequence``
``entity.tripUpdate.timestamp``
``entity.tripUpdate.trip.routeId``
``entity.tripUpdate.trip.scheduleRelationship``
``entity.tripUpdate.trip.tripId``
``entity.tripUpdate.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### alerts
``entity.alert.cause``
``entity.alert.descriptionText.translation.language``
``entity.alert.descriptionText.translation.text``
``entity.alert.effect``
``entity.alert.headerText.translation.language``
``entity.alert.headerText.translation.text``
``entity.alert.informedEntity.agencyId``
``entity.alert.informedEntity.stopId``
``entity.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/PSTA-GTFSRT-2020-03.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/PSTA-GTFSRT-2020-03.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/PSTA-GTFSRT-2020-03.png)
