### PSTA-GTFSRT-2019-13
#### Metadata
PSTA-GTFSRT-2019-13.json.zip: 368.64 MB
PSTA-GTFSRT-2019-13.pb.zip: 318.97 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 16997 | 16997 | 16997 |
| Sampling | 35.58 | 35.58 | 35.58 |
| 2019-03-25 | 2428 | 2428 | 2428 |
| 2019-03-26 | 2418 | 2418 | 2418 |
| 2019-03-27 | 2423 | 2423 | 2423 |
| 2019-03-28 | 2428 | 2428 | 2428 |
| 2019-03-29 | 2429 | 2429 | 2429 |
| 2019-03-30 | 2433 | 2433 | 2433 |
| 2019-03-31 | 2438 | 2438 | 2438 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.position.speed``
``entity.vehicle.timestamp``
``entity.vehicle.trip.routeId``
``entity.vehicle.trip.scheduleRelationship``
``entity.vehicle.trip.tripId``
``entity.vehicle.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
``entity.id``
``entity.tripUpdate.stopTimeUpdate.arrival.time``
``entity.tripUpdate.stopTimeUpdate.departure.time``
``entity.tripUpdate.stopTimeUpdate.scheduleRelationship``
``entity.tripUpdate.stopTimeUpdate.stopId``
``entity.tripUpdate.stopTimeUpdate.stopSequence``
``entity.tripUpdate.timestamp``
``entity.tripUpdate.trip.routeId``
``entity.tripUpdate.trip.scheduleRelationship``
``entity.tripUpdate.trip.tripId``
``entity.tripUpdate.vehicle.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### alerts
``entity.alert.cause``
``entity.alert.descriptionText.translation.language``
``entity.alert.descriptionText.translation.text``
``entity.alert.effect``
``entity.alert.headerText.translation.language``
``entity.alert.headerText.translation.text``
``entity.alert.informedEntity.agencyId``
``entity.alert.informedEntity.stopId``
``entity.id``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/PSTA-GTFSRT-2019-13.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/PSTA-GTFSRT-2019-13.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/PSTA-GTFSRT-2019-13.png)
