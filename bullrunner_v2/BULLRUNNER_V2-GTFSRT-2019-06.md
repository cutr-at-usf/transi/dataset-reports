### BULLRUNNER_V2-GTFSRT-2019-06
#### Metadata
BULLRUNNER_V2-GTFSRT-2019-06.json.zip: 11.40 MB
BULLRUNNER_V2-GTFSRT-2019-06.pb.zip: 8.22 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 20042 | 0 | 0 |
| Sampling | 30.18 | 0.00 | 0.00 |
| 2019-02-04 | 2864 | 0 | 0 |
| 2019-02-05 | 2859 | 0 | 0 |
| 2019-02-06 | 2861 | 0 | 0 |
| 2019-02-07 | 2856 | 0 | 0 |
| 2019-02-08 | 2864 | 0 | 0 |
| 2019-02-09 | 2866 | 0 | 0 |
| 2019-02-10 | 2872 | 0 | 0 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.occupancyStatus``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.position.speed``
``entity.vehicle.timestamp``
``entity.vehicle.trip.routeId``
``entity.vehicle.trip.scheduleRelationship``
``entity.vehicle.trip.tripId``
``entity.vehicle.vehicle.id``
``entity.vehicle.vehicle.label``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/BULLRUNNER_V2-GTFSRT-2019-06.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/BULLRUNNER_V2-GTFSRT-2019-06.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/BULLRUNNER_V2-GTFSRT-2019-06.png)
