### BULLRUNNER_V2-GTFSRT-2019-49
#### Metadata
BULLRUNNER_V2-GTFSRT-2019-49.json.zip: 11.06 MB
BULLRUNNER_V2-GTFSRT-2019-49.pb.zip: 8.06 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 19232 | 0 | 0 |
| Sampling | 31.45 | 0.00 | 0.00 |
| 2019-12-02 | 2760 | 0 | 0 |
| 2019-12-03 | 2728 | 0 | 0 |
| 2019-12-04 | 2549 | 0 | 0 |
| 2019-12-05 | 2727 | 0 | 0 |
| 2019-12-06 | 2745 | 0 | 0 |
| 2019-12-07 | 2861 | 0 | 0 |
| 2019-12-08 | 2862 | 0 | 0 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.occupancyStatus``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.position.speed``
``entity.vehicle.timestamp``
``entity.vehicle.trip.routeId``
``entity.vehicle.trip.scheduleRelationship``
``entity.vehicle.trip.tripId``
``entity.vehicle.vehicle.id``
``entity.vehicle.vehicle.label``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/BULLRUNNER_V2-GTFSRT-2019-49.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/BULLRUNNER_V2-GTFSRT-2019-49.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/BULLRUNNER_V2-GTFSRT-2019-49.png)
