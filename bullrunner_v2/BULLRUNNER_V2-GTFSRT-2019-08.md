### BULLRUNNER_V2-GTFSRT-2019-08
#### Metadata
BULLRUNNER_V2-GTFSRT-2019-08.json.zip: 11.41 MB
BULLRUNNER_V2-GTFSRT-2019-08.pb.zip: 8.24 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 20039 | 0 | 0 |
| Sampling | 30.18 | 0.00 | 0.00 |
| 2019-02-18 | 2864 | 0 | 0 |
| 2019-02-19 | 2865 | 0 | 0 |
| 2019-02-20 | 2854 | 0 | 0 |
| 2019-02-21 | 2858 | 0 | 0 |
| 2019-02-22 | 2861 | 0 | 0 |
| 2019-02-23 | 2868 | 0 | 0 |
| 2019-02-24 | 2869 | 0 | 0 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.occupancyStatus``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.position.speed``
``entity.vehicle.timestamp``
``entity.vehicle.trip.routeId``
``entity.vehicle.trip.scheduleRelationship``
``entity.vehicle.trip.tripId``
``entity.vehicle.vehicle.id``
``entity.vehicle.vehicle.label``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/BULLRUNNER_V2-GTFSRT-2019-08.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/BULLRUNNER_V2-GTFSRT-2019-08.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/BULLRUNNER_V2-GTFSRT-2019-08.png)
