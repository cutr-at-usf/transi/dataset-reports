### BULLRUNNER_V2-GTFSRT-2020-21
#### Metadata
BULLRUNNER_V2-GTFSRT-2020-21.json.zip: 9.60 MB
BULLRUNNER_V2-GTFSRT-2020-21.pb.zip: 7.08 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 20015 | 0 | 0 |
| Sampling | 30.22 | 0.00 | 0.00 |
| 2020-05-18 | 2862 | 0 | 0 |
| 2020-05-19 | 2844 | 0 | 0 |
| 2020-05-20 | 2842 | 0 | 0 |
| 2020-05-21 | 2861 | 0 | 0 |
| 2020-05-22 | 2867 | 0 | 0 |
| 2020-05-23 | 2870 | 0 | 0 |
| 2020-05-24 | 2869 | 0 | 0 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.occupancyStatus``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.position.speed``
``entity.vehicle.timestamp``
``entity.vehicle.trip.routeId``
``entity.vehicle.trip.scheduleRelationship``
``entity.vehicle.trip.tripId``
``entity.vehicle.vehicle.id``
``entity.vehicle.vehicle.label``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/BULLRUNNER_V2-GTFSRT-2020-21.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/BULLRUNNER_V2-GTFSRT-2020-21.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/BULLRUNNER_V2-GTFSRT-2020-21.png)
