### BULLRUNNER_V2-GTFSRT-2019-05
#### Metadata
BULLRUNNER_V2-GTFSRT-2019-05.json.zip: 2.51 MB
BULLRUNNER_V2-GTFSRT-2019-05.pb.zip: 1.82 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 5898 | 0 | 0 |
| Sampling | 30.25 | 0.00 | 0.00 |
| 2019-01-28 | 0 | 0 | 0 |
| 2019-01-29 | 0 | 0 | 0 |
| 2019-01-30 | 0 | 0 | 0 |
| 2019-01-31 | 0 | 0 | 0 |
| 2019-02-01 | 187 | 0 | 0 |
| 2019-02-02 | 2869 | 0 | 0 |
| 2019-02-03 | 2842 | 0 | 0 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.occupancyStatus``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.position.speed``
``entity.vehicle.timestamp``
``entity.vehicle.trip.routeId``
``entity.vehicle.trip.scheduleRelationship``
``entity.vehicle.trip.tripId``
``entity.vehicle.vehicle.id``
``entity.vehicle.vehicle.label``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/BULLRUNNER_V2-GTFSRT-2019-05.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/BULLRUNNER_V2-GTFSRT-2019-05.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/BULLRUNNER_V2-GTFSRT-2019-05.png)
