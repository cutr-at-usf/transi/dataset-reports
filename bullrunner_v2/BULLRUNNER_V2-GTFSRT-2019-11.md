### BULLRUNNER_V2-GTFSRT-2019-11
#### Metadata
BULLRUNNER_V2-GTFSRT-2019-11.json.zip: 10.83 MB
BULLRUNNER_V2-GTFSRT-2019-11.pb.zip: 7.85 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 20040 | 0 | 0 |
| Sampling | 30.18 | 0.00 | 0.00 |
| 2019-03-11 | 2863 | 0 | 0 |
| 2019-03-12 | 2862 | 0 | 0 |
| 2019-03-13 | 2866 | 0 | 0 |
| 2019-03-14 | 2863 | 0 | 0 |
| 2019-03-15 | 2866 | 0 | 0 |
| 2019-03-16 | 2855 | 0 | 0 |
| 2019-03-17 | 2865 | 0 | 0 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.occupancyStatus``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.position.speed``
``entity.vehicle.timestamp``
``entity.vehicle.trip.routeId``
``entity.vehicle.trip.scheduleRelationship``
``entity.vehicle.trip.tripId``
``entity.vehicle.vehicle.id``
``entity.vehicle.vehicle.label``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/BULLRUNNER_V2-GTFSRT-2019-11.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/BULLRUNNER_V2-GTFSRT-2019-11.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/BULLRUNNER_V2-GTFSRT-2019-11.png)
