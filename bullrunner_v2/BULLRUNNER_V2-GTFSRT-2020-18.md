### BULLRUNNER_V2-GTFSRT-2020-18
#### Metadata
BULLRUNNER_V2-GTFSRT-2020-18.json.zip: 9.15 MB
BULLRUNNER_V2-GTFSRT-2020-18.pb.zip: 6.70 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 19664 | 0 | 0 |
| Sampling | 30.76 | 0.00 | 0.00 |
| 2020-04-27 | 2512 | 0 | 0 |
| 2020-04-28 | 2860 | 0 | 0 |
| 2020-04-29 | 2863 | 0 | 0 |
| 2020-04-30 | 2833 | 0 | 0 |
| 2020-05-01 | 2865 | 0 | 0 |
| 2020-05-02 | 2871 | 0 | 0 |
| 2020-05-03 | 2860 | 0 | 0 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.occupancyStatus``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.position.speed``
``entity.vehicle.timestamp``
``entity.vehicle.trip.routeId``
``entity.vehicle.trip.scheduleRelationship``
``entity.vehicle.trip.tripId``
``entity.vehicle.vehicle.id``
``entity.vehicle.vehicle.label``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/BULLRUNNER_V2-GTFSRT-2020-18.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/BULLRUNNER_V2-GTFSRT-2020-18.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/BULLRUNNER_V2-GTFSRT-2020-18.png)
