### BULLRUNNER_V2-GTFSRT-2019-24
#### Metadata
BULLRUNNER_V2-GTFSRT-2019-24.json.zip: 11.38 MB
BULLRUNNER_V2-GTFSRT-2019-24.pb.zip: 8.24 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 19870 | 0 | 0 |
| Sampling | 30.44 | 0.00 | 0.00 |
| 2019-06-10 | 2855 | 0 | 0 |
| 2019-06-11 | 2856 | 0 | 0 |
| 2019-06-12 | 2695 | 0 | 0 |
| 2019-06-13 | 2860 | 0 | 0 |
| 2019-06-14 | 2864 | 0 | 0 |
| 2019-06-15 | 2868 | 0 | 0 |
| 2019-06-16 | 2872 | 0 | 0 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.occupancyStatus``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.position.speed``
``entity.vehicle.timestamp``
``entity.vehicle.trip.routeId``
``entity.vehicle.trip.scheduleRelationship``
``entity.vehicle.trip.tripId``
``entity.vehicle.vehicle.id``
``entity.vehicle.vehicle.label``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/BULLRUNNER_V2-GTFSRT-2019-24.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/BULLRUNNER_V2-GTFSRT-2019-24.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/BULLRUNNER_V2-GTFSRT-2019-24.png)
