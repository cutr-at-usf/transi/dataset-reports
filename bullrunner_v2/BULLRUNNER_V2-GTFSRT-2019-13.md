### BULLRUNNER_V2-GTFSRT-2019-13
#### Metadata
BULLRUNNER_V2-GTFSRT-2019-13.json.zip: 11.51 MB
BULLRUNNER_V2-GTFSRT-2019-13.pb.zip: 8.34 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 20057 | 0 | 0 |
| Sampling | 30.15 | 0.00 | 0.00 |
| 2019-03-25 | 2864 | 0 | 0 |
| 2019-03-26 | 2864 | 0 | 0 |
| 2019-03-27 | 2866 | 0 | 0 |
| 2019-03-28 | 2865 | 0 | 0 |
| 2019-03-29 | 2865 | 0 | 0 |
| 2019-03-30 | 2866 | 0 | 0 |
| 2019-03-31 | 2867 | 0 | 0 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.occupancyStatus``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.position.speed``
``entity.vehicle.timestamp``
``entity.vehicle.trip.routeId``
``entity.vehicle.trip.scheduleRelationship``
``entity.vehicle.trip.tripId``
``entity.vehicle.vehicle.id``
``entity.vehicle.vehicle.label``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/BULLRUNNER_V2-GTFSRT-2019-13.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/BULLRUNNER_V2-GTFSRT-2019-13.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/BULLRUNNER_V2-GTFSRT-2019-13.png)
