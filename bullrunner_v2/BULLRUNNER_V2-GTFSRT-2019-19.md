### BULLRUNNER_V2-GTFSRT-2019-19
#### Metadata
BULLRUNNER_V2-GTFSRT-2019-19.json.zip: 10.77 MB
BULLRUNNER_V2-GTFSRT-2019-19.pb.zip: 7.80 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 19930 | 0 | 0 |
| Sampling | 30.35 | 0.00 | 0.00 |
| 2019-05-06 | 2854 | 0 | 0 |
| 2019-05-07 | 2857 | 0 | 0 |
| 2019-05-08 | 2744 | 0 | 0 |
| 2019-05-09 | 2863 | 0 | 0 |
| 2019-05-10 | 2873 | 0 | 0 |
| 2019-05-11 | 2868 | 0 | 0 |
| 2019-05-12 | 2871 | 0 | 0 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.occupancyStatus``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.position.speed``
``entity.vehicle.timestamp``
``entity.vehicle.trip.routeId``
``entity.vehicle.trip.scheduleRelationship``
``entity.vehicle.trip.tripId``
``entity.vehicle.vehicle.id``
``entity.vehicle.vehicle.label``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/BULLRUNNER_V2-GTFSRT-2019-19.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/BULLRUNNER_V2-GTFSRT-2019-19.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/BULLRUNNER_V2-GTFSRT-2019-19.png)
