### BULLRUNNER_V2-GTFSRT-2019-16
#### Metadata
BULLRUNNER_V2-GTFSRT-2019-16.json.zip: 10.21 MB
BULLRUNNER_V2-GTFSRT-2019-16.pb.zip: 7.38 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 17976 | 0 | 0 |
| Sampling | 33.64 | 0.00 | 0.00 |
| 2019-04-15 | 2742 | 0 | 0 |
| 2019-04-16 | 2566 | 0 | 0 |
| 2019-04-17 | 2565 | 0 | 0 |
| 2019-04-18 | 2526 | 0 | 0 |
| 2019-04-19 | 2472 | 0 | 0 |
| 2019-04-20 | 2507 | 0 | 0 |
| 2019-04-21 | 2598 | 0 | 0 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.occupancyStatus``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.position.speed``
``entity.vehicle.timestamp``
``entity.vehicle.trip.routeId``
``entity.vehicle.trip.scheduleRelationship``
``entity.vehicle.trip.tripId``
``entity.vehicle.vehicle.id``
``entity.vehicle.vehicle.label``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/BULLRUNNER_V2-GTFSRT-2019-16.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/BULLRUNNER_V2-GTFSRT-2019-16.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/BULLRUNNER_V2-GTFSRT-2019-16.png)
