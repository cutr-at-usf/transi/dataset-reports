### BULLRUNNER_V2-GTFSRT-2020-08
#### Metadata
BULLRUNNER_V2-GTFSRT-2020-08.json.zip: 10.39 MB
BULLRUNNER_V2-GTFSRT-2020-08.pb.zip: 7.55 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 18336 | 0 | 0 |
| Sampling | 32.99 | 0.00 | 0.00 |
| 2020-02-17 | 2846 | 0 | 0 |
| 2020-02-18 | 2761 | 0 | 0 |
| 2020-02-19 | 2830 | 0 | 0 |
| 2020-02-20 | 2829 | 0 | 0 |
| 2020-02-21 | 2837 | 0 | 0 |
| 2020-02-22 | 2172 | 0 | 0 |
| 2020-02-23 | 2061 | 0 | 0 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.occupancyStatus``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.position.speed``
``entity.vehicle.timestamp``
``entity.vehicle.trip.routeId``
``entity.vehicle.trip.scheduleRelationship``
``entity.vehicle.trip.tripId``
``entity.vehicle.vehicle.id``
``entity.vehicle.vehicle.label``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/BULLRUNNER_V2-GTFSRT-2020-08.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/BULLRUNNER_V2-GTFSRT-2020-08.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/BULLRUNNER_V2-GTFSRT-2020-08.png)
