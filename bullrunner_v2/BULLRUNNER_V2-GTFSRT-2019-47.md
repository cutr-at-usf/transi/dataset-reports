### BULLRUNNER_V2-GTFSRT-2019-47
#### Metadata
BULLRUNNER_V2-GTFSRT-2019-47.json.zip: 11.40 MB
BULLRUNNER_V2-GTFSRT-2019-47.pb.zip: 8.37 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 19208 | 0 | 0 |
| Sampling | 31.49 | 0.00 | 0.00 |
| 2019-11-18 | 2701 | 0 | 0 |
| 2019-11-19 | 2649 | 0 | 0 |
| 2019-11-20 | 2652 | 0 | 0 |
| 2019-11-21 | 2727 | 0 | 0 |
| 2019-11-22 | 2754 | 0 | 0 |
| 2019-11-23 | 2865 | 0 | 0 |
| 2019-11-24 | 2860 | 0 | 0 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.occupancyStatus``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.position.speed``
``entity.vehicle.timestamp``
``entity.vehicle.trip.routeId``
``entity.vehicle.trip.scheduleRelationship``
``entity.vehicle.trip.tripId``
``entity.vehicle.vehicle.id``
``entity.vehicle.vehicle.label``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/BULLRUNNER_V2-GTFSRT-2019-47.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/BULLRUNNER_V2-GTFSRT-2019-47.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/BULLRUNNER_V2-GTFSRT-2019-47.png)
