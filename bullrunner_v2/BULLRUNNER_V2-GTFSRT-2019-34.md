### BULLRUNNER_V2-GTFSRT-2019-34
#### Metadata
BULLRUNNER_V2-GTFSRT-2019-34.json.zip: 11.04 MB
BULLRUNNER_V2-GTFSRT-2019-34.pb.zip: 8.03 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 19870 | 0 | 0 |
| Sampling | 30.44 | 0.00 | 0.00 |
| 2019-08-19 | 2863 | 0 | 0 |
| 2019-08-20 | 2863 | 0 | 0 |
| 2019-08-21 | 2750 | 0 | 0 |
| 2019-08-22 | 2858 | 0 | 0 |
| 2019-08-23 | 2822 | 0 | 0 |
| 2019-08-24 | 2852 | 0 | 0 |
| 2019-08-25 | 2862 | 0 | 0 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.occupancyStatus``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.position.speed``
``entity.vehicle.timestamp``
``entity.vehicle.trip.routeId``
``entity.vehicle.trip.scheduleRelationship``
``entity.vehicle.trip.tripId``
``entity.vehicle.vehicle.id``
``entity.vehicle.vehicle.label``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/BULLRUNNER_V2-GTFSRT-2019-34.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/BULLRUNNER_V2-GTFSRT-2019-34.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/BULLRUNNER_V2-GTFSRT-2019-34.png)
