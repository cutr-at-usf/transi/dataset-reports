### BULLRUNNER_V2-GTFSRT-2020-24
#### Metadata
BULLRUNNER_V2-GTFSRT-2020-24.json.zip: 9.73 MB
BULLRUNNER_V2-GTFSRT-2020-24.pb.zip: 7.20 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 20000 | 0 | 0 |
| Sampling | 30.24 | 0.00 | 0.00 |
| 2020-06-08 | 2858 | 0 | 0 |
| 2020-06-09 | 2861 | 0 | 0 |
| 2020-06-10 | 2818 | 0 | 0 |
| 2020-06-11 | 2858 | 0 | 0 |
| 2020-06-12 | 2866 | 0 | 0 |
| 2020-06-13 | 2870 | 0 | 0 |
| 2020-06-14 | 2869 | 0 | 0 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.occupancyStatus``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.position.speed``
``entity.vehicle.timestamp``
``entity.vehicle.trip.routeId``
``entity.vehicle.trip.scheduleRelationship``
``entity.vehicle.trip.tripId``
``entity.vehicle.vehicle.id``
``entity.vehicle.vehicle.label``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/BULLRUNNER_V2-GTFSRT-2020-24.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/BULLRUNNER_V2-GTFSRT-2020-24.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/BULLRUNNER_V2-GTFSRT-2020-24.png)
