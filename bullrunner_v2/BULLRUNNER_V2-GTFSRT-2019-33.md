### BULLRUNNER_V2-GTFSRT-2019-33
#### Metadata
BULLRUNNER_V2-GTFSRT-2019-33.json.zip: 10.30 MB
BULLRUNNER_V2-GTFSRT-2019-33.pb.zip: 7.50 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 18692 | 0 | 0 |
| Sampling | 32.36 | 0.00 | 0.00 |
| 2019-08-12 | 2645 | 0 | 0 |
| 2019-08-13 | 2638 | 0 | 0 |
| 2019-08-14 | 2549 | 0 | 0 |
| 2019-08-15 | 2471 | 0 | 0 |
| 2019-08-16 | 2661 | 0 | 0 |
| 2019-08-17 | 2859 | 0 | 0 |
| 2019-08-18 | 2869 | 0 | 0 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.occupancyStatus``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.position.speed``
``entity.vehicle.timestamp``
``entity.vehicle.trip.routeId``
``entity.vehicle.trip.scheduleRelationship``
``entity.vehicle.trip.tripId``
``entity.vehicle.vehicle.id``
``entity.vehicle.vehicle.label``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/BULLRUNNER_V2-GTFSRT-2019-33.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/BULLRUNNER_V2-GTFSRT-2019-33.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/BULLRUNNER_V2-GTFSRT-2019-33.png)
