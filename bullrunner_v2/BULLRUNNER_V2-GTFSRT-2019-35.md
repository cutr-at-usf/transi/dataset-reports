### BULLRUNNER_V2-GTFSRT-2019-35
#### Metadata
BULLRUNNER_V2-GTFSRT-2019-35.json.zip: 10.19 MB
BULLRUNNER_V2-GTFSRT-2019-35.pb.zip: 7.28 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 14035 | 0 | 0 |
| Sampling | 30.78 | 0.00 | 0.00 |
| 2019-08-26 | 2859 | 0 | 0 |
| 2019-08-27 | 2736 | 0 | 0 |
| 2019-08-28 | 2733 | 0 | 0 |
| 2019-08-29 | 2844 | 0 | 0 |
| 2019-08-30 | 2863 | 0 | 0 |
| 2019-08-31 | 0 | 0 | 0 |
| 2019-09-01 | 0 | 0 | 0 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.occupancyStatus``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.position.speed``
``entity.vehicle.timestamp``
``entity.vehicle.trip.routeId``
``entity.vehicle.trip.scheduleRelationship``
``entity.vehicle.trip.tripId``
``entity.vehicle.vehicle.id``
``entity.vehicle.vehicle.label``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/BULLRUNNER_V2-GTFSRT-2019-35.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/BULLRUNNER_V2-GTFSRT-2019-35.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/BULLRUNNER_V2-GTFSRT-2019-35.png)
