### BULLRUNNER_V2-GTFSRT-2019-51
#### Metadata
BULLRUNNER_V2-GTFSRT-2019-51.json.zip: 9.97 MB
BULLRUNNER_V2-GTFSRT-2019-51.pb.zip: 7.33 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 19327 | 0 | 0 |
| Sampling | 31.29 | 0.00 | 0.00 |
| 2019-12-16 | 2830 | 0 | 0 |
| 2019-12-17 | 2847 | 0 | 0 |
| 2019-12-18 | 2754 | 0 | 0 |
| 2019-12-19 | 2725 | 0 | 0 |
| 2019-12-20 | 2554 | 0 | 0 |
| 2019-12-21 | 2752 | 0 | 0 |
| 2019-12-22 | 2865 | 0 | 0 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.occupancyStatus``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.position.speed``
``entity.vehicle.timestamp``
``entity.vehicle.trip.routeId``
``entity.vehicle.trip.scheduleRelationship``
``entity.vehicle.trip.tripId``
``entity.vehicle.vehicle.id``
``entity.vehicle.vehicle.label``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/BULLRUNNER_V2-GTFSRT-2019-51.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/BULLRUNNER_V2-GTFSRT-2019-51.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/BULLRUNNER_V2-GTFSRT-2019-51.png)
