### BULLRUNNER_V2-GTFSRT-2019-36
#### Metadata
BULLRUNNER_V2-GTFSRT-2019-36.json.zip: 9.21 MB
BULLRUNNER_V2-GTFSRT-2019-36.pb.zip: 6.24 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 14080 | 0 | 0 |
| Sampling | 30.68 | 0.00 | 0.00 |
| 2019-09-02 | 2868 | 0 | 0 |
| 2019-09-03 | 2730 | 0 | 0 |
| 2019-09-04 | 2759 | 0 | 0 |
| 2019-09-05 | 2858 | 0 | 0 |
| 2019-09-06 | 2865 | 0 | 0 |
| 2019-09-07 | 0 | 0 | 0 |
| 2019-09-08 | 0 | 0 | 0 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.occupancyStatus``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.position.speed``
``entity.vehicle.timestamp``
``entity.vehicle.trip.routeId``
``entity.vehicle.trip.scheduleRelationship``
``entity.vehicle.trip.tripId``
``entity.vehicle.vehicle.id``
``entity.vehicle.vehicle.label``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/BULLRUNNER_V2-GTFSRT-2019-36.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/BULLRUNNER_V2-GTFSRT-2019-36.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/BULLRUNNER_V2-GTFSRT-2019-36.png)
