### BULLRUNNER_V2-GTFSRT-2019-42
#### Metadata
BULLRUNNER_V2-GTFSRT-2019-42.json.zip: 11.11 MB
BULLRUNNER_V2-GTFSRT-2019-42.pb.zip: 8.14 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 18780 | 0 | 0 |
| Sampling | 32.20 | 0.00 | 0.00 |
| 2019-10-14 | 2765 | 0 | 0 |
| 2019-10-15 | 2733 | 0 | 0 |
| 2019-10-16 | 2680 | 0 | 0 |
| 2019-10-17 | 2299 | 0 | 0 |
| 2019-10-18 | 2802 | 0 | 0 |
| 2019-10-19 | 2779 | 0 | 0 |
| 2019-10-20 | 2722 | 0 | 0 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.occupancyStatus``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.position.speed``
``entity.vehicle.timestamp``
``entity.vehicle.trip.routeId``
``entity.vehicle.trip.scheduleRelationship``
``entity.vehicle.trip.tripId``
``entity.vehicle.vehicle.id``
``entity.vehicle.vehicle.label``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/BULLRUNNER_V2-GTFSRT-2019-42.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/BULLRUNNER_V2-GTFSRT-2019-42.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/BULLRUNNER_V2-GTFSRT-2019-42.png)
