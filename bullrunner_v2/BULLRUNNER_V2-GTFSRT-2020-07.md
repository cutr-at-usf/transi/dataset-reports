### BULLRUNNER_V2-GTFSRT-2020-07
#### Metadata
BULLRUNNER_V2-GTFSRT-2020-07.json.zip: 10.48 MB
BULLRUNNER_V2-GTFSRT-2020-07.pb.zip: 7.66 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 18200 | 0 | 0 |
| Sampling | 33.23 | 0.00 | 0.00 |
| 2020-02-10 | 2830 | 0 | 0 |
| 2020-02-11 | 2803 | 0 | 0 |
| 2020-02-12 | 2705 | 0 | 0 |
| 2020-02-13 | 2791 | 0 | 0 |
| 2020-02-14 | 2822 | 0 | 0 |
| 2020-02-15 | 2226 | 0 | 0 |
| 2020-02-16 | 2023 | 0 | 0 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.occupancyStatus``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.position.speed``
``entity.vehicle.timestamp``
``entity.vehicle.trip.routeId``
``entity.vehicle.trip.scheduleRelationship``
``entity.vehicle.trip.tripId``
``entity.vehicle.vehicle.id``
``entity.vehicle.vehicle.label``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/BULLRUNNER_V2-GTFSRT-2020-07.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/BULLRUNNER_V2-GTFSRT-2020-07.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/BULLRUNNER_V2-GTFSRT-2020-07.png)
