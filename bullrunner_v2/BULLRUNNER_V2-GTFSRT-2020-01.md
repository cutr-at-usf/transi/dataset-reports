### BULLRUNNER_V2-GTFSRT-2020-01
#### Metadata
BULLRUNNER_V2-GTFSRT-2020-01.json.zip: 7.36 MB
BULLRUNNER_V2-GTFSRT-2020-01.pb.zip: 5.55 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 17877 | 0 | 0 |
| Sampling | 33.83 | 0.00 | 0.00 |
| 2019-12-30 | 2855 | 0 | 0 |
| 2019-12-31 | 2861 | 0 | 0 |
| 2020-01-01 | 2774 | 0 | 0 |
| 2020-01-02 | 2787 | 0 | 0 |
| 2020-01-03 | 2500 | 0 | 0 |
| 2020-01-04 | 2058 | 0 | 0 |
| 2020-01-05 | 2042 | 0 | 0 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.occupancyStatus``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.position.speed``
``entity.vehicle.timestamp``
``entity.vehicle.trip.routeId``
``entity.vehicle.trip.scheduleRelationship``
``entity.vehicle.trip.tripId``
``entity.vehicle.vehicle.id``
``entity.vehicle.vehicle.label``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/BULLRUNNER_V2-GTFSRT-2020-01.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/BULLRUNNER_V2-GTFSRT-2020-01.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/BULLRUNNER_V2-GTFSRT-2020-01.png)
