### BULLRUNNER_V2-GTFSRT-2020-06
#### Metadata
BULLRUNNER_V2-GTFSRT-2020-06.json.zip: 10.66 MB
BULLRUNNER_V2-GTFSRT-2020-06.pb.zip: 7.81 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 18284 | 0 | 0 |
| Sampling | 33.08 | 0.00 | 0.00 |
| 2020-02-03 | 2812 | 0 | 0 |
| 2020-02-04 | 2707 | 0 | 0 |
| 2020-02-05 | 2820 | 0 | 0 |
| 2020-02-06 | 2844 | 0 | 0 |
| 2020-02-07 | 2833 | 0 | 0 |
| 2020-02-08 | 2074 | 0 | 0 |
| 2020-02-09 | 2194 | 0 | 0 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.occupancyStatus``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.position.speed``
``entity.vehicle.timestamp``
``entity.vehicle.trip.routeId``
``entity.vehicle.trip.scheduleRelationship``
``entity.vehicle.trip.tripId``
``entity.vehicle.vehicle.id``
``entity.vehicle.vehicle.label``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/BULLRUNNER_V2-GTFSRT-2020-06.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/BULLRUNNER_V2-GTFSRT-2020-06.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/BULLRUNNER_V2-GTFSRT-2020-06.png)
