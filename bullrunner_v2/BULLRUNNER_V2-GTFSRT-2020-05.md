### BULLRUNNER_V2-GTFSRT-2020-05
#### Metadata
BULLRUNNER_V2-GTFSRT-2020-05.json.zip: 10.16 MB
BULLRUNNER_V2-GTFSRT-2020-05.pb.zip: 7.41 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 17883 | 0 | 0 |
| Sampling | 33.82 | 0.00 | 0.00 |
| 2020-01-27 | 2795 | 0 | 0 |
| 2020-01-28 | 2778 | 0 | 0 |
| 2020-01-29 | 2712 | 0 | 0 |
| 2020-01-30 | 2661 | 0 | 0 |
| 2020-01-31 | 2811 | 0 | 0 |
| 2020-02-01 | 2063 | 0 | 0 |
| 2020-02-02 | 2063 | 0 | 0 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.occupancyStatus``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.position.speed``
``entity.vehicle.timestamp``
``entity.vehicle.trip.routeId``
``entity.vehicle.trip.scheduleRelationship``
``entity.vehicle.trip.tripId``
``entity.vehicle.vehicle.id``
``entity.vehicle.vehicle.label``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/BULLRUNNER_V2-GTFSRT-2020-05.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/BULLRUNNER_V2-GTFSRT-2020-05.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/BULLRUNNER_V2-GTFSRT-2020-05.png)
