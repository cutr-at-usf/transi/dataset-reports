### BULLRUNNER_V2-GTFSRT-2019-21
#### Metadata
BULLRUNNER_V2-GTFSRT-2019-21.json.zip: 11.24 MB
BULLRUNNER_V2-GTFSRT-2019-21.pb.zip: 8.13 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 19572 | 0 | 0 |
| Sampling | 30.90 | 0.00 | 0.00 |
| 2019-05-20 | 2864 | 0 | 0 |
| 2019-05-21 | 2853 | 0 | 0 |
| 2019-05-22 | 2556 | 0 | 0 |
| 2019-05-23 | 2777 | 0 | 0 |
| 2019-05-24 | 2858 | 0 | 0 |
| 2019-05-25 | 2797 | 0 | 0 |
| 2019-05-26 | 2867 | 0 | 0 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.occupancyStatus``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.position.speed``
``entity.vehicle.timestamp``
``entity.vehicle.trip.routeId``
``entity.vehicle.trip.scheduleRelationship``
``entity.vehicle.trip.tripId``
``entity.vehicle.vehicle.id``
``entity.vehicle.vehicle.label``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/BULLRUNNER_V2-GTFSRT-2019-21.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/BULLRUNNER_V2-GTFSRT-2019-21.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/BULLRUNNER_V2-GTFSRT-2019-21.png)
