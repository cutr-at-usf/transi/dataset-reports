### BULLRUNNER_V2-GTFSRT-2019-52
#### Metadata
BULLRUNNER_V2-GTFSRT-2019-52.json.zip: 6.81 MB
BULLRUNNER_V2-GTFSRT-2019-52.pb.zip: 5.25 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 20034 | 0 | 0 |
| Sampling | 30.19 | 0.00 | 0.00 |
| 2019-12-23 | 2852 | 0 | 0 |
| 2019-12-24 | 2861 | 0 | 0 |
| 2019-12-25 | 2873 | 0 | 0 |
| 2019-12-26 | 2859 | 0 | 0 |
| 2019-12-27 | 2860 | 0 | 0 |
| 2019-12-28 | 2860 | 0 | 0 |
| 2019-12-29 | 2869 | 0 | 0 |
#### Structures
##### vehicle_positions
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/BULLRUNNER_V2-GTFSRT-2019-52.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/BULLRUNNER_V2-GTFSRT-2019-52.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/BULLRUNNER_V2-GTFSRT-2019-52.png)
