### BULLRUNNER_V2-GTFSRT-2019-07
#### Metadata
BULLRUNNER_V2-GTFSRT-2019-07.json.zip: 11.40 MB
BULLRUNNER_V2-GTFSRT-2019-07.pb.zip: 8.21 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 20048 | 0 | 0 |
| Sampling | 30.17 | 0.00 | 0.00 |
| 2019-02-11 | 2866 | 0 | 0 |
| 2019-02-12 | 2860 | 0 | 0 |
| 2019-02-13 | 2862 | 0 | 0 |
| 2019-02-14 | 2860 | 0 | 0 |
| 2019-02-15 | 2867 | 0 | 0 |
| 2019-02-16 | 2868 | 0 | 0 |
| 2019-02-17 | 2865 | 0 | 0 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.occupancyStatus``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.position.speed``
``entity.vehicle.timestamp``
``entity.vehicle.trip.routeId``
``entity.vehicle.trip.scheduleRelationship``
``entity.vehicle.trip.tripId``
``entity.vehicle.vehicle.id``
``entity.vehicle.vehicle.label``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/BULLRUNNER_V2-GTFSRT-2019-07.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/BULLRUNNER_V2-GTFSRT-2019-07.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/BULLRUNNER_V2-GTFSRT-2019-07.png)
