### BULLRUNNER_V2-GTFSRT-2019-10
#### Metadata
BULLRUNNER_V2-GTFSRT-2019-10.json.zip: 11.71 MB
BULLRUNNER_V2-GTFSRT-2019-10.pb.zip: 8.53 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 19903 | 0 | 0 |
| Sampling | 30.21 | 0.00 | 0.00 |
| 2019-03-04 | 2839 | 0 | 0 |
| 2019-03-05 | 2862 | 0 | 0 |
| 2019-03-06 | 2859 | 0 | 0 |
| 2019-03-07 | 2858 | 0 | 0 |
| 2019-03-08 | 2869 | 0 | 0 |
| 2019-03-09 | 2871 | 0 | 0 |
| 2019-03-10 | 2745 | 0 | 0 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.occupancyStatus``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.position.speed``
``entity.vehicle.timestamp``
``entity.vehicle.trip.routeId``
``entity.vehicle.trip.scheduleRelationship``
``entity.vehicle.trip.tripId``
``entity.vehicle.vehicle.id``
``entity.vehicle.vehicle.label``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/BULLRUNNER_V2-GTFSRT-2019-10.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/BULLRUNNER_V2-GTFSRT-2019-10.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/BULLRUNNER_V2-GTFSRT-2019-10.png)
