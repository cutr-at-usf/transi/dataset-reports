### BULLRUNNER_V2-GTFSRT-2020-14
#### Metadata
BULLRUNNER_V2-GTFSRT-2020-14.json.zip: 10.01 MB
BULLRUNNER_V2-GTFSRT-2020-14.pb.zip: 7.30 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 19944 | 0 | 0 |
| Sampling | 30.33 | 0.00 | 0.00 |
| 2020-03-30 | 2838 | 0 | 0 |
| 2020-03-31 | 2844 | 0 | 0 |
| 2020-04-01 | 2830 | 0 | 0 |
| 2020-04-02 | 2850 | 0 | 0 |
| 2020-04-03 | 2856 | 0 | 0 |
| 2020-04-04 | 2867 | 0 | 0 |
| 2020-04-05 | 2859 | 0 | 0 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.occupancyStatus``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.position.speed``
``entity.vehicle.timestamp``
``entity.vehicle.trip.routeId``
``entity.vehicle.trip.scheduleRelationship``
``entity.vehicle.trip.tripId``
``entity.vehicle.vehicle.id``
``entity.vehicle.vehicle.label``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/BULLRUNNER_V2-GTFSRT-2020-14.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/BULLRUNNER_V2-GTFSRT-2020-14.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/BULLRUNNER_V2-GTFSRT-2020-14.png)
