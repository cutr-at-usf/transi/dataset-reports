### BULLRUNNER_V2-GTFSRT-2020-22
#### Metadata
BULLRUNNER_V2-GTFSRT-2020-22.json.zip: 8.95 MB
BULLRUNNER_V2-GTFSRT-2020-22.pb.zip: 6.65 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 19925 | 0 | 0 |
| Sampling | 30.35 | 0.00 | 0.00 |
| 2020-05-25 | 2873 | 0 | 0 |
| 2020-05-26 | 2862 | 0 | 0 |
| 2020-05-27 | 2721 | 0 | 0 |
| 2020-05-28 | 2862 | 0 | 0 |
| 2020-05-29 | 2867 | 0 | 0 |
| 2020-05-30 | 2870 | 0 | 0 |
| 2020-05-31 | 2870 | 0 | 0 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.occupancyStatus``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.position.speed``
``entity.vehicle.timestamp``
``entity.vehicle.trip.routeId``
``entity.vehicle.trip.scheduleRelationship``
``entity.vehicle.trip.tripId``
``entity.vehicle.vehicle.id``
``entity.vehicle.vehicle.label``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/BULLRUNNER_V2-GTFSRT-2020-22.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/BULLRUNNER_V2-GTFSRT-2020-22.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/BULLRUNNER_V2-GTFSRT-2020-22.png)
