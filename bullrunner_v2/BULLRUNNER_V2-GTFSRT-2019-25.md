### BULLRUNNER_V2-GTFSRT-2019-25
#### Metadata
BULLRUNNER_V2-GTFSRT-2019-25.json.zip: 11.39 MB
BULLRUNNER_V2-GTFSRT-2019-25.pb.zip: 8.26 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 19837 | 0 | 0 |
| Sampling | 30.49 | 0.00 | 0.00 |
| 2019-06-17 | 2862 | 0 | 0 |
| 2019-06-18 | 2861 | 0 | 0 |
| 2019-06-19 | 2715 | 0 | 0 |
| 2019-06-20 | 2864 | 0 | 0 |
| 2019-06-21 | 2868 | 0 | 0 |
| 2019-06-22 | 2796 | 0 | 0 |
| 2019-06-23 | 2871 | 0 | 0 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.occupancyStatus``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.position.speed``
``entity.vehicle.timestamp``
``entity.vehicle.trip.routeId``
``entity.vehicle.trip.scheduleRelationship``
``entity.vehicle.trip.tripId``
``entity.vehicle.vehicle.id``
``entity.vehicle.vehicle.label``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/BULLRUNNER_V2-GTFSRT-2019-25.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/BULLRUNNER_V2-GTFSRT-2019-25.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/BULLRUNNER_V2-GTFSRT-2019-25.png)
