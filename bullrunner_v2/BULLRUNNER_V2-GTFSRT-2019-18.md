### BULLRUNNER_V2-GTFSRT-2019-18
#### Metadata
BULLRUNNER_V2-GTFSRT-2019-18.json.zip: 10.46 MB
BULLRUNNER_V2-GTFSRT-2019-18.pb.zip: 7.54 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 18762 | 0 | 0 |
| Sampling | 32.24 | 0.00 | 0.00 |
| 2019-04-29 | 2583 | 0 | 0 |
| 2019-04-30 | 2634 | 0 | 0 |
| 2019-05-01 | 2494 | 0 | 0 |
| 2019-05-02 | 2646 | 0 | 0 |
| 2019-05-03 | 2690 | 0 | 0 |
| 2019-05-04 | 2851 | 0 | 0 |
| 2019-05-05 | 2864 | 0 | 0 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.occupancyStatus``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.position.speed``
``entity.vehicle.timestamp``
``entity.vehicle.trip.routeId``
``entity.vehicle.trip.scheduleRelationship``
``entity.vehicle.trip.tripId``
``entity.vehicle.vehicle.id``
``entity.vehicle.vehicle.label``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/BULLRUNNER_V2-GTFSRT-2019-18.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/BULLRUNNER_V2-GTFSRT-2019-18.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/BULLRUNNER_V2-GTFSRT-2019-18.png)
