### BULLRUNNER_V2-GTFSRT-2019-43
#### Metadata
BULLRUNNER_V2-GTFSRT-2019-43.json.zip: 10.80 MB
BULLRUNNER_V2-GTFSRT-2019-43.pb.zip: 7.90 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 18601 | 0 | 0 |
| Sampling | 32.52 | 0.00 | 0.00 |
| 2019-10-21 | 2508 | 0 | 0 |
| 2019-10-22 | 2274 | 0 | 0 |
| 2019-10-23 | 2712 | 0 | 0 |
| 2019-10-24 | 2756 | 0 | 0 |
| 2019-10-25 | 2644 | 0 | 0 |
| 2019-10-26 | 2839 | 0 | 0 |
| 2019-10-27 | 2868 | 0 | 0 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.occupancyStatus``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.position.speed``
``entity.vehicle.timestamp``
``entity.vehicle.trip.routeId``
``entity.vehicle.trip.scheduleRelationship``
``entity.vehicle.trip.tripId``
``entity.vehicle.vehicle.id``
``entity.vehicle.vehicle.label``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/BULLRUNNER_V2-GTFSRT-2019-43.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/BULLRUNNER_V2-GTFSRT-2019-43.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/BULLRUNNER_V2-GTFSRT-2019-43.png)
