### BULLRUNNER_V2-GTFSRT-2020-28
#### Metadata
BULLRUNNER_V2-GTFSRT-2020-28.json.zip: 9.46 MB
BULLRUNNER_V2-GTFSRT-2020-28.pb.zip: 6.98 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 19826 | 0 | 0 |
| Sampling | 30.51 | 0.00 | 0.00 |
| 2020-07-06 | 2849 | 0 | 0 |
| 2020-07-07 | 2855 | 0 | 0 |
| 2020-07-08 | 2717 | 0 | 0 |
| 2020-07-09 | 2820 | 0 | 0 |
| 2020-07-10 | 2860 | 0 | 0 |
| 2020-07-11 | 2862 | 0 | 0 |
| 2020-07-12 | 2863 | 0 | 0 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.occupancyStatus``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.position.speed``
``entity.vehicle.timestamp``
``entity.vehicle.trip.routeId``
``entity.vehicle.trip.scheduleRelationship``
``entity.vehicle.trip.tripId``
``entity.vehicle.vehicle.id``
``entity.vehicle.vehicle.label``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/BULLRUNNER_V2-GTFSRT-2020-28.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/BULLRUNNER_V2-GTFSRT-2020-28.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/BULLRUNNER_V2-GTFSRT-2020-28.png)
