### BULLRUNNER_V2-GTFSRT-2019-27
#### Metadata
BULLRUNNER_V2-GTFSRT-2019-27.json.zip: 10.90 MB
BULLRUNNER_V2-GTFSRT-2019-27.pb.zip: 7.99 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 20045 | 0 | 0 |
| Sampling | 30.17 | 0.00 | 0.00 |
| 2019-07-01 | 2863 | 0 | 0 |
| 2019-07-02 | 2865 | 0 | 0 |
| 2019-07-03 | 2867 | 0 | 0 |
| 2019-07-04 | 2860 | 0 | 0 |
| 2019-07-05 | 2868 | 0 | 0 |
| 2019-07-06 | 2870 | 0 | 0 |
| 2019-07-07 | 2852 | 0 | 0 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.occupancyStatus``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.position.speed``
``entity.vehicle.timestamp``
``entity.vehicle.trip.routeId``
``entity.vehicle.trip.scheduleRelationship``
``entity.vehicle.trip.tripId``
``entity.vehicle.vehicle.id``
``entity.vehicle.vehicle.label``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/BULLRUNNER_V2-GTFSRT-2019-27.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/BULLRUNNER_V2-GTFSRT-2019-27.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/BULLRUNNER_V2-GTFSRT-2019-27.png)
