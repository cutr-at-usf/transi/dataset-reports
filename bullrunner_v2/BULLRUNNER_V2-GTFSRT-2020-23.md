### BULLRUNNER_V2-GTFSRT-2020-23
#### Metadata
BULLRUNNER_V2-GTFSRT-2020-23.json.zip: 9.62 MB
BULLRUNNER_V2-GTFSRT-2020-23.pb.zip: 7.10 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 19987 | 0 | 0 |
| Sampling | 30.26 | 0.00 | 0.00 |
| 2020-06-01 | 2859 | 0 | 0 |
| 2020-06-02 | 2871 | 0 | 0 |
| 2020-06-03 | 2820 | 0 | 0 |
| 2020-06-04 | 2845 | 0 | 0 |
| 2020-06-05 | 2857 | 0 | 0 |
| 2020-06-06 | 2863 | 0 | 0 |
| 2020-06-07 | 2872 | 0 | 0 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.occupancyStatus``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.position.speed``
``entity.vehicle.timestamp``
``entity.vehicle.trip.routeId``
``entity.vehicle.trip.scheduleRelationship``
``entity.vehicle.trip.tripId``
``entity.vehicle.vehicle.id``
``entity.vehicle.vehicle.label``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/BULLRUNNER_V2-GTFSRT-2020-23.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/BULLRUNNER_V2-GTFSRT-2020-23.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/BULLRUNNER_V2-GTFSRT-2020-23.png)
