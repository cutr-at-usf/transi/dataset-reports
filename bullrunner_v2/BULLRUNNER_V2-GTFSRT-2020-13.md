### BULLRUNNER_V2-GTFSRT-2020-13
#### Metadata
BULLRUNNER_V2-GTFSRT-2020-13.json.zip: 9.52 MB
BULLRUNNER_V2-GTFSRT-2020-13.pb.zip: 6.93 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 18238 | 0 | 0 |
| Sampling | 33.16 | 0.00 | 0.00 |
| 2020-03-23 | 2846 | 0 | 0 |
| 2020-03-24 | 2842 | 0 | 0 |
| 2020-03-25 | 2727 | 0 | 0 |
| 2020-03-26 | 2834 | 0 | 0 |
| 2020-03-27 | 2796 | 0 | 0 |
| 2020-03-28 | 2162 | 0 | 0 |
| 2020-03-29 | 2031 | 0 | 0 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.occupancyStatus``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.position.speed``
``entity.vehicle.timestamp``
``entity.vehicle.trip.routeId``
``entity.vehicle.trip.scheduleRelationship``
``entity.vehicle.trip.tripId``
``entity.vehicle.vehicle.id``
``entity.vehicle.vehicle.label``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/BULLRUNNER_V2-GTFSRT-2020-13.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/BULLRUNNER_V2-GTFSRT-2020-13.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/BULLRUNNER_V2-GTFSRT-2020-13.png)
