### BULLRUNNER_V2-GTFSRT-2020-11
#### Metadata
BULLRUNNER_V2-GTFSRT-2020-11.json.zip: 10.47 MB
BULLRUNNER_V2-GTFSRT-2020-11.pb.zip: 7.64 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 18157 | 0 | 0 |
| Sampling | 33.31 | 0.00 | 0.00 |
| 2020-03-09 | 2668 | 0 | 0 |
| 2020-03-10 | 2761 | 0 | 0 |
| 2020-03-11 | 2731 | 0 | 0 |
| 2020-03-12 | 2824 | 0 | 0 |
| 2020-03-13 | 2813 | 0 | 0 |
| 2020-03-14 | 2155 | 0 | 0 |
| 2020-03-15 | 2205 | 0 | 0 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.occupancyStatus``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.position.speed``
``entity.vehicle.timestamp``
``entity.vehicle.trip.routeId``
``entity.vehicle.trip.scheduleRelationship``
``entity.vehicle.trip.tripId``
``entity.vehicle.vehicle.id``
``entity.vehicle.vehicle.label``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/BULLRUNNER_V2-GTFSRT-2020-11.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/BULLRUNNER_V2-GTFSRT-2020-11.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/BULLRUNNER_V2-GTFSRT-2020-11.png)
