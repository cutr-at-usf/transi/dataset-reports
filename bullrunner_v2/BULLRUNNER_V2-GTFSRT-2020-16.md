### BULLRUNNER_V2-GTFSRT-2020-16
#### Metadata
BULLRUNNER_V2-GTFSRT-2020-16.json.zip: 9.31 MB
BULLRUNNER_V2-GTFSRT-2020-16.pb.zip: 6.83 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 19977 | 0 | 0 |
| Sampling | 30.27 | 0.00 | 0.00 |
| 2020-04-13 | 2864 | 0 | 0 |
| 2020-04-14 | 2864 | 0 | 0 |
| 2020-04-15 | 2787 | 0 | 0 |
| 2020-04-16 | 2869 | 0 | 0 |
| 2020-04-17 | 2864 | 0 | 0 |
| 2020-04-18 | 2863 | 0 | 0 |
| 2020-04-19 | 2866 | 0 | 0 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.occupancyStatus``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.position.speed``
``entity.vehicle.timestamp``
``entity.vehicle.trip.routeId``
``entity.vehicle.trip.scheduleRelationship``
``entity.vehicle.trip.tripId``
``entity.vehicle.vehicle.id``
``entity.vehicle.vehicle.label``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/BULLRUNNER_V2-GTFSRT-2020-16.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/BULLRUNNER_V2-GTFSRT-2020-16.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/BULLRUNNER_V2-GTFSRT-2020-16.png)
