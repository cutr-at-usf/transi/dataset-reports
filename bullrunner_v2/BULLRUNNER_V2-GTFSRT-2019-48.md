### BULLRUNNER_V2-GTFSRT-2019-48
#### Metadata
BULLRUNNER_V2-GTFSRT-2019-48.json.zip: 9.89 MB
BULLRUNNER_V2-GTFSRT-2019-48.pb.zip: 7.27 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 19936 | 0 | 0 |
| Sampling | 30.34 | 0.00 | 0.00 |
| 2019-11-25 | 2814 | 0 | 0 |
| 2019-11-26 | 2839 | 0 | 0 |
| 2019-11-27 | 2831 | 0 | 0 |
| 2019-11-28 | 2873 | 0 | 0 |
| 2019-11-29 | 2864 | 0 | 0 |
| 2019-11-30 | 2861 | 0 | 0 |
| 2019-12-01 | 2854 | 0 | 0 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.occupancyStatus``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.position.speed``
``entity.vehicle.timestamp``
``entity.vehicle.trip.routeId``
``entity.vehicle.trip.scheduleRelationship``
``entity.vehicle.trip.tripId``
``entity.vehicle.vehicle.id``
``entity.vehicle.vehicle.label``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/BULLRUNNER_V2-GTFSRT-2019-48.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/BULLRUNNER_V2-GTFSRT-2019-48.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/BULLRUNNER_V2-GTFSRT-2019-48.png)
