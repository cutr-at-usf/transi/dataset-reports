### BULLRUNNER_V2-GTFSRT-2019-45
#### Metadata
BULLRUNNER_V2-GTFSRT-2019-45.json.zip: 10.86 MB
BULLRUNNER_V2-GTFSRT-2019-45.pb.zip: 7.91 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 18971 | 0 | 0 |
| Sampling | 31.88 | 0.00 | 0.00 |
| 2019-11-04 | 2717 | 0 | 0 |
| 2019-11-05 | 2687 | 0 | 0 |
| 2019-11-06 | 2576 | 0 | 0 |
| 2019-11-07 | 2537 | 0 | 0 |
| 2019-11-08 | 2739 | 0 | 0 |
| 2019-11-09 | 2861 | 0 | 0 |
| 2019-11-10 | 2854 | 0 | 0 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.occupancyStatus``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.position.speed``
``entity.vehicle.timestamp``
``entity.vehicle.trip.routeId``
``entity.vehicle.trip.scheduleRelationship``
``entity.vehicle.trip.tripId``
``entity.vehicle.vehicle.id``
``entity.vehicle.vehicle.label``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/BULLRUNNER_V2-GTFSRT-2019-45.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/BULLRUNNER_V2-GTFSRT-2019-45.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/BULLRUNNER_V2-GTFSRT-2019-45.png)
