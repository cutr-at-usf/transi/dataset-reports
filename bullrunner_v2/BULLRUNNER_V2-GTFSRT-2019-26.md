### BULLRUNNER_V2-GTFSRT-2019-26
#### Metadata
BULLRUNNER_V2-GTFSRT-2019-26.json.zip: 11.66 MB
BULLRUNNER_V2-GTFSRT-2019-26.pb.zip: 8.47 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 20036 | 0 | 0 |
| Sampling | 30.18 | 0.00 | 0.00 |
| 2019-06-24 | 2845 | 0 | 0 |
| 2019-06-25 | 2857 | 0 | 0 |
| 2019-06-26 | 2862 | 0 | 0 |
| 2019-06-27 | 2863 | 0 | 0 |
| 2019-06-28 | 2866 | 0 | 0 |
| 2019-06-29 | 2870 | 0 | 0 |
| 2019-06-30 | 2873 | 0 | 0 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.occupancyStatus``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.position.speed``
``entity.vehicle.timestamp``
``entity.vehicle.trip.routeId``
``entity.vehicle.trip.scheduleRelationship``
``entity.vehicle.trip.tripId``
``entity.vehicle.vehicle.id``
``entity.vehicle.vehicle.label``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/BULLRUNNER_V2-GTFSRT-2019-26.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/BULLRUNNER_V2-GTFSRT-2019-26.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/BULLRUNNER_V2-GTFSRT-2019-26.png)
