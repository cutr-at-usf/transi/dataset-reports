### BULLRUNNER_V2-GTFSRT-2019-31
#### Metadata
BULLRUNNER_V2-GTFSRT-2019-31.json.zip: 11.92 MB
BULLRUNNER_V2-GTFSRT-2019-31.pb.zip: 8.69 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 20049 | 0 | 0 |
| Sampling | 30.17 | 0.00 | 0.00 |
| 2019-07-29 | 2865 | 0 | 0 |
| 2019-07-30 | 2873 | 0 | 0 |
| 2019-07-31 | 2869 | 0 | 0 |
| 2019-08-01 | 2866 | 0 | 0 |
| 2019-08-02 | 2868 | 0 | 0 |
| 2019-08-03 | 2868 | 0 | 0 |
| 2019-08-04 | 2840 | 0 | 0 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.occupancyStatus``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.position.speed``
``entity.vehicle.timestamp``
``entity.vehicle.trip.routeId``
``entity.vehicle.trip.scheduleRelationship``
``entity.vehicle.trip.tripId``
``entity.vehicle.vehicle.id``
``entity.vehicle.vehicle.label``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/BULLRUNNER_V2-GTFSRT-2019-31.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/BULLRUNNER_V2-GTFSRT-2019-31.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/BULLRUNNER_V2-GTFSRT-2019-31.png)
