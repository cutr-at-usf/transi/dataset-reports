### BULLRUNNER_V2-GTFSRT-2019-14
#### Metadata
BULLRUNNER_V2-GTFSRT-2019-14.json.zip: 11.68 MB
BULLRUNNER_V2-GTFSRT-2019-14.pb.zip: 8.51 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 19879 | 0 | 0 |
| Sampling | 30.42 | 0.00 | 0.00 |
| 2019-04-01 | 2866 | 0 | 0 |
| 2019-04-02 | 2865 | 0 | 0 |
| 2019-04-03 | 2771 | 0 | 0 |
| 2019-04-04 | 2855 | 0 | 0 |
| 2019-04-05 | 2866 | 0 | 0 |
| 2019-04-06 | 2795 | 0 | 0 |
| 2019-04-07 | 2861 | 0 | 0 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.occupancyStatus``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.position.speed``
``entity.vehicle.timestamp``
``entity.vehicle.trip.routeId``
``entity.vehicle.trip.scheduleRelationship``
``entity.vehicle.trip.tripId``
``entity.vehicle.vehicle.id``
``entity.vehicle.vehicle.label``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/BULLRUNNER_V2-GTFSRT-2019-14.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/BULLRUNNER_V2-GTFSRT-2019-14.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/BULLRUNNER_V2-GTFSRT-2019-14.png)
