### BULLRUNNER_V2-GTFSRT-2019-09
#### Metadata
BULLRUNNER_V2-GTFSRT-2019-09.json.zip: 11.31 MB
BULLRUNNER_V2-GTFSRT-2019-09.pb.zip: 8.15 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 19960 | 0 | 0 |
| Sampling | 30.30 | 0.00 | 0.00 |
| 2019-02-25 | 2859 | 0 | 0 |
| 2019-02-26 | 2849 | 0 | 0 |
| 2019-02-27 | 2857 | 0 | 0 |
| 2019-02-28 | 2857 | 0 | 0 |
| 2019-03-01 | 2858 | 0 | 0 |
| 2019-03-02 | 2848 | 0 | 0 |
| 2019-03-03 | 2832 | 0 | 0 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.occupancyStatus``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.position.speed``
``entity.vehicle.timestamp``
``entity.vehicle.trip.routeId``
``entity.vehicle.trip.scheduleRelationship``
``entity.vehicle.trip.tripId``
``entity.vehicle.vehicle.id``
``entity.vehicle.vehicle.label``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/BULLRUNNER_V2-GTFSRT-2019-09.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/BULLRUNNER_V2-GTFSRT-2019-09.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/BULLRUNNER_V2-GTFSRT-2019-09.png)
