### BULLRUNNER_V2-GTFSRT-2019-40
#### Metadata
BULLRUNNER_V2-GTFSRT-2019-40.json.zip: 11.32 MB
BULLRUNNER_V2-GTFSRT-2019-40.pb.zip: 8.26 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 18894 | 0 | 0 |
| Sampling | 32.01 | 0.00 | 0.00 |
| 2019-09-30 | 2741 | 0 | 0 |
| 2019-10-01 | 2699 | 0 | 0 |
| 2019-10-02 | 2744 | 0 | 0 |
| 2019-10-03 | 2799 | 0 | 0 |
| 2019-10-04 | 2193 | 0 | 0 |
| 2019-10-05 | 2865 | 0 | 0 |
| 2019-10-06 | 2853 | 0 | 0 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.occupancyStatus``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.position.speed``
``entity.vehicle.timestamp``
``entity.vehicle.trip.routeId``
``entity.vehicle.trip.scheduleRelationship``
``entity.vehicle.trip.tripId``
``entity.vehicle.vehicle.id``
``entity.vehicle.vehicle.label``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/BULLRUNNER_V2-GTFSRT-2019-40.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/BULLRUNNER_V2-GTFSRT-2019-40.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/BULLRUNNER_V2-GTFSRT-2019-40.png)
