### BULLRUNNER_V2-GTFSRT-2019-28
#### Metadata
BULLRUNNER_V2-GTFSRT-2019-28.json.zip: 11.80 MB
BULLRUNNER_V2-GTFSRT-2019-28.pb.zip: 8.59 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 20052 | 0 | 0 |
| Sampling | 30.16 | 0.00 | 0.00 |
| 2019-07-08 | 2863 | 0 | 0 |
| 2019-07-09 | 2857 | 0 | 0 |
| 2019-07-10 | 2867 | 0 | 0 |
| 2019-07-11 | 2866 | 0 | 0 |
| 2019-07-12 | 2863 | 0 | 0 |
| 2019-07-13 | 2870 | 0 | 0 |
| 2019-07-14 | 2866 | 0 | 0 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.occupancyStatus``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.position.speed``
``entity.vehicle.timestamp``
``entity.vehicle.trip.routeId``
``entity.vehicle.trip.scheduleRelationship``
``entity.vehicle.trip.tripId``
``entity.vehicle.vehicle.id``
``entity.vehicle.vehicle.label``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/BULLRUNNER_V2-GTFSRT-2019-28.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/BULLRUNNER_V2-GTFSRT-2019-28.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/BULLRUNNER_V2-GTFSRT-2019-28.png)
