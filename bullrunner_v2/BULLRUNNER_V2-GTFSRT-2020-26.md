### BULLRUNNER_V2-GTFSRT-2020-26
#### Metadata
BULLRUNNER_V2-GTFSRT-2020-26.json.zip: 9.66 MB
BULLRUNNER_V2-GTFSRT-2020-26.pb.zip: 7.14 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 20008 | 0 | 0 |
| Sampling | 30.23 | 0.00 | 0.00 |
| 2020-06-22 | 2864 | 0 | 0 |
| 2020-06-23 | 2846 | 0 | 0 |
| 2020-06-24 | 2856 | 0 | 0 |
| 2020-06-25 | 2863 | 0 | 0 |
| 2020-06-26 | 2856 | 0 | 0 |
| 2020-06-27 | 2858 | 0 | 0 |
| 2020-06-28 | 2865 | 0 | 0 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.occupancyStatus``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.position.speed``
``entity.vehicle.timestamp``
``entity.vehicle.trip.routeId``
``entity.vehicle.trip.scheduleRelationship``
``entity.vehicle.trip.tripId``
``entity.vehicle.vehicle.id``
``entity.vehicle.vehicle.label``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/BULLRUNNER_V2-GTFSRT-2020-26.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/BULLRUNNER_V2-GTFSRT-2020-26.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/BULLRUNNER_V2-GTFSRT-2020-26.png)
