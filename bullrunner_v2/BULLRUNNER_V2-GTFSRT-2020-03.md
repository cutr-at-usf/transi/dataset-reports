### BULLRUNNER_V2-GTFSRT-2020-03
#### Metadata
BULLRUNNER_V2-GTFSRT-2020-03.json.zip: 9.50 MB
BULLRUNNER_V2-GTFSRT-2020-03.pb.zip: 6.99 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 16673 | 0 | 0 |
| Sampling | 36.28 | 0.00 | 0.00 |
| 2020-01-13 | 1924 | 0 | 0 |
| 2020-01-14 | 1976 | 0 | 0 |
| 2020-01-15 | 2749 | 0 | 0 |
| 2020-01-16 | 2817 | 0 | 0 |
| 2020-01-17 | 2820 | 0 | 0 |
| 2020-01-18 | 2136 | 0 | 0 |
| 2020-01-19 | 2251 | 0 | 0 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.occupancyStatus``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.position.speed``
``entity.vehicle.timestamp``
``entity.vehicle.trip.routeId``
``entity.vehicle.trip.scheduleRelationship``
``entity.vehicle.trip.tripId``
``entity.vehicle.vehicle.id``
``entity.vehicle.vehicle.label``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/BULLRUNNER_V2-GTFSRT-2020-03.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/BULLRUNNER_V2-GTFSRT-2020-03.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/BULLRUNNER_V2-GTFSRT-2020-03.png)
