### BULLRUNNER_V2-GTFSRT-2019-29
#### Metadata
BULLRUNNER_V2-GTFSRT-2019-29.json.zip: 11.77 MB
BULLRUNNER_V2-GTFSRT-2019-29.pb.zip: 8.56 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 20048 | 0 | 0 |
| Sampling | 30.17 | 0.00 | 0.00 |
| 2019-07-15 | 2859 | 0 | 0 |
| 2019-07-16 | 2867 | 0 | 0 |
| 2019-07-17 | 2865 | 0 | 0 |
| 2019-07-18 | 2859 | 0 | 0 |
| 2019-07-19 | 2868 | 0 | 0 |
| 2019-07-20 | 2868 | 0 | 0 |
| 2019-07-21 | 2862 | 0 | 0 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.occupancyStatus``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.position.speed``
``entity.vehicle.timestamp``
``entity.vehicle.trip.routeId``
``entity.vehicle.trip.scheduleRelationship``
``entity.vehicle.trip.tripId``
``entity.vehicle.vehicle.id``
``entity.vehicle.vehicle.label``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/BULLRUNNER_V2-GTFSRT-2019-29.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/BULLRUNNER_V2-GTFSRT-2019-29.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/BULLRUNNER_V2-GTFSRT-2019-29.png)
