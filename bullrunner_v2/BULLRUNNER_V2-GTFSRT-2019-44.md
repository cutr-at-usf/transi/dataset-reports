### BULLRUNNER_V2-GTFSRT-2019-44
#### Metadata
BULLRUNNER_V2-GTFSRT-2019-44.json.zip: 10.93 MB
BULLRUNNER_V2-GTFSRT-2019-44.pb.zip: 8.00 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 18803 | 0 | 0 |
| Sampling | 32.36 | 0.00 | 0.00 |
| 2019-10-28 | 2777 | 0 | 0 |
| 2019-10-29 | 2618 | 0 | 0 |
| 2019-10-30 | 2288 | 0 | 0 |
| 2019-10-31 | 2592 | 0 | 0 |
| 2019-11-01 | 2777 | 0 | 0 |
| 2019-11-02 | 2857 | 0 | 0 |
| 2019-11-03 | 2774 | 0 | 0 |
| 2019-11-04 | 120 | 0 | 0 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.occupancyStatus``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.position.speed``
``entity.vehicle.timestamp``
``entity.vehicle.trip.routeId``
``entity.vehicle.trip.scheduleRelationship``
``entity.vehicle.trip.tripId``
``entity.vehicle.vehicle.id``
``entity.vehicle.vehicle.label``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/BULLRUNNER_V2-GTFSRT-2019-44.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/BULLRUNNER_V2-GTFSRT-2019-44.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/BULLRUNNER_V2-GTFSRT-2019-44.png)
