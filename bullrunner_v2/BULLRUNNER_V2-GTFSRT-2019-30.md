### BULLRUNNER_V2-GTFSRT-2019-30
#### Metadata
BULLRUNNER_V2-GTFSRT-2019-30.json.zip: 11.91 MB
BULLRUNNER_V2-GTFSRT-2019-30.pb.zip: 8.71 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 19910 | 0 | 0 |
| Sampling | 30.38 | 0.00 | 0.00 |
| 2019-07-22 | 2867 | 0 | 0 |
| 2019-07-23 | 2867 | 0 | 0 |
| 2019-07-24 | 2737 | 0 | 0 |
| 2019-07-25 | 2830 | 0 | 0 |
| 2019-07-26 | 2870 | 0 | 0 |
| 2019-07-27 | 2867 | 0 | 0 |
| 2019-07-28 | 2872 | 0 | 0 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.occupancyStatus``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.position.speed``
``entity.vehicle.timestamp``
``entity.vehicle.trip.routeId``
``entity.vehicle.trip.scheduleRelationship``
``entity.vehicle.trip.tripId``
``entity.vehicle.vehicle.id``
``entity.vehicle.vehicle.label``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/BULLRUNNER_V2-GTFSRT-2019-30.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/BULLRUNNER_V2-GTFSRT-2019-30.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/BULLRUNNER_V2-GTFSRT-2019-30.png)
