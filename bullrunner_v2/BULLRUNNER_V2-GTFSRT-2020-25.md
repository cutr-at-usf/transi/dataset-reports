### BULLRUNNER_V2-GTFSRT-2020-25
#### Metadata
BULLRUNNER_V2-GTFSRT-2020-25.json.zip: 9.52 MB
BULLRUNNER_V2-GTFSRT-2020-25.pb.zip: 7.03 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 19696 | 0 | 0 |
| Sampling | 30.71 | 0.00 | 0.00 |
| 2020-06-15 | 2862 | 0 | 0 |
| 2020-06-16 | 2858 | 0 | 0 |
| 2020-06-17 | 2860 | 0 | 0 |
| 2020-06-18 | 2725 | 0 | 0 |
| 2020-06-19 | 2650 | 0 | 0 |
| 2020-06-20 | 2870 | 0 | 0 |
| 2020-06-21 | 2871 | 0 | 0 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.occupancyStatus``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.position.speed``
``entity.vehicle.timestamp``
``entity.vehicle.trip.routeId``
``entity.vehicle.trip.scheduleRelationship``
``entity.vehicle.trip.tripId``
``entity.vehicle.vehicle.id``
``entity.vehicle.vehicle.label``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/BULLRUNNER_V2-GTFSRT-2020-25.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/BULLRUNNER_V2-GTFSRT-2020-25.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/BULLRUNNER_V2-GTFSRT-2020-25.png)
