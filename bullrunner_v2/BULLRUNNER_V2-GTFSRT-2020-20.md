### BULLRUNNER_V2-GTFSRT-2020-20
#### Metadata
BULLRUNNER_V2-GTFSRT-2020-20.json.zip: 9.62 MB
BULLRUNNER_V2-GTFSRT-2020-20.pb.zip: 7.11 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 20057 | 0 | 0 |
| Sampling | 30.15 | 0.00 | 0.00 |
| 2020-05-11 | 2867 | 0 | 0 |
| 2020-05-12 | 2864 | 0 | 0 |
| 2020-05-13 | 2866 | 0 | 0 |
| 2020-05-14 | 2862 | 0 | 0 |
| 2020-05-15 | 2860 | 0 | 0 |
| 2020-05-16 | 2870 | 0 | 0 |
| 2020-05-17 | 2868 | 0 | 0 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.occupancyStatus``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.position.speed``
``entity.vehicle.timestamp``
``entity.vehicle.trip.routeId``
``entity.vehicle.trip.scheduleRelationship``
``entity.vehicle.trip.tripId``
``entity.vehicle.vehicle.id``
``entity.vehicle.vehicle.label``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/BULLRUNNER_V2-GTFSRT-2020-20.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/BULLRUNNER_V2-GTFSRT-2020-20.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/BULLRUNNER_V2-GTFSRT-2020-20.png)
