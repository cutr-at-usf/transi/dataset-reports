### BULLRUNNER_V2-GTFSRT-2020-19
#### Metadata
BULLRUNNER_V2-GTFSRT-2020-19.json.zip: 9.54 MB
BULLRUNNER_V2-GTFSRT-2020-19.pb.zip: 7.05 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 19845 | 0 | 0 |
| Sampling | 30.48 | 0.00 | 0.00 |
| 2020-05-04 | 2850 | 0 | 0 |
| 2020-05-05 | 2752 | 0 | 0 |
| 2020-05-06 | 2777 | 0 | 0 |
| 2020-05-07 | 2865 | 0 | 0 |
| 2020-05-08 | 2871 | 0 | 0 |
| 2020-05-09 | 2858 | 0 | 0 |
| 2020-05-10 | 2872 | 0 | 0 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.occupancyStatus``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.position.speed``
``entity.vehicle.timestamp``
``entity.vehicle.trip.routeId``
``entity.vehicle.trip.scheduleRelationship``
``entity.vehicle.trip.tripId``
``entity.vehicle.vehicle.id``
``entity.vehicle.vehicle.label``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/BULLRUNNER_V2-GTFSRT-2020-19.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/BULLRUNNER_V2-GTFSRT-2020-19.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/BULLRUNNER_V2-GTFSRT-2020-19.png)
