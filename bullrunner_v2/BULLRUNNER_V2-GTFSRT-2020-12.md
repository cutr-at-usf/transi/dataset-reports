### BULLRUNNER_V2-GTFSRT-2020-12
#### Metadata
BULLRUNNER_V2-GTFSRT-2020-12.json.zip: 9.86 MB
BULLRUNNER_V2-GTFSRT-2020-12.pb.zip: 7.20 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 18525 | 0 | 0 |
| Sampling | 32.65 | 0.00 | 0.00 |
| 2020-03-16 | 2836 | 0 | 0 |
| 2020-03-17 | 2826 | 0 | 0 |
| 2020-03-18 | 2832 | 0 | 0 |
| 2020-03-19 | 2835 | 0 | 0 |
| 2020-03-20 | 2822 | 0 | 0 |
| 2020-03-21 | 2147 | 0 | 0 |
| 2020-03-22 | 2227 | 0 | 0 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.occupancyStatus``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.position.speed``
``entity.vehicle.timestamp``
``entity.vehicle.trip.routeId``
``entity.vehicle.trip.scheduleRelationship``
``entity.vehicle.trip.tripId``
``entity.vehicle.vehicle.id``
``entity.vehicle.vehicle.label``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/BULLRUNNER_V2-GTFSRT-2020-12.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/BULLRUNNER_V2-GTFSRT-2020-12.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/BULLRUNNER_V2-GTFSRT-2020-12.png)
