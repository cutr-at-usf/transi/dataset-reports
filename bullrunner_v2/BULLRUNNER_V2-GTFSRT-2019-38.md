### BULLRUNNER_V2-GTFSRT-2019-38
#### Metadata
BULLRUNNER_V2-GTFSRT-2019-38.json.zip: 10.80 MB
BULLRUNNER_V2-GTFSRT-2019-38.pb.zip: 7.86 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 18494 | 0 | 0 |
| Sampling | 32.70 | 0.00 | 0.00 |
| 2019-09-16 | 2667 | 0 | 0 |
| 2019-09-17 | 2547 | 0 | 0 |
| 2019-09-18 | 2173 | 0 | 0 |
| 2019-09-19 | 2772 | 0 | 0 |
| 2019-09-20 | 2626 | 0 | 0 |
| 2019-09-21 | 2843 | 0 | 0 |
| 2019-09-22 | 2866 | 0 | 0 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.occupancyStatus``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.position.speed``
``entity.vehicle.timestamp``
``entity.vehicle.trip.routeId``
``entity.vehicle.trip.scheduleRelationship``
``entity.vehicle.trip.tripId``
``entity.vehicle.vehicle.id``
``entity.vehicle.vehicle.label``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/BULLRUNNER_V2-GTFSRT-2019-38.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/BULLRUNNER_V2-GTFSRT-2019-38.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/BULLRUNNER_V2-GTFSRT-2019-38.png)
