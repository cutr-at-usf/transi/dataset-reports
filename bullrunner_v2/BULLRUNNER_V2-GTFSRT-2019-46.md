### BULLRUNNER_V2-GTFSRT-2019-46
#### Metadata
BULLRUNNER_V2-GTFSRT-2019-46.json.zip: 10.07 MB
BULLRUNNER_V2-GTFSRT-2019-46.pb.zip: 7.40 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 18964 | 0 | 0 |
| Sampling | 31.89 | 0.00 | 0.00 |
| 2019-11-11 | 2812 | 0 | 0 |
| 2019-11-12 | 2619 | 0 | 0 |
| 2019-11-13 | 2631 | 0 | 0 |
| 2019-11-14 | 2586 | 0 | 0 |
| 2019-11-15 | 2590 | 0 | 0 |
| 2019-11-16 | 2862 | 0 | 0 |
| 2019-11-17 | 2864 | 0 | 0 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.occupancyStatus``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.position.speed``
``entity.vehicle.timestamp``
``entity.vehicle.trip.routeId``
``entity.vehicle.trip.scheduleRelationship``
``entity.vehicle.trip.tripId``
``entity.vehicle.vehicle.id``
``entity.vehicle.vehicle.label``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/BULLRUNNER_V2-GTFSRT-2019-46.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/BULLRUNNER_V2-GTFSRT-2019-46.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/BULLRUNNER_V2-GTFSRT-2019-46.png)
