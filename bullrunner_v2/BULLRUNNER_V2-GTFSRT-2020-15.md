### BULLRUNNER_V2-GTFSRT-2020-15
#### Metadata
BULLRUNNER_V2-GTFSRT-2020-15.json.zip: 9.20 MB
BULLRUNNER_V2-GTFSRT-2020-15.pb.zip: 6.75 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 19917 | 0 | 0 |
| Sampling | 30.37 | 0.00 | 0.00 |
| 2020-04-06 | 2865 | 0 | 0 |
| 2020-04-07 | 2863 | 0 | 0 |
| 2020-04-08 | 2735 | 0 | 0 |
| 2020-04-09 | 2866 | 0 | 0 |
| 2020-04-10 | 2853 | 0 | 0 |
| 2020-04-11 | 2868 | 0 | 0 |
| 2020-04-12 | 2867 | 0 | 0 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.occupancyStatus``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.position.speed``
``entity.vehicle.timestamp``
``entity.vehicle.trip.routeId``
``entity.vehicle.trip.scheduleRelationship``
``entity.vehicle.trip.tripId``
``entity.vehicle.vehicle.id``
``entity.vehicle.vehicle.label``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/BULLRUNNER_V2-GTFSRT-2020-15.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/BULLRUNNER_V2-GTFSRT-2020-15.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/BULLRUNNER_V2-GTFSRT-2020-15.png)
