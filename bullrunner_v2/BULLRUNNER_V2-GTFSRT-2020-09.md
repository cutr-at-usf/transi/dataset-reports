### BULLRUNNER_V2-GTFSRT-2020-09
#### Metadata
BULLRUNNER_V2-GTFSRT-2020-09.json.zip: 8.65 MB
BULLRUNNER_V2-GTFSRT-2020-09.pb.zip: 6.30 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 15346 | 0 | 0 |
| Sampling | 39.41 | 0.00 | 0.00 |
| 2020-02-24 | 2736 | 0 | 0 |
| 2020-02-25 | 2746 | 0 | 0 |
| 2020-02-26 | 2021 | 0 | 0 |
| 2020-02-27 | 1339 | 0 | 0 |
| 2020-02-28 | 2389 | 0 | 0 |
| 2020-02-29 | 1987 | 0 | 0 |
| 2020-03-01 | 2128 | 0 | 0 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.occupancyStatus``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.position.speed``
``entity.vehicle.timestamp``
``entity.vehicle.trip.routeId``
``entity.vehicle.trip.scheduleRelationship``
``entity.vehicle.trip.tripId``
``entity.vehicle.vehicle.id``
``entity.vehicle.vehicle.label``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/BULLRUNNER_V2-GTFSRT-2020-09.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/BULLRUNNER_V2-GTFSRT-2020-09.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/BULLRUNNER_V2-GTFSRT-2020-09.png)
