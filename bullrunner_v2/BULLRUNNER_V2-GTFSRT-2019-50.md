### BULLRUNNER_V2-GTFSRT-2019-50
#### Metadata
BULLRUNNER_V2-GTFSRT-2019-50.json.zip: 11.31 MB
BULLRUNNER_V2-GTFSRT-2019-50.pb.zip: 8.25 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 19441 | 0 | 0 |
| Sampling | 31.11 | 0.00 | 0.00 |
| 2019-12-09 | 2706 | 0 | 0 |
| 2019-12-10 | 2712 | 0 | 0 |
| 2019-12-11 | 2721 | 0 | 0 |
| 2019-12-12 | 2763 | 0 | 0 |
| 2019-12-13 | 2827 | 0 | 0 |
| 2019-12-14 | 2849 | 0 | 0 |
| 2019-12-15 | 2863 | 0 | 0 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.occupancyStatus``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.position.speed``
``entity.vehicle.timestamp``
``entity.vehicle.trip.routeId``
``entity.vehicle.trip.scheduleRelationship``
``entity.vehicle.trip.tripId``
``entity.vehicle.vehicle.id``
``entity.vehicle.vehicle.label``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/BULLRUNNER_V2-GTFSRT-2019-50.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/BULLRUNNER_V2-GTFSRT-2019-50.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/BULLRUNNER_V2-GTFSRT-2019-50.png)
