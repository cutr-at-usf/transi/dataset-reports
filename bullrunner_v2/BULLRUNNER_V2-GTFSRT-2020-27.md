### BULLRUNNER_V2-GTFSRT-2020-27
#### Metadata
BULLRUNNER_V2-GTFSRT-2020-27.json.zip: 9.10 MB
BULLRUNNER_V2-GTFSRT-2020-27.pb.zip: 6.77 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 20009 | 0 | 0 |
| Sampling | 30.23 | 0.00 | 0.00 |
| 2020-06-29 | 2862 | 0 | 0 |
| 2020-06-30 | 2856 | 0 | 0 |
| 2020-07-01 | 2856 | 0 | 0 |
| 2020-07-02 | 2857 | 0 | 0 |
| 2020-07-03 | 2852 | 0 | 0 |
| 2020-07-04 | 2864 | 0 | 0 |
| 2020-07-05 | 2862 | 0 | 0 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.occupancyStatus``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.position.speed``
``entity.vehicle.timestamp``
``entity.vehicle.trip.routeId``
``entity.vehicle.trip.scheduleRelationship``
``entity.vehicle.trip.tripId``
``entity.vehicle.vehicle.id``
``entity.vehicle.vehicle.label``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/BULLRUNNER_V2-GTFSRT-2020-27.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/BULLRUNNER_V2-GTFSRT-2020-27.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/BULLRUNNER_V2-GTFSRT-2020-27.png)
