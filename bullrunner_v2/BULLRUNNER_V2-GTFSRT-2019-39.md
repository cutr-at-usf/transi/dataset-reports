### BULLRUNNER_V2-GTFSRT-2019-39
#### Metadata
BULLRUNNER_V2-GTFSRT-2019-39.json.zip: 11.24 MB
BULLRUNNER_V2-GTFSRT-2019-39.pb.zip: 8.25 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 19219 | 0 | 0 |
| Sampling | 31.47 | 0.00 | 0.00 |
| 2019-09-23 | 2783 | 0 | 0 |
| 2019-09-24 | 2442 | 0 | 0 |
| 2019-09-25 | 2728 | 0 | 0 |
| 2019-09-26 | 2767 | 0 | 0 |
| 2019-09-27 | 2775 | 0 | 0 |
| 2019-09-28 | 2861 | 0 | 0 |
| 2019-09-29 | 2863 | 0 | 0 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.occupancyStatus``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.position.speed``
``entity.vehicle.timestamp``
``entity.vehicle.trip.routeId``
``entity.vehicle.trip.scheduleRelationship``
``entity.vehicle.trip.tripId``
``entity.vehicle.vehicle.id``
``entity.vehicle.vehicle.label``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/BULLRUNNER_V2-GTFSRT-2019-39.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/BULLRUNNER_V2-GTFSRT-2019-39.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/BULLRUNNER_V2-GTFSRT-2019-39.png)
