### BULLRUNNER_V2-GTFSRT-2019-32
#### Metadata
BULLRUNNER_V2-GTFSRT-2019-32.json.zip: 11.22 MB
BULLRUNNER_V2-GTFSRT-2019-32.pb.zip: 8.19 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 19992 | 0 | 0 |
| Sampling | 30.25 | 0.00 | 0.00 |
| 2019-08-05 | 2859 | 0 | 0 |
| 2019-08-06 | 2851 | 0 | 0 |
| 2019-08-07 | 2868 | 0 | 0 |
| 2019-08-08 | 2855 | 0 | 0 |
| 2019-08-09 | 2866 | 0 | 0 |
| 2019-08-10 | 2863 | 0 | 0 |
| 2019-08-11 | 2830 | 0 | 0 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.occupancyStatus``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.position.speed``
``entity.vehicle.timestamp``
``entity.vehicle.trip.routeId``
``entity.vehicle.trip.scheduleRelationship``
``entity.vehicle.trip.tripId``
``entity.vehicle.vehicle.id``
``entity.vehicle.vehicle.label``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/BULLRUNNER_V2-GTFSRT-2019-32.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/BULLRUNNER_V2-GTFSRT-2019-32.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/BULLRUNNER_V2-GTFSRT-2019-32.png)
