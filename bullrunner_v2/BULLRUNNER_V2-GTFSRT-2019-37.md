### BULLRUNNER_V2-GTFSRT-2019-37
#### Metadata
BULLRUNNER_V2-GTFSRT-2019-37.json.zip: 9.80 MB
BULLRUNNER_V2-GTFSRT-2019-37.pb.zip: 7.09 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 16773 | 0 | 0 |
| Sampling | 32.24 | 0.00 | 0.00 |
| 2019-09-09 | 738 | 0 | 0 |
| 2019-09-10 | 2289 | 0 | 0 |
| 2019-09-11 | 2706 | 0 | 0 |
| 2019-09-12 | 2659 | 0 | 0 |
| 2019-09-13 | 2732 | 0 | 0 |
| 2019-09-14 | 2804 | 0 | 0 |
| 2019-09-15 | 2845 | 0 | 0 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.occupancyStatus``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.position.speed``
``entity.vehicle.timestamp``
``entity.vehicle.trip.routeId``
``entity.vehicle.trip.scheduleRelationship``
``entity.vehicle.trip.tripId``
``entity.vehicle.vehicle.id``
``entity.vehicle.vehicle.label``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/BULLRUNNER_V2-GTFSRT-2019-37.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/BULLRUNNER_V2-GTFSRT-2019-37.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/BULLRUNNER_V2-GTFSRT-2019-37.png)
