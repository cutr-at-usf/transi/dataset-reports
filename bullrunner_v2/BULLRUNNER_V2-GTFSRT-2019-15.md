### BULLRUNNER_V2-GTFSRT-2019-15
#### Metadata
BULLRUNNER_V2-GTFSRT-2019-15.json.zip: 10.82 MB
BULLRUNNER_V2-GTFSRT-2019-15.pb.zip: 7.81 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 18976 | 0 | 0 |
| Sampling | 31.87 | 0.00 | 0.00 |
| 2019-04-08 | 2861 | 0 | 0 |
| 2019-04-09 | 2864 | 0 | 0 |
| 2019-04-10 | 2703 | 0 | 0 |
| 2019-04-11 | 2465 | 0 | 0 |
| 2019-04-12 | 2570 | 0 | 0 |
| 2019-04-13 | 2694 | 0 | 0 |
| 2019-04-14 | 2819 | 0 | 0 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.occupancyStatus``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.position.speed``
``entity.vehicle.timestamp``
``entity.vehicle.trip.routeId``
``entity.vehicle.trip.scheduleRelationship``
``entity.vehicle.trip.tripId``
``entity.vehicle.vehicle.id``
``entity.vehicle.vehicle.label``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/BULLRUNNER_V2-GTFSRT-2019-15.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/BULLRUNNER_V2-GTFSRT-2019-15.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/BULLRUNNER_V2-GTFSRT-2019-15.png)
