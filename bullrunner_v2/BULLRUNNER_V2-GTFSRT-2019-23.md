### BULLRUNNER_V2-GTFSRT-2019-23
#### Metadata
BULLRUNNER_V2-GTFSRT-2019-23.json.zip: 11.51 MB
BULLRUNNER_V2-GTFSRT-2019-23.pb.zip: 8.35 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 19858 | 0 | 0 |
| Sampling | 30.46 | 0.00 | 0.00 |
| 2019-06-03 | 2864 | 0 | 0 |
| 2019-06-04 | 2849 | 0 | 0 |
| 2019-06-05 | 2682 | 0 | 0 |
| 2019-06-06 | 2855 | 0 | 0 |
| 2019-06-07 | 2866 | 0 | 0 |
| 2019-06-08 | 2873 | 0 | 0 |
| 2019-06-09 | 2869 | 0 | 0 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.occupancyStatus``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.position.speed``
``entity.vehicle.timestamp``
``entity.vehicle.trip.routeId``
``entity.vehicle.trip.scheduleRelationship``
``entity.vehicle.trip.tripId``
``entity.vehicle.vehicle.id``
``entity.vehicle.vehicle.label``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/BULLRUNNER_V2-GTFSRT-2019-23.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/BULLRUNNER_V2-GTFSRT-2019-23.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/BULLRUNNER_V2-GTFSRT-2019-23.png)
