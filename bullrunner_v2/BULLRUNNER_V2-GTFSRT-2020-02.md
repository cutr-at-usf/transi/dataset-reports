### BULLRUNNER_V2-GTFSRT-2020-02
#### Metadata
BULLRUNNER_V2-GTFSRT-2020-02.json.zip: 9.29 MB
BULLRUNNER_V2-GTFSRT-2020-02.pb.zip: 6.82 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 17402 | 0 | 0 |
| Sampling | 34.75 | 0.00 | 0.00 |
| 2020-01-06 | 2404 | 0 | 0 |
| 2020-01-07 | 2536 | 0 | 0 |
| 2020-01-08 | 2730 | 0 | 0 |
| 2020-01-09 | 2838 | 0 | 0 |
| 2020-01-10 | 2823 | 0 | 0 |
| 2020-01-11 | 1983 | 0 | 0 |
| 2020-01-12 | 2088 | 0 | 0 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.occupancyStatus``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.position.speed``
``entity.vehicle.timestamp``
``entity.vehicle.trip.routeId``
``entity.vehicle.trip.scheduleRelationship``
``entity.vehicle.trip.tripId``
``entity.vehicle.vehicle.id``
``entity.vehicle.vehicle.label``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/BULLRUNNER_V2-GTFSRT-2020-02.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/BULLRUNNER_V2-GTFSRT-2020-02.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/BULLRUNNER_V2-GTFSRT-2020-02.png)
