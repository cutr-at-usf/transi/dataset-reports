### BULLRUNNER_V2-GTFSRT-2020-04
#### Metadata
BULLRUNNER_V2-GTFSRT-2020-04.json.zip: 9.41 MB
BULLRUNNER_V2-GTFSRT-2020-04.pb.zip: 6.93 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 17945 | 0 | 0 |
| Sampling | 33.70 | 0.00 | 0.00 |
| 2020-01-20 | 2859 | 0 | 0 |
| 2020-01-21 | 2726 | 0 | 0 |
| 2020-01-22 | 2671 | 0 | 0 |
| 2020-01-23 | 2711 | 0 | 0 |
| 2020-01-24 | 2814 | 0 | 0 |
| 2020-01-25 | 2045 | 0 | 0 |
| 2020-01-26 | 2119 | 0 | 0 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.occupancyStatus``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.position.speed``
``entity.vehicle.timestamp``
``entity.vehicle.trip.routeId``
``entity.vehicle.trip.scheduleRelationship``
``entity.vehicle.trip.tripId``
``entity.vehicle.vehicle.id``
``entity.vehicle.vehicle.label``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/BULLRUNNER_V2-GTFSRT-2020-04.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/BULLRUNNER_V2-GTFSRT-2020-04.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/BULLRUNNER_V2-GTFSRT-2020-04.png)
