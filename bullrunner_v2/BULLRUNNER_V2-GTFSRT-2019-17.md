### BULLRUNNER_V2-GTFSRT-2019-17
#### Metadata
BULLRUNNER_V2-GTFSRT-2019-17.json.zip: 10.27 MB
BULLRUNNER_V2-GTFSRT-2019-17.pb.zip: 7.43 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 18046 | 0 | 0 |
| Sampling | 33.52 | 0.00 | 0.00 |
| 2019-04-22 | 2595 | 0 | 0 |
| 2019-04-23 | 2604 | 0 | 0 |
| 2019-04-24 | 2616 | 0 | 0 |
| 2019-04-25 | 2527 | 0 | 0 |
| 2019-04-26 | 2576 | 0 | 0 |
| 2019-04-27 | 2608 | 0 | 0 |
| 2019-04-28 | 2520 | 0 | 0 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.occupancyStatus``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.position.speed``
``entity.vehicle.timestamp``
``entity.vehicle.trip.routeId``
``entity.vehicle.trip.scheduleRelationship``
``entity.vehicle.trip.tripId``
``entity.vehicle.vehicle.id``
``entity.vehicle.vehicle.label``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/BULLRUNNER_V2-GTFSRT-2019-17.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/BULLRUNNER_V2-GTFSRT-2019-17.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/BULLRUNNER_V2-GTFSRT-2019-17.png)
