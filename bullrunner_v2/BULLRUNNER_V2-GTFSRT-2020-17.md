### BULLRUNNER_V2-GTFSRT-2020-17
#### Metadata
BULLRUNNER_V2-GTFSRT-2020-17.json.zip: 9.29 MB
BULLRUNNER_V2-GTFSRT-2020-17.pb.zip: 6.81 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 19973 | 0 | 0 |
| Sampling | 30.28 | 0.00 | 0.00 |
| 2020-04-20 | 2865 | 0 | 0 |
| 2020-04-21 | 2867 | 0 | 0 |
| 2020-04-22 | 2792 | 0 | 0 |
| 2020-04-23 | 2855 | 0 | 0 |
| 2020-04-24 | 2858 | 0 | 0 |
| 2020-04-25 | 2866 | 0 | 0 |
| 2020-04-26 | 2870 | 0 | 0 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.occupancyStatus``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.position.speed``
``entity.vehicle.timestamp``
``entity.vehicle.trip.routeId``
``entity.vehicle.trip.scheduleRelationship``
``entity.vehicle.trip.tripId``
``entity.vehicle.vehicle.id``
``entity.vehicle.vehicle.label``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/BULLRUNNER_V2-GTFSRT-2020-17.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/BULLRUNNER_V2-GTFSRT-2020-17.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/BULLRUNNER_V2-GTFSRT-2020-17.png)
