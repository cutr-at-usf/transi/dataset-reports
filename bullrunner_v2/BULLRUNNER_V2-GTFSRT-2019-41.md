### BULLRUNNER_V2-GTFSRT-2019-41
#### Metadata
BULLRUNNER_V2-GTFSRT-2019-41.json.zip: 11.31 MB
BULLRUNNER_V2-GTFSRT-2019-41.pb.zip: 8.26 MB
##### Captured protocol buffers
|  | vehicle_positions | trip_updates | alerts |
|-|-|-|-|
| Total | 19261 | 0 | 0 |
| Sampling | 31.40 | 0.00 | 0.00 |
| 2019-10-07 | 2759 | 0 | 0 |
| 2019-10-08 | 2746 | 0 | 0 |
| 2019-10-09 | 2626 | 0 | 0 |
| 2019-10-10 | 2616 | 0 | 0 |
| 2019-10-11 | 2786 | 0 | 0 |
| 2019-10-12 | 2866 | 0 | 0 |
| 2019-10-13 | 2862 | 0 | 0 |
#### Structures
##### vehicle_positions
``entity.id``
``entity.vehicle.occupancyStatus``
``entity.vehicle.position.bearing``
``entity.vehicle.position.latitude``
``entity.vehicle.position.longitude``
``entity.vehicle.position.speed``
``entity.vehicle.timestamp``
``entity.vehicle.trip.routeId``
``entity.vehicle.trip.scheduleRelationship``
``entity.vehicle.trip.tripId``
``entity.vehicle.vehicle.id``
``entity.vehicle.vehicle.label``
``header.gtfsRealtimeVersion``
``header.incrementality``
``header.timestamp``
##### trip_updates
##### alerts
#### Graphics
##### Vehicle Entities
![vehicle entity timeline](img-timeline-vehicle/BULLRUNNER_V2-GTFSRT-2019-41.png)
##### Trip Entities
![trip entity timeline](img-timeline-trip/BULLRUNNER_V2-GTFSRT-2019-41.png)
##### Alert Entities
![alert entity timeline](img-timeline-alert/BULLRUNNER_V2-GTFSRT-2019-41.png)
